//
//  TruckInfo.swift
//
//  Created by Vivan Raghuvanshi on 20/12/17
//  Copyright (c) . All rights reserved.
//

import Foundation
import SwiftyJSON

public final class TruckInfo {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let trailerHeight = "trailerHeight"
    static let trailersCount = "trailersCount"
    static let profileId = "profileId"
    static let emissionType = "emissionType"
    static let passengersCount = "passengersCount"
    static let trailerType = "trailerType"
    static let vehicleAxlesCount = "vehicleAxlesCount"
    static let vehicleType = "vehicleType"
    static let height = "height"
    static let limitedWeight = "limitedWeight"
    static let optionsId = "optionsId"
    static let trailerAxlesCount = "trailerAxlesCount"
    static let tiresCount = "tiresCount"
    static let vehicleWeight = "vehicleWeight"
  }

  // MARK: Properties
  public var trailerHeight: Double?
  public var trailersCount: Int?
  public var profileId: Int?
  public var emissionType: String?
  public var passengersCount: Int?
  public var trailerType: String?
  public var vehicleAxlesCount: Int?
  public var vehicleType: String?
  public var height: Double?
  public var limitedWeight: Double?
  public var optionsId: Int?
  public var trailerAxlesCount: Int?
  public var tiresCount: Int?
  public var vehicleWeight: Double?

  // MARK: SwiftyJSON Initializers
  /// Initiates the instance based on the object.
  ///
  /// - parameter object: The object of either Dictionary or Array kind that was passed.
  /// - returns: An initialized instance of the class.
  public convenience init(object: Any) {
    self.init(json: JSON(object))
  }

  /// Initiates the instance based on the JSON that was passed.
  ///
  /// - parameter json: JSON object from SwiftyJSON.
  public required init(json: JSON) {
    trailerHeight = json[SerializationKeys.trailerHeight].double
    trailersCount = json[SerializationKeys.trailersCount].int
    profileId = json[SerializationKeys.profileId].int
    emissionType = json[SerializationKeys.emissionType].string
    passengersCount = json[SerializationKeys.passengersCount].int
    trailerType = json[SerializationKeys.trailerType].string
    vehicleAxlesCount = json[SerializationKeys.vehicleAxlesCount].int
    vehicleType = json[SerializationKeys.vehicleType].string
    height = json[SerializationKeys.height].double
    limitedWeight = json[SerializationKeys.limitedWeight].double
    optionsId = json[SerializationKeys.optionsId].int
    trailerAxlesCount = json[SerializationKeys.trailerAxlesCount].int
    tiresCount = json[SerializationKeys.tiresCount].int
    vehicleWeight = json[SerializationKeys.vehicleWeight].double
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = trailerHeight { dictionary[SerializationKeys.trailerHeight] = value }
    if let value = trailersCount { dictionary[SerializationKeys.trailersCount] = value }
    if let value = profileId { dictionary[SerializationKeys.profileId] = value }
    if let value = emissionType { dictionary[SerializationKeys.emissionType] = value }
    if let value = passengersCount { dictionary[SerializationKeys.passengersCount] = value }
    if let value = trailerType { dictionary[SerializationKeys.trailerType] = value }
    if let value = vehicleAxlesCount { dictionary[SerializationKeys.vehicleAxlesCount] = value }
    if let value = vehicleType { dictionary[SerializationKeys.vehicleType] = value }
    if let value = height { dictionary[SerializationKeys.height] = value }
    if let value = limitedWeight { dictionary[SerializationKeys.limitedWeight] = value }
    if let value = optionsId { dictionary[SerializationKeys.optionsId] = value }
    if let value = trailerAxlesCount { dictionary[SerializationKeys.trailerAxlesCount] = value }
    if let value = tiresCount { dictionary[SerializationKeys.tiresCount] = value }
    if let value = vehicleWeight { dictionary[SerializationKeys.vehicleWeight] = value }
    return dictionary
  }

}
