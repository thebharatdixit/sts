//
//  User.swift
//  SmartTrucking
//
//  Created by Vivan Raghuvanshi on 25/08/17.
//  Copyright © 2017 Vivan Raghuvanshi. All rights reserved.
//

import Foundation

var user:User?

class UserInfo {
    
    //1- Singleton
    class var sharedInfo : UserInfo {
        struct Singleton {
            static let instance = UserInfo()
        }
        return Singleton.instance
    }
    //2- Set user information
    func setUserProfileWithInfo(_ userInfo : [String: Any]?) {
        let saveUser = User.init(userInfo)
        user = saveUser
        self.saveUserInfo(saveUser)
    }
    //3- Save user information in local database
    private func saveUserInfo(_ user : User) {
        let encodedObj = NSKeyedArchiver.archivedData(withRootObject: user)
        UserDefaults.standard.set(encodedObj, forKey: "user")
    }
    //4- Get user information from local database
    func getUserInfo() -> User? {
        
        if let encodedObj  = UserDefaults.standard.object(forKey: "user") {
            let user = NSKeyedUnarchiver.unarchiveObject(with: encodedObj as! Data) as! User
            return user
        }
        return nil
    }
}


class User : NSObject, NSCoding {
    
    var userId:String?
    var userTitle:String?
    var userEmail:String?
    var userName:String?
    var userMobile:String?
    var userCity:String?
    var userState:String?
    var userZip:String?
    var userCompany:String?
    var userChargesPerMile:String?
    var userPaidForExtra:String?
    private var isPercentage:String?
    var userOnPercaentage = false
    var userPercaentageCharge:String?
    var userTruckInfo:[String:Any]?
    var selectedTrailerType:TrailerType?

    
    override init() {
        super.init()
    }
    //Initializer to make the user model
    convenience init(_ userDict : Any?) {
        self.init()
        
        userId = nil
        userTitle = nil
        userEmail = nil
        userName = nil
        userMobile = nil
        userCity = nil
        userState = nil
        userZip = nil
        userCompany = nil
        userChargesPerMile = nil
        userPaidForExtra = nil
        isPercentage = "false"
        userOnPercaentage = false
        userPercaentageCharge = nil
        userTruckInfo = nil
        
        if (userDict is NSNull) {return}
        guard let userInfo =  userDict as? Dictionary<String, Any> else { return }
        
        if let truckInfo = userInfo["options"], !(truckInfo is NSNull) {
            userTruckInfo = truckInfo as? [String:Any] ?? [:]
            if let tType = userTruckInfo?["trailerType"] as? String {
                getAllTrailerTypes({ (allTrailerTypes) in
                    let result = allTrailerTypes.filter({ trailType -> Bool in
                        trailType.trailerType == tType
                    })
                    selectedTrailerType = result.first
                })
            }
        }
        
        if let id = userInfo["generated_id"], !(id is NSNull) {
            userId = id as? String ?? ""
        }
        
        if let title = userInfo["title"], !(title is NSNull) {
            userTitle = title as? String  ?? ""
        }
        
        if let title = userInfo["title"], !(title is NSNull) {
            userTitle = title as? String  ?? ""
        }
        
        if let atPercentage = userInfo["atPercentage"], !(atPercentage is NSNull) {
            userPercaentageCharge = "\(atPercentage)"
            
            let p = Double(userPercaentageCharge ?? "0.0") ?? 0.0
            if p > 0.0 {
                 isPercentage = "true"
            } else {
                 isPercentage = "false"
            }
            userOnPercaentage = isPercentage!.boolValue
            //print(userPercaentageCharge ?? "jdksjd")
        }
        
        if let onPercentage = userInfo["onPercentage"], !(onPercentage is NSNull) {
            let isPercent = onPercentage as? String  ?? "false"
            isPercentage = isPercent
            userOnPercaentage = isPercent.boolValue
        }
        
        if let email = userInfo["registered_email"], !(email is NSNull) {
            userEmail = email as? String ?? ""
        }
        
        if let email = userInfo["email"], !(email is NSNull) {
            userEmail = email as? String ?? ""
        }
        
        if let name = userInfo["name"], !(name is NSNull) {
            userName = name as? String  ?? ""
        }
        
        if let company = userInfo["company"], !(company is NSNull) {
            userCompany = company as? String  ?? ""
        }
        
        if let city = userInfo["city"], !(city is NSNull) {
            userCity = city as? String  ?? ""
        }
        
        if let state = userInfo["state"], !(state is NSNull) {
            userState = state as? String  ?? ""
        }
        
        if let zip = userInfo["zip"], !(zip is NSNull) {
            userZip = zip as? String  ?? ""
        }
        
        if let mobile = userInfo["mobile"], !(mobile is NSNull) {
            userMobile = mobile as? String  ?? ""
        }
        
        if let chargesPerMile = userInfo["chargesPerMile"], !(chargesPerMile is NSNull) {
            userChargesPerMile = "\(chargesPerMile)"
        }
        
        if let paidForExtraMile = userInfo["paidForExtraMile"], !(paidForExtraMile is NSNull) {
            userPaidForExtra = "\(paidForExtraMile)"
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        
        let _userId =  aDecoder.decodeObject(forKey: "generated_id")
        let _title =  aDecoder.decodeObject(forKey: "title")
        let _email =  aDecoder.decodeObject(forKey: "registered_email")
        let _name =  aDecoder.decodeObject(forKey: "name")
        let _mobile =  aDecoder.decodeObject(forKey: "mobile")
        let _company =  aDecoder.decodeObject(forKey: "company")
        let _city =  aDecoder.decodeObject(forKey: "city")
        let _state =  aDecoder.decodeObject(forKey: "state")
        let _zip =  aDecoder.decodeObject(forKey: "zip")
        let _isPercentage =  aDecoder.decodeObject(forKey: "isPercentage")
        let _atPercentage =  aDecoder.decodeObject(forKey: "atPercentage")
        let _chargesPerMile =  aDecoder.decodeObject(forKey: "chargesPerMile")
        let _paidForExtraMile =  aDecoder.decodeObject(forKey: "paidForExtraMile")
        let _userTruckInfo =  aDecoder.decodeObject(forKey: "userTruckInfo")



        self.userId = _userId as? String
        self.userTitle = _title as? String
        self.userEmail = _email as? String
        self.userName = _name as? String
        self.userMobile = _mobile as? String
        self.userCompany = _company as? String
        self.userCity = _city as? String
        self.userState = _state as? String
        self.userZip = _zip as? String
        self.isPercentage = _isPercentage as? String
        self.userOnPercaentage = (self.isPercentage ?? "false").boolValue
        self.userPercaentageCharge = _atPercentage as? String
        self.userChargesPerMile = _chargesPerMile as? String
        self.userPaidForExtra = _paidForExtraMile as? String
        self.userTruckInfo = _userTruckInfo as? [String:Any]
    }
    
    func encode(with aCoder: NSCoder) {
        
        aCoder.encode(userId ?? "",forKey: "generated_id")
        aCoder.encode(userTitle ?? "",forKey: "title")
        aCoder.encode(userEmail ?? "",forKey: "registered_email")
        aCoder.encode(userName ?? "",forKey: "name")
        aCoder.encode(userMobile ?? "",forKey: "mobile")
        aCoder.encode(userCompany ?? "",forKey: "company")
        aCoder.encode(userCity ?? "",forKey: "city")
        aCoder.encode(userState ?? "",forKey: "state")
        aCoder.encode(userZip ?? "",forKey: "zip")
        aCoder.encode(isPercentage ?? "",forKey: "isPercentage")
        aCoder.encode(userPercaentageCharge ?? "",forKey: "atPercentage")
        aCoder.encode(userChargesPerMile ?? "",forKey: "chargesPerMile")
        aCoder.encode(userPaidForExtra ?? "",forKey: "paidForExtraMile")
        aCoder.encode(userTruckInfo ?? [:],forKey: "userTruckInfo")
    }
}

