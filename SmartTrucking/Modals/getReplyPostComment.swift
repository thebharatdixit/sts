//
//  getReplyPostComment.swift
//  SmartTrucking
//
//  Created by Prachi Pandey on 04/06/19.
//  Copyright © 2019 Vivan Raghuvanshi. All rights reserved.
//

import Foundation

struct getReplyPostComment {
    
    
    var commentId : Int
    var comment : String
    var filePath : String
    var fileName : String
    var createdDateTime : String
    var type : String
    var userName : String
    var latitude : String
    var longitude : String
    
    init(jsonData: [String: Any]) {
        
        self.commentId = jsonData["commentId"] as? Int ?? 0
        self.comment = jsonData["comment"] as? String ?? ""
        self.filePath = jsonData["filePath"] as? String ?? ""
        self.fileName = jsonData["fileName"] as? String ?? ""
        self.createdDateTime = jsonData["createdDateTime"] as? String ?? ""
        self.type = jsonData["type"] as? String ?? ""
        self.userName = jsonData["userName"] as? String ?? ""
        self.latitude = jsonData["latitude"] as? String ?? ""
        self.longitude = jsonData["longitude"] as? String ?? ""
        
    }
    
}
