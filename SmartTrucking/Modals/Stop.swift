//
//  Stop.swift
//  SmartTrucking
//
//  Created by Vivan Raghuvanshi on 23/09/17.
//  Copyright © 2017 Vivan Raghuvanshi. All rights reserved.
//

import Foundation

// Stop model

struct Stop {
    var stopId:String
    var stopAddress:String
    var stopLatitude:String
    var stopLongitude:String
    var stopSeq: Int
    
    internal init(_ id:String = "", address:String = "", latitude:String = "", longitude:String = "", stopSeq: Int) {
        self.stopId = id
        self.stopAddress = address
        self.stopLatitude = latitude
        self.stopLongitude = longitude
        self.stopSeq = stopSeq
    }
}

