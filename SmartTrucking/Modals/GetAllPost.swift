//
//  GetAllPost.swift
//  SmartTrucking
//
//  Created by Prachi Pandey on 30/05/19.
//  Copyright © 2019 Vivan Raghuvanshi. All rights reserved.
//

import Foundation

struct GetAllPost {
    
    var id : Int
    var comment:String
    var numberOfComments : Int
    var createdDateTime:String
    var filePath:String
    var userName:String
    var userProfilePicture:String
    var type:String
    
    init(jsonData: [String: Any]) {
        
        self.id = jsonData["id"] as? Int ?? 0
        self.comment = jsonData["comment"] as? String ?? ""
        self.numberOfComments = jsonData["numberOfComments"] as? Int ?? 0
        self.createdDateTime = jsonData["createdDateTime"] as? String ?? ""
        self.filePath = jsonData["filePath"] as? String ?? ""
        self.userName = jsonData["userName"] as? String ?? ""
        self.userProfilePicture = jsonData["userProfilePicture"] as? String ?? ""
        self.type = jsonData["type"] as? String ?? ""

        
 }
    
}
