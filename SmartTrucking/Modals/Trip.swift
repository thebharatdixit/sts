//
//  Trip.swift
//  SmartTrucking
//
//  Created by Vivan Raghuvanshi on 17/09/17.
//  Copyright © 2017 Vivan Raghuvanshi. All rights reserved.
//

import Foundation
import GooglePlacesSearchController
import CoreLocation
// Trip model

struct Trip {
    var tripId:String
    var tripName:String
    var sourceAddress:String
    var sourceLatitude:String
    var sourceLongitude:String
    var destinationAddress:String
    var destinationLatitude:String
    var destinationLongitude:String
    var tripMiles:String
    var tripSpent:String
    var tripTotalPaid:String
    var tripExpenses:[Expense]?
    var tripStops:[Stop]?
    var tripPayDetail:PayDetail?
    var tripLoadValue:String
    var tripPerMileCharge:String
    var tripPerStopCharge:String
    var percentageCharge:String
    
    var sourcePlace:AvPlaceDetail?
    var destinationPlace:AvPlaceDetail?
    var stopPlaces = [AvPlaceDetail]()


    init(_ tripInfo:[String:Any]) {
        
        self.tripId = "\(tripInfo["tripId"] ?? "")"
        self.tripName = "\(tripInfo["tripName"] ?? "")"
        self.sourceAddress = "\(tripInfo["sourceAddress"] ?? "")"
        self.sourceLatitude = "\(tripInfo["sourceLatitude"] ?? "")"
        self.sourceLongitude = "\(tripInfo["sourceLongitude"] ?? "")"
        self.destinationAddress = "\(tripInfo["destinationAddress"] ?? "")"
        self.destinationLatitude = "\(tripInfo["destinationLatitude"] ?? "")"
        self.destinationLongitude = "\(tripInfo["destinationLongitude"] ?? "")"
        self.tripMiles = "\(tripInfo["milesCovered"] ?? "")"
        self.tripSpent = "\(tripInfo["spentOnTrip"] ?? "0.0")"
        self.tripTotalPaid = "\(tripInfo["paidForTrip"] ?? "0.0")"
        
        self.tripLoadValue = "\(tripInfo["loadValue"] ?? "0.0")"
        self.tripPerMileCharge = "\(tripInfo["perMile"] ?? "0.0")"
        self.tripPerStopCharge = "\(tripInfo["perStop"] ?? "0.0")"
        self.percentageCharge = "\(tripInfo["percentage"] ?? "0.0")"

        var sourceInfo = [String:Any]()
        sourceInfo["title"] = self.sourceAddress
        sourceInfo["vicinity"] = self.sourceAddress
        sourceInfo["position"] = [(Double(self.sourceLatitude) ?? 0.000), (Double(self.sourceLongitude) ?? 0.000)]
        self.sourcePlace = AvPlaceDetail(json:sourceInfo)
        
        var destInfo = [String:Any]()
        destInfo["title"] = self.destinationAddress
        destInfo["vicinity"] = self.destinationAddress
        destInfo["position"] = [(Double(self.destinationLatitude) ?? 0.000), (Double(self.destinationLongitude) ?? 0.000)]
        self.destinationPlace = AvPlaceDetail(json:destInfo)
        
        
        // Add expenses
        if let expenses = tripInfo["expenses"] as? [[String:Any]] {
            let expenseModels = expenses.map({ expenseInfo -> Expense in
                Expense.init(expenseInfo)
            })
           self.tripExpenses = expenseModels
        }
//        else {
//            self.tripExpenses = [Expense.init(["":""])]
//        }
        // Add paydetails
        if let paidDetails = tripInfo["paidDetails"] as? [String:Any] {
            self.tripPayDetail = PayDetail.init(paidDetails)
        }
//        else {
//            self.tripPayDetail = PayDetail.init(["":""])
//        }
        
        // Add stops
        if let stops = tripInfo["stops"] as? [[String:Any]] {
            let stopModels = stops.map({ stopInfo -> Stop in
                let stopId = "\(String(describing: stopInfo["stopId"] ?? ""))"
                let stopAddress = "\(String(describing: stopInfo["stopAddress"] ?? ""))"
                let stopLatitude = "\(String(describing: stopInfo["stopLatitude"] ?? ""))"
                let stopLongitude = "\(String(describing: stopInfo["stopLongitude"] ?? ""))"
                if let stopSeq = stopInfo["stopSeq"] as? Int {
                    return Stop(stopId, address: stopAddress, latitude: stopLatitude, longitude: stopLongitude, stopSeq: stopSeq)
                } else {
                   return Stop(stopId, address: stopAddress, latitude: stopLatitude, longitude: stopLongitude, stopSeq: 0)
                }
            })
            self.stopPlaces = stopModels.map({ stop -> AvPlaceDetail in
                
                var stopInfo = [String:Any]()
                stopInfo["title"] = stop.stopAddress
                stopInfo["vicinity"] = stop.stopAddress
                stopInfo["position"] = [(Double(stop.stopLatitude) ?? 0.000),(Double(stop.stopLongitude) ?? 0.000)]
                return AvPlaceDetail(json:stopInfo)
            })
            self.tripStops = stopModels
        }
    }
}

struct Expense {
    var expenseType:String
    var expenseTitle:String
    var expense:String
    var units:String
    
    init(_ expenseInfo:[String:Any]) {
        self.expenseType = "\(expenseInfo["expenseType"] ?? "")"
        self.expenseTitle = "\(expenseInfo["expenseTitle"] ?? "")"
        self.expense = "\(expenseInfo["expense"] ?? "0.0")"
        self.units = "\(expenseInfo["units"] ?? "")"
    }
}

struct PayDetail {
    var detail_PerMile:String
    var detail_Miles:String
    var detail_PaidForMiles:String
    var detail_PerStop:String
    var detail_Stops:String
    var detail_PaidForStop:String

    
    init(_ payInfo:[String:Any]) {
        self.detail_PerMile = "\(payInfo["perMile"] ?? "")"
        self.detail_Miles = "\(payInfo["miles"] ?? "")"
        self.detail_PaidForMiles = "\(payInfo["paidForMiles"] ?? "")"
        self.detail_PerStop = "\(payInfo["perStop"] ?? "")"
        self.detail_Stops = "\(payInfo["stops"] ?? "")"
        self.detail_PaidForStop = "\(payInfo["paidForStops"] ?? "")"
    }
}

class AvPlaceDetail:CustomStringConvertible {
    
    let name: String
    var formattedAddress: String
    let coordinate: CLLocationCoordinate2D
    let raw: [String: Any]
    
    public init(json: [String: Any]) {
        self.name = "\(json["title"] ?? "")"
        self.formattedAddress = "\(json["vicinity"] ?? "")"
        if self.formattedAddress.contains("<br/>") {
            self.formattedAddress = self.formattedAddress.replacingOccurrences(of: "<br/>", with: ", ")
        }
        let coordinates = json["position"] as? [Double]
        let latitude = coordinates?.first ?? 0.0000
        let longitude = coordinates?.last ?? 0.0000
        self.coordinate = CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
        raw = json
    }
    
    open var description: String {
        return "\nPlace: \(name).\nAddress: \(formattedAddress).\ncoordinate: (\(coordinate.latitude), \(coordinate.longitude))\n"
    }
}
