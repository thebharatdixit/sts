//
//  TrailerType.swift
//  SmartTrucking
//
//  Created by Vivan Raghuvanshi on 30/12/17.
//  Copyright © 2017 Vivan Raghuvanshi. All rights reserved.
//

import Foundation
import SwiftyJSON

public final class  TrailerType {
    
    private struct SerializationKeys {
        static let trailerType = "trailerType"
        static let trailerCode = "trailerCode"
        static let trailerWeight = "trailerWeight"
        static let trailerLength = "trailerLength"
        static let trailerHeight = "trailerHeight"
    }
    
    // MARK: Properties
    public var trailerType: String?
    public var trailerCode: Int?
    public var trailerWeight: Double?
    public var trailerLength: Double?
    public var trailerHeight: Double?
    
    /// - returns: An initialized instance of the class.
    public convenience init(object: Any) {
        self.init(json: JSON(object))
    }
    
    /// Initiates the instance based on the JSON that was passed.
    ///
    /// - parameter json: JSON object from SwiftyJSON.
    public required init(json: JSON) {
        trailerType = json[SerializationKeys.trailerType].string
        trailerCode = json[SerializationKeys.trailerCode].int
        trailerWeight = json[SerializationKeys.trailerWeight].double
        trailerLength = json[SerializationKeys.trailerLength].double
        trailerHeight = json[SerializationKeys.trailerHeight].double
    }
}
