//
//  ExpenseReport.swift
//
//  Created by Vivan Raghuvanshi on 24/12/17
//  Copyright (c) . All rights reserved.
//

import Foundation
import SwiftyJSON

public final class ExpenseReport {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let fixedExpensesMonthWise = "fixedExpensesMonthWise"
    static let earnings = "earnings"
    static let miles = "miles"
    static let spentOnTripsMonthWise = "spentOnTripsMonthWise"
    static let expenses = "expenses"
    static let monthlyExpenses = "monthlyExpenses"
    static let trips = "trips"
  }

  // MARK: Properties
  public var fixedExpensesMonthWise: FixedExpensesMonthWise?
  public var fixedMonthlyExpenses: [String:Any]?
  public var earnings: Double?
  public var miles: Double?
  public var spentOnTripsMonthWise: SpentOnTripsMonthWise?
  public var spentOnTripsMonthly: [String:Any]?
  public var expenses: Double?
  public var monthlyExpenses: Double?
  public var trips: Int?

  // MARK: SwiftyJSON Initializers
  /// Initiates the instance based on the object.
  ///
  /// - parameter object: The object of either Dictionary or Array kind that was passed.
  /// - returns: An initialized instance of the class.
  public convenience init(object: Any) {
    self.init(json: JSON(object))
  }

  /// Initiates the instance based on the JSON that was passed.
  ///
  /// - parameter json: JSON object from SwiftyJSON.
  public required init(json: JSON) {
    
    fixedExpensesMonthWise = FixedExpensesMonthWise(json: json[SerializationKeys.fixedExpensesMonthWise])
    fixedMonthlyExpenses = json[SerializationKeys.fixedExpensesMonthWise].dictionary
    earnings = json[SerializationKeys.earnings].double
    miles = json[SerializationKeys.miles].double
    spentOnTripsMonthWise = SpentOnTripsMonthWise(json: json[SerializationKeys.spentOnTripsMonthWise])
    spentOnTripsMonthly = json[SerializationKeys.spentOnTripsMonthWise].dictionary
    expenses = json[SerializationKeys.expenses].double
    monthlyExpenses = json[SerializationKeys.monthlyExpenses].double
    trips = json[SerializationKeys.trips].int
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = fixedExpensesMonthWise { dictionary[SerializationKeys.fixedExpensesMonthWise] = value.dictionaryRepresentation() }
    if let value = earnings { dictionary[SerializationKeys.earnings] = value }
    if let value = miles { dictionary[SerializationKeys.miles] = value }
    if let value = spentOnTripsMonthWise { dictionary[SerializationKeys.spentOnTripsMonthWise] = value.dictionaryRepresentation() }
    if let value = expenses { dictionary[SerializationKeys.expenses] = value }
    if let value = monthlyExpenses { dictionary[SerializationKeys.monthlyExpenses] = value }
    if let value = trips { dictionary[SerializationKeys.trips] = value }
    return dictionary
  }

}

public final class FixedExpensesMonthWise {
    
    // MARK: Declaration for string constants to be used to decode and also serialize.
    private struct SerializationKeys {
        static let trailerSpending = "Trailer Spending"
    }
    
    // MARK: Properties
    public var trailerSpending: Double?
    
    // MARK: SwiftyJSON Initializers
    /// Initiates the instance based on the object.
    ///
    /// - parameter object: The object of either Dictionary or Array kind that was passed.
    /// - returns: An initialized instance of the class.
    public convenience init(object: Any) {
        self.init(json: JSON(object))
    }
    
    /// Initiates the instance based on the JSON that was passed.
    ///
    /// - parameter json: JSON object from SwiftyJSON.
    public required init(json: JSON) {
        trailerSpending = json[SerializationKeys.trailerSpending].double
    }
    
    /// Generates description of the object in the form of a NSDictionary.
    ///
    /// - returns: A Key value pair containing all valid values in the object.
    public func dictionaryRepresentation() -> [String: Any] {
        var dictionary: [String: Any] = [:]
        if let value = trailerSpending { dictionary[SerializationKeys.trailerSpending] = value }
        return dictionary
    }
    
}


public final class SpentOnTripsMonthWise {
    
    // MARK: Declaration for string constants to be used to decode and also serialize.
    private struct SerializationKeys {
        static let penalty = "Penalty"
        static let toll = "Toll"
        static let tollExpenses = "Toll Expenses"
    }
    
    // MARK: Properties
    public var penalty: Double?
    public var toll: Double?
    public var tollExpenses: Double?
    
    // MARK: SwiftyJSON Initializers
    /// Initiates the instance based on the object.
    ///
    /// - parameter object: The object of either Dictionary or Array kind that was passed.
    /// - returns: An initialized instance of the class.
    public convenience init(object: Any) {
        self.init(json: JSON(object))
    }
    
    /// Initiates the instance based on the JSON that was passed.
    ///
    /// - parameter json: JSON object from SwiftyJSON.
    public required init(json: JSON) {
        penalty = json[SerializationKeys.penalty].double
        toll = json[SerializationKeys.toll].double
        tollExpenses = json[SerializationKeys.tollExpenses].double
    }
    
    /// Generates description of the object in the form of a NSDictionary.
    ///
    /// - returns: A Key value pair containing all valid values in the object.
    public func dictionaryRepresentation() -> [String: Any] {
        var dictionary: [String: Any] = [:]
        if let value = penalty { dictionary[SerializationKeys.penalty] = value }
        if let value = toll { dictionary[SerializationKeys.toll] = value }
        if let value = tollExpenses { dictionary[SerializationKeys.tollExpenses] = value }
        return dictionary
    }
    
}

