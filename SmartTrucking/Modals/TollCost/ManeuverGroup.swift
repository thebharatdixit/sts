//
//  ManeuverGroup.swift
//
//  Created by Vivan Raghuvanshi on 25/11/17
//  Copyright (c) . All rights reserved.
//

import Foundation
import SwiftyJSON

public final class ManeuverGroup {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let arrivalDescription = "arrivalDescription"
    static let mode = "mode"
    static let lastManeuver = "lastManeuver"
    static let firstManeuver = "firstManeuver"
    static let summaryDescription = "summaryDescription"
  }

  // MARK: Properties
  public var arrivalDescription: String?
  public var mode: String?
  public var lastManeuver: String?
  public var firstManeuver: String?
  public var summaryDescription: String?

  // MARK: SwiftyJSON Initializers
  /// Initiates the instance based on the object.
  ///
  /// - parameter object: The object of either Dictionary or Array kind that was passed.
  /// - returns: An initialized instance of the class.
  public convenience init(object: Any) {
    self.init(json: JSON(object))
  }

  /// Initiates the instance based on the JSON that was passed.
  ///
  /// - parameter json: JSON object from SwiftyJSON.
  public required init(json: JSON) {
    arrivalDescription = json[SerializationKeys.arrivalDescription].string
    mode = json[SerializationKeys.mode].string
    lastManeuver = json[SerializationKeys.lastManeuver].string
    firstManeuver = json[SerializationKeys.firstManeuver].string
    summaryDescription = json[SerializationKeys.summaryDescription].string
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = arrivalDescription { dictionary[SerializationKeys.arrivalDescription] = value }
    if let value = mode { dictionary[SerializationKeys.mode] = value }
    if let value = lastManeuver { dictionary[SerializationKeys.lastManeuver] = value }
    if let value = firstManeuver { dictionary[SerializationKeys.firstManeuver] = value }
    if let value = summaryDescription { dictionary[SerializationKeys.summaryDescription] = value }
    return dictionary
  }

}
