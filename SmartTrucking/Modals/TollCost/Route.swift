//
//  Route.swift
//
//  Created by Vivan Raghuvanshi on 25/11/17
//  Copyright (c) . All rights reserved.
//

import Foundation
import SwiftyJSON

public final class Route {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let summary = "summary"
    static let maneuverGroup = "maneuverGroup"
    static let mode = "mode"
    static let leg = "leg"
    static let waypoint = "waypoint"
  }

  // MARK: Properties
  public var summary: Summary?
  public var maneuverGroup: [ManeuverGroup]?
  public var mode: Mode?
  public var leg: [Leg]?
  public var waypoint: [Waypoint]?

  // MARK: SwiftyJSON Initializers
  /// Initiates the instance based on the object.
  ///
  /// - parameter object: The object of either Dictionary or Array kind that was passed.
  /// - returns: An initialized instance of the class.
  public convenience init(object: Any) {
    self.init(json: JSON(object))
  }

  /// Initiates the instance based on the JSON that was passed.
  ///
  /// - parameter json: JSON object from SwiftyJSON.
  public required init(json: JSON) {
    summary = Summary(json: json[SerializationKeys.summary])
    if let items = json[SerializationKeys.maneuverGroup].array { maneuverGroup = items.map { ManeuverGroup(json: $0) } }
    mode = Mode(json: json[SerializationKeys.mode])
    if let items = json[SerializationKeys.leg].array { leg = items.map { Leg(json: $0) } }
    if let items = json[SerializationKeys.waypoint].array { waypoint = items.map { Waypoint(json: $0) } }
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = summary { dictionary[SerializationKeys.summary] = value.dictionaryRepresentation() }
    if let value = maneuverGroup { dictionary[SerializationKeys.maneuverGroup] = value.map { $0.dictionaryRepresentation() } }
    if let value = mode { dictionary[SerializationKeys.mode] = value.dictionaryRepresentation() }
    if let value = leg { dictionary[SerializationKeys.leg] = value.map { $0.dictionaryRepresentation() } }
    if let value = waypoint { dictionary[SerializationKeys.waypoint] = value.map { $0.dictionaryRepresentation() } }
    return dictionary
  }

}
