//
//  Summary.swift
//
//  Created by Vivan Raghuvanshi on 25/11/17
//  Copyright (c) . All rights reserved.
//

import Foundation
import SwiftyJSON

public final class Summary {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let trafficTime = "trafficTime"
    static let text = "text"
    static let type = "_type"
    static let distance = "distance"
    static let baseTime = "baseTime"
    static let travelTime = "travelTime"
    static let flags = "flags"
  }

  // MARK: Properties
  public var trafficTime: Int?
  public var text: String?
  public var type: String?
  public var distance: Int?
  public var baseTime: Int?
  public var travelTime: Int?
  public var flags: [String]?

  // MARK: SwiftyJSON Initializers
  /// Initiates the instance based on the object.
  ///
  /// - parameter object: The object of either Dictionary or Array kind that was passed.
  /// - returns: An initialized instance of the class.
  public convenience init(object: Any) {
    self.init(json: JSON(object))
  }

  /// Initiates the instance based on the JSON that was passed.
  ///
  /// - parameter json: JSON object from SwiftyJSON.
  public required init(json: JSON) {
    trafficTime = json[SerializationKeys.trafficTime].int
    text = json[SerializationKeys.text].string
    type = json[SerializationKeys.type].string
    distance = json[SerializationKeys.distance].int
    baseTime = json[SerializationKeys.baseTime].int
    travelTime = json[SerializationKeys.travelTime].int
    if let items = json[SerializationKeys.flags].array { flags = items.map { $0.stringValue } }
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = trafficTime { dictionary[SerializationKeys.trafficTime] = value }
    if let value = text { dictionary[SerializationKeys.text] = value }
    if let value = type { dictionary[SerializationKeys.type] = value }
    if let value = distance { dictionary[SerializationKeys.distance] = value }
    if let value = baseTime { dictionary[SerializationKeys.baseTime] = value }
    if let value = travelTime { dictionary[SerializationKeys.travelTime] = value }
    if let value = flags { dictionary[SerializationKeys.flags] = value }
    return dictionary
  }

}
