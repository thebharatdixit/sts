//
//  Conditions.swift
//
//  Created by Vivan Raghuvanshi on 25/11/17
//  Copyright (c) . All rights reserved.
//

import Foundation
import SwiftyJSON

public final class Conditions {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let tollSystemsIds = "tollSystemsIds"
    static let currency = "currency"
    static let daylightHours = "daylightHours"
    static let amount = "amount"
    static let methodsOfPayment = "methodsOfPayment"
    static let time = "time"
    static let discountAvailable = "discountAvailable"
  }

  // MARK: Properties
  public var tollSystemsIds: String?
  public var currency: String?
  public var daylightHours: Int?
  public var amount: Float?
  public var methodsOfPayment: Int?
  public var time: String?
  public var discountAvailable: Int?

  // MARK: SwiftyJSON Initializers
  /// Initiates the instance based on the object.
  ///
  /// - parameter object: The object of either Dictionary or Array kind that was passed.
  /// - returns: An initialized instance of the class.
  public convenience init(object: Any) {
    self.init(json: JSON(object))
  }

  /// Initiates the instance based on the JSON that was passed.
  ///
  /// - parameter json: JSON object from SwiftyJSON.
  public required init(json: JSON) {
    tollSystemsIds = json[SerializationKeys.tollSystemsIds].string
    currency = json[SerializationKeys.currency].string
    daylightHours = json[SerializationKeys.daylightHours].int
    amount = json[SerializationKeys.amount].float
    methodsOfPayment = json[SerializationKeys.methodsOfPayment].int
    time = json[SerializationKeys.time].string
    discountAvailable = json[SerializationKeys.discountAvailable].int
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = tollSystemsIds { dictionary[SerializationKeys.tollSystemsIds] = value }
    if let value = currency { dictionary[SerializationKeys.currency] = value }
    if let value = daylightHours { dictionary[SerializationKeys.daylightHours] = value }
    if let value = amount { dictionary[SerializationKeys.amount] = value }
    if let value = methodsOfPayment { dictionary[SerializationKeys.methodsOfPayment] = value }
    if let value = time { dictionary[SerializationKeys.time] = value }
    if let value = discountAvailable { dictionary[SerializationKeys.discountAvailable] = value }
    return dictionary
  }

}
