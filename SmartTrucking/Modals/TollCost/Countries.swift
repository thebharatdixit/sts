//
//  Countries.swift
//
//  Created by Vivan Raghuvanshi on 25/11/17
//  Copyright (c) . All rights reserved.
//

import Foundation
import SwiftyJSON

public final class Countries {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let usageFeeRequiredLinks = "usageFeeRequiredLinks"
    static let name = "name"
    static let tollSystemsNames = "tollSystemsNames"
    static let roadSectionsCosts = "roadSectionsCosts"
  }

  // MARK: Properties
  public var usageFeeRequiredLinks: UsageFeeRequiredLinks?
  public var name: String?
  public var tollSystemsNames: [TollSystemsNames]?
  public var roadSectionsCosts: [RoadSectionsCosts]?

  // MARK: SwiftyJSON Initializers
  /// Initiates the instance based on the object.
  ///
  /// - parameter object: The object of either Dictionary or Array kind that was passed.
  /// - returns: An initialized instance of the class.
  public convenience init(object: Any) {
    self.init(json: JSON(object))
  }

  /// Initiates the instance based on the JSON that was passed.
  ///
  /// - parameter json: JSON object from SwiftyJSON.
  public required init(json: JSON) {
    usageFeeRequiredLinks = UsageFeeRequiredLinks(json: json[SerializationKeys.usageFeeRequiredLinks])
    name = json[SerializationKeys.name].string
    if let items = json[SerializationKeys.tollSystemsNames].array { tollSystemsNames = items.map { TollSystemsNames(json: $0) } }
    if let items = json[SerializationKeys.roadSectionsCosts].array { roadSectionsCosts = items.map { RoadSectionsCosts(json: $0) } }
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = usageFeeRequiredLinks { dictionary[SerializationKeys.usageFeeRequiredLinks] = value.dictionaryRepresentation() }
    if let value = name { dictionary[SerializationKeys.name] = value }
    if let value = tollSystemsNames { dictionary[SerializationKeys.tollSystemsNames] = value.map { $0.dictionaryRepresentation() } }
    if let value = roadSectionsCosts { dictionary[SerializationKeys.roadSectionsCosts] = value.map { $0.dictionaryRepresentation() } }
    return dictionary
  }

}
