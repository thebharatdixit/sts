//
//  Link.swift
//
//  Created by Vivan Raghuvanshi on 25/11/17
//  Copyright (c) . All rights reserved.
//

import Foundation
import SwiftyJSON

public final class Link {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let functionalClass = "functionalClass"
    static let remainTime = "remainTime"
    static let shape = "shape"
    static let linkId = "linkId"
    static let type = "_type"
    static let speedLimit = "speedLimit"
  }

  // MARK: Properties
  public var functionalClass: Int?
  public var remainTime: Int?
  public var shape: [Float]?
  public var linkId: String?
  public var type: String?
  public var speedLimit: Float?

  // MARK: SwiftyJSON Initializers
  /// Initiates the instance based on the object.
  ///
  /// - parameter object: The object of either Dictionary or Array kind that was passed.
  /// - returns: An initialized instance of the class.
  public convenience init(object: Any) {
    self.init(json: JSON(object))
  }

  /// Initiates the instance based on the JSON that was passed.
  ///
  /// - parameter json: JSON object from SwiftyJSON.
  public required init(json: JSON) {
    functionalClass = json[SerializationKeys.functionalClass].int
    remainTime = json[SerializationKeys.remainTime].int
    if let items = json[SerializationKeys.shape].array { shape = items.map { $0.floatValue } }
    linkId = json[SerializationKeys.linkId].string
    type = json[SerializationKeys.type].string
    speedLimit = json[SerializationKeys.speedLimit].float
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = functionalClass { dictionary[SerializationKeys.functionalClass] = value }
    if let value = remainTime { dictionary[SerializationKeys.remainTime] = value }
    if let value = shape { dictionary[SerializationKeys.shape] = value }
    if let value = linkId { dictionary[SerializationKeys.linkId] = value }
    if let value = type { dictionary[SerializationKeys.type] = value }
    if let value = speedLimit { dictionary[SerializationKeys.speedLimit] = value }
    return dictionary
  }

}
