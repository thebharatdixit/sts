//
//  UsageFeeRequiredLinks.swift
//
//  Created by Vivan Raghuvanshi on 25/11/17
//  Copyright (c) . All rights reserved.
//

import Foundation
import SwiftyJSON

public final class UsageFeeRequiredLinks {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let linksIds = "linksIds"
    static let tollStructures = "tollStructures"
  }

  // MARK: Properties
  public var linksIds: [Int]?
  public var tollStructures: [Any]?

  // MARK: SwiftyJSON Initializers
  /// Initiates the instance based on the object.
  ///
  /// - parameter object: The object of either Dictionary or Array kind that was passed.
  /// - returns: An initialized instance of the class.
  public convenience init(object: Any) {
    self.init(json: JSON(object))
  }

  /// Initiates the instance based on the JSON that was passed.
  ///
  /// - parameter json: JSON object from SwiftyJSON.
  public required init(json: JSON) {
    if let items = json[SerializationKeys.linksIds].array { linksIds = items.map { $0.intValue } }
    if let items = json[SerializationKeys.tollStructures].array { tollStructures = items.map { $0.object} }
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = linksIds { dictionary[SerializationKeys.linksIds] = value }
    if let value = tollStructures { dictionary[SerializationKeys.tollStructures] = value }
    return dictionary
  }

}
