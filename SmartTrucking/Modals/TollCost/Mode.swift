//
//  Mode.swift
//
//  Created by Vivan Raghuvanshi on 25/11/17
//  Copyright (c) . All rights reserved.
//

import Foundation
import SwiftyJSON

public final class Mode {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let transportModes = "transportModes"
    static let type = "type"
    static let feature = "feature"
    static let trafficMode = "trafficMode"
  }

  // MARK: Properties
  public var transportModes: [String]?
  public var type: String?
  public var feature: [Any]?
  public var trafficMode: String?

  // MARK: SwiftyJSON Initializers
  /// Initiates the instance based on the object.
  ///
  /// - parameter object: The object of either Dictionary or Array kind that was passed.
  /// - returns: An initialized instance of the class.
  public convenience init(object: Any) {
    self.init(json: JSON(object))
  }

  /// Initiates the instance based on the JSON that was passed.
  ///
  /// - parameter json: JSON object from SwiftyJSON.
  public required init(json: JSON) {
    if let items = json[SerializationKeys.transportModes].array { transportModes = items.map { $0.stringValue } }
    type = json[SerializationKeys.type].string
    if let items = json[SerializationKeys.feature].array { feature = items.map { $0.object} }
    trafficMode = json[SerializationKeys.trafficMode].string
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = transportModes { dictionary[SerializationKeys.transportModes] = value }
    if let value = type { dictionary[SerializationKeys.type] = value }
    if let value = feature { dictionary[SerializationKeys.feature] = value }
    if let value = trafficMode { dictionary[SerializationKeys.trafficMode] = value }
    return dictionary
  }

}
