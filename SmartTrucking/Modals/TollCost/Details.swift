//
//  Details.swift
//
//  Created by Vivan Raghuvanshi on 25/11/17
//  Copyright (c) . All rights reserved.
//

import Foundation
import SwiftyJSON

public final class Details {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let driverCost = "driverCost"
    static let vehicleCost = "vehicleCost"
    static let tollCost = "tollCost"
  }

  // MARK: Properties
  public var driverCost: String?
  public var vehicleCost: String?
  public var tollCost: String?

  // MARK: SwiftyJSON Initializers
  /// Initiates the instance based on the object.
  ///
  /// - parameter object: The object of either Dictionary or Array kind that was passed.
  /// - returns: An initialized instance of the class.
  public convenience init(object: Any) {
    self.init(json: JSON(object))
  }

  /// Initiates the instance based on the JSON that was passed.
  ///
  /// - parameter json: JSON object from SwiftyJSON.
  public required init(json: JSON) {
    driverCost = json[SerializationKeys.driverCost].string
    vehicleCost = json[SerializationKeys.vehicleCost].string
    tollCost = json[SerializationKeys.tollCost].string
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = driverCost { dictionary[SerializationKeys.driverCost] = value }
    if let value = vehicleCost { dictionary[SerializationKeys.vehicleCost] = value }
    if let value = tollCost { dictionary[SerializationKeys.tollCost] = value }
    return dictionary
  }

}
