//
//  Start.swift
//
//  Created by Vivan Raghuvanshi on 25/11/17
//  Copyright (c) . All rights reserved.
//

import Foundation
import SwiftyJSON

public final class Start {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let label = "label"
    static let mappedPosition = "mappedPosition"
    static let linkId = "linkId"
    static let mappedRoadName = "mappedRoadName"
    static let shapeIndex = "shapeIndex"
    static let sideOfStreet = "sideOfStreet"
    static let type = "type"
    static let spot = "spot"
    static let originalPosition = "originalPosition"
  }

  // MARK: Properties
  public var label: String?
  public var mappedPosition: MappedPosition?
  public var linkId: String?
  public var mappedRoadName: String?
  public var shapeIndex: Int?
  public var sideOfStreet: String?
  public var type: String?
  public var spot: Float?
  public var originalPosition: OriginalPosition?

  // MARK: SwiftyJSON Initializers
  /// Initiates the instance based on the object.
  ///
  /// - parameter object: The object of either Dictionary or Array kind that was passed.
  /// - returns: An initialized instance of the class.
  public convenience init(object: Any) {
    self.init(json: JSON(object))
  }

  /// Initiates the instance based on the JSON that was passed.
  ///
  /// - parameter json: JSON object from SwiftyJSON.
  public required init(json: JSON) {
    label = json[SerializationKeys.label].string
    mappedPosition = MappedPosition(json: json[SerializationKeys.mappedPosition])
    linkId = json[SerializationKeys.linkId].string
    mappedRoadName = json[SerializationKeys.mappedRoadName].string
    shapeIndex = json[SerializationKeys.shapeIndex].int
    sideOfStreet = json[SerializationKeys.sideOfStreet].string
    type = json[SerializationKeys.type].string
    spot = json[SerializationKeys.spot].float
    originalPosition = OriginalPosition(json: json[SerializationKeys.originalPosition])
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = label { dictionary[SerializationKeys.label] = value }
    if let value = mappedPosition { dictionary[SerializationKeys.mappedPosition] = value.dictionaryRepresentation() }
    if let value = linkId { dictionary[SerializationKeys.linkId] = value }
    if let value = mappedRoadName { dictionary[SerializationKeys.mappedRoadName] = value }
    if let value = shapeIndex { dictionary[SerializationKeys.shapeIndex] = value }
    if let value = sideOfStreet { dictionary[SerializationKeys.sideOfStreet] = value }
    if let value = type { dictionary[SerializationKeys.type] = value }
    if let value = spot { dictionary[SerializationKeys.spot] = value }
    if let value = originalPosition { dictionary[SerializationKeys.originalPosition] = value.dictionaryRepresentation() }
    return dictionary
  }

}
