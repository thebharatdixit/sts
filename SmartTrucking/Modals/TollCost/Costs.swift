//
//  Costs.swift
//
//  Created by Vivan Raghuvanshi on 25/11/17
//  Copyright (c) . All rights reserved.
//

import Foundation
import SwiftyJSON

public final class Costs {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let details = "details"
    static let currency = "currency"
    static let totalCost = "totalCost"
  }

  // MARK: Properties
  public var details: Details?
  public var currency: String?
  public var totalCost: String?

  // MARK: SwiftyJSON Initializers
  /// Initiates the instance based on the object.
  ///
  /// - parameter object: The object of either Dictionary or Array kind that was passed.
  /// - returns: An initialized instance of the class.
  public convenience init(object: Any) {
    self.init(json: JSON(object))
  }

  /// Initiates the instance based on the JSON that was passed.
  ///
  /// - parameter json: JSON object from SwiftyJSON.
  public required init(json: JSON) {
    details = Details(json: json[SerializationKeys.details])
    currency = json[SerializationKeys.currency].string
    totalCost = json[SerializationKeys.totalCost].string
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = details { dictionary[SerializationKeys.details] = value.dictionaryRepresentation() }
    if let value = currency { dictionary[SerializationKeys.currency] = value }
    if let value = totalCost { dictionary[SerializationKeys.totalCost] = value }
    return dictionary
  }

}
