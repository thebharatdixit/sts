//
//  Response.swift
//
//  Created by Vivan Raghuvanshi on 25/11/17
//  Copyright (c) . All rights reserved.
//

import Foundation
import SwiftyJSON

public final class Response {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let route = "route"
    static let language = "language"
    static let metaInfo = "metaInfo"
  }

  // MARK: Properties
  public var route: [Route]?
  public var language: String?
  public var metaInfo: MetaInfo?

  // MARK: SwiftyJSON Initializers
  /// Initiates the instance based on the object.
  ///
  /// - parameter object: The object of either Dictionary or Array kind that was passed.
  /// - returns: An initialized instance of the class.
  public convenience init(object: Any) {
    self.init(json: JSON(object))
  }

  /// Initiates the instance based on the JSON that was passed.
  ///
  /// - parameter json: JSON object from SwiftyJSON.
  public required init(json: JSON) {
    if let items = json[SerializationKeys.route].array { route = items.map { Route(json: $0) } }
    language = json[SerializationKeys.language].string
    metaInfo = MetaInfo(json: json[SerializationKeys.metaInfo])
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = route { dictionary[SerializationKeys.route] = value.map { $0.dictionaryRepresentation() } }
    if let value = language { dictionary[SerializationKeys.language] = value }
    if let value = metaInfo { dictionary[SerializationKeys.metaInfo] = value.dictionaryRepresentation() }
    return dictionary
  }

}
