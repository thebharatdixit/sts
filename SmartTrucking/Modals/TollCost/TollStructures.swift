//
//  TollStructures.swift
//
//  Created by Vivan Raghuvanshi on 25/11/17
//  Copyright (c) . All rights reserved.
//

import Foundation
import SwiftyJSON

public final class TollStructures {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let latitude = "latitude"
    static let name = "name"
    static let tollStructureConditionId = "tollStructureConditionId"
    static let linkId1 = "linkId1"
    static let linkId2 = "linkId2"
    static let longitude = "longitude"
    static let lngCode = "lngCode"
  }

  // MARK: Properties
  public var latitude: Float?
  public var name: String?
  public var tollStructureConditionId: Int?
  public var linkId1: Int?
  public var linkId2: Int?
  public var longitude: Float?
  public var lngCode: String?

  // MARK: SwiftyJSON Initializers
  /// Initiates the instance based on the object.
  ///
  /// - parameter object: The object of either Dictionary or Array kind that was passed.
  /// - returns: An initialized instance of the class.
  public convenience init(object: Any) {
    self.init(json: JSON(object))
  }

  /// Initiates the instance based on the JSON that was passed.
  ///
  /// - parameter json: JSON object from SwiftyJSON.
  public required init(json: JSON) {
    latitude = json[SerializationKeys.latitude].float
    name = json[SerializationKeys.name].string
    tollStructureConditionId = json[SerializationKeys.tollStructureConditionId].int
    linkId1 = json[SerializationKeys.linkId1].int
    linkId2 = json[SerializationKeys.linkId2].int
    longitude = json[SerializationKeys.longitude].float
    lngCode = json[SerializationKeys.lngCode].string
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = latitude { dictionary[SerializationKeys.latitude] = value }
    if let value = name { dictionary[SerializationKeys.name] = value }
    if let value = tollStructureConditionId { dictionary[SerializationKeys.tollStructureConditionId] = value }
    if let value = linkId1 { dictionary[SerializationKeys.linkId1] = value }
    if let value = linkId2 { dictionary[SerializationKeys.linkId2] = value }
    if let value = longitude { dictionary[SerializationKeys.longitude] = value }
    if let value = lngCode { dictionary[SerializationKeys.lngCode] = value }
    return dictionary
  }

}
