//
//  TollCost.swift
//
//  Created by Vivan Raghuvanshi on 25/11/17
//  Copyright (c) . All rights reserved.
//

import Foundation
import SwiftyJSON

public final class TollCost {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let onError = "onError"
    static let detailedTollCosts = "detailedTollCosts"
    static let costs = "costs"
    static let response = "response"
    static let warnings = "warnings"
    static let errors = "errors"
  }

  // MARK: Properties
  public var onError: Bool? = false
  public var detailedTollCosts: DetailedTollCosts?
  public var costs: Costs?
  public var response: Response?
  public var warnings: [Warnings]?
  public var errors: [Any]?

  // MARK: SwiftyJSON Initializers
  /// Initiates the instance based on the object.
  ///
  /// - parameter object: The object of either Dictionary or Array kind that was passed.
  /// - returns: An initialized instance of the class.
  public convenience init(object: Any) {
    self.init(json: JSON(object))
  }

  /// Initiates the instance based on the JSON that was passed.
  ///
  /// - parameter json: JSON object from SwiftyJSON.
  public required init(json: JSON) {
    onError = json[SerializationKeys.onError].boolValue
    detailedTollCosts = DetailedTollCosts(json: json[SerializationKeys.detailedTollCosts])
    costs = Costs(json: json[SerializationKeys.costs])
    response = Response(json: json[SerializationKeys.response])
    if let items = json[SerializationKeys.warnings].array { warnings = items.map { Warnings(json: $0) } }
    if let items = json[SerializationKeys.errors].array { errors = items.map { $0.object} }
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    dictionary[SerializationKeys.onError] = onError
    if let value = detailedTollCosts { dictionary[SerializationKeys.detailedTollCosts] = value.dictionaryRepresentation() }
    if let value = costs { dictionary[SerializationKeys.costs] = value.dictionaryRepresentation() }
    if let value = response { dictionary[SerializationKeys.response] = value.dictionaryRepresentation() }
    if let value = warnings { dictionary[SerializationKeys.warnings] = value.map { $0.dictionaryRepresentation() } }
    if let value = errors { dictionary[SerializationKeys.errors] = value }
    return dictionary
  }

}
