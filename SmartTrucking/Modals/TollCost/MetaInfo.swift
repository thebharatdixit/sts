//
//  MetaInfo.swift
//
//  Created by Vivan Raghuvanshi on 25/11/17
//  Copyright (c) . All rights reserved.
//

import Foundation
import SwiftyJSON

public final class MetaInfo {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let interfaceVersion = "interfaceVersion"
    static let timestamp = "timestamp"
    static let availableMapVersion = "availableMapVersion"
    static let mapVersion = "mapVersion"
    static let moduleVersion = "moduleVersion"
  }

  // MARK: Properties
  public var interfaceVersion: String?
  public var timestamp: String?
  public var availableMapVersion: [String]?
  public var mapVersion: String?
  public var moduleVersion: String?

  // MARK: SwiftyJSON Initializers
  /// Initiates the instance based on the object.
  ///
  /// - parameter object: The object of either Dictionary or Array kind that was passed.
  /// - returns: An initialized instance of the class.
  public convenience init(object: Any) {
    self.init(json: JSON(object))
  }

  /// Initiates the instance based on the JSON that was passed.
  ///
  /// - parameter json: JSON object from SwiftyJSON.
  public required init(json: JSON) {
    interfaceVersion = json[SerializationKeys.interfaceVersion].string
    timestamp = json[SerializationKeys.timestamp].string
    if let items = json[SerializationKeys.availableMapVersion].array { availableMapVersion = items.map { $0.stringValue } }
    mapVersion = json[SerializationKeys.mapVersion].string
    moduleVersion = json[SerializationKeys.moduleVersion].string
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = interfaceVersion { dictionary[SerializationKeys.interfaceVersion] = value }
    if let value = timestamp { dictionary[SerializationKeys.timestamp] = value }
    if let value = availableMapVersion { dictionary[SerializationKeys.availableMapVersion] = value }
    if let value = mapVersion { dictionary[SerializationKeys.mapVersion] = value }
    if let value = moduleVersion { dictionary[SerializationKeys.moduleVersion] = value }
    return dictionary
  }

}
