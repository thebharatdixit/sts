//
//  Leg.swift
//
//  Created by Vivan Raghuvanshi on 25/11/17
//  Copyright (c) . All rights reserved.
//

import Foundation
import SwiftyJSON

public final class Leg {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let link = "link"
    static let end = "end"
    static let maneuver = "maneuver"
    static let travelTime = "travelTime"
    static let start = "start"
    static let length = "length"
  }

  // MARK: Properties
  public var link: [Link]?
  public var end: End?
  public var maneuver: [Maneuver]?
  public var travelTime: Int?
  public var start: Start?
  public var length: Int?

  // MARK: SwiftyJSON Initializers
  /// Initiates the instance based on the object.
  ///
  /// - parameter object: The object of either Dictionary or Array kind that was passed.
  /// - returns: An initialized instance of the class.
  public convenience init(object: Any) {
    self.init(json: JSON(object))
  }

  /// Initiates the instance based on the JSON that was passed.
  ///
  /// - parameter json: JSON object from SwiftyJSON.
  public required init(json: JSON) {
    if let items = json[SerializationKeys.link].array { link = items.map { Link(json: $0) } }
    end = End(json: json[SerializationKeys.end])
    if let items = json[SerializationKeys.maneuver].array { maneuver = items.map { Maneuver(json: $0) } }
    travelTime = json[SerializationKeys.travelTime].int
    start = Start(json: json[SerializationKeys.start])
    length = json[SerializationKeys.length].int
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = link { dictionary[SerializationKeys.link] = value.map { $0.dictionaryRepresentation() } }
    if let value = end { dictionary[SerializationKeys.end] = value.dictionaryRepresentation() }
    if let value = maneuver { dictionary[SerializationKeys.maneuver] = value.map { $0.dictionaryRepresentation() } }
    if let value = travelTime { dictionary[SerializationKeys.travelTime] = value }
    if let value = start { dictionary[SerializationKeys.start] = value.dictionaryRepresentation() }
    if let value = length { dictionary[SerializationKeys.length] = value }
    return dictionary
  }

}
