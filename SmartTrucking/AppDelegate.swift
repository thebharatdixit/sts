//
//  AppDelegate.swift
//  SmartTrucking
//
//  Created by Vivan Raghuvanshi on 19/08/17.
//  Copyright © 2017 Vivan Raghuvanshi. All rights reserved.
// fb1292412620868903
// com.appsomaticnet.smarttrucking
/*
 - Done in picker
 - Create account
 
 - Trip Creation Service
 - Current location Service
 - Turn by turn detail should be dynamic even if we take wrong turn.
 - Current manuever should be highlighted
 - 
*/

import UIKit
//import Firebase
import Google
import GoogleSignIn
//import GoogleMaps
//import GooglePlaces
import IQKeyboardManagerSwift
import SVProgressHUD
import MFSideMenu
import Fabric
import Crashlytics
import GoogleMaps
import GooglePlaces

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    
    var navigationVC:UINavigationController?
    var mapNavigationVC:UINavigationController?
    var mfContainer:MFSideMenuContainerViewController?
    var mapVCRef:MapVC?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        // Configue Firebase
        //FirebaseApp.configure()
        GMSServices.provideAPIKey("AIzaSyA7vKLbRHU5Dtp3rX-_wePQdvWaauzoZQU")
        GMSPlacesClient.provideAPIKey("AIzaSyDSV9ztBznRj_x509SonIuTjy5c1idLSaU")
        self.mapNavigationVC = UINavigationController()
        
        // Configure google sdk
        var configureError: NSError?
        GGLContext.sharedInstance().configureWithError(&configureError)
        assert(configureError == nil, "Error configuring Google services: \(String(describing: configureError))")
        
        //GMSServices.provideAPIKey(GOOGLE_API_KEY)
        //GMSPlacesClient.provideAPIKey(GOOGLE_API_KEY)

        // Enable IQ KeyboardManager to manage keyboard
        IQKeyboardManager.shared.enable = true
        // Cover A mask when showing progress
        SVProgressHUD.setDefaultMaskType(.black)
        
        //Configure Here Map
        
        // Configure fabric to check crash
        Fabric.with([Crashlytics.self])
        
       // Configure facebook sdk
    return FBSDKApplicationDelegate.sharedInstance().application(application, didFinishLaunchingWithOptions: launchOptions)

    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    
    
    func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any] = [:]) -> Bool  {
        
        let isFacebookURL = FBSDKApplicationDelegate.sharedInstance().application(app, open: url, sourceApplication: options[UIApplication.OpenURLOptionsKey.sourceApplication] as? String, annotation:  options[UIApplication.OpenURLOptionsKey.annotation])
        
        let isGooglePlusURL = GIDSignIn.sharedInstance().handle(url, sourceApplication: options[UIApplication.OpenURLOptionsKey.sourceApplication] as? String, annotation: options[UIApplication.OpenURLOptionsKey.annotation])
        
        return isFacebookURL || isGooglePlusURL
        
    }
}
