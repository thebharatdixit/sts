//
//  ServicesCollectionViewCell.swift
//  SmartTrucking
//
//  Created by Prabhakar G on 28/10/18.
//  Copyright © 2018 Vivan Raghuvanshi. All rights reserved.
//

import UIKit

class ServicesCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var serviceImageView: UIImageView!
    
}
