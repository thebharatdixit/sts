//
//  EndTripView.swift
//  SmartTrucking
//
//  Created by Vivan Raghuvanshi on 04/09/17.
//  Copyright © 2017 Vivan Raghuvanshi. All rights reserved.
//

import UIKit
import MFSideMenu

class EndTripView: PopUpView {
    
    
    // MARK:- ------ Button Action Method-------------------
        
    // Add Expense Button Method
    @IBAction func buttonClicked_EndTrip(_ sender : UIButton) {
        animateOut()
        sender.animateView()
        endTrip()
    }
    
    @IBAction func buttonClicked_AddExpense(_ sender : UIButton) {
        animateOut()
        sender.animateView()
        if let parentVC = self.parentViewController as? MapVC {
            parentVC.showHideBottomView()
            let myIndexPath = IndexPath.init(row: 1, section: 0)
            parentVC.collection_View.selectItem(at:myIndexPath, animated: false, scrollPosition:.centeredHorizontally)
        }
    }
    
    @IBAction func buttonClicked_CancelTrip(_ sender : UIButton) {
        animateOut()
        sender.animateView()
        cancelATrip()
    }
    
    private func endTrip() {
        
        if let parentVC = self.parentViewController as? MapVC {
            parentVC.view_.isHidden = true
            if let existingTripId = parentVC.tripId {
                let params = ["tripId":existingTripId]
                showHud("Processing..")
                let networkManager = NetworkManager()
                networkManager.getDataForRequest(closeTrip, andParameter: params) { (response, error) in
                    
                    hideHud()
                    if error == nil {
                        if let result = response as? [String:Any] {
                            if let status = result["status"] as? String, (status == SUCCESS_STATUS) {
                                if let mynavigationVC = APPDELEGATE.navigationVC {
                                    parentVC.refreshDashboard()
                                    if let homeVC = mynavigationVC.viewControllers.first as? HomeVC {
                                        homeVC.endIfOnGoingTrip()
                                    }
                                    
                                    APPDELEGATE.mfContainer?.centerViewController = mynavigationVC
                                    showAlert("", message: "Trip is successfully completed", onView: (mynavigationVC))
                                }
                            } else {
                                showAlert("", message:INTERNAL_ERROR_MESSAGE, onView: parentVC)
                            }
                        }
                    } else {
                        showAlert("", message:error?.localizedDescription ?? SOMETHING_WRONG, onView: parentVC)
                    }
                }
            }
        }

    }
    
    private func cancelATrip() {
        
        if let parentVC = self.parentViewController as? MapVC {
            parentVC.view_.isHidden = true
            if let existingTripId = parentVC.tripId {
                let params = ["tripId":existingTripId]
                showHud("Processing..")
                let networkManager = NetworkManager()
                networkManager.getDataForRequest(cancelTrip, andParameter: params) { (response, error) in
                    
                    hideHud()
                    if error == nil {
                        if let result = response as? [String:Any] {
                            if let status = result["status"] as? String, (status == SUCCESS_STATUS) {
                                if let mynavigationVC = APPDELEGATE.navigationVC {
                                    parentVC.refreshDashboard()
                                    if let homeVC = mynavigationVC.viewControllers.first as? HomeVC {
                                        homeVC.endIfOnGoingTrip()
                                    }
                                    
                                    APPDELEGATE.mfContainer?.centerViewController = mynavigationVC
                                    showAlert("", message: "Trip was cancelled", onView: (mynavigationVC))
                                }
                            } else {
                                showAlert("", message:INTERNAL_ERROR_MESSAGE, onView: parentVC)
                            }
                        }
                    } else {
                        showAlert("", message:error?.localizedDescription ?? SOMETHING_WRONG, onView: parentVC)
                    }
                }
            }
        }
        
    }
}
