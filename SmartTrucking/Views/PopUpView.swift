//
//  PopUpView.swift
//  SmartTrucking
//
//  Created by Vivan Raghuvanshi on 07/09/17.
//  Copyright © 2017 Vivan Raghuvanshi. All rights reserved.
//

import UIKit

class PopUpView: UIView {

    @IBOutlet weak var _view: UIView!
    var senderVC : UIViewController?

    // MARK:- ------ Public Method to show this view------------------
    
    func showPopUp(_ sender : UIViewController) {
        senderVC = sender
        self.frame = sender.view.frame
        animateIn(sender)
    }
    
    // MARK:- ------ Button Action Method-------------------
    
    // Cancel Button Method
    @IBAction func buttonClicked_HidePopUp(_ sender : UIButton) {
        animateOut()
    }
    
    public func animateIn(_ sender : UIViewController) {
        sender.view.addSubview(self)
        self.center = sender.view.center
        _view.transform = CGAffineTransform(scaleX:0.3, y:0.3)
        _view.alpha = 0.5
        
        UIView.animate(withDuration: 0.3) {
            self._view.alpha = 1
            self._view.transform = .identity
        }
    }
    
    public func animateOut() {
        if let parentVC = self.parentViewController as? MapVC {
            parentVC.endPopUpShowing = false
        }
        UIView.animate(withDuration: 0.3, animations: {
            self._view.transform = CGAffineTransform(scaleX:0.3, y:0.3)
            self._view.alpha = 0
        }) { (true) in
            self.removeFromSuperview()
        }
    }
}
