//
//  MonthlyExpensesVC.swift
//  SmartTrucking
//
//  Created by Vivan Raghuvanshi on 15/10/17.
//  Copyright © 2017 Vivan Raghuvanshi. All rights reserved.
//

import UIKit
import MFSideMenu

class MonthlyExpensesVC: UIViewController {
    
    enum ExpenseType:String {
        case Trailer_Spending = "Trailer Spending"
        case Trailer_Insurance = "Trailer Liability Insurance"
        case Truck_Payment = "Truck Payment"
        case Truck_Insurance = "Truck Liability Insurance"
        case Cargo_Insurance = "Cargo Liability Insurance"
        case Occupational_Insurance = "Occupational Insurance"
        case Workers_Insurance = "Workers Comp Insurance"
    }
    
    @IBOutlet weak var tf_ExpenseType:UITextField!
    @IBOutlet weak var tf_Amount:UITextField!
    @IBOutlet weak var img_Cancel:UIImageView!

    
    fileprivate var array_ExpenseTypes :[ExpenseType] {
        let expenseTypes = [ExpenseType.Trailer_Spending,
                            ExpenseType.Trailer_Insurance,
                            ExpenseType.Truck_Payment,
                            ExpenseType.Truck_Insurance,
                            ExpenseType.Cargo_Insurance,
                            ExpenseType.Occupational_Insurance,
                            ExpenseType.Workers_Insurance]
        return expenseTypes
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        let optionPicker = UIPickerView()
        optionPicker.delegate = self
        optionPicker.dataSource = self
        tf_ExpenseType.inputView = optionPicker
        tf_ExpenseType.text = ExpenseType.Trailer_Spending.rawValue
    }
    
    // MARK:- --------------Button Action Methods-------------------
    
    @IBAction func buttonClicked_LeftMenu(_ sender:UIButton) {
        sender.animateView()
        self.menuContainerViewController.toggleLeftSideMenuCompletion {
            
        }
    }
    
    @IBAction func buttonClicked_Add(_ sender:UIButton) {
        sender.animateView()
        let amount = Double(tf_Amount.text ?? "0") ?? 0.0
        if isBlankField(tf_Amount)  || amount <= 0.0 {
            showAlert("", message: "Please enter the amount", onView: self)
        } else {
            self.addMonthlyExpenses()
        }
    }
    
    @IBAction func buttonClicked_Home(_ sender:UIButton) {
        sender.animateView()
        img_Cancel.animateView()

        APPDELEGATE.mfContainer?.centerViewController = APPDELEGATE.navigationVC
        APPDELEGATE.mfContainer?.setMenuState(MFSideMenuStateClosed, completion: nil)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    
    // MARK:- --------UITextField Delegate Methods----------
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == tf_ExpenseType {
            return false
        } else {
            let text = textField.text ?? ""
            if string == "." || text.contains(find: ".") || string == "" {
                // Payment info is restricted upto two decimal
                return allowUptoTwoDecimalPlace(textField.text ?? "", forRange: range, replacementString: string)
            } else {
                return text.count < 5 ? true : false
            }
        }
    }
    
    private func refreshDashboard() {
        
        if let mynavigationVC = APPDELEGATE.navigationVC {
            if let homeVC = mynavigationVC.viewControllers.first as? HomeVC {
                homeVC.getTripSummary()
            }
        }
    }
    
    func addMonthlyExpenses() {
        
        let params = ["profileId": user!.userId!,
                      "expenseTitle":tf_ExpenseType.text ?? "",
                      "amount":tf_Amount.text ?? "0"]
        
        showHud("Processing..")
        
        let networkManager = NetworkManager()
        networkManager.getDataForRequest(monthlyExpenses, andParameter: params) { (response, error) in
            
            hideHud()
            if error == nil {
                
                if let result = response as? [String:Any] {
                    if let status = result["status"] as? String, (status == SUCCESS_STATUS) {
                        showAlert("", message:"Expense Added Successfully", onView: self)
                        self.tf_ExpenseType.text = ExpenseType.Trailer_Spending.rawValue
                        self.tf_Amount.text = ""
                        self.refreshDashboard()
                    } else {
                        showAlert("", message:INTERNAL_ERROR_MESSAGE, onView: self)
                    }
                }
            } else {
                showAlert("", message:error?.localizedDescription ?? SOMETHING_WRONG, onView: self)
            }
        }
    }
}


// MARK:- --------------UIPickerViewDataSource Methods-------------------

extension MonthlyExpensesVC : UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return array_ExpenseTypes.count
    }
}

// MARK:- --------------UIPickerViewDelegate Methods-------------------

extension MonthlyExpensesVC : UIPickerViewDelegate {
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        tf_ExpenseType?.text = array_ExpenseTypes[row].rawValue
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return array_ExpenseTypes[row].rawValue
    }
}





