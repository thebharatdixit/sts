//
//  ExpensesReportVC.swift
//  SmartTrucking
//
//  Created by Vivan Raghuvanshi on 24/12/17.
//  Copyright © 2017 Vivan Raghuvanshi. All rights reserved.
// TotalPaidForTrip ExpenseTypeCell ExpenseCell TotalPaidCell ProfitLossCell

import UIKit
import MFSideMenu
import NTMonthYearPicker

class ExpensesReportVC: UIViewController {
    
    // MARK:- --------------Outlets/Variables-------------------
    
    @IBOutlet weak var tf_Month:UITextField!
    @IBOutlet weak var tf_Year:UITextField!
    @IBOutlet weak var img_Cancel:UIImageView!

    
    var selectedMonth:Int?
    var selectedYear:Int?
    
    var monthlySpendRows:Int = 0
    var monthlyOthersRows:Int = 0
    
    let monthArray = ["January","February","March","April",
                      "May","June","July","August",
                      "September","October","November","December"]
    
    let dateComponents = getDateComponentsFromDate(Date())
    var selectedComponent:DateComponent?
    var yearsArray = [Int]()
    
    var myPicker: MyPicker?
    
    @IBOutlet weak var table_View:UITableView!

    var expenseReport:ExpenseReport? {
        didSet {
            self.table_View.reloadData()
            self.table_View.isHidden = false
        }
    }
    
    var sectionCount = 0
    
    // MARK:- -------------- View Life Cycle Methods-------------------
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if myPicker == nil {
            let arr = Bundle.main.loadNibNamed("MyView", owner: self, options: nil)
            myPicker = arr?.last as? MyPicker
        }
        
        selectedComponent = dateComponents
        
        myPicker?.picker?.datePickerMode = NTMonthYearPickerModeMonthAndYear
        myPicker?.picker?.maximumDate = Date()
        
        myPicker?.picker?.addTarget(self, action: #selector(ExpensesReportVC.onDatePicked), for: .valueChanged)
        
        yearsArray = Array((dateComponents.year - 10)...dateComponents.year)
        
        tf_Month.inputView = myPicker
        tf_Year.inputView = myPicker
        
        table_View.tableFooterView = UIView()
        
        tf_Month.text = dateComponents.monthStr
        tf_Year.text = "\(dateComponents.year)"
        
        selectedMonth = dateComponents.month
        selectedYear = dateComponents.year
        self.getMonthlyExpenses()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    // MARK:- -------------- UIButton Action Methods-------------------
    
    @IBAction func buttonClicked_Home(_ sender:UIButton) {
        sender.animateView()
        img_Cancel.animateView()
        APPDELEGATE.mfContainer?.centerViewController = APPDELEGATE.navigationVC
        APPDELEGATE.mfContainer?.setMenuState(MFSideMenuStateClosed, completion: nil)
    }
    
    // MARK:- --------------Private Methods------------------
    
    private func getMonthlyExpenses() {
        self.view.endEditing(true)
        table_View.isHidden = true
        showHud("Processing..")
        let params = ["month":selectedMonth ?? dateComponents.month,
                      "year":selectedYear ?? dateComponents.year,
                      "profileId":user?.userId ?? ""] as [String : Any]
        
        let networkManager = NetworkManager()
        networkManager.getDataForRequest(monthWiseReport, andParameter: params) { (response, error) in
            hideHud()
            if error == nil {
                if let result = response as? [String:Any] {
                    if let status = result["status"] as? String, (status == SUCCESS_STATUS) {
                        if let expenses = result["_obj"] as? [String:Any] {
                            self.expenseReport = ExpenseReport.init(object: expenses)
                            
                            let fixedMonthlyExpenses = self.expenseReport?.fixedMonthlyExpenses ?? [:]
                            
                            let spentOnTripsMonthly = self.expenseReport?.spentOnTripsMonthly ?? [:]
                            
                            let tripNo = self.expenseReport?.trips ?? 0
                            let fixedExpense = fixedMonthlyExpenses.keys
                            let spentExpense = spentOnTripsMonthly.keys
                            if tripNo == 0 && fixedExpense.count == 0 && spentExpense.count == 0 {
                                self.table_View.isHidden = true
                                showAlert("", message:"No monthly reports found!", onView: self)
                                return
                            }
                        }
                    } else {
                        showAlert("", message:"No monthly reports found!", onView: self)
                    }
                }
            } else {
                showAlert("", message:error?.localizedDescription ?? SOMETHING_WRONG, onView: self)
            }
        }
        //monthlyExpensesHistory
    }
    
    // MARK:- --------------UITextField Delegate Methods------------------
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        return false
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        self.getMonthlyExpenses()
    }
    
    @objc func onDatePicked() {
        if let dateFound = myPicker?.picker?.date {
            let dateComp = getDateComponentsFromDate(dateFound)
            selectedComponent = dateComp
            tf_Month.text = dateComp.monthStr
            tf_Year.text = "\(dateComp.year)"
            selectedMonth = dateComp.month
            selectedYear = dateComp.year
        }
    }
}

//MARK:- ------------- UITableViewDataSource Methods ----------


extension ExpensesReportVC : UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 4
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return 1
        }else if section == 1 {
            if let report = expenseReport {
                if let spentTypes = report.spentOnTripsMonthly {
                    if spentTypes.count == 0 {
                        monthlySpendRows = 0
                    } else {
                        monthlySpendRows = 2 + spentTypes.count
                    }
                }
                return monthlySpendRows
            }
            
        } else if section == 2 {
            if let report = expenseReport {
                if let spentTypes = report.fixedMonthlyExpenses {
                    if spentTypes.count == 0 {
                        monthlyOthersRows = 0
                    } else {
                        monthlyOthersRows = 2 + spentTypes.count
                    }
                }
                return monthlyOthersRows
            }
        } else {
            let tripNo = self.expenseReport?.trips ?? 0
            return tripNo > 0 ? 1:0
        }
        return 0
    }
    // TotalPaidForTrip ExpenseTypeCell ExpenseCell TotalPaidCell ProfitLossCell

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            let titleCell = tableView.dequeueReusableCell(withIdentifier:"TotalPaidForTrip")
            let titleLabel = titleCell?.contentView.viewWithTag(100) as! UILabel
            titleLabel.text = "Total paid for \(expenseReport?.trips ?? 0) trips"
            let paidLabel = titleCell?.contentView.viewWithTag(101) as! UILabel
            let totalMilesLabel = titleCell?.contentView.viewWithTag(200) as! UILabel
            var monthlyEarning = 0.0
            if let earnings = expenseReport?.earnings {
                monthlyEarning = earnings
                 paidLabel.text = "= \(String(describing: earnings.dollarString))"
            }
            if let miles = expenseReport?.miles {
                totalMilesLabel.text = "= $\(String(format: "%.2f", (monthlyEarning / miles)))"
            }
            titleCell?.selectionStyle = .none
            return titleCell!

        } else if indexPath.section == 1 {
            if indexPath.row == 0 {
                let titleCell = tableView.dequeueReusableCell(withIdentifier:"ExpenseTypeCell")
                let titleLabel = titleCell?.contentView.viewWithTag(100) as! UILabel
                titleLabel.text = "Spend"
                titleCell?.selectionStyle = .none
                return titleCell!
            } else if indexPath.row == (monthlySpendRows - 1) {
                let titleCell = tableView.dequeueReusableCell(withIdentifier:"TotalPaidCell")
                let titleLabel = titleCell?.contentView.viewWithTag(100) as! UILabel
                titleLabel.text = "Expense for \(expenseReport?.trips ?? 0) trips" //
                if let expense = expenseReport?.spentOnTripsMonthly {
                    let allvalues = Array(expense.values)
                    let paidLabel = titleCell?.contentView.viewWithTag(101) as! UILabel
                    var expenses = 0.0
                    for text in allvalues {
                        let expense = "\(text)"
                        expenses = expenses + (Double(expense) ?? 0.0)
                    }
                    paidLabel.text = "= \(expenses.dollarString)"
                }
                titleCell?.selectionStyle = .none
                return titleCell!
                
            } else {
                let titleCell = tableView.dequeueReusableCell(withIdentifier:"ExpenseCell")
                if let expense = expenseReport?.spentOnTripsMonthly {
                    
                    let allKeys = Array(expense.keys)
                    let allvalues = Array(expense.values)
                    let titleLabel = titleCell?.contentView.viewWithTag(100) as! UILabel
                    titleLabel.text = "\(allKeys[indexPath.row - 1])"
                    let paidLabel = titleCell?.contentView.viewWithTag(101) as! UILabel
                    let expense = "\(allvalues[indexPath.row - 1])"
                    paidLabel.text = "= \((Double(expense) ?? 0.0).dollarString)"
                }
                
                titleCell?.selectionStyle = .none
                return titleCell!
            }
        }  else if indexPath.section == 2 {
            if indexPath.row == 0 {
                let titleCell = tableView.dequeueReusableCell(withIdentifier:"ExpenseTypeCell")
                let titleLabel = titleCell?.contentView.viewWithTag(100) as! UILabel
                titleLabel.text = "Other"
                titleCell?.selectionStyle = .none
                return titleCell!
            } else if indexPath.row == (monthlyOthersRows - 1) {
                let titleCell = tableView.dequeueReusableCell(withIdentifier:"TotalPaidCell")
                let titleLabel = titleCell?.contentView.viewWithTag(100) as! UILabel
                titleLabel.text = "Fixed Expenses"
                if let expense = expenseReport?.fixedMonthlyExpenses {
                    let allvalues = Array(expense.values)
                    let paidLabel = titleCell?.contentView.viewWithTag(101) as! UILabel
                    var expenses = 0.0
                    for text in allvalues {
                        let expense = "\(text)"
                        expenses = expenses + (Double(expense) ?? 0.0)
                    }
                    paidLabel.text = "= \(expenses.dollarString)"
                }
                titleCell?.selectionStyle = .none
                return titleCell!
                
            } else {
                let titleCell = tableView.dequeueReusableCell(withIdentifier:"ExpenseCell")
                if let expense = expenseReport?.fixedMonthlyExpenses {
                    
                    let allKeys = Array(expense.keys)
                    let allvalues = Array(expense.values)
                    let titleLabel = titleCell?.contentView.viewWithTag(100) as! UILabel
                    titleLabel.text = "\(allKeys[indexPath.row - 1])"
                    let paidLabel = titleCell?.contentView.viewWithTag(101) as! UILabel
                    let expense = "\(allvalues[indexPath.row - 1])"
                    paidLabel.text = "= \((Double(expense) ?? 0.0).dollarString)"
                }
                titleCell?.selectionStyle = .none
                return titleCell!
            }
        } else {
            let titleCell = tableView.dequeueReusableCell(withIdentifier:"ProfitLossCell")
            
            ////////////////////////////////////////////////////////////
            let paidLabel = titleCell?.contentView.viewWithTag(101) as! UILabel
            var profitLoss = 0.0
            var totalEarnings = 0.0
            
            // Extract total Earnings
            if let earnings = expenseReport?.earnings {
                totalEarnings = earnings
            }
            
            var tripsExpenses = 0.0
            // Extract total spenditure on trips
            if let expense = expenseReport?.spentOnTripsMonthly {
                let allvalues = Array(expense.values)
                for text in allvalues {
                    let expense = "\(text)"
                    tripsExpenses = tripsExpenses + (Double(expense) ?? 0.0)
                }
            }
            var fixedExpenses = 0.0
            // Extract total fixed monthly expenses on trips
            if let expense = expenseReport?.fixedMonthlyExpenses {
                let allvalues = Array(expense.values)
                for text in allvalues {
                    let expense = "\(text)"
                    fixedExpenses = fixedExpenses + (Double(expense) ?? 0.0)
                }
            }
            profitLoss = totalEarnings - (fixedExpenses + tripsExpenses)
            paidLabel.text = "= \((abs(profitLoss)).dollarString)"
            
            let titleLabel = titleCell?.contentView.viewWithTag(100) as! UILabel
            
            if let comp = selectedComponent {
                if profitLoss >= 0 { // for \(comp.monthStr.prefix(3)) \(comp.year)
                    titleLabel.text = "Total profit"
                } else {
                    titleLabel.text = "Total loss"
                } // for \(comp.monthStr.prefix(3)) \(comp.year)
            }
            /////////////////////////////////////////////////////////////
            
            titleCell?.selectionStyle = .none
            return titleCell!
        }
    }
}

//MARK:- ------------- UITableViewDelegate Methods ----------

extension ExpensesReportVC : UITableViewDelegate {
    // Height of cell
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0 {
            return 100 * getScaleFactor()
        }
        else if indexPath.section == 1 {
            switch indexPath.row {
            case 0:
                return 60 * getScaleFactor()
            case (monthlySpendRows - 1):
                return 90 * getScaleFactor()
            default:
                return 30 * getScaleFactor()
            }
        } else if indexPath.section == 2 {
            switch indexPath.row {
            case 0:
                return 54 * getScaleFactor()
            case (monthlyOthersRows - 1):
                return 90 * getScaleFactor()
            default:
                return 30 * getScaleFactor()
            }
        } else {
            return 54 * getScaleFactor()
        }
    }
}
