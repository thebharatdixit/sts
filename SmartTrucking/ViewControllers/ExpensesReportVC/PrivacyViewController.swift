//
//  PrivacyViewController.swift
//  Alamofire
//
//  Created by Prabhakar G on 10/06/18.
//

import UIKit
import WebKit
import MFSideMenu

class PrivacyViewController: UIViewController {

    @IBOutlet weak var img_Cancel: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if let pdfURL = Bundle.main.url(forResource: "application disclosure", withExtension: "pdf", subdirectory: nil, localization: nil)  {
            do {
                let data = try Data(contentsOf: pdfURL)
                let webView = WKWebView(frame: CGRect(x:0,y:64,width:view.frame.size.width, height:view.frame.size.height))
                webView.load(data, mimeType: "application/pdf", characterEncodingName:"", baseURL: pdfURL.deletingLastPathComponent())
                webView.scrollView.setZoomScale(30.0, animated: true)
                view.addSubview(webView)
                
            }
            catch {
               
            }
            
        }
    }

    @IBAction func closeClicked(_ sender: UIButton) {
        img_Cancel.animateView()
        APPDELEGATE.mfContainer?.centerViewController = APPDELEGATE.navigationVC
        APPDELEGATE.mfContainer?.setMenuState(MFSideMenuStateClosed, completion: nil)
    }
}
