//
//  ExpensesReportOptionVC.swift
//  SmartTrucking
//
//  Created by Vivan Raghuvanshi on 11/12/17.
//  Copyright © 2017 Vivan Raghuvanshi. All rights reserved.
//

import UIKit
import MFSideMenu
import NTMonthYearPicker

class MyPicker:UIView {
    var picker: NTMonthYearPicker?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        picker = NTMonthYearPicker.init(frame: self.bounds)
        self.addSubview(picker!)
        picker?.center = self.center
    }
}

class ExpensesReportOptionVC: UIViewController {

    // MARK:- --------------Outlets/Variables-------------------
    
    @IBOutlet weak var tf_Month:UITextField!
    @IBOutlet weak var tf_Year:UITextField!
    @IBOutlet weak var img_Cancel:UIImageView!

    
    var selectedMonth:Int?
    var selectedYear:Int?
    
    let monthArray = ["January","February","March","April",
                      "May","June","July","August",
                      "September","October","November","December"]
    
    let dateComponents = getDateComponentsFromDate(Date())
    var yearsArray = [Int]()
    
    var myPicker: MyPicker?
    
    @IBOutlet weak var table_View:UITableView!
    
    var monthlyExpenses = [MonthlyExpensesReport]()

    
    // MARK:- -------------- View Life Cycle Methods-------------------

    override func viewDidLoad() {
        super.viewDidLoad()
        
        table_View.isHidden = true
        
        if myPicker == nil {
            let arr = Bundle.main.loadNibNamed("MyView", owner: self, options: nil)
            myPicker = arr?.last as? MyPicker
        }
        
        myPicker?.picker?.datePickerMode = NTMonthYearPickerModeMonthAndYear
        myPicker?.picker?.maximumDate = Date()
        
        myPicker?.picker?.addTarget(self, action: #selector(ExpensesReportOptionVC.onDatePicked), for: .valueChanged)
        
        yearsArray = Array((dateComponents.year - 10)...dateComponents.year)
        
        tf_Month.inputView = myPicker
        tf_Year.inputView = myPicker
        
        table_View.tableFooterView = UIView()
        
        tf_Month.text = dateComponents.monthStr
        tf_Year.text = "\(dateComponents.year)"
        
        selectedMonth = dateComponents.month
        selectedYear = dateComponents.year
        self.getMonthlyExpenses()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()

    }
    
    // MARK:- -------------- UIButton Action Methods-------------------
    
    @IBAction func buttonClicked_Home(_ sender:UIButton) {
        sender.animateView()
        APPDELEGATE.mfContainer?.centerViewController = APPDELEGATE.navigationVC
        APPDELEGATE.mfContainer?.setMenuState(MFSideMenuStateClosed, completion: nil)
    }
    
    // MARK:- --------------Private Methods------------------
    
    private func getMonthlyExpenses() {
        self.view.endEditing(true)
         showHud("Processing..")
        let params = ["month":selectedMonth ?? dateComponents.month,
                      "year":selectedYear ?? dateComponents.year,
                      "profileId":user?.userId ?? ""] as [String : Any]
        
        let networkManager = NetworkManager()
        networkManager.getDataForRequest(monthlyExpensesHistory, andParameter: params) { (response, error) in
            hideHud()
            if error == nil {
                if let result = response as? [String:Any] {
                    if let status = result["status"] as? String, (status == SUCCESS_STATUS) {
                        if let expenses = result["_obj"] as? [[String:Any]] {
                            
                            let allExpenses = expenses.map({ expenseInfo -> MonthlyExpensesReport in
                                MonthlyExpensesReport.init(expenseInfo)
                            })
                            self.monthlyExpenses = allExpenses
                            self.table_View.reloadData()
                            self.table_View.isHidden = false
                        }
                    } else {
                        showAlert("", message:"No Expense Found!", onView: self)
                    }
                }
            } else {
                showAlert("", message:error?.localizedDescription ?? SOMETHING_WRONG, onView: self)
            }
        }
        //monthlyExpensesHistory
    }
    
    // MARK:- --------------UITextField Delegate Methods------------------
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        return false
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        self.getMonthlyExpenses()
    }
    
    @objc func onDatePicked() {
        if let dateFound = myPicker?.picker?.date {
            let dateComp = getDateComponentsFromDate(dateFound)
            tf_Month.text = dateComp.monthStr
            tf_Year.text = "\(dateComp.year)"
            selectedMonth = dateComp.month
            selectedYear = dateComp.year
        }
    }
}

//MARK:----------- UITableView Data Source Method ------------

extension ExpensesReportOptionVC:UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return monthlyExpenses.count
        }
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier:"MonthlyExpensesReportCell")
            let expense = monthlyExpenses[indexPath.row]
            let expenseNameLbl = cell?.contentView.viewWithTag(1000) as! UILabel
            expenseNameLbl.text = expense.title
            let expenseAmountLbl = cell?.contentView.viewWithTag(1001) as! UILabel
            expenseAmountLbl.text = "= \(String(describing: (Double(expense.amount) ?? 0).dollarString))"
            return cell ?? UITableViewCell()
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier:"TotalMonthlyExpensesReportCell")
            let totalExpense = monthlyExpenses.reduce(0) { sum, expense in
                sum + (Double(expense.amount) ?? 0)
            }
            let expenseTotalLbl = cell?.contentView.viewWithTag(1000) as! UILabel
            expenseTotalLbl.text = "Total Expenses = \(totalExpense.dollarString)"
            return cell ?? UITableViewCell()
        }
    }
}

//MARK:----------- UITableView Delegate Method ------------

extension ExpensesReportOptionVC : UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        var height = 50.0
        if indexPath.section == 1 {
            height = 60.0
        }
        
        if isIpad() {
            return CGFloat(height) * getScaleFactor()
        }
        return CGFloat(height)
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 0 {
            return 0.1
        }
        return 20
        
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView?
    {
        if section == 0 {
            return UIView()
        }
        let headerView = UIView(frame: CGRect(x: 0, y: 0, width: tableView.bounds.size.width, height: 30))
            headerView.backgroundColor = UIColor.clear
        return headerView
    }
}

//MARK:----------- Modal to represent Monthly Expenses Report ------------

class MonthlyExpensesReport {
    var title:String
    var amount:String
    
    init(_ expenseInfo:[String:Any]) {
        self.title = "\(expenseInfo["title"] ?? "")"
        self.amount = "\(expenseInfo["amount"] ?? "")"
    }
}
