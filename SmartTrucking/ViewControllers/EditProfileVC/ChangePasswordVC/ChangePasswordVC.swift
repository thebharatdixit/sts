//
//  ChangePasswordVC.swift
//  SmartTrucking
//
//  Created by Umesh Sharma on 05/10/17.
//  Copyright © 2017 Vivan Raghuvanshi. All rights reserved.
//

import UIKit
import Material
import MFSideMenu

class ChangePasswordVC: UIViewController {
    
    // MARK:- --------------Outlets/Variables-------------------
    
    @IBOutlet weak var tf_OldPwd:TextField!
    @IBOutlet weak var tf_NewPwd:TextField!
    @IBOutlet weak var tf_ConfirmPwd:TextField!
    @IBOutlet weak var img_Cancel:UIImageView!


    // MARK:- --------------View Life Cycle Methods-------------------

    override func viewDidLoad() {
        super.viewDidLoad()

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
      // MARK:- --------------Button Action Methods-------------------
    
    @IBAction func buttonClicked_Back(_ sender:UIButton) {
        sender.animateView()
        self.navigationController?.popViewController(animated: true)
    }
    
    // Proceed for Sign up
    @IBAction func buttonClicked_Update(_ sender:UIButton) {
        sender.animateView()
        self.view.endEditing(true)
        if checkIfAllFieldsValid() {
            self.changePassword()
        }
    }
    
    @IBAction func buttonClicked_TogglePasswordSecurity(sender: UIButton) {
        sender.animateView()
        if sender.tag == 1 {
            self.tf_OldPwd.isSecureTextEntry = !self.tf_OldPwd.isSecureTextEntry
        } else if sender.tag == 2 {
            self.tf_NewPwd.isSecureTextEntry = !self.tf_NewPwd.isSecureTextEntry
        } else {
            self.tf_ConfirmPwd.isSecureTextEntry = !self.tf_ConfirmPwd.isSecureTextEntry
        }
    }
    
    @IBAction func buttonClicked_Home(_ sender:UIButton) {
        sender.animateView()
        img_Cancel.animateView()
        APPDELEGATE.mfContainer?.centerViewController = APPDELEGATE.navigationVC
        APPDELEGATE.mfContainer?.setMenuState(MFSideMenuStateClosed, completion: nil)
    }
    
    // MARK:- --------------Private Methods------------------
    
    private func changePassword() {
        
        let params = ["profileId":user!.userId!,
                      "current":tf_OldPwd.text ?? "",
                      "latest":tf_NewPwd.text ?? ""]
        
        showHud("Processing..")
        
        let networkManager = NetworkManager()
        networkManager.getDataForRequest(resetPassword, andParameter: params) { (response, error) in
            
            hideHud()
            if error == nil {
                
                print(response ?? "Avav")
                
                if let result = response as? [String:Any] {
                    if let status = result["status"] as? String, (status == SUCCESS_STATUS) {
                        showAlert("", message:"Your Profile Updated Successfully", onView: self)
                        self.navigationController?.popViewController(animated: true)
                    } else {
                        if let message = result["message"] as? String {
                             showAlert("", message:message, onView: self)
                        } else {
                            showAlert("", message:INTERNAL_ERROR_MESSAGE, onView: self)
                        }
                    }
                }
            } else {
                showAlert("", message:error?.localizedDescription ?? SOMETHING_WRONG, onView: self)
            }
        }
    }
    
    // MARK:- --------------UITextField Delegate Methods-------------------
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField is TextField {
            let tf = textField as! TextField
            tf.dividerNormalColor = .lightGray
            tf.detailLabel.text = "   "
        }
    }
    

    // MARK:- --------------Private Methods-------------------
    //  Check Fields validity
    private func checkIfAllFieldsValid() -> Bool {
        if (isBlankField(tf_OldPwd) || (tf_OldPwd.text ?? "").characters.count < 5) {
            tf_OldPwd.dividerNormalColor = .red
            tf_OldPwd.detailLabel.text = "*Please Enter 6 Characters For Old Password"
        }
        
        if (isBlankField(tf_NewPwd) || (tf_NewPwd.text ?? "").characters.count < 5) {
            tf_NewPwd.dividerNormalColor = .red
            tf_NewPwd.detailLabel.text = "*Please Enter 6 Characters For New Password"
        }
        if (tf_ConfirmPwd.text != tf_NewPwd.text) {
            tf_ConfirmPwd.dividerNormalColor = .red
            tf_ConfirmPwd.detailLabel.text = "*Password Not Matched"
        }
        
        
        if (tf_OldPwd.dividerNormalColor == .red ||
            tf_NewPwd.dividerNormalColor == .red ||
            tf_ConfirmPwd.dividerNormalColor == .red) {
            return false
        } else {
            return true
        }
    }

}
