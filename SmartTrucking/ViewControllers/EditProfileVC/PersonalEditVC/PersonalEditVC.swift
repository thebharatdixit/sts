//
//  PersonalEditVC.swift
//  SmartTrucking
//
//  Created by Umesh Sharma on 05/10/17.
//  Copyright © 2017 Vivan Raghuvanshi. All rights reserved.
//

import UIKit
import Material
import MFSideMenu

class PersonalEditVC: UIViewController {
    
    // MARK:- --------------Outlets/Variables-------------------
    
    var imageManage:ImageManager?

    
    @IBOutlet weak var tf_Name:TextField!
    @IBOutlet weak var tf_Email:TextField!
    @IBOutlet weak var tf_Mobile:TextField!
    @IBOutlet weak var img_View:UIImageView!
    @IBOutlet weak var img_Cancel:UIImageView!

    
    var imageEdited = false

    
    
    weak var delegate:UserProfileUpdateDelegate?
    
    // MARK:- --------------View Life Cycle Methods-------------------
    
       override func viewDidLoad() {
        super.viewDidLoad()

        tf_Name.text = user?.userName ?? ""
        tf_Email.text = user?.userEmail ?? ""
        tf_Mobile.text = user?.userMobile ?? ""
        
      //  startIndicator(self, forFrame: nil, andColor: .black, withCenter: img_View.center)
        
        let imgPath = getDirectoryPath()
        if let imgExist = UIImage.init(contentsOfFile: imgPath) {
            img_View.image = imgExist
        }
        
        DispatchQueue.global().async {
            self.fetchImage()
        }

    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        img_View.roundCorner()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    
    // MARK:- --------------Button Action Methods-------------------
    
    @IBAction func buttonClicked_Back(_ sender:UIButton) {
        sender.animateView()
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func buttonClicked_AddImage(_ sender:UIButton) {
        sender.animateView()
        imageManage = ImageManager()
        imageManage?.showImagePicker(self.navigationController!) { image in
            DispatchQueue.main.async {
                if image != nil {
                    self.imageEdited = true
                    saveImageDocumentDirectory(image!)
                    print(getDirectoryPath())
                    self.img_View.image = image
                }
            }
        }
    }
    
    // Proceed to update information
    @IBAction func buttonClicked_Update(_ sender:UIButton) {
        sender.animateView()
        self.view.endEditing(true)
        
        if checkIfAllFieldsValid() {
            self.changePersonalInfo()
        }
        
        if self.imageEdited == true {
            DispatchQueue.global().async {
                self.uploadImage()
            }
        }
    }
    
    @IBAction func buttonClicked_Home(_ sender:UIButton) {
        sender.animateView()
        img_Cancel.animateView()
        APPDELEGATE.mfContainer?.centerViewController = APPDELEGATE.navigationVC
        APPDELEGATE.mfContainer?.setMenuState(MFSideMenuStateClosed, completion: nil)
    }
    
    
    // MARK:- --------------UITextField Delegate Methods-------------------
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField is TextField {
            let tf = textField as! TextField
            tf.dividerNormalColor = .lightGray
            tf.detailLabel.text = "   "
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range:NSRange, replacementString string: String) -> Bool {
        if textField == tf_Mobile {
            if (tf_Mobile.text == "" && (string == "0" || string == "1" || string == "2")) {
                return false
            }
            return checkValidPhoneNumberLength(textField.text ?? "", forRange: range, replacementString: string)
        }
     return true
    }
    
    // MARK:- --------------Private Methods------------------
    
     private func fetchImage() {
        let params = ["pid":user!.userId!]
        
        let networkManager = NetworkManager()
        
        networkManager.getImageForRequest(fetchimage, andParameter: params) { (imageExist) in
            if let userImage = imageExist {
                self.img_View.image = userImage
                if let leftVC = APPDELEGATE.mfContainer?.leftMenuViewController as? LeftMenuVC {
                    leftVC.img_View.image = userImage
                }
            }
        }
    }
    
    // Method to change personal info
    private func changePersonalInfo() {
        
        let params = ["profileId":user!.userId!,
                      "name":tf_Name.text ?? "",
                      "mobile":tf_Mobile.text ?? ""]
        
        showHud("Processing..")
        
        let networkManager = NetworkManager()
        networkManager.getDataForRequest(personalInfo, andParameter: params) { (response, error) in
            
            hideHud()
            if error == nil {
                
                print(response ?? "Avav")
                
                if let result = response as? [String:Any] {
                    if let status = result["status"] as? String, (status == SUCCESS_STATUS) {
                        
                        self.delegate?.profileUpdated()
                        
                        showAlert("", message:"Your Profile Updated Successfully", onView: self)
                        
                        if let leftVC = APPDELEGATE.mfContainer?.leftMenuViewController as? LeftMenuVC {
                            leftVC.lbl_Username.text = "\(params["name"]  ?? "")"
                        }
                        if let mynavigationVC = APPDELEGATE.navigationVC {
                            if let homeVC = mynavigationVC.viewControllers.first as? HomeVC {
                                homeVC.lbl_Username.text = "\(params["name"]  ?? "")"
                            }
                        }
                        
                        self.navigationController?.popViewController(animated: true)
                    } else {
                        showAlert("", message:INTERNAL_ERROR_MESSAGE, onView: self)
                    }
                }
            } else {
                showAlert("", message:error?.localizedDescription ?? SOMETHING_WRONG, onView: self)
            }
        }
    }
    
    private func uploadImage() {
        
        if let imgExist = img_View.image {
            let compressedImage = imgExist.scaleImage(imgExist)
            let imageData = compressedImage.jpegData(compressionQuality: 0.8) as NSData?

            if let dataExist = imageData {
                let dataString = dataExist.base64EncodedString(options: .lineLength64Characters)

                let params = ["pid":user!.userId!,
                              "image":dataString] as [String : Any]
                
                let networkManager = NetworkManager()
                networkManager.getDataForRequest(uploadimage, andParameter: params) { (response, error) in
                    
                    hideHud()
                    if error == nil {
                        
                        print(response ?? "Avav")
                        
                        if let result = response as? [String:Any] {
                            if let status = result["status"] as? String, (status == SUCCESS_STATUS) {
                               saveImageDocumentDirectory(compressedImage)
                                if let leftVC = APPDELEGATE.mfContainer?.leftMenuViewController as? LeftMenuVC {
                                    leftVC.img_View.image = compressedImage
                                }
                            } else {
                                
                                print("Error in loading")
                                //showAlert("", message:INTERNAL_ERROR_MESSAGE, onView: self)
                            }
                        }
                    } else {
                        print("Error in loading",error?.localizedDescription ?? SOMETHING_WRONG)
//                        showAlert("", message:error?.localizedDescription ?? SOMETHING_WRONG, onView: self)
                    }
                }
            }

        }
    }
    
    //  Check Fields validity
    private func checkIfAllFieldsValid() -> Bool {
        if (isBlankField(tf_Name)) {
            tf_Name.dividerNormalColor = .red
            tf_Name.detailLabel.text = "Please Enter Your Name"
        }
//        if ((tf_Mobile.text ?? "").count < 10 || (tf_Mobile.text ?? "").count > 11) {
//            tf_Mobile.dividerNormalColor = .red
//            tf_Mobile.detailLabel.text = "Please Enter a Valid Phone Number"
//        }
//
        if (tf_Mobile.text ?? "").count != 0 {
            if (tf_Mobile.text ?? "").count < 10 || (tf_Mobile.text ?? "").count > 11 {
                tf_Mobile.dividerNormalColor = .red
                tf_Mobile.detailLabel.text = "Please Enter a Valid Phone Number"
            }
        }
        if (tf_Name.text ?? "").count < 4 {
            tf_Name.dividerNormalColor = .red
            tf_Name.detailLabel.text = "Please Enter 4 Characters For Name"
        }
        
        if (tf_Name.dividerNormalColor == .red ||
            tf_Mobile.dividerNormalColor == .red) {
            return false
        } else {
            return true
        }
    }
}
