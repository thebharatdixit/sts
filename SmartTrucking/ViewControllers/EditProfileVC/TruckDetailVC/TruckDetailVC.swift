//
//  TruckDetailVC.swift
//  SmartTrucking
//
//  Created by Vivan Raghuvanshi on 19/12/17.
//  Copyright © 2017 Vivan Raghuvanshi. All rights reserved.
//

import UIKit
import Material
import MFSideMenu

class TruckDetailVC: UIViewController {
    
    // MARK:- --------------Outlets/Variables-------------------
    
    @IBOutlet weak var tf_Weight:TextField!
    @IBOutlet weak var tf_Height:TextField!
    @IBOutlet weak var tf_NoOfAxeles:TextField!
    @IBOutlet weak var tf_NoOfTires:TextField!

    @IBOutlet weak var tf_NoOfPassengers:TextField!
    @IBOutlet weak var tf_TrailerType:TextField!
    @IBOutlet weak var tf_NoOfTrailers:TextField!
    @IBOutlet weak var tf_NoOfAxels:TextField!
    @IBOutlet weak var img_Cancel:UIImageView!
    var pickerArray = [String]()
    var isWeight = false
    var isHeight = false
    var isTrailerType = false
    var isAxles = false
    
    weak var delegate:UserProfileUpdateDelegate?
    var truckWeights = ["12000", "16000", "20000", "24000", "28000", "32000", "36000", "40000", "44000", "48000","52,000", "56000", "60000", "64000", "68000", "72000", "76000", "80000"]
    
    var truckHeights = ["11.0", "11.2", "11.4", "11.6", "11.8", "11.10", "12.0", "12.2", "12.4", "12.6", "12.8", "12.10", "13.0", "13.2", "13.4", "13.6", "13.8", "13.10", "14.0", "14.2", "14.4", "14.6"]
    
    var trailerTypes = ["Flatbed", "Enclosed", "Dry Van", "Refrigerated", "Reefers", "Lowboy", "Step Deck/ Single Drop", "Extendable Flat Stretch", "Stretch Single Drop Deck", "Stretch Double Drop", "Extended Double Drop", "RGN", "Stretch RGN", "Conestoga", "Side Kit", "Tanker"]
    var axlesCollection = ["1", "2", "3", "4", "5", "6", "7", "9", "10", "12"]

    var isHome = false
    let trailerPicker = UIPickerView()
    let weightPicker = UIPickerView()
    let heightPicker = UIPickerView()
    let axlesPicker = UIPickerView()
    
    fileprivate var array_TrailerTypes = [TrailerType]()

    // MARK:- --------------View Life Cycle Methods-------------------

    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let truckDetail = user?.userTruckInfo {
            let truckInfo = TruckInfo(object: truckDetail)
            tf_Weight.text = "\(truckInfo.vehicleWeight ?? 0)"
            tf_Height.text = "\(truckInfo.height ?? 0)"
            tf_NoOfAxeles.text = "\(truckInfo.vehicleAxlesCount ?? 0)"
            tf_TrailerType.text = "\(truckInfo.trailerType ?? "")"
           // tf_NoOfTires.text = "\(truckInfo.tiresCount ?? 0)"
        } else {
            tf_Weight.text = "24000"
            tf_Height.text = "13.6"
            tf_TrailerType.text = "Dry Van"
            tf_NoOfAxeles.text = "1"
        }

        trailerPicker.delegate = self
        trailerPicker.dataSource = self
        weightPicker.delegate = self
        weightPicker.dataSource = self
        heightPicker.delegate = self
        heightPicker.dataSource = self
        axlesPicker.delegate = self
        axlesPicker.dataSource = self
        tf_TrailerType.inputView = trailerPicker
        tf_Weight.inputView = weightPicker
        tf_Height.inputView = heightPicker
        tf_NoOfAxeles.inputView = axlesPicker
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK:- --------------Button Action Methods-------------------
    
    @IBAction func buttonClicked_Back(_ sender:UIButton) {
        sender.animateView()
        if self.isHome == false {
            self.navigationController?.popViewController(animated: true)
        } else {
            APPDELEGATE.mfContainer?.centerViewController = APPDELEGATE.navigationVC
            APPDELEGATE.mfContainer?.setMenuState(MFSideMenuStateClosed, completion: nil)
        }
    }
    
    // Proceed for Update Account
    @IBAction func buttonClicked_Update(_ sender:UIButton) {
        sender.animateView()
        self.view.endEditing(true)
            UserDefaults.standard.set(true, forKey: "\(String(describing: user?.userName!))truckDetailsAlertAllowClicked")
            self.saveTruckInformation()
    }
    
    @IBAction func buttonClicked_Home(_ sender:UIButton) {
        sender.animateView()
        img_Cancel.animateView()
        APPDELEGATE.mfContainer?.centerViewController = APPDELEGATE.navigationVC
        APPDELEGATE.mfContainer?.setMenuState(MFSideMenuStateClosed, completion: nil)
    }
    
    
    // Method to get user details
    fileprivate func getUserDetails() {
        
        let params = ["profileId":user!.userId!]
        
       // showHud("Processing..")
        
        let networkManager = NetworkManager()
        networkManager.getDataForRequest(getUserInfo, andParameter: params) { (response, error) in
            
            hideHud()
            if error == nil {
                
                if let result = response as? [String:Any] {
                    if let status = result["status"] as? String, (status == SUCCESS_STATUS) {
                        if let userDetail = result["_obj"] as? [String:Any] {
                            
                            var userInfo = userDetail
                            userInfo["generated_id"] = params["profileId"]
                            // Store the info in local database
                            
                            UserInfo.sharedInfo.setUserProfileWithInfo(userInfo)
                            if let leftVC = APPDELEGATE.mfContainer?.leftMenuViewController as? LeftMenuVC {
                                leftVC.table_View.reloadData()
                            }
                        }
                    } else {
                        showAlert("", message:INTERNAL_ERROR_MESSAGE, onView: self)
                    }
                }
            } else {
                showAlert("", message:error?.localizedDescription ?? SOMETHING_WRONG, onView: self)
            }
        }
    }
    
    
    func saveTruckInformation() {
        var parameters = [String:Any]()
        
       
        parameters["profileId"] = user?.userId ?? ""
        parameters["vehicleType"] = "truck"
        //parameters["emissionType"] = "5"
        parameters["vehicleWeight"] = "\(tf_Weight.text ?? "1")"
        parameters["vehicleAxleCount"] = "\(tf_NoOfAxeles.text ?? "1")"
        parameters["height"] = "\(tf_Height.text ?? "10")"
        parameters["vehicleTiresCount"] = "4"
        parameters["limitedWeight"] = "\(tf_Weight.text ?? "1")"
        parameters["passengerCount"] = "2"
        parameters["trailerType"] = "\(tf_TrailerType.text ?? "Flat Bed Trailer")"
        parameters["trailersCount"] = "1"
        parameters["trailerAxlesCount"] = "0"
        parameters["trailerHeight"] = "11"
        
        showHud("Processing..")
        
        let networkManager = NetworkManager()
        networkManager.getDataForRequest(truckOptions, andParameter: parameters) { (response, error) in
            
            hideHud()
            if error == nil {
                
                if let result = response as? [String:Any] {
                    if let status = result["status"] as? String, (status == SUCCESS_STATUS) {
                        self.delegate?.profileUpdated()
                        //showAlert("", message:"Your Profile Updated Successfully", onView: self)
                        if self.isHome == false {
                            self.navigationController?.popViewController(animated: true)
                        } else {
                            self.getUserDetails()
                        }
                    } else {
                        showAlert("", message:INTERNAL_ERROR_MESSAGE, onView: self)
                    }
                }
            } else {
                showAlert("", message:error?.localizedDescription ?? SOMETHING_WRONG, onView: self)
            }
        }
        
    }
    
    
    // MARK:- --------------UITextField Delegate Methods-------------------
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField is TextField {
            let tf = textField as! TextField
            tf.dividerNormalColor = .lightGray
            tf.detailLabel.text = "   "
        }
        
        if textField == tf_TrailerType {
            pickerArray = trailerTypes
            isTrailerType = true
            isWeight = false
            isHeight = false
            isAxles = false
            trailerPicker.reloadAllComponents()

        } else if textField == tf_Weight {
            pickerArray = truckWeights
            isTrailerType = false
            isWeight = true
            isHeight = false
            isAxles = false
            weightPicker.reloadAllComponents()
        } else if textField == tf_Height{
            pickerArray = truckHeights
            isTrailerType = false
            isWeight = false
            isHeight = true
            isAxles = false
            heightPicker.reloadAllComponents()
        } else {
            pickerArray = axlesCollection
            isTrailerType = false
            isWeight = false
            isHeight = false
            isAxles = true

            axlesPicker.reloadAllComponents()
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField != tf_TrailerType {
//            // Payment info is restricted upto two decimal
//            let text = textField.text ?? ""
//            if string == "." || text.contains(find: ".") || string == "" {
//                // Payment info is restricted upto two decimal
//                return allowUptoTwoDecimalPlace(textField.text ?? "", forRange: range, replacementString: string)
//            } else {
//                if textField == tf_Weight || textField == tf_Height {
//                    return text.count < 3 ? true : false
//                } else {
//                    return text.count < 2 ? true : false
//                }
//            }
        }
        return false
    }
    
    // MARK:- -------------- Private Methods-------------------
    
    //  Check Fields validity
    private func checkIfAllFieldsValid() -> Bool {
        
        let weight = Double(tf_Weight.text ?? "0.0") ?? 0.0
        if (isBlankField(tf_Weight)) || weight <= 0.0 {
            errorMessage("*Enter valid weight", tf_Weight)
        }
        
        let height = Double(tf_Height.text ?? "0.0") ?? 0.0
        if (isBlankField(tf_Height)) || height <= 0.0 {
            errorMessage("*Enter valid height", tf_Height)
        }
        
        let noOfAxeles = Int(tf_NoOfAxeles.text ?? "0") ?? 0
        if (isBlankField(tf_NoOfAxeles)) || noOfAxeles <= 0 {
            errorMessage("*Enter valid no of axles", tf_NoOfAxeles)
        }
        
        let noOfTires = Int(tf_NoOfTires.text ?? "0") ?? 0
        if (isBlankField(tf_NoOfTires)) || noOfTires <= 0 {
            errorMessage("*Enter valid no of tires", tf_NoOfTires)
        }
        
        let noOfPassengers = Int(tf_NoOfPassengers.text ?? "0") ?? 0
        if (isBlankField(tf_NoOfPassengers)) || noOfPassengers <= 0 {
            errorMessage("*Enter valid no of passengrs", tf_NoOfPassengers)
        }
        
        if (isBlankField(tf_TrailerType)) {
            errorMessage("*Select trailer type", tf_TrailerType)
        }
        
        let noOfTrailers = Int(tf_NoOfTrailers.text ?? "0") ?? 0
        if (isBlankField(tf_NoOfTrailers)) || noOfTrailers <= 0 {
            errorMessage("*Enter valid no of trailers", tf_NoOfTrailers)
        }
        
        let noOfAxels = Int(tf_NoOfAxels.text ?? "0") ?? 0
        if (isBlankField(tf_NoOfAxels)) || noOfAxels <= 0 {
            errorMessage("*Enter valid no of axles", tf_NoOfAxels)
        }
        
        if (tf_Weight.dividerNormalColor == .red ||
            tf_Height.dividerNormalColor == .red ||
            tf_NoOfTires.dividerNormalColor == .red ||
            tf_NoOfAxeles.dividerNormalColor == .red ||
            tf_NoOfPassengers.dividerNormalColor == .red  ||
            tf_TrailerType.dividerNormalColor == .red ||
            tf_NoOfTrailers.dividerNormalColor == .red ||
            tf_NoOfAxels.dividerNormalColor == .red) {
            return false
        } else {
            return true
        }
    }
    
    private func errorMessage(_ msg:String,_ textField:TextField) {
        textField.dividerNormalColor = .red
        textField.detailLabel.text = msg
    }
}


// MARK:- --------------UIPickerViewDataSource Methods-------------------

extension TruckDetailVC : UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return pickerArray.count
    }
}

// MARK:- --------------UIPickerViewDelegate Methods-------------------

extension TruckDetailVC : UIPickerViewDelegate {
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if isTrailerType {
            tf_TrailerType.text = trailerTypes[row]
        } else if isWeight {
            tf_Weight.text = truckWeights[row]
        } else if isAxles {
            tf_NoOfAxeles.text = axlesCollection[row]
        }else {
            tf_Height.text = truckHeights[row]
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if isTrailerType {
             return trailerTypes[row]
        } else if isWeight {
            return truckWeights[row]
        } else if isAxles {
            return axlesCollection[row]
        } else {
            return truckHeights[row]
        }
    }
}

