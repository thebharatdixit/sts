//
//  EditProfileOptionVC.swift
//  SmartTrucking
//
//  Created by Umesh Sharma on 05/10/17.
//  Copyright © 2017 Vivan Raghuvanshi. All rights reserved.
//

import UIKit
import MFSideMenu

protocol UserProfileUpdateDelegate:class {
    func profileUpdated()
}

class EditProfileOptionVC: UIViewController {
    
    // MARK:- --------------View Life Cycle Methods-------------------
    
    @IBOutlet weak var btn_ChangePassword:UIButton!
    @IBOutlet weak var img_Cancel:UIImageView!


    override func viewDidLoad() {
        super.viewDidLoad()
        let isSocialUser = UserDefaults.standard.bool(forKey: "SocialUser")
        btn_ChangePassword.isHidden = isSocialUser
        self.getUserDetails()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK:- --------------Button Action Methods-------------------
    // Open Side menu
    @IBAction func buttonClicked_LeftMenu(_ sender:UIButton) {
        sender.animateView()
        self.menuContainerViewController.toggleLeftSideMenuCompletion {
        }
    }
    // Go to change personal info
    @IBAction func buttonClicked_PersonalInfo(_ sender:UIButton) {
        sender.animateView()
        let personalEditVC = self.storyboard?.instantiateViewController(withIdentifier:"PersonalEditVC") as! PersonalEditVC
        personalEditVC.delegate = self
        self.navigationController?.pushViewController(personalEditVC, animated: true)
    }
    // Go to change professional info
    @IBAction func buttonClicked_ProfessionalInfo(_ sender:UIButton) {
        sender.animateView()
        let professionalEditVC = self.storyboard?.instantiateViewController(withIdentifier:"ProfessionalEditVC") as! ProfessionalEditVC
         professionalEditVC.delegate = self
        self.navigationController?.pushViewController(professionalEditVC, animated: true)
    }
    // Go to change address info
    @IBAction func buttonClicked_EditAddress(_ sender:UIButton) {
        sender.animateView()
        let editAddressVC = self.storyboard?.instantiateViewController(withIdentifier:"EditAddressVC") as! EditAddressVC
        editAddressVC.delegate = self
        self.navigationController?.pushViewController(editAddressVC, animated: true)
    }
    
    // Go to change Truck info
    @IBAction func buttonClicked_TruckDetail(_ sender:UIButton) {
        sender.animateView()
        if let truckDetailVC = self.storyboard?.instantiateViewController(withIdentifier:"TruckDetailVC") as? TruckDetailVC {
            truckDetailVC.delegate = self
            self.navigationController?.pushViewController(truckDetailVC, animated: true)
        }
    }
    
    // Go to change password
    @IBAction func buttonClicked_ChangePassword(_ sender:UIButton) {
        sender.animateView()
        let changePasswordVC = self.storyboard?.instantiateViewController(withIdentifier:"ChangePasswordVC") as! ChangePasswordVC
        self.navigationController?.pushViewController(changePasswordVC, animated: true)
    }
    
    @IBAction func buttonClicked_Logout(_ sender:UIButton) {
        sender.animateView()
        let alertAction = UIAlertAction.init(title: "Yes", style: .destructive) { action in
            APPDELEGATE.mfContainer?.dismiss(animated: true, completion: nil)
        }
        showAlert("", message: "Do you want to logout!", withAction: [alertAction], with: true, andTitle: "No", onView: self)
    }
    
    @IBAction func buttonClicked_Home(_ sender:UIButton) {
        sender.animateView()
        img_Cancel.animateView()
        APPDELEGATE.mfContainer?.centerViewController = APPDELEGATE.navigationVC
        APPDELEGATE.mfContainer?.setMenuState(MFSideMenuStateClosed, completion: nil)
    }
    
    
    // MARK:- --------------Private Methods------------------
    
    // Method to get user details
    fileprivate func getUserDetails() {
        
        let params = ["profileId":user!.userId!]
        
        showHud("Processing..")
        
        let networkManager = NetworkManager()
        networkManager.getDataForRequest(getUserInfo, andParameter: params) { (response, error) in
            
            hideHud()
            if error == nil {
                
                if let result = response as? [String:Any] {
                    if let status = result["status"] as? String, (status == SUCCESS_STATUS) {
                        if let userDetail = result["_obj"] as? [String:Any] {
                            
                            var userInfo = userDetail
                            userInfo["generated_id"] = params["profileId"]
                            // Store the info in local database
                            
                            UserInfo.sharedInfo.setUserProfileWithInfo(userInfo)
                            if let leftVC = APPDELEGATE.mfContainer?.leftMenuViewController as? LeftMenuVC {
                                leftVC.table_View.reloadData()
                            }
                        }
                    } else {
                        showAlert("", message:INTERNAL_ERROR_MESSAGE, onView: self)
                    }
                }
            } else {
                showAlert("", message:error?.localizedDescription ?? SOMETHING_WRONG, onView: self)
            }
        }
    }
}

extension EditProfileOptionVC:UserProfileUpdateDelegate {
    // Update the local database 
    func profileUpdated() {
         self.getUserDetails()
    }
}
