//
//  EditAddressVC.swift
//  SmartTrucking
//
//  Created by Umesh Sharma on 05/10/17.
//  Copyright © 2017 Vivan Raghuvanshi. All rights reserved.
//

import UIKit
import Material
import MFSideMenu

class EditAddressVC: UIViewController {
    
    // MARK:- --------------Outlets/Variables-------------------
    
    @IBOutlet weak var tf_City:TextField!
    @IBOutlet weak var tf_Zip:TextField!
    @IBOutlet weak var tf_State:TextField!
    
    @IBOutlet weak var lbl_Username:UILabel!
    @IBOutlet weak var img_Cancel:UIImageView!

    
    weak var delegate:UserProfileUpdateDelegate?
    
    // MARK:- --------------View Life Cycle Methods-------------------

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        tf_City.text = user?.userCity ?? ""
        tf_State.text = user?.userState ?? ""
        tf_Zip.text = user?.userZip ?? ""

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    // MARK:- --------------Button Action Methods-------------------
    
    @IBAction func buttonClicked_Back(_ sender:UIButton) {
        sender.animateView()
        self.navigationController?.popViewController(animated: true)
    }
    
    // Update user's Information and go to home view
    @IBAction func buttonClicked_Done(_ sender:UIButton) {
        sender.animateView()
        self.view.endEditing(true)
        if checkIfAllFieldsValid() {
            self.changeAddress()
        }
    }
    
    @IBAction func buttonClicked_Home(_ sender:UIButton) {
        sender.animateView()
        img_Cancel.animateView()
        APPDELEGATE.mfContainer?.centerViewController = APPDELEGATE.navigationVC
        APPDELEGATE.mfContainer?.setMenuState(MFSideMenuStateClosed, completion: nil)
    }
    
    // MARK:- --------------Private Methods------------------
    
    private func changeAddress() {
        
        let params = ["profileId":user!.userId!,
                      "city":tf_City.text ?? "",
                      "zip":tf_Zip.text ?? "",
                      "state":tf_State.text ?? ""]
        
        showHud("Processing..")
        
        let networkManager = NetworkManager()
        networkManager.getDataForRequest(resetAddress, andParameter: params) { (response, error) in
            
            hideHud()
            if error == nil {
                
                print(response ?? "Avav")
                
                if let result = response as? [String:Any] {
                    if let status = result["status"] as? String, (status == SUCCESS_STATUS) {
                        self.delegate?.profileUpdated()
                        showAlert("", message:"Your Profile Updated Successfully", onView: self)
                        self.navigationController?.popViewController(animated: true)
                    } else {
                        showAlert("", message:INTERNAL_ERROR_MESSAGE, onView: self)
                    }
                }
            } else {
                showAlert("", message:error?.localizedDescription ?? SOMETHING_WRONG, onView: self)
            }
        }
    }
    
    // MARK:- --------------UITextField Delegate Methods-------------------
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField is TextField {
            let tf = textField as! TextField
            tf.dividerNormalColor = .lightGray
            tf.detailLabel.text = "   "
        }
    }
    
//    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
//        if textField == tf_Zip {
//            let text = textField.text ?? ""
//            return text.count < 5 ? true : false
//        }
//        return true
//    }
    
    // MARK:- --------------Private Methods-------------------
    
    //  Check Fields validity
    private func checkIfAllFieldsValid() -> Bool {
        if isBlankField(tf_City) {
            tf_City.dividerNormalColor = .red
            tf_City.detailLabel.text = "*Please Enter City Name"
        }
        if isBlankField(tf_Zip) {
            tf_Zip.dividerNormalColor = .red
            tf_Zip.detailLabel.text = "*Please Enter Zip Code"
        }
        if isBlankField(tf_State) {
            tf_State.dividerNormalColor = .red
            tf_State.detailLabel.text = "*Please Enter State Name"
        }
        
        if (tf_City.text ?? "").count < 3 {
            tf_City.dividerNormalColor = .red
            tf_City.detailLabel.text = "Please Enter 3 Characters For City"
        }
        
        if (tf_Zip.text ?? "").count < 5 {
            tf_Zip.dividerNormalColor = .red
            tf_Zip.detailLabel.text = "Please Enter 5 Characters For Zip"
        }
        
        if let zip = tf_Zip.text {
            let zipCode = Double(zip) ?? 0.0
            if zipCode <= 0.0 {
                tf_Zip.dividerNormalColor = .red
                tf_Zip.detailLabel.text = "Please Enter a valid Zip"
            }
        }
        
        if (tf_State.text ?? "").count < 2 {
            tf_State.dividerNormalColor = .red
            tf_State.detailLabel.text = "Please Enter 2 Characters For State"
        }
        
        if (tf_City.dividerNormalColor == .red ||
            tf_Zip.dividerNormalColor == .red ||
            tf_State.dividerNormalColor == .red) {
            return false
        } else {
            return true
        }
    }

}
