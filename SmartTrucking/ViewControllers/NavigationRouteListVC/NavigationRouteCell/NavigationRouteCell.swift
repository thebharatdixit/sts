//
//  NavigationRouteCell.swift
//  SmartTrucking
//
//  Created by Vivan Raghuvanshi on 14/11/17.
//  Copyright © 2017 Vivan Raghuvanshi. All rights reserved.
//

import UIKit

class NavigationRouteCell: UITableViewCell {
    
    @IBOutlet weak var img_View:UIImageView!
    @IBOutlet weak var lbl_Turn:UILabel!
    @IBOutlet weak var lbl_RoadName:UILabel!
    @IBOutlet weak var lbl_Distance:UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
}
