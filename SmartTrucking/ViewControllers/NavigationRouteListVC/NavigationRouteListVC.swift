//
//  NavigationRouteListVC.swift
//  SmartTrucking
//
//  Created by Vivan Raghuvanshi on 14/11/17.
//  Copyright © 2017 Vivan Raghuvanshi. All rights reserved.
//

import UIKit

class NavigationRouteListVC: UIViewController {
    
    @IBOutlet weak var table_View:UITableView!
    

    override func viewDidLoad() {
        super.viewDidLoad()
        table_View.estimatedRowHeight = 95
        table_View.rowHeight = UITableView.automaticDimension
        table_View.tableFooterView = UIView()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func buttonClicked_Back(_ sender:UIButton) {
        sender.animateView()
        self.dismiss(animated: true, completion: nil)
    }
}

