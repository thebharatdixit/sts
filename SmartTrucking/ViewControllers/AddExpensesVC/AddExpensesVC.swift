//
//  AddExpensesVC.swift
//  SmartTrucking
//
//  Created by Vivan Raghuvanshi on 05/10/17.
//  Copyright © 2017 Vivan Raghuvanshi. All rights reserved.
//

import UIKit
import MFSideMenu

protocol AddExpensesDelegate {
    func expensesIsAdded()
}

class AddExpensesVC: UIViewController {
    
    // MARK:- --------------Outlets/Variables-------------------
    
    enum ExpenseOption:String {
        case FUEL = "FUEL"
        case OTHERS = "OTHERS"
    }
    
    @IBOutlet weak var tf_ChooseType:UITextField!
    @IBOutlet weak var tf_ExpenseType:UITextField!
    @IBOutlet weak var tf_Amount:UITextField!
    @IBOutlet weak var img_DropDown:UIImageView!
    @IBOutlet weak var lbl_ExpenseTitle:UILabel!
    
    @IBOutlet weak var btn_Back:UIButton!
    @IBOutlet weak var img_Cancel:UIImageView!

    
    private var shouldEditable = true
    
    fileprivate var array_Options :[ExpenseOption] {
        let options = [ExpenseOption.FUEL, ExpenseOption.OTHERS]
        return options
    }
    
    fileprivate var array_ExpenseTypes :[String] {
        let expenseTypes = ["TICKET","PENALTY", "PARKING", "FOOD", "MISC"]
        return expenseTypes
    }
    
    var tripId:String?
    var delegate:AddExpensesDelegate?
    
    // MARK:- --------------View Life Cycle Methods-------------------

    override func viewDidLoad() {
        super.viewDidLoad()

        let optionPicker = UIPickerView()
        optionPicker.delegate = self
        optionPicker.dataSource = self
        optionPicker.tag = 301
        tf_ChooseType.inputView = optionPicker
        tf_ChooseType.text = ExpenseOption.FUEL.rawValue
        
        if self.delegate == nil {
            btn_Back.isHidden = true
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK:- --------------Button Action Methods-------------------
    
    @IBAction func buttonClicked_LeftMenu(_ sender:UIButton) {
        sender.animateView()
        self.menuContainerViewController.toggleLeftSideMenuCompletion {
            
        }
    }
    
    @IBAction func buttonClicked_Back(_ sender:UIButton) {
        sender.animateView()
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func buttonClicked_Home(_ sender:UIButton) {
        sender.animateView()
        img_Cancel.animateView()
        APPDELEGATE.mfContainer?.centerViewController = APPDELEGATE.navigationVC
        APPDELEGATE.mfContainer?.setMenuState(MFSideMenuStateClosed, completion: nil)
    }
    
    @IBAction func buttonClicked_Add(_ sender:UIButton) {
        sender.animateView()
        if isBlankField(tf_ExpenseType) || isBlankField(tf_Amount) {
            showAlert("", message: "Please fill all the required fields", onView: self)
            return;
        }
        
        let loadVal = Double(tf_Amount.text ?? "0.0") ?? 0.0
        if loadVal <= 0.0 {
            showAlert("", message: "Please Enter A Valid Amount", onView: self)
            return
        }
        
        self.view.endEditing(true)
        
        if let existingTripId = tripId {
            var params = ["tripId":existingTripId,
                          "isCreate":"true",
                          "expense":tf_Amount.text ?? "0"] as [String:Any]
            
            // "expenseTitle":"title"
            
            if tf_ChooseType.text == ExpenseOption.FUEL.rawValue {
                params["expenseType"] = "FUEL"
                params["expenseTitle"] = "Fuel Expenses"
                
                if let amount = Double(tf_ExpenseType.text ?? "0") {
                    if let rate = Double(tf_Amount.text ?? "0") {
                        
                        if amount <= 0.0 || rate <= 0.0 {
                            showAlert("", message: "Please Enter A Valid Amount", onView: self)
                            return
                        }
                        
                        let fuelExpense = (amount * rate)
                        params["expense"] = "\(fuelExpense.rounded(toPlaces: 2))"
                    }
                }
            } else {
                params["expenseType"] = tf_ExpenseType.text ?? "MISC"
                params["expenseTitle"] = "\(tf_ExpenseType.text ?? "Other") Expenses"
            }
            self.addExpenseForParamameters(params)
        }
    }
    
    // MARK:- ------ Private Method-------------------
    
    // Method to add expense
    func addExpenseForParamameters(_ params:[String:Any]) {
        
        showHud("Processing..")
        let networkManager = NetworkManager()
        networkManager.getDataForRequest(addExpenses, andParameter: params) { (response, error) in
            
            hideHud()
            
            if error == nil {
                if let result = response as? [String:Any] {
                    if let status = result["status"] as? String, (status == SUCCESS_STATUS) {
                        self.tf_Amount.text = ""
                        if self.tf_ChooseType.text == ExpenseOption.FUEL.rawValue {                            self.tf_ExpenseType.text = ""
                        }
                        if let mapVC = APPDELEGATE.mapVCRef, mapVC.tripId == self.tripId {
                            if let expense = params["expense"] as? String {
                                if let amount = Double(expense) {
                                    mapVC.totalExpense = amount + mapVC.totalExpense
                                }
                            }
                        } else {
                            self.delegate?.expensesIsAdded()
                            self.refreshDashboard()
                        }
                        
                        showAlert("", message: "Expenses Added Successfully", onView: self)
                        
                    } else {
                        showAlert("", message:INTERNAL_ERROR_MESSAGE, onView: self)
                    }
                }
            } else {
                showAlert("", message:error?.localizedDescription ?? SOMETHING_WRONG, onView: self)
            }
        }
    }
    
    fileprivate func setUpView(_ row:Int) {
        
        if row == 0 {
            tf_ExpenseType.placeholder = "# of Gallons"
            tf_Amount.placeholder = "$ Per Gallon"
            tf_ExpenseType.inputView = nil;
            tf_ExpenseType.reloadInputViews();
            tf_ExpenseType.text = ""
            lbl_ExpenseTitle.text = "Add Fuel Expenses"
            img_DropDown.isHidden = true
            self.shouldEditable = true
            
        } else {
            
            tf_ExpenseType.placeholder = "# Type"
            tf_Amount.placeholder = "$ Amount"
            tf_ExpenseType.text = array_ExpenseTypes[0]
            lbl_ExpenseTitle.text = "Add Other Expenses"
            img_DropDown.isHidden = false
            self.shouldEditable = false
            
            let expenseTypesPicker = UIPickerView()
            expenseTypesPicker.delegate = self
            expenseTypesPicker.dataSource = self
            tf_ExpenseType.inputView = expenseTypesPicker
        }
    }
    
    // MARK:- --------- Refresh Dashboard -------------
    
    private func refreshDashboard() {
        
        if let mynavigationVC = APPDELEGATE.navigationVC {
            if let homeVC = mynavigationVC.viewControllers.first as? HomeVC {
                homeVC.getTripSummary()
            }
        }
    }
    
    // MARK:- --------UITextField Delegate Methods----------
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        let txt = textField.text ?? ""
        if txt.count == 0 && string == "." {
            return false
        }
        
        if textField == tf_ChooseType {
            return false
        } else if textField == tf_ExpenseType {
            let text = textField.text ?? ""
            if self.shouldEditable {
                if string == "." || text.contains(find: ".") || string == "" {
                    // Payment info is restricted upto two decimal
                    return allowUptoTwoDecimalPlace(textField.text ?? "", forRange: range, replacementString: string)
                } else {
                    return text.count < 4 ? true : false
                }
            } else {
                return self.shouldEditable
            }
        } else {
            let text = textField.text ?? ""
            if string == "." || text.contains(find: ".") || string == "" {
                // Payment info is restricted upto two decimal
                return allowUptoTwoDecimalPlace(textField.text ?? "", forRange: range, replacementString: string)
            } else {
                return text.count < 3 ? true : false
            }
        }
    }
}

// MARK:- --------UIPickerViewDataSource Methods-----------

extension AddExpensesVC : UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if pickerView.tag == 301 {
            return array_Options.count
        } else {
            return array_ExpenseTypes.count
        }
    }
}

// MARK:- --------------UIPickerViewDelegate Methods-------------------

extension AddExpensesVC : UIPickerViewDelegate {
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if pickerView.tag == 301 {
            let expenseOption = array_Options[row]
            tf_ChooseType?.text = expenseOption.rawValue
            self.setUpView(row)
        } else {
            tf_ExpenseType?.text = array_ExpenseTypes[row]
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if pickerView.tag == 301 {
            let expenseOption = array_Options[row]
            return expenseOption.rawValue
        } else {
            return array_ExpenseTypes[row]
        }
    }
}

