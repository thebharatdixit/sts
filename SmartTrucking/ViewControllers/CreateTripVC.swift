//
//  CreateTripVC.swift
//  SmartTrucking
//
//  Created by Vivan Raghuvanshi on 02/09/17.
//  Copyright © 2017 Vivan Raghuvanshi. All rights reserved.
//

import UIKit
import Material
import GooglePlacesSearchController

class CreateTripVC: UIViewController {
    
    // MARK:- --------------Outlets/Variables-------------------

    @IBOutlet weak var tf_Source:UITextField!
    @IBOutlet weak var tf_Destination:UITextField!
    
    @IBOutlet weak var table_View:UITableView!
    
    @IBOutlet weak var btn_AddStop:UIButton!
    
    var sourcePlace:PlaceDetails?
    var destinationPlace:PlaceDetails?
    var waypointsPlace = [PlaceDetails]()
    
    lazy var controller:GooglePlacesSearchController = {
        let searchController = GooglePlacesSearchController(
            apiKey: GOOGLE_API_KEY,
            placeType: PlaceType.address
        )
        return searchController
    }()

    // MARK:- --------------View Life Cycle Methods-------------------
    
    override func viewDidLoad() {
        super.viewDidLoad()
        table_View.estimatedRowHeight = 50
        table_View.rowHeight = UITableViewAutomaticDimension
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK:- --------------Button Action Methods-------------------
    
    @IBAction func buttonClicked_Back(_ sender:UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func buttonClicked_AddStop(_ sender:UIButton) {
        
        if sender.tag == 105 {
            showAlert("", message: "You can add 5 waypoints", onView: self)
            return;
        }
        
        controller.didSelectGooglePlace { (place) -> Void in
            
             self.waypointsPlace.append(place)
            
            // Get textField from tag
            let tfTag = sender.tag + 190
            let tfStop = self.view.viewWithTag(tfTag) as! UITextField
            tfStop.text = place.name
            // Get View for stops
            let viewTag = sender.tag + 90
            let viewStop = self.view.viewWithTag(viewTag)
            
            viewStop?.alpha = 0
            sender.tag = sender.tag + 1
            UIView.animate(withDuration: 0.3) {
                viewStop?.alpha = 1
                viewStop?.isHidden = false
            }
            //Dismiss Search
            self.controller.isActive = false
        }
        present(controller, animated: true, completion: nil)
    }
    
    @IBAction func buttonClicked_RemoveStop(_ sender:UIButton) {
        
        let index = sender.tag - 1001
        self.waypointsPlace.remove(at: index)
        
        btn_AddStop.tag = btn_AddStop.tag - 1
        
        let viewTag = sender.tag - 900 // remove button tag
        let viewStop = self.view.viewWithTag(viewTag)
        viewStop?.alpha = 1
        UIView.animate(withDuration: 0.3) {
            viewStop?.alpha = 0
            viewStop?.isHidden = true
        }
    }
    
    @IBAction func buttonClicked_CreateTrip(_ sender:UIButton) {
        
        if checkIfAllFieldsValid() {
            self.createTrip()
        }
    }
    
    //MARK:- ---------- UITextField delegate Methods -----------

    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        if textField is TextField {
            let tf = textField as! TextField
            tf.dividerNormalColor = .lightGray
            tf.detailLabel.text = "   "
        }
        
        controller.didSelectGooglePlace { (place) -> Void in
            print(place.description)
            textField.text = place.name
            
            if textField == self.tf_Source {
                self.sourcePlace = place
            } else {
                self.destinationPlace = place
            }
            
            //Dismiss Search
            self.controller.isActive = false
        }
        
        present(controller, animated: true, completion: nil)
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        return false
    }
    
    // MARK:- --------------Private Methods-------------------
    
    //  Check Fields validity
    private func checkIfAllFieldsValid() -> Bool {
        
        if (isBlankField(tf_Source) || isBlankField(tf_Destination)) {
            return false
        } else {
            return true
        }
    }
    
    private func createTrip() {
        
        let params = ["profileId":user!.userId!,
        "sourceAddress":sourcePlace?.formattedAddress ?? (sourcePlace?.name ?? ""),
        "sourceLatitude":sourcePlace!.coordinate.latitude,
        "sourceLongitude":sourcePlace!.coordinate.longitude,
        "destinationAddress":destinationPlace?.formattedAddress ?? (destinationPlace?.name ?? ""),
        "destinationLatitude":destinationPlace!.coordinate.latitude,
        "destinationLongitude":destinationPlace!.coordinate.longitude,
        "tripStatus":"started",
        "startDate":getDateStringFromDate(Date())] as [String : Any]
        
        print(params)
        
        /*
         
         FUEL,PARKING,FOOD,TICKETS,MISC - type
         */
        
        showHud("Processing..")
        let networkManager = NetworkManager()
        networkManager.getDataForRequest(calculateTrip, andParameter: params) { (response, error) in
            
            hideHud()
            
            if error == nil {
                if let result = response as? [String:Any] {
                    if let status = result["status"] as? String, (status == SUCCESS_STATUS) {
                        if let tripInfo = result["_obj"] {
                            let tripId = "\(tripInfo)"
                            
                            let mapVC = self.storyboard?.instantiateViewController(withIdentifier: "MapVC") as! MapVC
                            mapVC.sourcePlace = self.sourcePlace
                            mapVC.destinationPlace = self.destinationPlace
                            mapVC.waypointsPlace = self.waypointsPlace
                            mapVC.tripId = tripId
                            self.navigationController?.pushViewController(mapVC, animated: true)
                        }
                    } else {
                        showAlert("", message: INTERNAL_ERROR_MESSAGE, onView: self)
                    }
                }
            } else {
                showAlert("", message:error?.localizedDescription ?? SOMETHING_WRONG, onView: self)
            }
        }
    }
}


extension CreateTripVC:UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return 2
        }
        return 3
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "TripHistoryCell")
        let tf = cell?.contentView.viewWithTag(501) as! UITextField
        tf.text = "Recent Trip \(indexPath.row + 1)"
        return cell!
    }
}

extension CreateTripVC:UITableViewDelegate {
    
}


