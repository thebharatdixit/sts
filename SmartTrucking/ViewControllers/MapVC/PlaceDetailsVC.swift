//
//  PlaceDetailsVC.swift
//  SmartTrucking
//
//  Created by Prabhakar G on 28/10/18.
//  Copyright © 2018 Vivan Raghuvanshi. All rights reserved.
//

import Foundation

class PlaceDetailsVC: UIViewController {
    
    @IBOutlet weak var placeDetailsCityLabel: UILabel!
    @IBOutlet weak var placeAddressLabel: UILabel!
    @IBOutlet weak var catogoryTitle: UILabel!
    @IBOutlet weak var catogoryImageView: UIImageView!
    
    @IBOutlet weak var parkingSpacesLabel: UILabel!
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var rsaLabel: UILabel!
    @IBOutlet weak var truckCareLabel: UILabel!
    @IBOutlet weak var showersLabel: UILabel!
    @IBOutlet weak var bulkDEFLanesLabel: UILabel!
    @IBOutlet weak var dieselLanesLabel: UILabel!
    
    @IBOutlet weak var servicesCollectionView: UICollectionView!
    var placeDetail: POI!
    var facilities = [String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        showAnimate()
        print(placeDetail)
        configPage()
        configFacilities()
        tableView.separatorStyle = .none
        categoryImage()
    }
    
    func configFacilities() {
        if (placeDetail?.chapelMinistry)! == 1 {
            facilities.append("Chapel")
        }
        if (placeDetail?.catScale)! == 1 {
            facilities.append("catScale")
        }
        
        if (placeDetail?.wifi)! == 1 {
            facilities.append("WiFi")
        }
        
        if (placeDetail?.fitnessRoom)! == 1 {
            facilities.append("Fitness")
        }
        
        if (placeDetail?.travelStore)! == 1 {
            facilities.append("Travelstore")
        }
        
        if (placeDetail?.casino)! == 1 {
            facilities.append("Casino")
        }
        
        if placeDetail.poiMarkers.count > 0 {
            facilities.append("Restaurant1")
        }
        
        if placeDetail.showers > 0 {
            facilities.append("Shower")
        }
        
        if placeDetail.parkingSpace > 0 {
            facilities.append("ST_Truck_Parking")
        }
    }
    
    func configPage() {
        let park = (placeDetail.parkingSpace != 0) ? "\(placeDetail.parkingSpace)" :  "N/A"
        let diesel = (placeDetail.dieselLanes != 0) ? "\(placeDetail.dieselLanes)" : "N/A"
        let defLanes = (placeDetail.bulkDefLanes != 0) ? "\(placeDetail.bulkDefLanes)": "N/A"
        let showers = (placeDetail.showers != 0) ? "\(placeDetail.showers)" : "N/A"
        let truckCare = (placeDetail.truckCareFacilities != 0) ? "\(placeDetail.truckCareFacilities)" : "N/A"
        let rsa = (placeDetail.roadSideAssistance != 0) ? "\(placeDetail.roadSideAssistance)" : "N/A"
        
        catogoryTitle.text = placeDetail.placeName
        placeAddressLabel.text = placeDetail.address
        placeDetailsCityLabel.text = "\(placeDetail.city), \(placeDetail.state), \(placeDetail.interstate)"
        parkingSpacesLabel.text = "Parking Spaces          \(park)"
        dieselLanesLabel.text =   "Diesel Lanes               \(diesel)"
        bulkDEFLanesLabel.text =  "Bulk DEF lanes             \(defLanes)"
        showersLabel.text =       "Showers                      \(showers)"
        truckCareLabel.text =     "Truck Care                 \(truckCare)"
        rsaLabel.text =           "Road Side Assistance \(rsa)"
    }
    
    func categoryImage() {
        catogoryImageView.image = UIImage(named: "\(placeDetail.category)1") ?? UIImage(named: "GAS1")
    }
    
    @IBAction func phoneTapped(_ sender: UIButton) {
        let phoneNumber = placeDetail.phoneNo.stripped
        
        if let phoneCallURL = URL(string: "tel://\(phoneNumber)") {
            let application = UIApplication.shared
            if application.canOpenURL(phoneCallURL) {
                application.open(phoneCallURL, options: [:], completionHandler: nil)
            }
        }
    }
    
    @IBAction func closeButtonTapped(_ sender: UIButton) {
        if let viewWithTag = self.parent?.view.viewWithTag(100) {
            viewWithTag.removeFromSuperview()
        }else{
            print("No!")
        }
        removeAnimate()
    }
    
    func showAnimate() {
        self.view.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
        self.view.alpha = 0.0
        UIView.animate(withDuration: 0.25, animations: {
            self.view.alpha = 1.0
            self.view.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
        })
    }
    
    func removeAnimate() {
        UIView.animate(withDuration: 0.25, animations: {
            self.view.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
            self.view.alpha = 0.0
        }, completion: {(finished : Bool) in
            if(finished)
            {
                self.willMove(toParent: nil)
                self.view.removeFromSuperview()
                self.removeFromParent()
            }
        })
    }
}


extension PlaceDetailsVC: UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return facilities.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = servicesCollectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath) as? ServicesCollectionViewCell
        cell?.serviceImageView.image = UIImage(named: facilities[indexPath.row])
        return cell!
    }
}

extension PlaceDetailsVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return placeDetail.poiMarkers.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "tableCell", for: indexPath)
        cell.textLabel?.text = placeDetail.poiMarkers[indexPath.row].name
        cell.textLabel?.font = UIFont.init(name: "Helvetica", size: 15)
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 20
    }
}


