//
//  EncodedHelper.h
//  
//
//  Created by Prabhakar G on 12/05/18.
//

#import <Foundation/Foundation.h>

@interface EncodedHelper : NSObject
+ (NSMutableArray *)decodePolyLine: (NSMutableString *)encoded;
@end
