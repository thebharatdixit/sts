//
//  ExpensesCell.swift
//  SmartTrucking
//
//  Created by Umesh Sharma on 15/09/17.
//  Copyright © 2017 Vivan Raghuvanshi. All rights reserved.
//

import UIKit

class ExpensesCell: UICollectionViewCell {
    
    @IBOutlet weak var tf_Expense:UITextField!
    @IBOutlet weak var tf_ExpenseFor:UITextField!
    
    fileprivate var tf_selected : UITextField?
    fileprivate var array_ExpenseType :[String] {
        let expenseTypes = ["PARKING", "FOOD", "MISC"]
        return expenseTypes
    }
    fileprivate var array_TicketType :[String] {
        let expenseTypes = ["TICKET", "PENALTY"]
        return expenseTypes
    }
    
    //FUEL,PARKING,FOOD,TICKETS,MISC - type
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    @IBAction func buttonClicked_Add(_ sender:UIButton) {
         sender.animateView()
         if let parentVC = self.parentViewController as? MapVC {
            if isBlankField(tf_Expense) || isBlankField(tf_ExpenseFor) {
                showAlert("", message: "Please fill all the required fields", onView: parentVC)
                return;
            }
            self.endEditing(true)
            if let existingTripId = parentVC.tripId {
                var params = ["tripId":existingTripId,
                              "isCreate":"true",
                              "expense":tf_ExpenseFor.text ?? "0"] as [String:Any]
                
               // "expenseTitle":"title"
                if sender.tag == 1001 {
                   params["expenseType"] = "FUEL"
                   params["expenseTitle"] = "Fuel Expenses"
                    
                    if let amount = Double(tf_Expense.text ?? "0") {
                        if let rate = Double(tf_ExpenseFor.text ?? "0") {
                            
                            if amount <= 0.0 || rate <= 0.0 {
                                showAlert("", message: "Please fill valid values", onView: parentVC)
                                tf_Expense.text = ""
                                tf_ExpenseFor.text = ""
                                return;
                            }
                            
                            let fuelExpense = (amount * rate)
                             params["expense"] = "\(fuelExpense.rounded(toPlaces: 2))"
                        }
                    }
                } else if sender.tag == 1002 {
                    if let rate = Double(tf_ExpenseFor.text ?? "0"), rate <= 0.0 {
                        showAlert("", message: "Please fill valid value", onView: parentVC)
                        tf_ExpenseFor.text = ""
                        return;
                    }
                    
                    params["expenseType"] = "TICKETS"
                    params["expenseTitle"] = "\(tf_Expense.text ?? "TICKET") Expenses"
                } else {
                    if let rate = Double(tf_ExpenseFor.text ?? "0"), rate <= 0.0 {
                        showAlert("", message: "Please fill valid value", onView: parentVC)
                        tf_ExpenseFor.text = ""
                        return;
                    }
                    params["expenseType"] = tf_Expense.text ?? "MISC"
                    params["expenseTitle"] = "Other Expenses"
                }
                print(params)
                
                self.addExpenseForParamameters(params, onView: parentVC)
            }
        }
    }
    
    // MARK:- ------ Private Method-------------------
    
    // Method to add expense
    func addExpenseForParamameters(_ params:[String:Any], onView parentVC:MapVC) {
    
        showHud("Processing..")
        let networkManager = NetworkManager()
        networkManager.getDataForRequest(addExpenses, andParameter: params) { (response, error) in
            
            hideHud()
            
            if error == nil {
                if let result = response as? [String:Any] {
                    if let status = result["status"] as? String, (status == SUCCESS_STATUS) {
                        self.tf_ExpenseFor.text = ""
                        if self.tf_Expense.tag != 201 {
                            self.tf_Expense.text = ""
                        }
                        
                        if let expense = params["expense"] as? String {
                            if let amount = Double(expense) {
                                parentVC.totalExpense = amount + parentVC.totalExpense
                            }
                        }
                        parentVC.showHideBottomView()
                        showAlert("", message: "Expenses Added Successfully", onView: parentVC)
                        
                    } else {
                        showAlert("", message:INTERNAL_ERROR_MESSAGE, onView: parentVC)
                    }
                }
            } else {
                showAlert("", message:error?.localizedDescription ?? SOMETHING_WRONG, onView: parentVC)
            }
        }
    }
    
    fileprivate func moveBottomViewUp() {
        
        var yOffset:CGFloat = -495
        if UIDevice.current.userInterfaceIdiom == .pad {
            yOffset = -650
        }
        
        if let parentVC = self.parentViewController as? MapVC {
            UIView.animate(withDuration: 0.3, animations: {
                parentVC.bottom_View.transform = CGAffineTransform.init(translationX: 0, y:yOffset)
            })
        }
    }
    
    fileprivate func moveBottomViewDown() {
        
        var yOffset:CGFloat = -260
        if UIDevice.current.userInterfaceIdiom == .pad {
            yOffset = -350
        }
        
        if let parentVC = self.parentViewController as? MapVC {
            UIView.animate(withDuration: 0.3, animations: {
                parentVC.bottom_View.transform = CGAffineTransform.init(translationX: 0, y:yOffset)
            })
        }
    }
}

extension ExpensesCell:UITextFieldDelegate {
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        self.moveBottomViewUp()
        tf_selected = textField
        let picker = UIPickerView()
        picker.delegate = self
        picker.dataSource = self
        picker.tag = textField.tag
        if textField.tag == 301 {
            textField.inputView = picker
            if isBlank(textField.text ?? "") {
                textField.text = array_ExpenseType[0]
            }
        } else if textField.tag == 201 {
            textField.inputView = picker
            if isBlank(textField.text ?? "") {
                textField.text = array_TicketType[0]
            }
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        self.moveBottomViewDown()
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range:NSRange, replacementString string: String) -> Bool {
        
        let txt = textField.text ?? ""
        if txt.count == 0 && string == "." {
            return false
        }
        
        if textField.tag == 201 {
            return false
        } else {
            let text = textField.text ?? ""
            if string == "." || text.contains(find: ".") || string == "" {
                // Payment info is restricted upto two decimal
                return allowUptoTwoDecimalPlace(textField.text ?? "", forRange: range, replacementString: string)
            } else {
                if textField.tag == 101 {
                    return text.count < 4 ? true : false
                }
                return text.count < 3 ? true : false
            }
        }
    }
}

extension ExpensesCell : UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if pickerView.tag == 301 {
            return array_ExpenseType.count
        } else {
            return array_TicketType.count
        }
    }
}


extension ExpensesCell : UIPickerViewDelegate {
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if pickerView.tag == 301 {
            tf_selected?.text = array_ExpenseType[row]
        } else {
            tf_selected?.text = array_TicketType[row]
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if pickerView.tag == 301 {
            return array_ExpenseType[row]
        } else {
            return array_TicketType[row]
        }
    }
}
