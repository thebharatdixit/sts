//
//  AllCategoryCell.swift
//  SmartTrucking
//
//  Created by Umesh Sharma on 15/09/17.
//  Copyright © 2017 Vivan Raghuvanshi. All rights reserved.
//
// 1-

import UIKit
import CoreLocation

class AllCategoryCell: UICollectionViewCell {
    
    @IBOutlet weak var collection_View:UICollectionView!
    
    fileprivate let allCategories = ["Gas Station",
                                     "Truck Stops",
                                     "Weigh Station",
                                     "Overview",
                                     "Detail",
                                     "Audio"]
    
    var array_SelectedCategory = ["Gas Station",
                                  "Truck Stops",
                                  "Weigh Station"] {
        didSet {
            // Save the categories
            UserDefaults.standard.set(array_SelectedCategory, forKey: SAVED_CATEGORY)
        }
    }
    
    var array_SelectedSubCategory = ["Gas Station", "TA/Petro", "Love's", "Ambest", "Pilot", "Flying J", "Rest Area", "Walmarts", "Weigh Station"]
    
    override func awakeFromNib() {
       
        super.awakeFromNib()
        let categoryCellNib =  UINib(nibName: "CategoryCell", bundle: Bundle.main)
        collection_View.register(categoryCellNib, forCellWithReuseIdentifier: "CategoryCell")
        if let categories = UserDefaults.standard.value(forKey: SAVED_CATEGORY) {

            if let savedCategories = categories as? [String] {
                self.array_SelectedCategory = savedCategories
                if (!self.array_SelectedCategory.contains("Gas Station")) {
                    array_SelectedSubCategory.removeFirst()
                }
                if (!self.array_SelectedCategory.contains("Weigh Station")) {
                    array_SelectedSubCategory.removeLast()
                }
                if (!self.array_SelectedCategory.contains("Truck Stops")) {
                    array_SelectedSubCategory.removeAll()
                    array_SelectedSubCategory.append("Gas Station")
                    array_SelectedSubCategory.append("Weigh Station")
                }
            }
        } else {
            UserDefaults.standard.set(array_SelectedCategory, forKey: SAVED_CATEGORY)
        }
    }
    
    fileprivate func checkWetherCategoryExist(_ category:String, inCell cell:CategoryCell) {

        if array_SelectedCategory.count > 0  {
            let existCategory = array_SelectedCategory.filter({ categoryInfo -> Bool in
                category == categoryInfo
            })
            if existCategory.count > 0 {
                cell.view_Bg.backgroundColor = blue1
            } else {
                if (category == "Detail") {
                    cell.view_Bg.backgroundColor = blue1
                } else {
                    cell.view_Bg.backgroundColor = UIColor.lightGray
                }
            }
        }
    }
    
    fileprivate func setTextAndImageOnCell(_ cell:CategoryCell, forText text:String) {
        
        let img = cell.contentView.viewWithTag(100) as! UIImageView
        let lbl = cell.contentView.viewWithTag(101) as! UILabel
        
        switch text {
        case "Gas Station":
            img.image = #imageLiteral(resourceName: "gas-station")
            break
        case "Truck Stops":
            img.image = #imageLiteral(resourceName: "parking")
            break
        case "Weigh Station":
            img.image = #imageLiteral(resourceName: "weight-station")
            break
        case "Overview":
            img.image = #imageLiteral(resourceName: "polyline-100")
            break
        case "Detail":
            img.image = #imageLiteral(resourceName: "view-details")
            break
        default:
            break;
        }
        
        lbl.text = text
        self.checkWetherCategoryExist(text, inCell: cell)
    }
    
    fileprivate func getNearby(_ text:String) {
        if let parentVC = self.parentViewController as? MapVC {
    
        }
    }
    
    fileprivate func getCurrentLocation(_ parentVC:MapVC) -> CLLocation? {
        var location:CLLocation?
        
//        if let currentLocation = parentVC.position_Manager?.currentPosition {
//            location = CLLocation.init(latitude: currentLocation.coordinates.latitude, longitude: currentLocation.coordinates.longitude)
//        } else {
//            location = CLLocation.init(latitude: parentVC.sourcePlace!.coordinate.latitude, longitude: parentVC.sourcePlace!.coordinate.longitude)
//        }
        return location
    }
}

//MARK:- ------------- UICollectionViewDataSource Method -----------------

extension AllCategoryCell:UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return allCategories.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        var cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CategoryCell", for: indexPath) as? CategoryCell
        
        if cell == nil {
            let nibs = Bundle.main.loadNibNamed("CategoryCell", owner: self, options: nil)
            cell = nibs?.first as? CategoryCell
        }
//        if indexPath.row == 5 {
//            if let parentVC = self.parentViewController as? MapVC {
//                if let nav_Manager = parentVC.navigation_Manager {
//                    let img = cell?.contentView.viewWithTag(100) as! UIImageView
//                    let lbl = cell?.contentView.viewWithTag(101) as! UILabel
//
//                        img.image = nav_Manager.isVoiceEnabled ?#imageLiteral(resourceName: "audio"):#imageLiteral(resourceName: "no-audio")
//                        lbl.text = "Audio"
//                        cell?.view_Bg.backgroundColor = nav_Manager.isVoiceEnabled ? blue1:UIColor.lightGray
//                }
//            }
//        } else {
//            self.setTextAndImageOnCell(cell!, forText: allCategories[indexPath.row])
//            if indexPath.row == 3 {
//                if let parentVC = self.parentViewController as? MapVC {
//                    if let nav_Manager = parentVC.navigation_Manager {
//                        if nav_Manager.navigationState == .running {
//                            cell?.view_Bg.backgroundColor = blue1
//                        }
//                    }
//                }
//            }
//        }
        return cell!
    }
}

//MARK:- ------------- UICollectionViewDelegate Method -----------------

extension AllCategoryCell:UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if let cell = collectionView.cellForItem(at: indexPath) as? CategoryCell {
            if let parentVC = self.parentViewController as? MapVC {
//                if let nav_Manager = parentVC.navigation_Manager {
//                    let img = cell.contentView.viewWithTag(100) as! UIImageView
//                    switch indexPath.row {
//                    case 0:
//                        
//                        if self.array_SelectedCategory.contains("Gas Station") {
//                            if parentVC.gasStations.count > 0 ||  cell.view_Bg.backgroundColor == blue1 {
//                                if parentVC.gasStations.count > 0 {
//                                    parentVC.map_View.removeMapObjects(parentVC.gasStations)
//                                    parentVC.gasStations.removeAll()
//                                }
//                                cell.view_Bg.backgroundColor = UIColor.lightGray
//                            } else {
//                                self.getNearby("Gas Station")
//                                cell.view_Bg.backgroundColor = blue1
//                            }
//                        }
//                        break
//                    case 1:
//                        if self.array_SelectedCategory.contains("Truck Stops") {
//                            if parentVC.truckStops.count > 0 ||  cell.view_Bg.backgroundColor == blue1 {
//                                if parentVC.truckStops.count > 0 {
//                                parentVC.map_View.removeMapObjects(parentVC.truckStops)
//                                    parentVC.truckStops.removeAll()
//                                }
//                                cell.view_Bg.backgroundColor = UIColor.lightGray
//                            } else {
//                                if array_SelectedSubCategory.count > 0 {
//                                    for value in array_SelectedSubCategory {
//                                        if (value != "Gas Station" || value != "Weigh Station") {
//                                            self.getNearby(value)
//                                            cell.view_Bg.backgroundColor = blue1
//                                        }
//                                    }
//                                }
//                            }
//                        }
//                        break
//                    case 2:
//                        if self.array_SelectedCategory.contains("Weigh Station") {
//                            if parentVC.weighStations.count > 0 ||  cell.view_Bg.backgroundColor == blue1 {
//                               
//                                if parentVC.weighStations.count > 0 {
//                                parentVC.map_View.removeMapObjects(parentVC.weighStations)
//                                    parentVC.weighStations.removeAll()
//                                }
//                                cell.view_Bg.backgroundColor = UIColor.lightGray
//                            } else {
//                                self.getNearby("Weigh Station")
//                                cell.view_Bg.backgroundColor = blue1
//                            }
//                        }
//                        break
//                    case 3:
//                        if nav_Manager.navigationState == .running {
//                            cell.view_Bg.backgroundColor = blue1
//                            nav_Manager.stop()
//                            parentVC.view_.isHidden = true;
//                            parentVC.map_View.setBoundingBox(parentVC.mapRoute?.route.boundingBox, with: .linear)
//                            
//                        } else {
//                            cell.view_Bg.backgroundColor = UIColor.lightGray
////                            if let myRoute = parentVC.mapRoute?.route {
////                              parentVC.manageNavigation(myRoute)
////                            }
//                        }
//                        
//                       // cell.view_Bg.backgroundColor = cell.view_Bg.backgroundColor == blue1 ? UIColor.lightGray:blue1
//                        break
//                    case 4:
//                        if let allManeuvers = parentVC.mapRoute?.route.maneuvers as? [NMAManeuver] {
//                            let navigationRouteListVC = parentVC.storyboard?.instantiateViewController(withIdentifier: "NavigationRouteListVC") as! NavigationRouteListVC
//                            navigationRouteListVC.routeLinks = allManeuvers
//                            parentVC.present(navigationRouteListVC, animated: true, completion: nil)
//                         //   cell.view_Bg.backgroundColor = cell.view_Bg.backgroundColor == blue1 ? UIColor.lightGray:blue1
//                        }
//                        break
//                    case 5:
//                        // To Enable/Disbale Audio Instruction
//                        img.image = img.image == #imageLiteral(resourceName: "audio") ? #imageLiteral(resourceName: "no-audio"):#imageLiteral(resourceName: "audio")
//                        nav_Manager.isVoiceEnabled = !nav_Manager.isVoiceEnabled
//                        cell.view_Bg.backgroundColor = cell.view_Bg.backgroundColor == blue1 ? UIColor.lightGray:blue1
//                        break
//                    default:
//                        break
//                    }
 //               }
            }
        }
    }
}

//MARK:- ------------- UICollectionViewDelegateFlowLayout Method -----------------

extension AllCategoryCell:UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let cellSize = CGSize.init(width: collectionView.bounds.width/3, height: collectionView.bounds.height/2)
        return cellSize
    }
}

//MARK:- ------------- CategorySearchDelegate Method -----------------

extension AllCategoryCell:CategorySearchDelegate {
    
    func choosedCategory(_ selectedCategories:[String], andSubCategories subCategories:[String]) {

        array_SelectedCategory = selectedCategories
        array_SelectedSubCategory = subCategories
        collection_View.reloadData()
        
        if let parentVC = self.parentViewController as? MapVC {
            if let location = self.getCurrentLocation(parentVC) {
                parentVC.refreshAllNearByPlacesForLocation(location)
            }
        }
    }
    
}

