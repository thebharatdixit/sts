//
//  MapVC.swift
//  SmartTrucking
//
//  Created by Vivan Raghuvanshi on 02/09/17.
//  Copyright © 2017 Vivan Raghuvanshi. All rights reserved.
//


import UIKit
import SwiftyJSON
import Alamofire
import MFSideMenu
import IQKeyboardManagerSwift
import GoogleMaps
import GooglePlaces
import CoreLocation
//import EncodedHelper

class MapVC: UIViewController, GMSMapViewDelegate {
    
    // MARK:- --------------Outlets/Variables-------------------
    
    @IBOutlet weak var map_View: GMSMapView!
    @IBOutlet weak var bottom_View: UIView!
    @IBOutlet weak var collection_View: UICollectionView!
    @IBOutlet weak var page_Control: UIPageControl!
    
    @IBOutlet weak var lbl_Distance:UILabel!
    @IBOutlet weak var lbl_EstimatedTime:UILabel!
    @IBOutlet weak var lbl_TotalExpense:UILabel!
    @IBOutlet weak var lbl_TripName:UILabel!
    
    @IBOutlet weak var lbl_Toast:UILabel!
    
    @IBOutlet weak var img_Direction:UIImageView!
    @IBOutlet weak var lbl_ManueverName:UILabel!
    @IBOutlet weak var lbl_TurnDirection:UILabel!
    @IBOutlet weak var lbl_ManuevrDistance:UILabel!
    @IBOutlet weak var view_:UIView!
    @IBOutlet weak var img_Cancel:UIImageView!
    
    
    var pendingTrip:Trip?
    
    var firstCell:AllCategoryCell?
    var sourcePlace:Stop?
    var destinationPlace:Stop?
    var waypointsPlace:[Stop]?
    var drawRouteCalled = false
    var keyboardShowing = false
    var waypointsArray: Array<String> = []
    var isFromNewVC = false
    var tollApi = 0
    var isAllPOILoaded = false
//    var places = []
    var totalExpense = 0.0 {
        didSet {
            lbl_TotalExpense.text = "$\(totalExpense.rounded(toPlaces: 2))"
        }
    }
    
    var tripId:String? = "88"
    var tripName:String?
    var sourceCoordinates = CLLocationCoordinate2D()
    var destinationCoordinates = CLLocationCoordinate2D()
    //Before EPG implementation currentPosition
    //Truck Stops
    //Is end trip pop up showing
    var endPopUpShowing = false
    let networkManager = NetworkManager()
    // MARK:- -------------- View Life Cycle Methods -------------------
    var locationManager = CLLocationManager()
    var pointOfInterests = [POI]()
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Memory for the router
        // Hide Default Navigation Bar
        self.navigationController?.isNavigationBarHidden = true
        // Save The Reference Of Map
        APPDELEGATE.mapVCRef = self;
        // Set trip name
        lbl_TripName.text = tripName ?? "Trip Name"
        // Set number of pages in tray
        page_Control.numberOfPages = 3
        map_View.delegate = self
        let expensesCellNib =  UINib(nibName: "ExpensesCell", bundle: Bundle.main)
        collection_View.register(expensesCellNib, forCellWithReuseIdentifier: "ExpensesCell")
        let ticketExpensesNib =  UINib(nibName: "TicketExpensesCell", bundle: Bundle.main)
        collection_View.register(ticketExpensesNib, forCellWithReuseIdentifier: "TicketExpensesCell")
        let fuelExpensesNib =  UINib(nibName: "FuelExpensesCell", bundle: Bundle.main)
        collection_View.register(fuelExpensesNib, forCellWithReuseIdentifier: "FuelExpensesCell")
        loadInitialMarkers()
        if let trip = self.pendingTrip {
            self.lbl_Distance.text = "\(trip.tripMiles)"
            self.lbl_TotalExpense.text = "$\(trip.tripSpent)"
        }
    }
    
    @IBAction func userLocationClicked(_ sender: UIButton) {
       askForCurrentLocation()

    }
    
    func loadInitialMarkers() {
        guard let sourceLatitude = sourcePlace?.stopLatitude, let sourceLongitude = sourcePlace?.stopLongitude else { return }
        guard let sourceLat = Double(sourceLatitude), let sourceLong =  Double(sourceLongitude) else { return }
        sourceCoordinates = CLLocationCoordinate2D(latitude: sourceLat, longitude: sourceLong)
        
        let sourceMarker = GMSMarker(position: sourceCoordinates)
        sourceMarker.title = sourcePlace?.stopAddress
        sourceMarker.icon = #imageLiteral(resourceName: "startpoint")
        sourceMarker.snippet = tripId
        sourceMarker.map = map_View
        
        guard let destinationLatitude = destinationPlace?.stopLatitude,  let destinationLongitude = destinationPlace?.stopLongitude else { return }
        guard let destLat = Double(destinationLatitude), let destLong =  Double(destinationLongitude) else { return }
        destinationCoordinates = CLLocationCoordinate2D(latitude: destLat, longitude: destLong)
        
        let destinationMarker = GMSMarker(position: destinationCoordinates)
        destinationMarker.title = destinationPlace?.stopAddress
        destinationMarker.icon = #imageLiteral(resourceName: "endpoint")
        destinationMarker.snippet = tripId
        destinationMarker.map = map_View
        map_View.isMyLocationEnabled = true
        map_View.settings.myLocationButton = true
        view_.isHidden = true
        let bounds = GMSCoordinateBounds(coordinate: sourceCoordinates, coordinate: destinationCoordinates)
        let camera = map_View.camera(for: bounds, insets: UIEdgeInsets(top: 70.0 , left: 50.0 ,bottom: 120.0 ,right: 50.0))!
        map_View.camera = camera
        map_View.drawPolygon(from: "\(sourceLat),\(sourceLong)", to: "\(destLat),\(destLong)", wayPoints: waypointsPlace!, completionHandler: { [unowned self] results in
            DispatchQueue.main.async {
                self.lbl_EstimatedTime.text = "\(results["hour"] as! String)"
                self.lbl_Distance.text = results["distance"] as! String
                self.isAllPOILoaded = results["isCompleted"] as! Bool
                if let miles = results["distance"] as? String {
                    self.sendTripInformationToServer(Double(miles)!)
                }
                var poi = results["poi"] as? [POI]
                self.pointOfInterests = poi ?? []
            }
        })
        
        if let wayPoints = waypointsPlace {
                for stop in wayPoints {
                    let stopCoordinates = CLLocationCoordinate2D(latitude: Double(stop.stopLatitude)!, longitude: Double(stop.stopLongitude)!)
                    let stopAddressMarker = GMSMarker(position: stopCoordinates)
                    stopAddressMarker.icon = #imageLiteral(resourceName: "current_location")
                    stopAddressMarker.title = stop.stopAddress
                    stopAddressMarker.snippet = stop.stopId
                    stopAddressMarker.map = map_View
                    let positionString = "\(stop.stopLatitude),\(stop.stopLongitude)"
                    waypointsArray.append(positionString)
                }
        }
        
        var wayStopsArray = [String]()
        
        if let waypoints = waypointsPlace {
            if  waypoints.count > 1 {
                wayStopsArray.append("\(destinationLatitude),\(destinationLongitude)")
                for i in 0..<waypoints.count {
                    if i != waypoints.count-1 {
                        wayStopsArray.append("\(waypoints[i].stopLatitude),\(waypoints[i].stopLongitude)")
                    }
                }
                if user?.userTitle == "Owner/Operator" {
                calculateTollCostGoogle(sourceCoordinates: sourceCoordinates, destinationCoordinates: CLLocationCoordinate2D.init(latitude: Double(waypoints[waypoints.count-1].stopLatitude)!, longitude: Double(waypoints[waypoints.count-1].stopLongitude)!), isHasWaypoint: true, waypoints: wayStopsArray)
                }
            } else if (waypoints.count == 1) && (user?.userTitle == "Owner/Operator") {
                calculateTollCostGoogle(sourceCoordinates: sourceCoordinates, destinationCoordinates: CLLocationCoordinate2D.init(latitude: Double(waypoints[waypoints.count-1].stopLatitude)!, longitude: Double(waypoints[waypoints.count-1].stopLongitude)!), isHasWaypoint: true, waypoints: ["\(destinationCoordinates.latitude),\(destinationCoordinates.longitude)"])
            }else if user?.userTitle == "Owner/Operator" {
                calculateTollCostGoogle(sourceCoordinates: sourceCoordinates, destinationCoordinates: destinationCoordinates, isHasWaypoint: false)
            }
            
        }else if user?.userTitle == "Owner/Operator" {
            calculateTollCostGoogle(sourceCoordinates: sourceCoordinates, destinationCoordinates: destinationCoordinates, isHasWaypoint: false)
        }

    }
    
    
    func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool {
    
        let placeId = marker.snippet!
        let result = pointOfInterests.filter { $0.placeId == Int(placeId) }
        if let place = result.first {
            let mainView = UIImageView.init(frame: self.view.bounds);
            let blurEffect = UIBlurEffect.init(style: .light);
            let effectView = UIVisualEffectView.init(effect: blurEffect);
            effectView.frame = mainView.bounds;
            effectView.alpha = 1;
            mainView.addSubview(effectView);
            mainView.tag = 100
            self.view.addSubview(mainView);
            let placeDetailsVc = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "PlaceDetailsVC") as! PlaceDetailsVC

            placeDetailsVc.placeDetail = place
            
            DispatchQueue.main.async {
                
                self.addChild(placeDetailsVc)
                
                placeDetailsVc.view.frame = self.view.frame
                
                self.view.addSubview(placeDetailsVc.view)
                
                placeDetailsVc.didMove(toParent: self)
                
            }
        }
       
        return true
    }
    
    @objc func keyboardWillShow(_ notification: NSNotification) {
        
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            keyboardShowing = true
            let yOffset = keyboardSize.height + 350.0
            print(yOffset)
            UIView.animate(withDuration: 0.5, animations: {
                self.bottom_View.transform = CGAffineTransform.init(translationX: 0, y: -CGFloat(yOffset))
            })
        }
    }
    
    @objc func keyboardWillHide(_ notification: NSNotification) {
        
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            keyboardShowing = false
        }
    }
    
    // MARK:- --------------Button Action Methods-------------------
    
    @IBAction func buttonClicked_Back(_ sender:UIButton) {
        sender.animateView()
        self.navigationController?.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func buttonClicked_LeftMenu(_ sender:UIButton) {
        sender.animateView()
        self.menuContainerViewController.toggleLeftSideMenuCompletion {
            
        }
    }
    
    
    
    // Show share options
    @IBAction func buttonClicked_Share(_ sender:UIButton) {
        sender.animateView()
        showActivityViewToShare(self)
    }
    
    // End button tapping on bottom view
    @IBAction func buttonClicked_End(_ sender:UIButton) {
        sender.animateView()
        if endPopUpShowing == false {
            self.showAlertToEndTheTrip()
        }
    }
    
    // Bottom view up/down
    @IBAction func buttonClicked_ShowExpenses(_ sender:UIButton) {
        sender.animateView()
        if keyboardShowing == true { return }
        self.showHideBottomView()
    }
    
    @IBAction func buttonClicked_SearchCategory() {
        let categorySearchVC = self.storyboard?.instantiateViewController(withIdentifier: "CategorySearchVC") as! CategorySearchVC
        if firstCell != nil {
            categorySearchVC.delegate = firstCell!;
            categorySearchVC.selectedOptions = firstCell!.array_SelectedSubCategory
            
        }
        self.navigationController?.pushViewController(categorySearchVC, animated: true)
    }
    
    @IBAction func buttonClicked_Home(_ sender:UIButton) {
        sender.animateView()
        img_Cancel.animateView()
        APPDELEGATE.mfContainer?.centerViewController = APPDELEGATE.navigationVC
        APPDELEGATE.mfContainer?.setMenuState(MFSideMenuStateClosed, completion: nil)
    }
    
    @IBAction func buttonClicked_Navigation(_ sender:UIButton) {
        sender.animateView()
    }
    
    // MARK:- ---------------- Private Methods -----------------
    
    // Show/Hide Tray
    func showHideBottomView() {
        self.view.endEditing(true)
        if bottom_View.transform == .identity {
            var yOffset:CGFloat = -260
            if UIDevice.current.userInterfaceIdiom == .pad {
                yOffset = -350
            }
            UIView.animate(withDuration: 0.5, animations: {
                self.bottom_View.transform = CGAffineTransform.init(translationX: 0, y: yOffset)
            })
        } else {
            UIView.animate(withDuration: 0.5, animations: {
                self.bottom_View.transform = .identity
            })
        }
    }
    
    // Send trip information to server
    private func sendTripInformationToServer(_ distance:Double) {
        
        if let trip_Id = tripId {
            let params = ["tripId":trip_Id, "miles":distance] as [String : Any]
            networkManager.getDataForRequest("distances", andParameter: params) { (response, error) in
                
                if error == nil {
                    if let result = response as? [String:Any] {
                        if let status = result["status"] as? String, (status == SUCCESS_STATUS) {
                            self.startIndicator(self, forFrame: nil, withCenter: self.lbl_TotalExpense.center)
                            
                        } else {
                            // self.sendTripInformationToServer(distance)
                        }
                    }
                } else {
                    // self.sendTripInformationToServer(distance)
                }
            }
        }
        
        
    }
    
//    func locationManager(manager: CLLocationManager!, didUpdateLocations locations: [AnyObject]!) {
//        if let location = locations.first as? CLLocation {
//            let location = GMSCameraPosition.camera(withLatitude: location.coordinate.latitude, longitude: location.coordinate.longitude, zoom: 14.0)
//
//            map_View.animate(to: location)
//            locationManager.stopUpdatingLocation()
//        }
//    }
    
    private func sendTollCostingToServer(_ cost:String) {
        if let trip_Id = tripId {
            let params = ["tripId":trip_Id,
                          "isCreate":"true",
                          "expense":cost,
                          "expenseType":"TOLL",
                          "expenseTitle":"Toll Expenses"] as [String:Any]
            let networkManager = NetworkManager()
            networkManager.getDataForRequest(addExpenses, andParameter: params) { (response, error) in
                if error == nil {
                    //self.refreshDashboard()
                }
            }
        }
    }
    
    private func calculateTollCostGoogle(sourceCoordinates: CLLocationCoordinate2D, destinationCoordinates: CLLocationCoordinate2D, isHasWaypoint: Bool, waypoints: [String] = []) {
        var truckDetail = [String: Any]()
        if let truckInfo = user?.userTruckInfo {
            truckDetail = truckInfo
        }else {
            return
        }
        let truckInfo = TruckInfo.init(object: truckDetail)
        var vehicleAxle = 0
        var trailerAxle = 0
        if let truckAxle = truckInfo.vehicleAxlesCount {
            vehicleAxle = truckAxle
        }
        if let axle = truckInfo.trailerAxlesCount {
            trailerAxle = axle
        }
        if let type = truckInfo.vehicleWeight {
            
        }
        var vehicle = ["trailer_has_dual_tires": true,
                       "is_franchise": false,
                       "number_of_occupants": (truckInfo.passengersCount)!,
                       "has_trailer": true,
                       "width": 102.0,
                       "is_clean_air": true,
                       "is_commercial": true,
                       "is_special_load": false,
                       "trailer_number_of_axles": trailerAxle,
                       "height": truckInfo.height ?? 0,
                       "number_of_axles_without_trailer": vehicleAxle,
                       "has_dual_tires":true,
                       "is_emergency_vehicle": false,
                       "weight":truckInfo.vehicleWeight ?? "",
                       "total_number_of_axles": vehicleAxle + trailerAxle,
                       "vehicle_type": (truckInfo.vehicleType)!,
                       "length":  636.0] as [String: Any]
        var params = ["use_express_lanes":"true",
                      "origin": "\(sourceCoordinates.latitude), \(sourceCoordinates.longitude)",
            "destination": "\(destinationCoordinates.latitude), \(destinationCoordinates.longitude)",
            "vehicle": vehicle,
            "key": "9254f78c53a1636ddd4cc75e34ba03e7",
            "waypoints": waypoints,
            "map_provider": "google",
            "departure_time":"60"] as! [String: Any]
        showHud("Calculating Toll Cost")
        let networkManager = NetworkManager()
        networkManager.getTollDataForRequest("http://tollsmart-cv.elasticbeanstalk.com/TollsAPI/tolls", andParameter: params, isHasWaypoint: isHasWaypoint, completionHandler: { result in
            hideHud()
            DispatchQueue.main.async {
                let cost = String(format: "%.1f", Double(result)!)
                self.totalExpense = Double(cost)!
                if self.isFromNewVC {
                    self.sendTollCostingToServer("\(result)")
                }
            }
        })
    }
    
    func showVehicleDetailsAlert() {
        let alertController = UIAlertController(title: "Vehicle Details", message: "We need vehicle details to calculate toll cost. Do you want to provide?", preferredStyle: .alert)
        
        // Create the actions
        let okAction = UIAlertAction(title: "Yes, I Will", style: UIAlertAction.Style.destructive) {
            UIAlertAction in
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "TruckDetailVC") as! TruckDetailVC
            vc.delegate = self
            self.navigationController?.pushViewController(vc, animated: true)
        }
        
        let cancelAction = UIAlertAction(title: "No, I'm good", style: UIAlertAction.Style.cancel) {
            UIAlertAction in
        }
        
        // Add the actions
        alertController.addAction(okAction)
        alertController.addAction(cancelAction)
        
        // Present the controller
        self.present(alertController, animated: true, completion: nil)
    }
    
    @objc private func askForCurrentLocation() {
        LocationManager.sharedLocation.startUpdatingCurrentLocation { (latitude, longitude, error) in
            if error == nil {
                if let lat = latitude {
                    if let lng = longitude {
                        let camera = GMSCameraPosition.camera(withLatitude: lat, longitude: lng, zoom: 10)
                        self.map_View?.camera = camera
                        self.map_View?.animate(to: camera)
                        
                    }
                }
            }
        }
    }

    // Method to load tray
    fileprivate func loadCellForIdentifier(_ cellId:String, forIndexPath indexPath:IndexPath) -> UICollectionViewCell {
        var expensesCell = collection_View.dequeueReusableCell(withReuseIdentifier:cellId, for: indexPath) as? ExpensesCell
        if expensesCell == nil {
            let nibs = Bundle.main.loadNibNamed(cellId, owner: self, options: nil)
            expensesCell = nibs?.first as? ExpensesCell
        }
        return expensesCell!
    }
    
    func refreshAllNearByPlacesForLocation(_ location:CLLocation) {
        DispatchQueue.global().async {
            if let cell = self.firstCell {
                
            }
        }
    }
    
    // When user reach the destination
    // Show automatic alert to end trip by calling this method
    fileprivate func showAlertToEndTheTrip() {
        if bottom_View.transform != .identity {
            self.showHideBottomView()
        }
        endPopUpShowing = true
        
        let nibs = Bundle.main.loadNibNamed("EndTripView", owner: self, options: nil)
        let endTripView = nibs?.last as? EndTripView
        endTripView?.showPopUp(self)
        
        //let endAction = UIAlertAction.init(title: "End", style: .destructive) { (alert) in
        //    self.endActionTapped()
        //}
        //showAlert("Would you like to end the trip", message:"", withAction:endAction, with: true, andTitle: "Cancel", onView: self)
    }
    
    // Action method to tap on end button of alert pop up
    private func endActionTapped() {
        view_.isHidden = true
        
        if let existingTripId = self.tripId {
            let params = ["tripId":existingTripId]
            showHud("Processing..")
            let networkManager = NetworkManager()
            networkManager.getDataForRequest(closeTrip, andParameter: params) { (response, error) in
                
                hideHud()
                
                if error == nil {
                    if let result = response as? [String:Any] {
                        if let status = result["status"] as? String, (status == SUCCESS_STATUS) {
                            if let mynavigationVC = APPDELEGATE.navigationVC {
                                self.refreshDashboard()
                                if let homeVC = mynavigationVC.viewControllers.first as? HomeVC {
                                    homeVC.endIfOnGoingTrip()
                                }
                                APPDELEGATE.mfContainer?.centerViewController = mynavigationVC
                                SmartTrucking.showAlert("", message: "Trip is successfully completed", onView: (mynavigationVC))
                            }
                        } else {
                            SmartTrucking.showAlert("", message:INTERNAL_ERROR_MESSAGE, onView: self)
                        }
                    }
                } else {
                    SmartTrucking.showAlert("", message:error?.localizedDescription ?? SOMETHING_WRONG, onView: self)
                }
            }
        }
    }
    
    // MARK:- --------- Refresh Dashboard -------------
    
    func refreshDashboard() {
        
        if let mynavigationVC = APPDELEGATE.navigationVC {
            if let homeVC = mynavigationVC.viewControllers.first as? HomeVC {
                homeVC.getTripSummary()
            }
        }
    }
    
    // MARK:- --------- Notification Observers method -------------
    
    // Method to fetch current position update
    @objc func positionDidUpdate() {
    }
}

// MARK:- --------------UIScrollViewDelegate Methods-------------------
// set page control current page
extension MapVC:UIScrollViewDelegate {
    // Set page controls current page
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        var visibleRect = CGRect()
        visibleRect.origin = collection_View.contentOffset
        visibleRect.size = collection_View.bounds.size
        let visiblePoint = CGPoint(x: visibleRect.midX, y: visibleRect.midY)
        if let visibleIndexPath = collection_View.indexPathForItem(at: visiblePoint) {
            page_Control.currentPage = visibleIndexPath.row
        }
    }
}

// MARK:- ---------UICollectionViewDataSource Methods-------------
extension MapVC: UserProfileUpdateDelegate {
    func profileUpdated() {
        tollApi = 1
        getUserDetails()
    }
    
    fileprivate func getUserDetails() {
        
        let params = ["profileId":user!.userId!]
        
        // showHud("Processing..")
        
        let networkManager = NetworkManager()
        networkManager.getDataForRequest(getUserInfo, andParameter: params) { (response, error) in
            
            hideHud()
            if error == nil {
                
                if let result = response as? [String:Any] {
                    if let status = result["status"] as? String, (status == SUCCESS_STATUS) {
                        if let userDetail = result["_obj"] as? [String:Any] {
                            
                            var userInfo = userDetail
                            userInfo["generated_id"] = params["profileId"]
                            // Store the info in local database
                            
                            UserInfo.sharedInfo.setUserProfileWithInfo(userInfo)
                            self.viewDidLoad()
                        }
                    } else {
                        SmartTrucking.showAlert("", message:INTERNAL_ERROR_MESSAGE, onView: self)
                    }
                }
            } else {
                SmartTrucking.showAlert("", message:error?.localizedDescription ?? SOMETHING_WRONG, onView: self)
            }
        }
    }
}
extension MapVC:UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 3
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        //        if indexPath.row == 0 {
        //            // First tray
        //            var allCategoryCell = collectionView.dequeueReusableCell(withReuseIdentifier: "AllCategoryCell", for: indexPath) as? AllCategoryCell
        //            if allCategoryCell == nil {
        //                let nibs = Bundle.main.loadNibNamed("AllCategoryCell", owner: self, options: nil)
        //                allCategoryCell = nibs?.first as? AllCategoryCell
        //            }
        //            firstCell = allCategoryCell
        //            return allCategoryCell!
        //        } else
        if indexPath.row == 0 {
            // Second tray
            return self.loadCellForIdentifier("FuelExpensesCell", forIndexPath: indexPath)
            
        }  else if indexPath.row == 1 {
            // Third tray
            return self.loadCellForIdentifier("TicketExpensesCell", forIndexPath: indexPath)
        } else {
            // Fourth tray
            return self.loadCellForIdentifier("ExpensesCell", forIndexPath: indexPath)
        }
    }
}

// MARK:- --------UICollectionViewDelegate Methods-------------------

extension MapVC:UICollectionViewDelegate {
    // Clear the tray
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if cell is ExpensesCell {
            if let nextCell = cell as? ExpensesCell {
                if indexPath.row != 2 {
                    nextCell.tf_Expense.text = ""
                }
                nextCell.tf_ExpenseFor.text = ""
            }
        }
    }
}

extension MapVC:UICollectionViewDelegateFlowLayout {
    // Tray size
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let cellSize = CGSize.init(width: collectionView.bounds.width, height: collectionView.bounds.height)
        return cellSize
    }
    
}

extension Double {
    func roundedMinute(toPlaces places:Int) -> Double {
        let divisor = pow(10.0, Double(places))
        return (self * divisor).rounded() / divisor
    }
}
