//
//  MapManager.swift
//  SmartTrucking
//
//  Created by Prabhakar G on 23/04/18.
//  Copyright © 2018 Vivan Raghuvanshi. All rights reserved.
//

import GoogleMaps
import GooglePlaces
import CoreLocation

private struct MapPath : Decodable{
    var routes : [Routes]?
}

private struct Routes : Decodable{
    var overview_polyline : OverView?
}

private struct OverView : Decodable {
    var points : String?
}

extension GMSMapView {
    
    //MARK:- Call API for polygon points
    
    func drawPolygon(from source: String, to destination: String, wayPoints: [Stop], completionHandler: @escaping (_ getReponse: ([String: Any])) -> ()) {
        let networkManager = NetworkManager()
        var pointOfInterest = [POI]()
        let config = URLSessionConfiguration.default
        let session = URLSession(configuration: config)
        var wayPointsArray = ""
        for i in 0..<wayPoints.count {
            if i != wayPoints.count-1 {
                wayPointsArray += "|" + "\(wayPoints[i].stopLatitude),\(wayPoints[i].stopLongitude)"
            }
        }
        var urlstring = ""
        if wayPoints.count == 1 {
            urlstring = "https://maps.googleapis.com/maps/api/directions/json?origin=\(source)&destination=\(wayPoints[wayPoints.count-1].stopLatitude),\(wayPoints[wayPoints.count-1].stopLongitude)&waypoints=\(destination)|&mode=driving&key=AIzaSyAFKgGgbqilpQW5nWU8QZij5bgOH5VYOCE"
        }else if wayPointsArray.count > 0 {
              urlstring = "https://maps.googleapis.com/maps/api/directions/json?origin=\(source)&destination=\(wayPoints[wayPoints.count-1].stopLatitude),\(wayPoints[wayPoints.count-1].stopLongitude)&waypoints=\(destination)|\(wayPointsArray)|&mode=driving&key=AIzaSyAFKgGgbqilpQW5nWU8QZij5bgOH5VYOCE"
           
        }else {
             urlstring = "https://maps.googleapis.com/maps/api/directions/json?origin=\(source)&destination=\(destination)&key=AIzaSyAFKgGgbqilpQW5nWU8QZij5bgOH5VYOCE"
        }
        
        guard let escapedString = urlstring.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) else { return }
        let url = URL(string: escapedString)
        DispatchQueue.main.async {
            session.dataTask(with: url!) { (data, response, error) in
                guard data != nil else { return }
                
                do {
                    guard let result = try JSONSerialization.jsonObject(with: data!, options: .allowFragments) as? [String: Any],
                    let routes = result["routes"] as? [[String: Any]], let firstRoute = routes.first, let legs = firstRoute["legs"] as? [[String: Any]]  else { return }
                    var legDistance = 0
                    var legDuration = 0
                    for leg in legs {
                        if let distance = leg["distance"] as? [String: Any],
                            let distanceText = distance["value"] as? Int {
                           // if let result = distanceText) {
                                legDistance += distanceText
                           // }
                        }
                        
                        if let duration = leg["duration"] as? [String: Any],
                            let durationText = duration["value"] as? Int {
                           // if let result = Int(durationText) {
                                legDuration += durationText
                            //}
                        }
                    }
                    
                    var distanceDuration = [String: Any]()
                    let hoursValue = Double(legDuration) / 3600.0
                    let numberFormatter = NumberFormatter()
                    numberFormatter.minimumFractionDigits = 2
                    numberFormatter.maximumFractionDigits = 2
                    let hoursMin = self.formatTimeInSec(totalSeconds: legDuration)
                    let str = numberFormatter.string(for: hoursValue)
                    var hourArray = str?.components(separatedBy: ".")
                    _ = hourArray?.first ?? "0"
                    let minutes = hourArray?[1] ?? "0"
                    let distanceMeters = Measurement(value: Double(legDistance), unit: UnitLength.meters)
                    let miles = distanceMeters.converted(to: UnitLength.miles).value
                    let strDist = numberFormatter.string(for: miles)
                    distanceDuration = ["distance": strDist ?? "0",
                                            "hour": "\(hoursMin)",
                                            "min": "\(minutes)",
                                            "isCompleted": true] as [String : Any]

                    let route = try JSONDecoder().decode(MapPath.self, from: data!)

                    if let points = route.routes?.first?.overview_polyline?.points {
                        self.drawPath(with: points)
                        let geoCoordinates: NSMutableArray = EncodedHelper.decodePolyLine(NSMutableString(string: points))
                        //var places = Set<Repo>()
                        for coordinate in 0..<geoCoordinates.count {
                            let coo = coordinate % 2
                             if let geoCoord = geoCoordinates[coordinate] as? CLLocation, coo == 0 {
                                let postParams = "places?lat=\(geoCoord.coordinate.latitude)&lon=\(geoCoord.coordinate.longitude)&radius=10"
                                
//                                networkManager.getApiCall(urlString: postParams) { result as [ReponseObj] in
//                                    print(result)
//                                }
                                networkManager.getApiCall(urlString: postParams) { [unowned self] (result) in
                                    DispatchQueue.main.async {
                                        for poi in result._obj {
                                            pointOfInterest.append(poi)
                                           // places.append(poi)
                                            let markerCoord = CLLocationCoordinate2D(latitude: poi.latitude, longitude: poi.longitude)
                                            let poiMarker = GMSMarker(position: markerCoord)
                                            poiMarker.title = poi.placeName
                                            poiMarker.snippet = "\(poi.placeId)"
                                            
                                            poiMarker.icon = UIImage(named: poi.category)
                                            //self.imageWithImage(image: #imageLiteral(resourceName: "loves_marker"), scaledToSize: CGSize(width: 30.0, height: 30.0))
                                            poiMarker.map = self
                                            print(coordinate)
                                            
                                        }
                                        if coordinate <= geoCoordinates.count - 2 {
                                            distanceDuration["poi"] = pointOfInterest
                                            completionHandler(distanceDuration)
                                        }
                                    }
                                    
                                }
//                                self.getPointOfIntrest("\(geoCoord.coordinate.latitude)", longitude: "\(geoCoord.coordinate.longitude)" , withCompletion: {
//                                    result in
//
//                                })
//
//                                self.getLovesCenter("\(geoCoord.coordinate.latitude)", longitude: "\(geoCoord.coordinate.longitude)" , withCompletion: {
//                                    result in
//
//                                })
//
//                                self.getSearchRestArea("\(geoCoord.coordinate.latitude)", longitude: "\(geoCoord.coordinate.longitude)", searchText: "RestArea", markerName: #imageLiteral(resourceName: "restarea_pin64x64"), withCompletion: {
//                                    result in
//
//                                })
//
//                                self.getSearchPetro("\(geoCoord.coordinate.latitude)", longitude: "\(geoCoord.coordinate.longitude)" , searchText: "Petro", markerName: #imageLiteral(resourceName: "TA_pin64x64"), withCompletion: {
//                                    result in
//
//                                })
//
//                                self.getSearchTA("\(geoCoord.coordinate.latitude)", longitude: "\(geoCoord.coordinate.longitude)" , searchText: "TravelCenters of America", markerName: #imageLiteral(resourceName: "TA_pin64x64"), withCompletion: {
//                                    result in
//
//                                })
//
//                                self.getSearchFlyingJ("\(geoCoord.coordinate.latitude)", longitude: "\(geoCoord.coordinate.longitude)" , searchText: "", markerName: #imageLiteral(resourceName: "flyingj_pin64x64"), withCompletion: {
//                                    result in
//
//                                })
//
//                                self.getSearchPilotTravelCenter("\(geoCoord.coordinate.latitude)", longitude: "\(geoCoord.coordinate.longitude)", searchText: "PilotTravelCenter", markerName: #imageLiteral(resourceName: "GasPump_pin64x64"), withCompletion: {
//                                    result in
//
//                                })
//
//                                self.getWeighStation("\(geoCoord.coordinate.latitude)", longitude: "\(geoCoord.coordinate.longitude)", searchText: "weigh station", markerName: #imageLiteral(resourceName: "weigh_stations_pin64x64"), withCompletion: {
//                                    result in
//
//                                })
                                
//                                if coordinate == geoCoordinates.count - 1 {
//                                    completionHandler(distanceDuration)
//                                }
                            }
                        }
                    }
                    
                } catch let error {
                    print("Failed to draw ",error.localizedDescription)
                }
                }.resume()
        }
        
        
    }
    
    func getPointOfIntrest(_ latitude: String, longitude: String , withCompletion getResponse: @escaping ([[String: Any]]) -> ()) {
        //let startLocation = step["end_location"] as! [String: Any]
        
        let poiUrl = "https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=\(latitude),\(longitude)&radius=3200&type=supermarket&keyword=wallmart&key=AIzaSyDSV9ztBznRj_x509SonIuTjy5c1idLSaU"
        let urlSession = URLSession.shared
        let url = URL(string: poiUrl)

        let result = urlSession.dataTask(with: url!, completionHandler: { (data, response, error) in
            do {
                guard let dataResponse = data else { return }
                let json = try JSONSerialization.jsonObject(with: dataResponse, options: .allowFragments) as! [String: Any]
                let results = json["results"] as! [[String: Any]]
                
                DispatchQueue.main.async {
                    for poi in results {
                        let geometry = poi["geometry"] as! [String: Any]
                        let name = poi["name"] as! String
                        let location = geometry["location"] as! [String: Any]
                        let lat = location["lat"] as! Double
                        let long = location["lng"] as! Double
                        let poiCoordinates = CLLocationCoordinate2D(latitude: lat, longitude: long)
                        let placeId = poi["place_id"] as! String
                        
                        let poiMarker = GMSMarker.init(position:poiCoordinates)
                        poiMarker.title = name
                        poiMarker.snippet = placeId
                        poiMarker.icon = #imageLiteral(resourceName: "Walmart_pin64x64")//self.imageWithImage(image: #imageLiteral(resourceName: "loves_marker"), scaledToSize: CGSize(width: 30.0, height: 30.0))
                        poiMarker.map = self
                    }
                }
                getResponse(results)
            }catch let error {
                print(error.localizedDescription)
            }
        }).resume()
    }
    
    func formatTimeInSec(totalSeconds: Int) -> String {
        let seconds = totalSeconds % 60
        let minutes = (totalSeconds / 60) % 60
        let hours = totalSeconds / 3600
        let strHours = hours > 9 ? String(hours) : "0" + String(hours)
        let strMinutes = minutes > 9 ? String(minutes) : "0" + String(minutes)
        let strSeconds = seconds > 9 ? String(seconds) : "0" + String(seconds)
        
        if hours > 0 {
            return "\(strHours)h \(strMinutes)m"
        }
        else {
            return "\(strMinutes)m \(strSeconds)s"
        }
    }
    
    func getSearchTA(_ latitude: String, longitude: String, searchText: String, markerName: UIImage, withCompletion getResponse: @escaping ([[String: Any]]) -> ()) {
        //let startLocation = step["end_location"] as! [String: Any]
        
        let poiUrl = "https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=\(latitude),\(longitude)&radius=3200&type=gas_staion&keyword=\(searchText)&key=AIzaSyDSV9ztBznRj_x509SonIuTjy5c1idLSaU"
        let urlSession = URLSession.shared
        guard let escapedString = poiUrl.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) else { return }

        let url = URL(string: escapedString)
        
        let result = urlSession.dataTask(with: url!, completionHandler: { (data, response, error) in
            do {
                guard let dataResponse = data else { return }
                let json = try JSONSerialization.jsonObject(with: dataResponse, options: .allowFragments) as! [String: Any]
                let results = json["results"] as! [[String: Any]]
                
                DispatchQueue.main.async {
                    for poi in results {
                        let geometry = poi["geometry"] as! [String: Any]
                        let name = poi["name"] as! String
                         let placeId = poi["place_id"] as! String
                        let location = geometry["location"] as! [String: Any]
                        let lat = location["lat"] as! Double
                        let long = location["lng"] as! Double
                        let poiCoordinates = CLLocationCoordinate2D(latitude: lat, longitude: long)
                        if name.contains(find: "TA") || name.contains(find: "TravelCenters of America") {
                            let poiMarker = GMSMarker(position:poiCoordinates)
                            poiMarker.title = name
                            poiMarker.snippet = placeId
                            poiMarker.icon = markerName
                            poiMarker.map = self
                        }
                    }
                }
                getResponse(results)
            }catch let error {
                print(error.localizedDescription)
            }
        }).resume()
    }
    
    func getSearchFlyingJ(_ latitude: String, longitude: String, searchText: String, markerName: UIImage, withCompletion getResponse: @escaping ([[String: Any]]) -> ()) {
        //let startLocation = step["end_location"] as! [String: Any]
        
        let poiUrl = "https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=\(latitude),\(longitude)&radius=3200&type=gas_staion&keyword=Flying J Travel Center&key=AIzaSyDSV9ztBznRj_x509SonIuTjy5c1idLSaU"
        let urlSession = URLSession.shared
        guard let escapedString = poiUrl.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) else { return }
        
        let url = URL(string: escapedString)
        
        let result = urlSession.dataTask(with: url!, completionHandler: { (data, response, error) in
            do {
                guard let dataResponse = data else { return }
                let json = try JSONSerialization.jsonObject(with: dataResponse, options: .allowFragments) as! [String: Any]
                let results = json["results"] as! [[String: Any]]
                
                DispatchQueue.main.async {
                    for poi in results {
                        let geometry = poi["geometry"] as! [String: Any]
                        let name = poi["name"] as! String
                        let placeId = poi["place_id"] as! String
                        let location = geometry["location"] as! [String: Any]
                        let lat = location["lat"] as! Double
                        let long = location["lng"] as! Double
                        let poiCoordinates = CLLocationCoordinate2D(latitude: lat, longitude: long)
                        if name.contains(find: "Flying J Travel Center") {
                            let poiMarker = GMSMarker.init(position:poiCoordinates)
                            poiMarker.title = name
                            poiMarker.snippet = placeId
                            poiMarker.icon = markerName
                            poiMarker.map = self
                        }
                    }
                }
                getResponse(results)
            }catch let error {
                print(error.localizedDescription)
            }
        }).resume()
    }
    
    func getSearchPetro(_ latitude: String, longitude: String, searchText: String, markerName: UIImage, withCompletion getResponse: @escaping ([[String: Any]]) -> ()) {
        //let startLocation = step["end_location"] as! [String: Any]
        
        let poiUrl = "https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=\(latitude),\(longitude)&radius=3200&type=gas_staion&keyword=\(searchText)&key=AIzaSyDSV9ztBznRj_x509SonIuTjy5c1idLSaU"
        let urlSession = URLSession.shared
        guard let escapedString = poiUrl.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) else { return }
        
        let url = URL(string: escapedString)
        
        let result = urlSession.dataTask(with: url!, completionHandler: { (data, response, error) in
            do {
                guard let dataResponse = data else { return }
                let json = try JSONSerialization.jsonObject(with: dataResponse, options: .allowFragments) as! [String: Any]
                let results = json["results"] as! [[String: Any]]
                
                DispatchQueue.main.async {
                    for poi in results {
                        let geometry = poi["geometry"] as! [String: Any]
                        let name = poi["name"] as! String
                        let placeId = poi["place_id"] as! String
                        let location = geometry["location"] as! [String: Any]
                        let lat = location["lat"] as! Double
                        let long = location["lng"] as! Double
                        let poiCoordinates = CLLocationCoordinate2D(latitude: lat, longitude: long)
                        if name == "Petro" || name == "TA" || name.contains(find: "TravelCenters of America") {
                            let poiMarker = GMSMarker.init(position:poiCoordinates)
                            poiMarker.title = name
                            poiMarker.snippet = placeId
                            poiMarker.icon = markerName
                            poiMarker.map = self
                        }
                    }
                }
                getResponse(results)
            }catch let error {
                print(error.localizedDescription)
            }
        }).resume()
    }
    
    func getSearchRestArea(_ latitude: String, longitude: String, searchText: String, markerName: UIImage, withCompletion getResponse: @escaping ([[String: Any]]) -> ()) {
        //let startLocation = step["end_location"] as! [String: Any]
        
        let poiUrl = "https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=\(latitude),\(longitude)&radius=3200&type=establishment&keyword=\(searchText)&key=AIzaSyDSV9ztBznRj_x509SonIuTjy5c1idLSaU"
        let urlSession = URLSession.shared
        guard let escapedString = poiUrl.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) else { return }
        
        let url = URL(string: escapedString)
        
        let result = urlSession.dataTask(with: url!, completionHandler: { (data, response, error) in
            do {
                guard let dataResponse = data else { return }
                let json = try JSONSerialization.jsonObject(with: dataResponse, options: .allowFragments) as! [String: Any]
                let results = json["results"] as! [[String: Any]]
                
                DispatchQueue.main.async {
                    for poi in results {
                        let geometry = poi["geometry"] as! [String: Any]
                        let name = poi["name"] as! String
                        let placeId = poi["place_id"] as! String
                        let location = geometry["location"] as! [String: Any]
                        let lat = location["lat"] as! Double
                        let long = location["lng"] as! Double
                        let poiCoordinates = CLLocationCoordinate2D(latitude: lat, longitude: long)
                        
                        let poiMarker = GMSMarker(position:poiCoordinates)
                        poiMarker.title = name
                        poiMarker.snippet = placeId
                        poiMarker.icon = markerName
                        poiMarker.map = self
                    }
                }
                getResponse(results)
            }catch let error {
                print(error.localizedDescription)
            }
        }).resume()
    }
    
    func getSearchPilotTravelCenter(_ latitude: String, longitude: String, searchText: String, markerName: UIImage, withCompletion getResponse: @escaping ([[String: Any]]) -> ()) {
        //let startLocation = step["end_location"] as! [String: Any]
        
        let poiUrl = "https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=\(latitude),\(longitude)&radius=3200&type=establishment&keyword=\(searchText)&key=AIzaSyDSV9ztBznRj_x509SonIuTjy5c1idLSaU"
        let urlSession = URLSession.shared
        guard let escapedString = poiUrl.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) else { return }
        
        let url = URL(string: escapedString)
        
        let result = urlSession.dataTask(with: url!, completionHandler: { (data, response, error) in
            do {
                guard let dataResponse = data else { return }
                let json = try JSONSerialization.jsonObject(with: dataResponse, options: .allowFragments) as! [String: Any]
                let results = json["results"] as! [[String: Any]]
                
                DispatchQueue.main.async {
                    for poi in results {
                        let geometry = poi["geometry"] as! [String: Any]
                        let name = poi["name"] as! String
                        let placeId = poi["place_id"] as! String
                        let location = geometry["location"] as! [String: Any]
                        let lat = location["lat"] as! Double
                        let long = location["lng"] as! Double
                        let poiCoordinates = CLLocationCoordinate2D(latitude: lat, longitude: long)
                        if name.contains(find: "Pilot Travel Center") {
                            let poiMarker = GMSMarker(position:poiCoordinates)
                            poiMarker.title = name
                            poiMarker.snippet = placeId
                            poiMarker.icon = markerName
                            poiMarker.map = self
                        }
                    }
                }
                getResponse(results)
            }catch let error {
                print(error.localizedDescription)
            }
        }).resume()
    }
    
    
    func getWeighStation(_ latitude: String, longitude: String, searchText: String, markerName: UIImage, withCompletion getResponse: @escaping ([[String: Any]]) -> ()) {
        //let startLocation = step["end_location"] as! [String: Any]
        
        let poiUrl = "https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=\(latitude),\(longitude)&radius=10000&type=establishment&keyword=\(searchText)&key=AIzaSyDSV9ztBznRj_x509SonIuTjy5c1idLSaU"
        let urlSession = URLSession.shared
        guard let escapedString = poiUrl.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) else { return }
        
        let url = URL(string: escapedString)
        
        let result = urlSession.dataTask(with: url!, completionHandler: { (data, response, error) in
            do {
                guard let dataResponse = data else { return }
                let json = try JSONSerialization.jsonObject(with: dataResponse, options: .allowFragments) as! [String: Any]
                let results = json["results"] as! [[String: Any]]
                
                DispatchQueue.main.async {
                    for poi in results {
                        let geometry = poi["geometry"] as! [String: Any]
                        let name = poi["name"] as! String
                        let placeId = poi["place_id"] as! String
                        let location = geometry["location"] as! [String: Any]
                        let lat = location["lat"] as! Double
                        let long = location["lng"] as! Double
                        let poiCoordinates = CLLocationCoordinate2D(latitude: lat, longitude: long)
                        if  name.contains(find: "Weigh Station") || name.contains(find: "Weight Station") || name.contains(find: "weigh station") || name.contains(find: "weighstation") {
                            let poiMarker = GMSMarker(position:poiCoordinates)
                            poiMarker.title = name
                            poiMarker.snippet = placeId
                            poiMarker.icon = markerName//self.imageWithImage(image: markerName, scaledToSize: CGSize(width: 30.0, height: 30.0))
                            poiMarker.map = self
                        }
                    }
                }
                getResponse(results)
            }catch let error {
                print(error.localizedDescription)
            }
        }).resume()
    }
    
    func getLovesCenter(_ latitude: String, longitude: String, withCompletion getResponse: @escaping ([[String: Any]]) -> ()) {
        
        let lovesUrl = "https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=\(latitude),\(longitude)&radius=3200&type=establishment&keyword=Love's Travel Stop&key=AIzaSyDSV9ztBznRj_x509SonIuTjy5c1idLSaU"
        guard let escapedString = lovesUrl.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) else { return }
        
        let urlSession = URLSession.shared
        let url = URL(string: escapedString)
        
        let result = urlSession.dataTask(with: url!, completionHandler: { (data, response, error) in
            do {
                guard let dataResponse = data else { return }
                let json = try JSONSerialization.jsonObject(with: dataResponse, options: .allowFragments) as! [String: Any]
                let results = json["results"] as! [[String: Any]]
                
                DispatchQueue.main.async {
                    for poi in results {
                        let geometry = poi["geometry"] as! [String: Any]
                        let name = poi["name"] as! String
                        let location = geometry["location"] as! [String: Any]
                        let lat = location["lat"] as! Double
                        let long = location["lng"] as! Double
                        let poiCoordinates = CLLocationCoordinate2D(latitude: lat, longitude: long)
                        let placeId = poi["place_id"] as! String
                        if name.contains(find: "Love's Travel Stop") {
                            let poiMarker = GMSMarker(position:poiCoordinates)
                            poiMarker.title = name
                            poiMarker.snippet = placeId
                            poiMarker.icon = #imageLiteral(resourceName: "Loves_pin64x64")
                            poiMarker.map = self
                        }
                    }
                }
                getResponse(results)
            }catch let error {
                print(error.localizedDescription)
            }
        }).resume()
    }

    //MARK:- Draw polygon
    
    private func drawPath(with points : String){
        DispatchQueue.main.async {
            let path = GMSPath(fromEncodedPath: points)
            let polyline = GMSPolyline(path: path)
            polyline.strokeWidth = 3.0
            polyline.strokeColor = .red
            polyline.map = self
        }
    }
    
    func getDistance(sourceCoordinates: CLLocationCoordinate2D, destinationCoordinates: CLLocationCoordinate2D, completionHandler: @escaping (_ distanceAndTime: [[String: Float]]) -> ()) {
        let a_coordinate_string = "\(sourceCoordinates.latitude),\(sourceCoordinates.longitude)"
        let b_coordinate_string = "\(destinationCoordinates.latitude),\(destinationCoordinates.longitude)"
        let urlString = "https://maps.googleapis.com/maps/api/distancematrix/json?units=metric&origins=\(a_coordinate_string)&destinations=\(b_coordinate_string)&key=AIzaSyDSV9ztBznRj_x509SonIuTjy5c1idLSaU"
        
        guard let url = URL(string: urlString) else {
            print("Error: cannot create URL")
            return
        }
        let urlRequest = URLRequest(url: url)
        
        // set up the session
        let config = URLSessionConfiguration.default
        let session = URLSession(configuration: config)
        
        // make the request
        let task = session.dataTask(with: urlRequest, completionHandler: { (data, response, error) in
            
            do {
                guard let data = data else {
                    throw JSONError.NoData
                }
                guard let json = try JSONSerialization.jsonObject(with: data, options: []) as? NSDictionary else {
                    throw JSONError.ConversionFailed
                }
                var distanceAndTime = [[String: Float]]()

                if let array = json["rows"] as? NSArray {
                    if let rows = array[0] as? NSDictionary{
                        if let array2 = rows["elements"] as? NSArray{
                            if let elements = array2[0] as? NSDictionary{
                                if let duration = elements["duration"] as? NSDictionary {
                                    if let duration = duration["value"] as? Int{
                                        DispatchQueue.main.async {
                                            let estimateTimeDict  = ["estimateTime": Float(duration)]
                                            distanceAndTime.append(estimateTimeDict)
                                        }
                                    }
                                }
                                if let duration = elements["distance"] as? NSDictionary {
                                    if let distance = duration["value"] as? Int{
                                        DispatchQueue.main.async {
                                            var distanceDict = ["distance":
                                              Float(distance)]
                                            distanceAndTime.append(distanceDict)
                                            completionHandler(distanceAndTime)
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                //distanceAndTime = json
            } catch let error as JSONError {
                print(error.rawValue)
            } catch let error as NSError {
                print(error.debugDescription)
            }
            
        })
        task.resume()
    }
}

enum JSONError: String, Error {
    case NoData = "ERROR: no data"
    case ConversionFailed = "ERROR: conversion from JSON failed"
}
