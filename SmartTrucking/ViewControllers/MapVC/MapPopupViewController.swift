//
//  MapPopupViewController.swift
//  SmartTrucking
//
//  Created by Prabhakar G on 12/05/18.
//  Copyright © 2018 Vivan Raghuvanshi. All rights reserved.
//

import UIKit

class MapPopupViewController: UIViewController {

    @IBOutlet weak var ratingLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var phoneLabel: UILabel!
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var phoneNumberButton: UIButton!
    
    var name = String()
    var rating = String()
    var address = String()
    var phone = String()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        showAnimate()
        nameLabel.text = name
        ratingLabel.text = rating
        addressLabel.text = address
        phoneNumberButton.setTitle(phone, for: .normal)
    }
    
    @IBAction func phoneNumberCliced(_ sender: UIButton) {
        let phoneNumer = phone.stripped
        if let phoneCallURL = URL(string: "tel://\(phoneNumer)") {
            
            let application:UIApplication = UIApplication.shared
            if (application.canOpenURL(phoneCallURL)) {
                application.open(phoneCallURL, options: [:], completionHandler: nil)
            }
        }
    }
    
    @IBAction func closeButtonClicked(_ sender: UIButton) {
        removeAnimate()
    }
    
    func showAnimate() {
        self.view.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
        self.view.alpha = 0.0
        UIView.animate(withDuration: 0.25, animations: {
            self.view.alpha = 1.0
            self.view.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
        })
    }
    
    func removeAnimate() {
        UIView.animate(withDuration: 0.25, animations: {
            self.view.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
            self.view.alpha = 0.0
        }, completion: {(finished : Bool) in
            if(finished)
            {
                self.willMove(toParent: nil)
                self.view.removeFromSuperview()
                self.removeFromParent()
            }
        })
    }
}

extension String {
    
    var stripped: String {
        let okayChars = Set("1234567890")
        return self.filter {okayChars.contains($0) }
    }
}
