//
//  HomeVC.swift
//  SmartTrucking
//
//  Created by Vivan Raghuvanshi on 26/08/17.
//  Copyright © 2017 Vivan Raghuvanshi. All rights reserved.
//

import UIKit
import GooglePlacesSearchController
import MFSideMenu

class HomeVC: UIViewController {
    
    // MARK:- --------------Outlets/Variables-------------------
    
    @IBOutlet weak var collection_View:UICollectionView!
    @IBOutlet weak var lbl_Username:UILabel!
    @IBOutlet weak var lbl_TotalProfit:UILabel!
    @IBOutlet weak var lbl_ProfitLoss:UILabel!
    
    @IBOutlet weak var bt_NewTrip:UIButton!
    @IBOutlet weak var btn_Setting:UIButton!
    
    var pendingTrip:Trip?
    let identifiers = ["TotalTrip","TotalMiles","TotalSpent","TotalPaid"]
    
    var dashBoardData:[String:Any]?
    
    var allExpenses = 0.0
    var allEarnings = 0.0 {
        didSet {
            let profitLoss = allEarnings - allExpenses
            if profitLoss >= 0 {
                lbl_ProfitLoss.text = "Total Profit"
            } else {
                lbl_ProfitLoss.text = "Total Loss"
            }
            lbl_TotalProfit.text = (abs(profitLoss)).dollarString
        }
    }
    
    // Variable to store trip history data
    var trips:[Trip]?
    
    // Variables to store current trip information
    var sourcePlace:Stop?
    var destinationPlace:Stop?
    var waypointsPlace:[Stop]?
    var tripId:String?
    var tripName:String?
    
    // MARK:- --------------View Life Cycle Methods-------------------
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        collection_View.dataSource = self
        collection_View.delegate = self
        self.getTripSummary()
        DispatchQueue.global().async {
            saveImageDocumentDirectory(nil)
            self.fetchImage()
            self.getUserDetails()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        APPDELEGATE.mfContainer?.panMode = MFSideMenuPanModeDefault
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        if self.tripId == nil {
            bt_NewTrip.setTitle("Plan A Trip", for: .normal)
            btn_Setting.isHidden = true
        } else {
            bt_NewTrip.setTitle("Resume Trip", for: .normal)
            btn_Setting.isHidden = true
        }
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        APPDELEGATE.mfContainer?.panMode = MFSideMenuPanModeNone
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK:- --------------Button Action Methods-------------------
    
    @IBAction func buttonClicked_LeftMenu(_ sender:UIButton) {
        sender.animateView()
        self.menuContainerViewController.toggleLeftSideMenuCompletion {
            
        }
    }
    
    @IBAction func buttonClicked_TripHistory(_ sender:UIButton) {
        sender.animateView()
        let yourTripsVC = self.storyboard?.instantiateViewController(withIdentifier: "YourTripsVC") as! YourTripsVC
        yourTripsVC.delegate = self
        if let allTrips = trips, allTrips.count > 0 {
            yourTripsVC.recentTrips = allTrips
        }
        let tripsNavVC = UINavigationController.init(rootViewController: yourTripsVC)
        tripsNavVC.navigationBar.isHidden = true
        
        APPDELEGATE.mfContainer?.centerViewController = tripsNavVC
        //  self.menuContainerViewController.setMenuState(MFSideMenuStateClosed, completion: nil)
    }
    
    @IBAction func buttonClicked_GetSummary(_ sender:UIButton) {
        sender.animateView()
        self.getTripSummary()
    }
    
    @IBAction func buttonClicked_PlanATrip(_ sender:UIButton) {
        sender.animateView()
        if (self.tripId == nil) {
            let defaults = UserDefaults.standard
            
            if user!.userTruckInfo == nil {
                let goToTruckInfo = UIAlertAction.init(title:"Yes", style: .default, handler: { (action) in
                   // defaults.set(true, forKey: "\(String(describing: user?.userName!))truckDetailsAlertAllowClicked")
                    let truckDetailVC = self.storyboard?.instantiateViewController(withIdentifier: "TruckDetailVC") as! TruckDetailVC
                    truckDetailVC.isHome = true
                    let truckDetailNavVC = UINavigationController(rootViewController: truckDetailVC)
                    truckDetailNavVC.navigationBar.isHidden = true
                    self.menuContainerViewController.centerViewController = truckDetailNavVC
                    APPDELEGATE.mfContainer?.setMenuState(MFSideMenuStateClosed, completion: nil)
                    
                })
                if user?.userTitle == "Owner/Operator" {
                    let defaultTruckInfo = UIAlertAction(title:"No. I'm Good.", style: .cancel, handler: { (action) in
                        defaults.set(true, forKey: "\(String(describing: user?.userName!))truckDetailsAlertDeniedClicked")
                        self.goToCreateNewTrip()
                    })
                    showAlert(nil, message: "More information about your truck would enable us to find an optimal route and to do a more accurate toll calculation. Would you like to add ?", withAction: [goToTruckInfo, defaultTruckInfo], with: false, andTitle: "", onView: self)
                } else {
                    goToCreateNewTrip()
                }
            } else {
                self.goToCreateNewTrip()
            }
            
        } else {
            if let mapExist = APPDELEGATE.mapVCRef {
                APPDELEGATE.mapNavigationVC?.viewControllers = [mapExist]
            } else {
                self.createMapIfNotExistAlready()
            }
            APPDELEGATE.mfContainer?.centerViewController = APPDELEGATE.mapNavigationVC
            APPDELEGATE.mfContainer?.setMenuState(MFSideMenuStateClosed, completion: nil)
        }
    }
    
    func createMapIfNotExistAlready() {
        
        showHud("Processing..")
        
        if  let mapVC = APPDELEGATE.mapVCRef {
            if let tripExist = self.pendingTrip {
                mapVC.pendingTrip = tripExist
            }
            mapVC.sourcePlace = self.sourcePlace
            mapVC.destinationPlace = self.destinationPlace
            mapVC.waypointsPlace = self.waypointsPlace
            mapVC.tripName = self.tripName
            mapVC.tripId = self.tripId
            mapVC.isFromNewVC = false
                       
            APPDELEGATE.mapNavigationVC?.viewControllers = [mapVC]
        }
        // APPDELEGATE.mapNavigationVC = UINavigationController.init(rootViewController: mapVC)
    }
    
    @IBAction func buttonClicked_SearchCategory(_ sender:UIButton) {
        sender.animateView()
        if let mapVC = APPDELEGATE.mapVCRef {
            let categorySearchVC = self.storyboard?.instantiateViewController(withIdentifier: "CategorySearchVC") as! CategorySearchVC
            if mapVC.firstCell != nil {
                categorySearchVC.delegate = mapVC.firstCell!;
                categorySearchVC.selectedOptions = mapVC.firstCell!.array_SelectedCategory
            }
            self.navigationController?.pushViewController(categorySearchVC, animated: true)
        }
    }
    
    // MARK:- --------------Private Methods------------------
    
    private func goToCreateNewTrip() {
        let newTripVC = self.storyboard?.instantiateViewController(withIdentifier: "NewTripVC") as! NewTripVC
        newTripVC.delegate = self
        
        if let allTrips = trips, allTrips.count > 0 {
            newTripVC.recentTrips = allTrips
        }
        self.navigationController?.pushViewController(newTripVC, animated: true)
    }
    
    private func fetchImage() {
        let params = ["pid":user!.userId!]
        
        let networkManager = NetworkManager()
        
        networkManager.getImageForRequest(fetchimage, andParameter: params) { (imageExist) in
            if let userImage = imageExist {
                if let leftVC = APPDELEGATE.mfContainer?.leftMenuViewController as? LeftMenuVC {
                    leftVC.img_View.image = userImage
                }
            }
        }
    }
    
    // Method to get user details
    fileprivate func getUserDetails() {
        
        let params = ["profileId":user!.userId!]
        
        showHud("Processing..")
        
        let networkManager = NetworkManager()
        networkManager.getDataForRequest(getUserInfo, andParameter: params) { (response, error) in
            
            hideHud()
            if error == nil {
                
                if let result = response as? [String:Any] {
                    if let status = result["status"] as? String, (status == SUCCESS_STATUS) {
                        if let userDetail = result["_obj"] as? [String:Any] {
                            
                            var userInfo = userDetail
                            userInfo["generated_id"] = params["profileId"]
                            // Store the info in local database
                            UserInfo.sharedInfo.setUserProfileWithInfo(userInfo)
                            if let leftVC = APPDELEGATE.mfContainer?.leftMenuViewController as? LeftMenuVC {
                                leftVC.table_View.reloadData()
                            }
                        }
                    } else {
                        // showAlert("", message:INTERNAL_ERROR_MESSAGE, onView: self)
                    }
                }
            } else {
                //   showAlert("", message:error?.localizedDescription ?? SOMETHING_WRONG, onView: self)
            }
        }
    }
    
    func endIfOnGoingTrip() {
        
        sourcePlace = nil
        destinationPlace = nil
        waypointsPlace = nil
        tripId = nil
        tripName = nil
        //APPDELEGATE.mapNavigationVC = nil
        APPDELEGATE.mapVCRef = nil
        
        if trips != nil && trips!.count > 0 {
            trips?.removeAll()
        }
    }
    
    func getTripSummary() {
        
        lbl_Username.text = user!.userName!
        
        let params = ["profileID":user!.userId!]
        showHud("Processing..")
        let networkManager = NetworkManager()
        networkManager.getDataForRequest(dashBoard, andParameter: params) { (response, error) in
            
            if error == nil {
                
                print(response ?? "Avav")
                
                if let result = response as? [String:Any] {
                    if let status = result["status"] as? String, (status == SUCCESS_STATUS) {
                        if let dashBoradHistory = result["_obj"] as? [String:Any] {
                            self.dashBoardData = dashBoradHistory
                            self.collection_View.reloadData()
                            
                            // Check wether a pending trip exist or not
                            self.checkAndResumeIfAnyPendingTrip()
                            return;
                        }
                    } else {
                        showAlert("", message:INTERNAL_ERROR_MESSAGE, onView: self)
                    }
                }
            } else {
                showAlert("", message:error?.localizedDescription ?? SOMETHING_WRONG, onView: self)
            }
        }
        hideHud()
    }
    
    // Check Wether a pending trip exist or not
    private func checkAndResumeIfAnyPendingTrip() {
        if let allPendingTrips = self.dashBoardData?["pendingTrips"] as? [[String:Any]], allPendingTrips.count > 0 {
            let pendingTrip = allPendingTrips[0]
            if let existTripId = pendingTrip["tripId"] {
                self.fetchExistingTripInformationFor(existTripId)
                return
            }
        }
        hideHud()
    }
    
    // If a pending trip exist fetch it's Information
    private func fetchExistingTripInformationFor(_ tripId:Any) {
        fetchTripInformationFor(tripId) { (response, error) in
            
            hideHud()
            if error == nil {
                
                print(response ?? "Avav")
                
                if let result = response as? [String:Any] {
                    if let status = result["status"] as? String, (status == SUCCESS_STATUS) {
                        if let pending_Trip = result["_obj"] as? [String:Any] {
                            let trip = Trip.init(pending_Trip)
                            self.pendingTrip = trip
                            let sourceModel = Stop.init(trip.tripId, address: trip.sourceAddress, latitude: trip.sourceLatitude, longitude: trip.sourceLongitude, stopSeq: 0)
                            let destinationModel = Stop.init(trip.tripId, address: trip.destinationAddress, latitude: trip.destinationLatitude, longitude: trip.destinationLongitude, stopSeq: 0)
                            
                            self.waypointsPlace = trip.tripStops
                            self.sourcePlace = sourceModel
                            self.destinationPlace = destinationModel
                            self.tripId = trip.tripId
                            self.tripName = trip.tripName
                            self.bt_NewTrip.setTitle("Resume Trip", for: .normal)
                            self.btn_Setting.isHidden = true
                            
                            if let mapVC =  self.storyboard?.instantiateViewController(withIdentifier: "MapVC") as? MapVC {
                                APPDELEGATE.mapVCRef = mapVC
                                mapVC.sourcePlace = self.sourcePlace
                                mapVC.destinationPlace = self.destinationPlace
                                mapVC.waypointsPlace = self.waypointsPlace
                                mapVC.tripName = self.tripName
                                mapVC.tripId = self.tripId
                                mapVC.pendingTrip = trip
                            }
                        }
                    } else {
                        showAlert("", message:INTERNAL_ERROR_MESSAGE, onView: self)
                    }
                }
            } else {
                showAlert("", message:error?.localizedDescription ?? SOMETHING_WRONG, onView: self)
            }
        }
    }
    
}

// MARK:- --------------UICollectionViewDataSource Methods-------------------

extension HomeVC:UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 4
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let identifier = self.identifiers[indexPath.row]
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: identifier, for: indexPath)
        if dashBoardData != nil {
            let lbl_Data = cell.contentView.viewWithTag(1001) as! UILabel
            switch indexPath.row {
            case 0:
                if let miles = dashBoardData?["trips"] {
                    lbl_Data.text = "\(miles)"
                }
                break
            case 1:
                if let trips = dashBoardData?["miles"] {
                    let totalMiles = "\(trips)"
                    lbl_Data.text = "\(String(describing:(Double(totalMiles) ?? 0.0).rounded(toPlaces: 2)))"
                }
                break
            case 2:
                if let expenses = dashBoardData?["expenses"] {
                    let otherExpenses = "\(expenses)"
                    var otherExpenditures = (Double(otherExpenses) ?? 0.0)
                    if let monthlyExpensesExist = dashBoardData?["monthlyExpenses"] {
                        let monthlyExpenses = "\(monthlyExpensesExist)"
                        let monthlyExpenditures = (Double(monthlyExpenses) ?? 0.0)
                        if monthlyExpenditures > 0.0 {
                            let lbl_IncludeMonthly = cell.contentView.viewWithTag(999) as! UILabel
                            lbl_IncludeMonthly.isHidden = false
                            otherExpenditures = (otherExpenditures + monthlyExpenditures)
                        }
                    }
                    allExpenses = otherExpenditures
                    lbl_Data.text = "\(String(describing: otherExpenditures.dollarString))"
                }
                break
            case 3:
                if let earnings = dashBoardData?["earnings"] {
                    let totalEarnings = "\(earnings)"
                    
                    allEarnings = (Double(totalEarnings) ?? 0.0)
                    
                    lbl_Data.text = "\(String(describing: (Double(totalEarnings) ?? 0.0).dollarString))"
                }
                break
            default:
                break
            }
        }
        return cell
    }
}

// MARK:- --------------UICollectionViewDelegate Methods-------------------

extension HomeVC:UICollectionViewDelegate {
    
}

extension HomeVC:UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let collectionViewSize = collectionView.frame
        return CGSize.init(width: collectionViewSize.width/2, height: collectionViewSize.height/2)
    }
}

extension HomeVC:CommonDelegate {
    
    func tripHistoryFound(_ tripsFound:[Trip]?) {
        self.trips = tripsFound
    }
    
    func sourcePlaceAdded(_ source:Stop?) {
        self.sourcePlace = source
    }
    
    func destinationPlaceAdded(_ destination:Stop?) {
        self.destinationPlace = destination
    }
    
    func waypointPlaceAdded(_ waypoint:[Stop]?) {
        self.waypointsPlace = waypoint
    }
    
    func currentTripId(_ tripId:String, withName name:String) {
        self.tripId = tripId
        self.tripName = name
    }
    
}

