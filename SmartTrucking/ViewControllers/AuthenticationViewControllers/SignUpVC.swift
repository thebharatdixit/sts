//
//  SignUpVC.swift
//  SmartTrucking
//
//  Created by Vivan Raghuvanshi on 19/08/17.
//  Copyright © 2017 Vivan Raghuvanshi. All rights reserved.
//

import UIKit
import Material

class SignUpVC: UIViewController {
    
    // MARK:- --------------Outlets/Variables-------------------
    
    @IBOutlet weak var tf_Name:TextField!
    @IBOutlet weak var tf_Email:TextField!
    @IBOutlet weak var tf_Phone:TextField!
    @IBOutlet weak var tf_Password:TextField!
    @IBOutlet weak var img_View:UIImageView!
    var imageEdited = false
    var imageManage:ImageManager?
    
    // MARK:- --------------View Life Cycle Methods-------------------

    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK:- --------------Button Action Methods-------------------

    @IBAction func buttonClicked_Back(_ sender:UIButton) {
        sender.animateView()
        self.navigationController?.popViewController(animated: true)
    }
    
    // Add Image
    @IBAction func buttonClicked_AddImage(_ sender:UIButton) {
        sender.animateView()
        imageManage = ImageManager()
        imageManage?.showImagePicker(self.navigationController!) { image in
            DispatchQueue.main.async {
                if image != nil {
                    self.imageEdited = true
                    saveImageDocumentDirectory(image!)
                    print(getDirectoryPath())
                    self.img_View.image = image
                }
            }
        }
    }
    
    // Proceed for Sign up
    @IBAction func buttonClicked_AlmostThere(_ sender:UIButton) {
        sender.animateView()
//        if isValidPhoneNumberFormat(tf_Phone.text!) {
//            print(tf_Phone.text!)
//        } else{
//            print("not valid number")
//        }
//        return;
        
        
        self.view.endEditing(true)
        
        if checkIfAllFieldsValid() {
            
           // sender.isEnabled = false
           // startIndicator(self, forFrame: aiFrame, andColor: .white)
            showHud("Processing..")
            let params = ["name":tf_Name.text!,
                          "email":tf_Email.text!,
                          "password":tf_Password.text!,
                          "mobile":tf_Phone.text!,
                          "isSocial":false] as [String : Any]
            
            let authenticateUser = AuthenticateUser()
            authenticateUser.registerUserWithParameters(params, withCompletion: { (status, response) in
           //     sender.isEnabled = true
           //     self.stopIndicator(self)
                hideHud()
                
                if (status == SUCCESS_STATUS || status == DUPLICATE_PROCEED) {
                    if status == DUPLICATE_PROCEED {
                        
                        let okAction = UIAlertAction.init(title: "Ok", style: .default, handler: { action in
                            self.goToSecondScreenForRegistration()
                        })
                        
                       showAlert("", message: "Email already registered as prospect.", withAction: [okAction], with: false, andTitle: "", onView: self)
                    } else {
                        self.goToSecondScreenForRegistration()
                    }
                } else {
                    showAlert("", message:"\(String(describing: response ?? ""))", onView: self)
                }
            })
        }
    }
    
    func goToSecondScreenForRegistration() {
        user = UserInfo.sharedInfo.getUserInfo()
        if self.imageEdited == true {
            if let imgExist = self.img_View.image {
                DispatchQueue.global().async {
                    self.uploadImage(imgExist)
                }
            }
        }
        let createAccVC = self.storyboard?.instantiateViewController(withIdentifier: "CreateAccVC") as! CreateAccVC
        self.navigationController?.pushViewController(createAccVC, animated: true)
    }
    
    // Run time checking for fields text validity
    @IBAction func textFieldIsTyping_EmailPasswordPhone(_ textField:UITextField) {

    }
    
    @IBAction func buttonClicked_TogglePasswordSecurity(sender: UIButton) {
        sender.animateView()
        self.tf_Password.isSecureTextEntry = !self.tf_Password.isSecureTextEntry
    }
    
    // MARK:- --------------UITextField Delegate Methods-------------------
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField is TextField {
            let tf = textField as! TextField
            tf.dividerNormalColor = .lightGray
            tf.detailLabel.text = "   "
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range:NSRange, replacementString string: String) -> Bool {
        if textField == tf_Phone {
            if (tf_Phone.text == "" && (string == "0" || string == "1" || string == "2")) {
                return false
            }
            return checkValidPhoneNumberLength(textField.text ?? "", forRange: range, replacementString: string)
        }
        if textField == tf_Password && string == " " {
            return false
        }
        return true
    }
    
    // MARK:- --------------Private Methods-------------------
    
    private func uploadImage(_ imgExist:UIImage) {
        let compressedImage = imgExist.scaleImage(imgExist)
        let imageData = compressedImage.jpegData(compressionQuality: 0.8) as NSData?
        
        if let dataExist = imageData {
            let dataString = dataExist.base64EncodedString(options: .lineLength64Characters)
            
            let params = ["pid":user!.userId!,
                          "image":dataString] as [String : Any]
            
            let networkManager = NetworkManager()
            networkManager.getDataForRequest(uploadimage, andParameter: params) { (response, error) in
                
                hideHud()
                if error == nil {
                    
                    print(response ?? "Avav")
                    
                    if let result = response as? [String:Any] {
                        if let status = result["status"] as? String, (status == SUCCESS_STATUS) {
                            saveImageDocumentDirectory(compressedImage)
                        } else {
                            
                            print("Error in loading")
                            //showAlert("", message:INTERNAL_ERROR_MESSAGE, onView: self)
                        }
                    }
                } else {
                    print("Error in loading",error?.localizedDescription ?? SOMETHING_WRONG)
                    //                        showAlert("", message:error?.localizedDescription ?? SOMETHING_WRONG, onView: self)
                }
            }
        }
    }
    
    //  Check Fields validity
    private func checkIfAllFieldsValid() -> Bool {
        if isBlankField(tf_Name) {
            tf_Name.dividerNormalColor = .red
            tf_Name.detailLabel.text = "Please Enter Your Name"
        }
        if (isBlankField(tf_Email) || isValidEmail(tf_Email.text ?? "") == false) {
            tf_Email.dividerNormalColor = .red
            tf_Email.detailLabel.text = "Please Enter a Valid Email Id"
        }
        if (isBlankField(tf_Password) || (tf_Password.text ?? "").count < 5) {
            tf_Password.dividerNormalColor = .red
            tf_Password.detailLabel.text = "Please Enter 6 Characters For Password"
        }
        if (tf_Phone.text ?? "").count != 0 {
            if (tf_Phone.text ?? "").count < 10 || (tf_Phone.text ?? "").count > 11 {
                tf_Phone.dividerNormalColor = .red
                tf_Phone.detailLabel.text = "Please Enter a Valid Phone Number"
            }
        }
//        if isValidPhoneNumber(tf_Phone.text ?? "")  {
//            tf_Phone.dividerNormalColor = .red
//        }
        if (tf_Name.text ?? "").count < 4 {
            tf_Name.dividerNormalColor = .red
            tf_Name.detailLabel.text = "Please Enter 4 Characters For Name"
        }
        
        if (tf_Name.dividerNormalColor == .red ||
            tf_Email.dividerNormalColor == .red ||
            tf_Password.dividerNormalColor == .red ||
            tf_Phone.dividerNormalColor == .red) {
            return false
        } else {
            return true
        }
    }
}


