//
//  CreateAccVC.swift
//  SmartTrucking
//
//  Created by Vivan Raghuvanshi on 19/08/17.
//  Copyright © 2017 Vivan Raghuvanshi. All rights reserved.
//

import UIKit
import Material

enum Title:String {
    case Driver = "Driver"
    case Contractor = "Owner/Operator"
}

enum PayType:String {
    case percentage = "percentage"
    case perMile = "perMile"
}

class CreateAccVC: UIViewController {
    
    // MARK:- --------------Outlets/Variables-------------------
    
    @IBOutlet weak var tf_Title:TextField!
    @IBOutlet weak var tf_CompanyName:TextField!
    @IBOutlet weak var tf_PaidPerMile:TextField!
    @IBOutlet weak var tf_PaidExtra:TextField!
    
    @IBOutlet weak var tf_Percentage:TextField!
    
    @IBOutlet weak var img_RadioOne:UIImageView!
    @IBOutlet weak var img_RadioTwo:UIImageView!
    
    @IBOutlet weak var lbl_Username:UILabel!
    
    @IBOutlet weak var view_Percetage:UIView!
    
    @IBOutlet weak var table_View:UITableView!
    @IBOutlet weak var constraint_TableBottom:NSLayoutConstraint!
    @IBOutlet weak var btn_CreateAcc:UIButton!


    
    fileprivate var payType = PayType.perMile
    
    fileprivate var array_Titles :[String] {
        let titles = ["Driver", "Owner/Operator"]
        return titles
    }
    
    fileprivate var array_Company :[String] {
        let titles = ["Smart Trucking Service", "Appsomatic", "Movers & Packers"]
        return titles
    }
    
    fileprivate var array_AllCompany = [String]()
    
    // MARK:- --------------View Life Cycle Methods-------------------

    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Swipe to back stop
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = false
        
        self.table_View.isHidden = true

        // Set user name here
        if let user = user, let name = user.userName {
            lbl_Username.text = "Welcome \(name)"
        }
        
        // Intially percent view will be hidden 
        // When user choose owner Operator it will come up
        view_Percetage.isHidden = true
    }
    
    /*override func viewDidLayoutSubviews(){
        
        let h = btn_CreateAcc.frame.minY - 30
        let contentHeight = table_View.contentSize.height
        
        if (h > contentHeight) {
            (constraint_TableBottom.constant = btn_CreateAcc.frame.minY - contentHeight)
        } else {
            constraint_TableBottom.constant = 30
        }
        
        let constraintHeight = (table_View.bounds.height - table_View.contentSize.height)
        if constraintHeight > 0 {
            constraint_TableBottom.constant = (constraintHeight + 30)
        }
        table_View.reloadData()
    }*/

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK:- --------------Button Action Methods-------------------

    @IBAction func buttonClicked_Back(_ sender:UIButton) {
        sender.animateView()
        self.navigationController?.popViewController(animated: true)
    }
    
    // Percentage option is chosen
    @IBAction func buttonClicked_Percentage(_ sender:UIButton) {
        sender.animateView()
        payType = PayType.percentage
        self.resetFieldOnTitleChange(payType)
    }
    // Per mile option is chosen
    @IBAction func buttonClicked_PerMile(_ sender:UIButton) {
        sender.animateView()
        payType = PayType.perMile
        self.resetFieldOnTitleChange(payType)
    }
    
    
    
    // Proceed for Creating account 
    @IBAction func buttonClicked_CreateAccount(_ sender:UIButton) {
        sender.animateView()
        self.view.endEditing(true)
        if checkIfAllFieldsValid() {
            
            //sender.isEnabled = false
            //startIndicator(self, forFrame: aiFrame, andColor: .white)
            showHud("Processing..")
            
            let user = UserInfo.sharedInfo.getUserInfo()
            
            var params = ["title":tf_Title.text!,
                          "company":tf_CompanyName.text!,
                          "profileId":user!.userId!] as [String:Any]
            // if payment is permile
            if payType == PayType.perMile {
                params["chargesPerMile"] = tf_PaidPerMile.text!
                params["paidForExtraMile"] = tf_PaidExtra.text!
            } else {
                params["percentage"] = tf_Percentage.text!
            }
            
            let authenticateUser = AuthenticateUser()
            authenticateUser.updateRegisteredUserWithParameters(params, withCompletion: { (status, response) in
                //sender.isEnabled = true
                //self.stopIndicator(self)
                hideHud()
                
                if (status == SUCCESS_STATUS) {
                    let registerCompleteVC = self.storyboard?.instantiateViewController(withIdentifier: "RegisterCompleteVC") as! RegisterCompleteVC
                    self.navigationController?.pushViewController(registerCompleteVC, animated: true)
                } else {
                    showAlert("", message:"\(String(describing: response))", onView: self)
                }
            })
        }
    }
    
    @IBAction func textFieldIsSearching(sender:UITextField) {
        
        if let textSearch = sender.text, textSearch.count > 2 {
            let param = ["q": sender.text ?? ""]
            let networkManager = NetworkManager()
            
            let ai = CGRect.init(x:(sender.bounds.width - 30), y:((sender.bounds.height - 30)/2), width: 30, height: 30)
            self.startIndicator(self, forFrame: ai, withCenter: nil)
            
            networkManager.getDataForRequest(companies, andParameter: param)
                { (response, error) in
               self.stopIndicator(self)
                if error == nil {
                    if let result = response as? [String:Any] {
                        if let allCompanies = result["_obj"] as? [String], allCompanies.count > 0 {
                            self.array_AllCompany = allCompanies
                            self.table_View.reloadData()
                            self.table_View.isHidden = false
                        } else {
                            self.table_View.isHidden = true
                        }
                    }
                } else {
                    self.table_View.isHidden = true
                }
            }
        }
       
    }
    
       //MARK:- ---------- UITextField delegate Methods -----------
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField is TextField {
            let tf = textField as! TextField
            tf.dividerNormalColor = .lightGray
            tf.detailLabel.text = "   "
        }
        if textField == tf_Title  { //|| textField == tf_CompanyName
            let picker = UIPickerView()
            picker.delegate = self
            picker.dataSource = self
            if textField == tf_Title {
                picker.tag = 1001
                if tf_Title.text == "" || tf_Title.text == Title.Driver.rawValue {
                    tf_Title.text = array_Titles[0]
                } else {
                    picker.selectRow(1, inComponent: 0, animated: false)
                }
            } else {
                picker.tag = 1002
                if tf_CompanyName.text == "" {
                    tf_CompanyName.text = array_Company[0]
                }
            }
            textField.inputView = picker
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        let txt = textField.text ?? ""
        if txt.count == 0 && string == "." {
            return false
        }
        
        
        // Title and company are not editable
        if textField == tf_Title {
            return false
        }
        if textField == tf_CompanyName {
            return true
        }
        let text = textField.text ?? ""
        if string == "." || text.contains(find: ".") || string == "" {
            // Payment info is restricted upto two decimal
            return allowUptoTwoDecimalPlace(textField.text ?? "", forRange: range, replacementString: string)
        } else {
            if textField == tf_Percentage {
                return text.count < 2 ? true : false
            }
            return text.count < 3 ? true : false
        }
    }
    
    // MARK:- --------------Private Methods-------------------
    
    // Set the fields values
    fileprivate func resetFieldOnTitleChange(_ pay_Type:PayType) {
        self.view.endEditing(true)
        if pay_Type == PayType.perMile {
            tf_Percentage.dividerNormalColor = .lightGray
            tf_Percentage.detailLabel.text = "   "
            img_RadioOne.backgroundColor = UIColor.lightGray
            // permile option deselected
            img_RadioTwo.backgroundColor = blue1
            if tf_Title.text == Title.Driver.rawValue {
                view_Percetage.isHidden = true
            } else {
                view_Percetage.isHidden = false
            }
            tf_Percentage.text = ""
            tf_Percentage.isHidden = true
            tf_PaidPerMile.isHidden = false
            tf_PaidExtra.isHidden = false
        } else {
            tf_PaidPerMile.dividerNormalColor = .lightGray
            tf_PaidPerMile.detailLabel.text = "   "
            tf_PaidPerMile.text = ""
            tf_PaidExtra.dividerNormalColor = .lightGray
            tf_PaidExtra.detailLabel.text = "   "
            tf_PaidExtra.text = ""
            img_RadioOne.backgroundColor = blue1
            // permile option deselected
            img_RadioTwo.backgroundColor = UIColor.lightGray
            view_Percetage.isHidden = false
            tf_Percentage.isHidden = false
            tf_PaidPerMile.isHidden = true
            tf_PaidExtra.isHidden = true
        }
    }
    
    //  Check Fields validity
    private func checkIfAllFieldsValid() -> Bool {
        if isBlankField(tf_Title) {
            tf_Title.dividerNormalColor = .red
            tf_Title.detailLabel.text = "Please Enter Your Title"
        }
//        if isBlankField(tf_CompanyName) {
//            tf_CompanyName.dividerNormalColor = .red
//            tf_CompanyName.detailLabel.text = "Please Enter Your Company Name"
//        }
        
        if payType == PayType.perMile {
            let paidPerMile = Double(tf_PaidPerMile.text ?? "0.0") ?? 0.0
            if isBlankField(tf_PaidPerMile) || paidPerMile <= 0.0{
                tf_PaidPerMile.dividerNormalColor = .red
                tf_PaidPerMile.detailLabel.text = "Please Enter Fair Charges"
            }
            let paidExtra = Double(tf_PaidExtra.text ?? "0.0") ?? 0.0
            if isBlankField(tf_PaidExtra) || paidExtra <= 0.0 {
                tf_PaidExtra.dividerNormalColor = .red
                tf_PaidExtra.detailLabel.text = "Please Enter Extra Stop Charges"
            }
        } else {
            let percentageVal = Double(tf_Percentage.text ?? "0.0") ?? 0.0
            if isBlankField(tf_Percentage) || percentageVal <= 0.0 {
                tf_Percentage.dividerNormalColor = .red
                tf_Percentage.detailLabel.text = "Please Enter Percentage Charges"
            }
        }
        
        if (tf_Title.dividerNormalColor == .red ||
            tf_CompanyName.dividerNormalColor == .red ||
            tf_PaidPerMile.dividerNormalColor == .red ||
            tf_PaidExtra.dividerNormalColor == .red ||
            tf_Percentage.dividerNormalColor == .red) {
            return false
        } else {
            return true
        }
    }

}

// MARK:- --------------UIPickerViewDataSource Methods-------------------

extension CreateAccVC : UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if pickerView.tag == 1001 {
            return array_Titles.count
        } else {
            return array_Company.count
        }
    }
}

// MARK:- --------------UIPickerViewDelegate Methods-------------------

extension CreateAccVC : UIPickerViewDelegate {
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if pickerView.tag == 1001 {
            let title = array_Titles[row]
            if title == Title.Driver.rawValue {
                payType = PayType.perMile
            } else {
                payType = PayType.percentage
            }
            tf_Title?.text = title
            self.resetFieldOnTitleChange(payType)
        } else {
            tf_CompanyName?.text = array_Company[row]
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if pickerView.tag == 1001 {
            return array_Titles[row]
        } else {
            return array_Company[row]
        }
    }
}

extension CreateAccVC : UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.array_AllCompany.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellId = "CellId"
        var cell = tableView.dequeueReusableCell(withIdentifier: cellId)
        
        if cell == nil {
            cell = UITableViewCell.init(style: .default, reuseIdentifier: cellId)
        }
        let comapanyName = self.array_AllCompany[indexPath.row]
        cell?.textLabel?.font = UIFont.init(name: "Avenir Next", size: 14 * getScaleFactor())
        cell?.textLabel?.text = comapanyName
        cell?.selectionStyle = .none
        return cell ?? UITableViewCell()
    }
}

extension CreateAccVC : UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.isHidden = true
        let comapanyName = self.array_AllCompany[indexPath.row]
        self.tf_CompanyName.text = comapanyName
    }
}
