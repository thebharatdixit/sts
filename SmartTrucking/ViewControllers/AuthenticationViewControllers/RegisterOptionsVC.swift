//
//  RegisterOptionsVC.swift
//  SmartTrucking
//
//  Created by Vivan Raghuvanshi on 19/08/17.
//  Copyright © 2017 Vivan Raghuvanshi. All rights reserved.
//

import UIKit
import Google
import MFSideMenu
import CoreLocation

class RegisterOptionsVC: UIViewController {
    
    // MARK:- --------------Outlets/Variables-------------------
    
    let socialManager = SocialManager()
    
    // MARK:- --------------View Life Cycle Methods-------------------

    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.isNavigationBarHidden = false
        self.navigationController?.navigationBar.isHidden = true
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = true
        if let userExist = UserInfo.sharedInfo.getUserInfo(), userExist.userTitle != "Prospect" {
            user = userExist
            self.goToDashBoard()
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK:- --------------Button Action Methods-------------------
    // Go to Sign Up Page
    @IBAction func buttonClicked_Register(_ sender:UIButton) {
        
        sender.animateView()
        
        UserDefaults.standard.set(false, forKey: "SocialUser")
        let signUpVC = self.storyboard?.instantiateViewController(withIdentifier: "SignUpVC") as! SignUpVC
        self.navigationController?.pushViewController(signUpVC, animated: true)
    }
    
    // Go to login page
    @IBAction func buttonClicked_Login(_ sender:UIButton) {
        sender.animateView()
        self.goToLoginVC()
    }
    
    // Go for facebook login
    @IBAction func buttonClicked_Facebook(_ sender:UIButton) {
        sender.animateView()
        socialManager.getFaceboookLoginInfoForView(self) { (result, error) in
            self.handleReponseAndError(result, error)
        }
    }
    
    // Go for google login
    @IBAction func buttonClicked_Google( _ sender:UIButton) {
        sender.animateView()
        socialManager.getGoogleLoginInfoForView(self) { (result, error) in
            self.handleReponseAndError(result, error)
        }
    }

    // MARK:- --------------Private Methods-------------------
    
    // Go to Login View Controller
    func goToLoginVC() {
        let loginVC = self.storyboard?.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
        self.navigationController?.pushViewController(loginVC, animated: true)
    }
    
    // Handle the response after Facebook and Google login
    private func handleReponseAndError(_ result:Any?, _ error:Error?) {
        
        if error == nil {
            if let user = result as? SocialUser {
                self.loginWithSocialUser(user)
                
            } else {
                showAlert("", message: "\(String(describing: result ?? SOMETHING_WRONG))", onView: self)
            }
        } else {
            if let code = error?.code, code == 1 {
                showAlert("", message:"The user canceled the sign-in flow", onView: self)
            }
            showAlert("", message: error?.localizedDescription ?? SOMETHING_WRONG, onView: self)
        }
    }
    
    // If Reponse is successfull the create user
    private func loginWithSocialUser(_ socialUser:SocialUser) {
        
        if let emailId = socialUser.email, emailId != "" {
            
           // startIndicator(self, forFrame: aiFrame, andColor: .white)
            showHud("Processing..")
            
            var fullName = socialUser.firstName ?? ""
            
            if let lastName = socialUser.lastName {
                fullName = "\(fullName) \(lastName)"
            }
            //isSocial
            //Key added 01Sep17
            let params = ["name":fullName,
                          "email":emailId,
                          "password":"",
                          "mobile":"",
                          "isSocial":"true"] as [String : Any]
            
          //  print(params)
            
            let authenticateUser = AuthenticateUser()
            authenticateUser.registerUserWithParameters(params, withCompletion: { (status, response) in
               // self.stopIndicator(self)
                 hideHud()
                if (status == SUCCESS_SOCIAL_PROSPECT || status == SOCIAL_LOGIN_SUCCESS) {
                    UserDefaults.standard.set(true, forKey: "SocialUser")
                }
                if (status == SUCCESS_STATUS || status == DUPLICATE_PROCEED || status == SUCCESS_SOCIAL_PROSPECT) {
                    user = UserInfo.sharedInfo.getUserInfo()
                    let createAccVC = self.storyboard?.instantiateViewController(withIdentifier: "CreateAccVC") as! CreateAccVC
                    self.navigationController?.pushViewController(createAccVC, animated: true)
                }else if (status == SOCIAL_LOGIN_SUCCESS || status == SUCCESS_REGULAR_LOGGEDIN) {
                    user = UserInfo.sharedInfo.getUserInfo()
                    
                    let mfSideMenu = STORYBOARD.instantiateViewController(withIdentifier: "MFSideMenuContainerViewController") as! MFSideMenuContainerViewController
                    
                    let leftMenuVC = STORYBOARD.instantiateViewController(withIdentifier: "LeftMenuVC") as! LeftMenuVC
                    
                    let navVC = STORYBOARD.instantiateViewController(withIdentifier: "MainNavigation") as! UINavigationController
                    
                    APPDELEGATE.navigationVC = navVC
                    APPDELEGATE.mfContainer = mfSideMenu
                    
                    mfSideMenu.leftMenuViewController = leftMenuVC
                    mfSideMenu.centerViewController = navVC
                    if UIDevice.current.userInterfaceIdiom == .pad {
                        mfSideMenu.leftMenuWidth = SCREEN_WIDTH - (150 * getScaleFactor())
                    } else {
                        mfSideMenu.leftMenuWidth = SCREEN_WIDTH - (80 * getScaleFactor())
                    }
                    
                    self.present(mfSideMenu, animated: true, completion: {
                    })

                }else {
                    showAlert("", message:"\(String(describing: response ?? SOMETHING_WRONG))", onView: self)
                }
            })
        } else {
            showAlert("", message:"User email is required and email is not available from your social account", onView: self)
        }
    }
    
    fileprivate func goToDashBoard() {
        let mfSideMenu = STORYBOARD.instantiateViewController(withIdentifier: "MFSideMenuContainerViewController") as! MFSideMenuContainerViewController
        
        let leftMenuVC = STORYBOARD.instantiateViewController(withIdentifier: "LeftMenuVC") as! LeftMenuVC
        
        let navVC = STORYBOARD.instantiateViewController(withIdentifier: "MainNavigation") as! UINavigationController
        
        APPDELEGATE.navigationVC = navVC
        APPDELEGATE.mfContainer = mfSideMenu
        
        mfSideMenu.leftMenuViewController = leftMenuVC
        mfSideMenu.centerViewController = navVC
        navVC.view.backgroundColor = blue1
        if UIDevice.current.userInterfaceIdiom == .pad {
            mfSideMenu.leftMenuWidth = SCREEN_WIDTH - (150 * getScaleFactor())
        } else {
            mfSideMenu.leftMenuWidth = SCREEN_WIDTH - (80 * getScaleFactor())
        }
        
        self.present(mfSideMenu, animated: false, completion: {
            self.goToLoginVC()
        })
    }
}




