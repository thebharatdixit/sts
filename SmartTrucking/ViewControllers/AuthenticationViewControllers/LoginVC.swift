//
//  LoginVC.swift
//  SmartTrucking
//
//  Created by Vivan Raghuvanshi on 19/08/17.
//  Copyright © 2017 Vivan Raghuvanshi. All rights reserved.
//

import UIKit
import Material
import IBAnimatable
import MFSideMenu
import CoreLocation

class LoginVC: UIViewController {
    
    // MARK:- --------------Outlets/Variables-------------------
    
    @IBOutlet weak var tf_Email:TextField!
    @IBOutlet weak var tf_Password:TextField!

    var locManager = CLLocationManager()
    var currentLocation: CLLocation!
    var longitude: String!
    var latitude: String!
    
    // MARK:- --------------View Life Cycle Methods-------------------
    
    override func viewDidLoad() {
        super.viewDidLoad()
       // tf_Email.text = "ravi@mail.com"
       // tf_Password.text = "123456"
        
       // self.goToHomeView()
     locManager.requestWhenInUseAuthorization()
        
        if( CLLocationManager.authorizationStatus() == .authorizedWhenInUse ||
            CLLocationManager.authorizationStatus() ==  .authorizedAlways){
            
            currentLocation = locManager.location
            
        }
        
        longitude = "\(currentLocation.coordinate.longitude)"
        latitude = "\(currentLocation.coordinate.latitude)"
        
        print("LatLong: \(latitude, longitude)")
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK:- --------------Button Action Methods-------------------
    
    @IBAction func buttonClicked_TogglePasswordSecurity(sender: UIButton) {
        sender.animateView()
        self.tf_Password.isSecureTextEntry = !self.tf_Password.isSecureTextEntry
    }
    
    // Go to after login process
    @IBAction func buttonClicked_Login(_ sender:UIButton) {
        sender.animateView()
        UserDefaults.standard.set(false, forKey: "SocialUser")
        self.view.endEditing(true)
        
        if checkIfAllFieldsValid() {
            showHud("Processing..")
            
            let params = ["username":"\(tf_Email.text ?? "")",
                          "password":"\(tf_Password.text ?? "")",
                          "latitude" : "\(latitude ?? "")",
                          "longitude" : "\(longitude ?? "")"
                ] as [String : String]
            
            let authenticateUser = AuthenticateUser()
            authenticateUser.loginWithParameters(params, withCompletion: { (status, response) in
                hideHud()
                if status == SUCCESS_STATUS {
                    
                    user = UserInfo.sharedInfo.getUserInfo()
                    DispatchQueue.main.async {
                        self.goToHomeView()
                    }
                    print(response ?? "Avaneesh")
                } else if status == INCOMPLETE_PROFILE {
                    
                    let okAction = UIAlertAction.init(title: "Ok", style: .default, handler: { action in
                        
                        user = UserInfo.sharedInfo.getUserInfo()
                        let createAccVC = self.storyboard?.instantiateViewController(withIdentifier: "CreateAccVC") as! CreateAccVC
                        self.navigationController?.pushViewController(createAccVC, animated: true)
                    })
                    
                    showAlert("", message: "Your registration is incomplete. Please Sign up completely.", withAction: [okAction], with: false, andTitle: "", onView: self)
                }
                else {
                    showAlert("", message:"\(String(describing: response ?? SOMETHING_WRONG))", onView: self)
                }
            })
        }
    }
    
    // Go baack to sign up page
    @IBAction func buttonClicked_SignUp(_ sender:UIButton) {
        sender.animateView()
        self.navigationController?.popViewController(animated: true)
    }
    
    // If user forgot password show option to enter email and reset pwd
    @IBAction func buttonClicked_ForgotPassword(_ sender:UIButton) {
        sender.animateView()
        self.view.endEditing(true)
        let alertController = UIAlertController(title: "Forgot Password", message: "Please enter your registered email id. We will send you email to reset your password.", preferredStyle: .alert)
        
        let saveAction = UIAlertAction(title: "Submit", style: .destructive, handler: {
            alert -> Void in
            
            let firstTextField = alertController.textFields![0] as UITextField
            
            print("firstName \(firstTextField.text ?? "")")
        })
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: {
            (action : UIAlertAction!) -> Void in
            
        })
        
        alertController.addTextField { (textField : UITextField!) -> Void in
            textField.placeholder = "Email Id"
        }
        
        alertController.addAction(saveAction)
        alertController.addAction(cancelAction)
        
        self.present(alertController, animated: true, completion: nil)
    }
    
    // Run time checking for fields text validity
    @IBAction func textFieldIsTyping_EmailPasswordPhone(_ textField:TextField) {
        
        /*if textField.text?.characters.count == 0{
            textField.detailLabel.text = ""
        }
        
        switch textField {
        case tf_Email:
            if isValidEmail(tf_Email.text ?? "") {
                tf_Email.dividerActiveColor = self.view.tintColor
                tf_Email.detailLabel.text = ""
            } else {
                tf_Email.dividerActiveColor = .red
                tf_Email.detailLabel.text = "Please Enter a Valid Email Id"
            }
            break
        default:
            break
        }*/
    }
    
    //MARK:- ---------- UITextField delegate Methods -----------
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField is TextField {
            let tf = textField as! TextField
            tf.dividerNormalColor = .lightGray
            tf.detailLabel.text = "   "
        }
    }
    
    // MARK:- --------------Private Methods-------------------
    
    //  Check Fields validity
    private func checkIfAllFieldsValid() -> Bool {
        if (isBlankField(tf_Email) || isValidEmail(tf_Email.text ?? "") == false) {
            tf_Email.dividerNormalColor = .red
            tf_Email.detailLabel.text = "Please Enter a Valid Email Id"
            
        }
        if isBlankField(tf_Password) {
            tf_Password.dividerNormalColor = .red
            tf_Password.detailLabel.text = "Please Enter Your Password"
        }
        
        if (tf_Email.dividerNormalColor == .red ||
            tf_Password.dividerNormalColor == .red) {
            return false
        } else {
            return true
        }
    }
    
    private func goToHomeView() {
        let mfSideMenu = STORYBOARD.instantiateViewController(withIdentifier: "MFSideMenuContainerViewController") as! MFSideMenuContainerViewController
        
        let leftMenuVC = STORYBOARD.instantiateViewController(withIdentifier: "LeftMenuVC") as! LeftMenuVC
        
        let navVC = STORYBOARD.instantiateViewController(withIdentifier: "MainNavigation") as! UINavigationController
        
        APPDELEGATE.navigationVC = navVC
        APPDELEGATE.mfContainer = mfSideMenu
        
        mfSideMenu.leftMenuViewController = leftMenuVC
        mfSideMenu.centerViewController = navVC
        navVC.view.backgroundColor = blue1
        if UIDevice.current.userInterfaceIdiom == .pad {
           mfSideMenu.leftMenuWidth = SCREEN_WIDTH - (150 * getScaleFactor())
        } else {
            mfSideMenu.leftMenuWidth = SCREEN_WIDTH - (80 * getScaleFactor())
        }
        
        
        self.present(mfSideMenu, animated: true, completion: {
            self.tf_Email.text = ""
            self.tf_Password.text = ""
        })
    }
}
