//
//  RegisterCompleteVC.swift
//  SmartTrucking
//
//  Created by Vivan Raghuvanshi on 19/08/17.
//  Copyright © 2017 Vivan Raghuvanshi. All rights reserved.
//

import UIKit
import Material
import MFSideMenu

class RegisterCompleteVC: UIViewController {
    
    // MARK:- --------------Outlets/Variables-------------------
    
    @IBOutlet weak var tf_City:TextField!
    @IBOutlet weak var tf_Zip:TextField!
    @IBOutlet weak var tf_State:TextField!
    
    @IBOutlet weak var lbl_Username:UILabel!

    // MARK:- --------------View Life Cycle Methods-------------------
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        lbl_Username.text = "Welcome \(user!.userName!)"
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    // MARK:- --------------Button Action Methods-------------------
    
    @IBAction func buttonClicked_Back(_ sender:UIButton) {
        sender.animateView()
        self.navigationController?.popViewController(animated: true)
    }
    
    // Update user's Information and go to home view
    @IBAction func buttonClicked_Done(_ sender:UIButton) {
        sender.animateView()
        self.view.endEditing(true)
         if checkIfAllFieldsValid() {
            let user = UserInfo.sharedInfo.getUserInfo()
            
            let params = ["city":tf_City.text!,
                          "zip":tf_Zip.text!,
                          "state":tf_State.text!,
                          "profileId":user!.userId!] as [String:Any]
            
            //sender.isEnabled = false
            //startIndicator(self, forFrame: aiFrame, andColor: .white)
            
            showHud("Processing..")
            
            let networkManager = NetworkManager()
            networkManager.getDataForRequest(signUp, andParameter: params) { (response, error) in
                //sender.isEnabled = true
                //self.stopIndicator(self)
                
                hideHud()
                
                if error == nil {
                    if let result = response as? [String:Any] {
                        if let status = result["status"] as? String, status != SUCCESS_STATUS {
                            showAlert("", message:"\(result["message"] ?? "")", onView: self)
                            return
                        }
                        self.goToHomeView()
                    }
                } else {
                    print(error?.localizedDescription ?? "Error")
                }
            }
        }
    }
    
    // Without Updating user's Information go to home view
    @IBAction func buttonClicked_Skip(_ sender:UIButton) {
        sender.animateView()
        self.view.endEditing(true)
        let alertAction = UIAlertAction.init(title: "Skip", style: .destructive) { action in
            
            self.goToHomeView()
        }
        showAlert("", message: "Do you want to skip this page!", withAction: [alertAction], with: true, andTitle: "Cancel", onView: self)
        
        
    }
    
    //MARK:- ---------- UITextField delegate Methods -----------
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField is TextField {
            let tf = textField as! TextField
            tf.dividerNormalColor = .lightGray
            tf.detailLabel.text = "   "
        }
    }
    
    // MARK:- --------------Private Methods-------------------
    
    //  Check Fields validity
    private func checkIfAllFieldsValid() -> Bool {
        if isBlankField(tf_City) {
            tf_City.dividerNormalColor = .red
            tf_City.detailLabel.text = "Please Enter City Name"
        }
        if isBlankField(tf_Zip) {
            tf_Zip.dividerNormalColor = .red
            tf_Zip.detailLabel.text = "Please Enter Zip Code"
        }
        if isBlankField(tf_State) {
            tf_State.dividerNormalColor = .red
            tf_State.detailLabel.text = "Please Enter State Name"
        }
        
        if (tf_City.text ?? "").count < 3 {
            tf_City.dividerNormalColor = .red
            tf_City.detailLabel.text = "Please Enter 3 Characters For City"
        }
        
        if (tf_Zip.text ?? "").count < 5 {
            tf_Zip.dividerNormalColor = .red
            tf_Zip.detailLabel.text = "Please Enter 5 Characters For Zip"
        }
        
        if let zip = tf_Zip.text {
            let zipCode = Double(zip) ?? 0.0
            if zipCode <= 0.0 {
                tf_Zip.dividerNormalColor = .red
                tf_Zip.detailLabel.text = "Please Enter a valid Zip"
            }
        }
        
        if (tf_State.text ?? "").count < 2 {
            tf_State.dividerNormalColor = .red
            tf_State.detailLabel.text = "Please Enter 2 Characters For State"
        }
        
        if (tf_City.dividerNormalColor == .red ||
            tf_Zip.dividerNormalColor == .red ||
            tf_State.dividerNormalColor == .red) {
            return false
        } else {
            return true
        }
    }

    // Show Home View
    private func goToHomeView() {
        
        let mfSideMenu = STORYBOARD.instantiateViewController(withIdentifier: "MFSideMenuContainerViewController") as! MFSideMenuContainerViewController
        
        let leftMenuVC = STORYBOARD.instantiateViewController(withIdentifier: "LeftMenuVC") as! LeftMenuVC
        
        let navVC = STORYBOARD.instantiateViewController(withIdentifier: "MainNavigation") as! UINavigationController
        
        APPDELEGATE.navigationVC = navVC
        APPDELEGATE.mfContainer = mfSideMenu

        mfSideMenu.leftMenuViewController = leftMenuVC
        mfSideMenu.centerViewController = navVC
        
        if UIDevice.current.userInterfaceIdiom == .pad {
            mfSideMenu.leftMenuWidth = SCREEN_WIDTH - (150 * getScaleFactor())
        } else {
            mfSideMenu.leftMenuWidth = SCREEN_WIDTH - (80 * getScaleFactor())
        }
        self.navigationController?.present(mfSideMenu, animated: true) {
            
            let controllers = self.navigationController?.viewControllers
            for vc in controllers! {
                if vc is RegisterOptionsVC {
                    let registerOptionsVC = vc as! RegisterOptionsVC
                self.navigationController?.popToViewController(registerOptionsVC, animated: false)
                    registerOptionsVC.goToLoginVC()
                }
            }
        }
    }

}
