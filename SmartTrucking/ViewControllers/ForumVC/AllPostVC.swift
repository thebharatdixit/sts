//
//  AllPostVC.swift
//  SmartTrucking
//
//  Created by Prachi Pandey on 02/05/19.
//  Copyright © 2019 Vivan Raghuvanshi. All rights reserved.
//

import UIKit

class AllPostVC: UIViewController, allPostDelegate {
   

    @IBOutlet weak var postTableView: UITableView!
    @IBOutlet weak var postCommentBtn: UIButton!
  
    var getAllPostList: [GetAllPost] = []
    var getAllPostObj: GetAllPost?

    
    override func viewDidLoad() {
        super.viewDidLoad()

        getAllPostList = []
          hideHud()
        self.postTableView.contentInset = UIEdgeInsets(top: -20, left: 0, bottom: 0, right: 0);
         postTableView.tableFooterView = UIView()
        // Do any additional setup after loading the view.
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        getAllPostWebService()
        
    }
    
    
    
    @IBAction func postCommentAction(_ sender: UIButton) {

        PostCommentVC.initialization().showAlertWithOkButton(aStrMessage: "This is normal alert message") { (index, title) in
            print(index,title)
        }
        
    }
    
    
    @IBAction func replyAction(_ sender: UIButton) {
        
        getAllPostObj = getAllPostList[sender.tag]
        let storyBoard = UIStoryboard(name:"Main", bundle: nil)
        let vc = storyBoard.instantiateViewController(withIdentifier: "ReplyVC") as!ReplyVC
        vc.replyCommentObj = getAllPostObj
        present(vc, animated: true, completion: nil)
        
        
    }
    
    func refreshAllPostList() {
        
        postTableView.reloadData()
        
    }
    
    
    // MARK: Get All Post Api
    func getAllPostWebService(){
         self.getAllPostList = []
        let params = ["profileId":user?.userId ?? ""]

        showHud("Processing..")

        let networkManager = NetworkManager()
        networkManager.getDataForPostRequest(getAllPost, andParameter: params) { (response, error) in

            if error == nil {
                
                 print(response ?? "")

                if let result = response as? [String:Any] {
                    print(result)
                    if let status = result["status"] as? String, (status == SUCCESS_STATUS) {
                        
                        print("SUCCESS####")
                        
                        if let userDetail = result["_obj"] as? NSArray {

                             print("User Details: \(userDetail)")
                        
                        for items in userDetail{
//
                            self.getAllPostList.append(GetAllPost.init(jsonData: items as! [String : Any]))
                            self.getAllPostObj = GetAllPost.init(jsonData: items as! [String : Any])
//                            print("PostId: \(String(describing: self.postId))")
                            
                        }
                            getMapListArray = self.getAllPostList
                            
                            if self.getAllPostList.count == 0{
                                
                                self.postTableView.setEmptyMessage("No Data Found")
                                
                            }
                            
                      }
                    } else {
                         showAlert("", message:INTERNAL_ERROR_MESSAGE, onView: self)
                    }
                }
                
                self.postTableView.reloadData()
                
            } else {
                   showAlert("", message:error?.localizedDescription ?? SOMETHING_WRONG, onView: self)
            }
             hideHud()
        }
       
    }
    
    
    // MARK: Delete All Post Api
    func deleteAllPostWebService(postId: Int){
        
//        getAllPostObj = getAllPostList[indexPath]
        
        let params = ["profileId":user?.userId ?? "", "postId": "\(postId)"]
        
        print("RequestParameter: \(params)")
        
        showHud("Processing..")
        
        let networkManager = NetworkManager()
        networkManager.getDataForPostRequest(deleteAllPost, andParameter: params) { (response, error) in
          
            if error == nil {
                
                print(response ?? "")
                
                if let result = response as? [String:Any] {
                    print(result)
                    if let status = result["status"] as? String, (status == SUCCESS_STATUS) {
                    
                         self.postTableView.reloadData()
                            
                        }
                    } else {
                        showAlert("", message:INTERNAL_ERROR_MESSAGE, onView: self)
                    }
                }
            
               hideHud()
            
            }
        }
        
}

extension AllPostVC: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return getAllPostList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = postTableView.dequeueReusableCell(withIdentifier: "allPostCell", for: indexPath) as! allPostCell
        getAllPostObj = getAllPostList[indexPath.row]
        cell.tag = indexPath.row
        cell.replyBtn.tag = indexPath.row
        cell.selectionStyle = .none
        cell.titleLbl.text = getAllPostObj?.userName
        cell.descLbl.text = getAllPostObj?.comment
        cell.dateTimeLbl.text = getAllPostObj?.createdDateTime.postDateFormate()
        cell.replyBtn.setTitle("\(getAllPostObj?.numberOfComments ?? 0)" + " Reply >", for: UIControl.State.normal)
//        cell.replyBtn.titleLabel?.text = "\(getAllPostObj?.numberOfComments ?? 0) " + " Reply"

        return cell
        
    }
    
    
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        
        getAllPostObj = getAllPostList[indexPath.row]
        
        let delete = UITableViewRowAction(style: .destructive, title: "Delete") { (action, indexPath) in
            // delete item at indexPath
            
            self.getAllPostList.remove(at: indexPath.row)
            self.postTableView.deleteRows(at: [indexPath],  with: UITableView.RowAnimation.automatic)
            self.deleteAllPostWebService(postId: self.getAllPostObj!.id)
            tableView.reloadData()
            
        }
        
        let hidePost = UITableViewRowAction(style: .destructive, title: "Hide Post") { (action, indexPath) in
            // edit item at indexPath
            print("Hide Post")
            
            tableView.reloadData()
            
        }
        
        hidePost.backgroundColor = blue4
        delete.backgroundColor = lightG
        return [delete, hidePost]
    }
    
    
}

class allPostCell: UITableViewCell{
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var descLbl: UILabel!
    @IBOutlet weak var profilePicImgVw: UIImageView!
    @IBOutlet weak var dateTimeLbl: UILabel!
    @IBOutlet weak var replyBtn: UIButton!
    
}


