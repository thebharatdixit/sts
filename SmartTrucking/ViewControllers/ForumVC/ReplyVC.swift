//
//  ReplyVC.swift
//  SmartTrucking
//
//  Created by Prachi Pandey on 03/05/19.
//  Copyright © 2019 Vivan Raghuvanshi. All rights reserved.
//

import UIKit
import MobileCoreServices
import Photos
import PhotosUI
import AVFoundation

class ReplyVC: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate, AVAudioPlayerDelegate, AVAudioRecorderDelegate{
    
    @IBOutlet weak var messageTblView: UITableView!
    @IBOutlet weak var enterMesgTxtFld: UITextField!
    @IBOutlet weak var sendBtn: UIButton!
    @IBOutlet var heightConstraint: NSLayoutConstraint!
    @IBOutlet var sendCommentView: UIView!
    @IBOutlet weak var headerImageView: UIImageView!
    @IBOutlet weak var headerTitle: UILabel!
    @IBOutlet weak var headerDescLbl: UILabel!
    @IBOutlet weak var headerDateTimeLbl: UILabel!
    @IBOutlet weak var headerRpylBtn: UIButton!
    @IBOutlet weak var viewBehindSelectImage: UIView!
    @IBOutlet weak var galleryImageView: UIImageView!
    @IBOutlet weak var galleryMesgTextField: UITextField!
    
    @IBOutlet weak var viewOpenImage: UIView!
    
    @IBOutlet weak var imgViewChat: UIImageView!
    @IBOutlet weak var btnDownload: UIButton!
    
    
    var nameArray = [String]()
    var desc = [String]()
    var date = [String]()
    var imageURLArray = [[String : Any]]()
    var imgArr = [UIImage]()
    var isPhotoupload:Bool = true
    let imagePicker = UIImagePickerController()
    var isImage:Bool = false
    var recordingSession: AVAudioSession!
    var audioRecorder: AVAudioRecorder!
    var audioPlayer: AVAudioPlayer!
    var isAudioRecordingGranted: Bool!
    var replyCommentObj: GetAllPost?
    var replyPostCommentList : [getReplyPostComment] = []
    var replyPostCommentObj: getReplyPostComment?
    var postId: String?
    var currentDate: String?
    var type: String?
    var imageNameToDownload: String?
    var imageToDownload: UIImage?
    var IsImage = false
    var isTextField = false
    var decodeImage: UIImage!
    var audioUrlBase64: String?
    var isRecording = false
    var isPlaying = false
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        updateHeader()
        imagePicker.delegate = self
        messageTblView.tableFooterView = UIView()
       
        getPostCommentWebService()
        viewBehindSelectImage.isHidden = true
        viewOpenImage.isHidden = true
        
        // To record Audio
        recordingSession = AVAudioSession.sharedInstance()
        
        do {
            try recordingSession.setCategory(.playAndRecord, mode: .default)
            try recordingSession.setActive(true)
            recordingSession.requestRecordPermission() { [unowned self] allowed in
                DispatchQueue.main.async {
                    if allowed {
                        self.isAudioRecordingGranted = true
                        //                        self.loadRecordingUI()
                    } else {
                        self.isAudioRecordingGranted = false
                        // failed to record!
                    }
                }
            }
        } catch {
            // failed to record!
        }
        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
          self.replyPostCommentList = []
    }
    
    
    func updateHeader(){
        
        let fileImage = replyCommentObj?.filePath
        print("fileImage: \(fileImage ?? "")")
        headerImageView.image = UIImage(named: replyCommentObj!.filePath)
        headerImageView.circleWithBorderColor(color: .lightGray, lineWidth: 0.5)
        headerTitle.text = replyCommentObj?.userName
        headerDescLbl.text = replyCommentObj?.comment
        headerRpylBtn.setTitle("\(replyCommentObj?.numberOfComments)", for: .normal)
        headerDateTimeLbl.text = replyCommentObj?.createdDateTime.postDateFormate()
        
    }
    
    @IBAction func btnCrossImgView(_ sender: Any) {
        viewOpenImage.isHidden = true
    }
    
    
    
    @IBAction func btnDownloadAction(_ sender: Any) {
        self.saveImageDocumentDirectory(image: imageToDownload!, imageName: imageNameToDownload!)
    }
    
    @IBAction func crossBtnAction(_ sender: UIButton) {
        
      viewBehindSelectImage.isHidden = true
        
    }
    
    
    // Get document directory path
    func getDirectoryPath() -> NSURL {
        let path = (NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as NSString).appendingPathComponent("SMImages")
        let url = NSURL(string: path)
        return url!
    }
    
    // Save images to document directory
    func saveImageDocumentDirectory(image: UIImage, imageName: String) {
        let fileManager = FileManager.default
        let path = (NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as NSString).appendingPathComponent("SMImages")
        if !fileManager.fileExists(atPath: path) {
            try! fileManager.createDirectory(atPath: path, withIntermediateDirectories: true, attributes: nil)
        }
        let url = NSURL(string: path)
        let imagePath = url!.appendingPathComponent(imageName)
        let urlString: String = imagePath!.absoluteString
        let imageData = image.jpegData(compressionQuality: 0.8)!
        //let imageData = UIImagePNGRepresentation(image)
        fileManager.createFile(atPath: urlString as String, contents: imageData, attributes: nil)
        btnDownload.isHidden = true
    }
    
    @IBAction func mediaAction(_ sender: UIButton) {
        
        let alertController = UIAlertController(title: "", message: "Please select your media source.", preferredStyle: .actionSheet)


        let gallery = UIAlertAction(title: "Choose from Library", style: .default) { action in
            self.imagePicker.allowsEditing = false
            self.imagePicker.sourceType = .photoLibrary
            self.imagePicker.mediaTypes = [kUTTypeMovie as String, kUTTypeImage as String]
            
            self.present(self.imagePicker, animated: true, completion: nil)
        }
        let camera = UIAlertAction(title: "Take a Photo", style: .default) { action in

            if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerController.SourceType.camera)){
                self.imagePicker.allowsEditing = false
                self.imagePicker.sourceType = UIImagePickerController.SourceType.camera
                self.imagePicker.mediaTypes = [kUTTypeMovie as String, kUTTypeImage as String]
                self.present(self.imagePicker, animated: true, completion: nil)
            }else{
                let alert = UIAlertController(title: "Camera Not Found", message: "This device has no Camera", preferredStyle: .alert)
                let ok = UIAlertAction(title: "OK", style:.default, handler: nil)
                alert.addAction(ok)
                self.present(alert, animated: true, completion: nil)
            }
        }
        let CancelAction = UIAlertAction(title: "Cancel", style: .cancel) { action in
            //            print("You've pressed Cancel Button")

        }

      
        alertController.addAction(camera)
        alertController.addAction(gallery)
        alertController.addAction(CancelAction)

        //alertController.view.tintColor = Common.Global.kBGCOLOR

        self.present(alertController, animated: true, completion: nil)
        
        
    }
    
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        
        if let pickedImage = info[.originalImage] as? UIImage {
            isImage = true
            let imageData = pickedImage.fixOrientation()
            imageURLArray.append(["type" : "image", "url" : imageData.jpegData(compressionQuality: 0.8)!])
            imgArr.append(pickedImage)
            type = "Image"
            IsImage = true
            viewBehindSelectImage.isHidden = false
            galleryImageView.image = pickedImage
            decodeImage = pickedImage
            
        }else  if  (kUTTypeMovie as? String) != nil {
            isImage = false
            let  urlOfVideo = (info[UIImagePickerController.InfoKey.mediaURL] as? NSURL)!
            debugPrint(urlOfVideo)
            do {
                
                let videoData = try Data(contentsOf: (urlOfVideo as URL))
                
                imageURLArray.append(["type" : "video", "data" : videoData])
            }catch  {
                print("exception catch at block - while uploading video")
            }
         
        }
        
        messageTblView.reloadData()
        
        picker.dismiss(animated: true, completion: nil)
    }
    
    func convertImageToBase64(image: UIImage) -> String {
        let imageData = image.pngData()!
        return imageData.base64EncodedString(options: Data.Base64EncodingOptions.lineLength64Characters)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "selectedImageVC") as! selectedImageVC
        self.navigationController?.present(vc, animated: false, completion: nil)
        picker.dismiss(animated: true, completion: nil)
    }
    
    func startRecording() {
        
        let audioFilename = getDocumentsDirectory().appendingPathComponent("recording.m4a")
        
        let settings = [
            AVFormatIDKey: Int(kAudioFormatMPEG4AAC),
            AVSampleRateKey: 12000,
            AVNumberOfChannelsKey: 1,
            AVEncoderAudioQualityKey: AVAudioQuality.high.rawValue
        ]
        
        do {
            audioRecorder = try AVAudioRecorder(url: audioFilename, settings: settings)
            audioRecorder.delegate = self
            audioRecorder.record()
            type = "Audio"
//            sendBtn.setImage(UIImage(named: "pause-button"), for: .normal)
        } catch {
            finishRecording(success: false)
        }
    }
    
    func getDocumentsDirectory() -> URL {
        
        let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        let pathsStr = String(describing: paths[0])
        print("pathsStr: \(pathsStr)")
        let data = (pathsStr).data(using: String.Encoding.utf8)
        let base64 = data!.base64EncodedString(options: NSData.Base64EncodingOptions(rawValue: 0))
        audioUrlBase64 = base64
//        print("base64: \(base64)")// dGVzdDEyMw==\n
        return paths[0]
    }
    
    func finishRecording(success: Bool) {
        audioRecorder.stop()
        audioRecorder = nil
        
        if success {
//            sendBtn.setTitle("record", for: .normal)
        } else {
//            sendBtn.setTitle("Record", for: .normal)
            // recording failed :(
        }
    }
    
    @IBAction func recordTapped(_ sender: Any) {
        if audioRecorder == nil {
            startRecording()
        } else {
            finishRecording(success: true)
        }
    }
    
    func getFileUrl() -> URL
    {
        let filename = "recording.m4a"
        let filePath = getDocumentsDirectory().appendingPathComponent(filename)
        return filePath
    }
    
    func prepare_play()
    {
        do
        {
            audioPlayer = try AVAudioPlayer(contentsOf: getFileUrl())
            audioPlayer.delegate = self
            audioPlayer.prepareToPlay()
        }
        catch{
            print("Error")
        }
    }
    
    func audioRecorderDidFinishRecording(_ recorder: AVAudioRecorder, successfully flag: Bool) {
        if !flag {
            finishRecording(success: false)
        }
    }
    
    func audioPlayerDidFinishPlaying(_ player: AVAudioPlayer, successfully flag: Bool)
    {
//        btnRecord.isEnabled = true
    }
    
    
    @IBAction func playBtnAction(_ sender: UIButton) {
        
        replyPostCommentObj = replyPostCommentList[sender.tag]
        let indexPath = NSIndexPath(row:sender.tag, section:0)
        let cell = messageTblView.cellForRow(at: indexPath as IndexPath) as! replyCell
        
//        cell.playAudioBtn.setImage(UIImage(named: "pause-button"), for: .normal)
//        let audioData = Data(base64Encoded: replyPostCommentObj!.filePath, options: .ignoreUnknownCharacters)
//        let player = try? AVAudioPlayer(data: audioData!)
//        player?.prepareToPlay()
//        player?.play()
//        isPlaying = true
        
      
        if(isPlaying)
        {
            audioPlayer.stop()
            isPlaying = false
            cell.playAudioBtn.setImage(UIImage(named: "play"), for: .normal)
        }
        else
        {

                let audioData = Data(base64Encoded: replyPostCommentObj!.filePath, options: .ignoreUnknownCharacters)
                audioPlayer = try? AVAudioPlayer(data: audioData!)
                cell.playAudioBtn.setImage(UIImage(named: "pause-button"), for: .normal)
                audioPlayer?.prepareToPlay()
                audioPlayer?.play()
                isPlaying = true

        }
        
    }
    
    
    @IBAction func sendBtnAction(_ sender: UIButton) {
        
        self.view.endEditing(true)

        
        if sendBtn.currentImage == UIImage(named: "record_audio_ic") && sender.tag == 0{
            
            if audioRecorder == nil {
                startRecording()
                 createPostCommentWebService(type: type, filePath: audioUrlBase64, fileName: "audio file\(sender.tag)")
                 messageTblView.reloadData()
            } else {
                finishRecording(success: true)
            }
            
            return
            
        }
        
        if sendBtn.currentImage == UIImage(named: "send_msg_ic") && sender.tag == 0 {
            
            print("Send: \(sender.tag)")
            
            createPostCommentWebService(type: type, filePath: "", fileName: "")
            
            messageTblView.reloadData()
            
            return
            
        }else{
            
            print(sender.tag)
            
            if isTextField == true && IsImage == true{
                
                let imageData = decodeImage.pngData()!
                let decodeImageToBase64 =  imageData.base64EncodedString(options: Data.Base64EncodingOptions.lineLength64Characters)
                createPostCommentWebService(type: type, filePath: decodeImageToBase64, fileName: "\(sender.tag)")
                
                return
                
            }else{
                
                let imageData = decodeImage.pngData()!
                let decodeImageToBase64 =  imageData.base64EncodedString(options: Data.Base64EncodingOptions.lineLength64Characters)
                createPostCommentWebService(type: type, filePath: decodeImageToBase64, fileName: "\(sender.tag)")

                
            }
            
        }
        
        
    }
    
    
    
    @IBAction func cancelBtnAction(_ sender: UIButton) {
        
        dismiss(animated: true, completion: nil)
        
    }
    
    //MARK:- ------------- UITextFieldDelegate Methods ----------
    
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        sendBtn.setImage(UIImage(named: "send_msg_ic"), for: .normal)
        if textField == enterMesgTxtFld{
            
             type = "Text"
            return
            
        }else if textField == galleryMesgTextField{
            
            type = "Both"
            isTextField = true
             return
        }
        
        
        
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        if textField.text!.isEmpty{
              sendBtn.setImage(UIImage(named: "record_audio_ic"), for: .normal)
        }
        
    }
    
    override func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder();
        print("textFieldShouldReturn")
        return true;
    }
    

    
    // MARK: Create Post Comment Api
    func createPostCommentWebService(type: String?, filePath: String?, fileName: String?){
    
        
        guard (enterMesgTxtFld != nil) else {
            return
        }
        showHud("Processing..")
        var comment = ""
        
        if isTextField == true{
            
            comment = galleryMesgTextField.text as! String
            
        }else{
            
            comment = enterMesgTxtFld.text as! String
        }
        
        
        let params = ["profileId":user?.userId ?? "",
                      "comment": comment,
                      "type": type,
                      "filePath": filePath,
                      "postId": "\(replyCommentObj?.id ?? 0)",
                      "createdDateTime": currentDate!,
                      "fileName": ""
        ] as [String : Any]
        
        print("Response: \(params)")
        
       
        
        let networkManager = NetworkManager()
        networkManager.getDataForPostRequest(createPostComment, andParameter: params) { (response, error) in
            
            if error == nil {
                
//                print(response ?? "")
                
                if let result = response as? [String:Any] {
//                    print(result)
                    if let status = result["status"] as? String, (status == SUCCESS_STATUS) {
                        
                        print("SUCCESS####")
                           self.enterMesgTxtFld.text = ""
                         self.galleryMesgTextField.text = ""
                        self.viewBehindSelectImage.isHidden = true
                       
                    } else {
                        showAlert("", message:INTERNAL_ERROR_MESSAGE, onView: self)
                    }
                }
                
                self.messageTblView.reloadData()
                
            } else {
                showAlert("", message:error?.localizedDescription ?? SOMETHING_WRONG, onView: self)
            }
            hideHud()
        }
        
    }
    
    
    // MARK: Get Post Comment Api
    func getPostCommentWebService(){
        
        
        let params = ["profileId":user?.userId ?? "", "postId": "\(replyCommentObj?.id ?? 0)"]
        
        
        print("Response: \(params)")
        
        showHud("Processing..")
        
        let networkManager = NetworkManager()
        networkManager.getDataForPostRequest(getAllPostComment, andParameter: params) { (response, error) in
            
            if error == nil {
                
                print(response ?? "")
                
                if let result = response as? [String:Any] {
                    print(result)
                    if let status = result["status"] as? String, (status == SUCCESS_STATUS) {
                        
                        print("SUCCESS####")
                        
                        if let userDetail = result["_obj"] as? NSArray {
                            
                            print("User Details: \(userDetail)")
                            
                            for items in userDetail{
                                //
                                self.replyPostCommentList.append(getReplyPostComment.init(jsonData: items as! [String : Any]))
                                self.replyPostCommentObj = getReplyPostComment.init(jsonData: items as! [String : Any])
                                
                            }
                            
                            if self.replyPostCommentList.count == 0{
                                
                                self.messageTblView.setEmptyMessage("No Data Found")
                                
                            }
                            
                        }
                        
                        self.messageTblView.reloadData()
                        
                    } else {
                        showAlert("", message:INTERNAL_ERROR_MESSAGE, onView: self)
                    }
                }
                
            } else {
                showAlert("", message:error?.localizedDescription ?? SOMETHING_WRONG, onView: self)
            }
            hideHud()
        }
        
    }
    
}


extension ReplyVC: UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return replyPostCommentList.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var cell:replyCell
        
        // Get Current Date
        let date = Date()
        let formattor = DateFormatter()
        formattor.dateFormat = "dd/MM/yyyy HH:mm:ss"
        currentDate = formattor.string(from: date)
        debugPrint("DateTime: \(currentDate ?? "")")
        
       
        let clearView = UIView()
        clearView.backgroundColor = UIColor.clear// Whatever color you like
            
            replyPostCommentObj = replyPostCommentList[indexPath.row]
            if  replyPostCommentObj?.type == "Audio"  {
                
                cell = messageTblView.dequeueReusableCell(withIdentifier: "playAudioCell", for: indexPath) as! replyCell
                cell.playAudioConfigureCell(jsonData: replyPostCommentObj!, currentDateTime: currentDate)
                cell.playAudioBtn.tag = indexPath.row
                cell.selectionStyle = .none
                cell.selectedBackgroundView = clearView
                return cell
                
            }else if replyPostCommentObj?.type == "Both" || replyPostCommentObj?.type == "Image"{
                cell = messageTblView.dequeueReusableCell(withIdentifier: "galleryCell", for: indexPath) as! replyCell
                cell.galleryConfigureCell(jsonData: replyPostCommentObj!, currentDateTime: currentDate)
                cell.selectionStyle = .none
                cell.selectedBackgroundView = clearView
                return cell
            }
            else
            {
                cell = messageTblView.dequeueReusableCell(withIdentifier: "replyCell", for: indexPath) as! replyCell
                cell.configureCell(jsonData: replyPostCommentObj!, currentDateTime: currentDate)
                cell.selectionStyle = .none
                cell.selectedBackgroundView = clearView
                return cell
            }
    }
    
        
   
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        replyPostCommentObj = replyPostCommentList[indexPath.row]
        imageNameToDownload = replyPostCommentList[indexPath.row].fileName
        if replyPostCommentObj?.type == "Both" || replyPostCommentObj?.type == "Image"{
            if let decodedData = Data(base64Encoded: replyPostCommentList[indexPath.row].filePath, options: .ignoreUnknownCharacters) {
                
                print("decodedData: \(decodedData)")
                imageToDownload = UIImage(data: decodedData)
                imgViewChat.image = UIImage(data: decodedData)
                
            }
            viewOpenImage.isHidden = false
        }
        
        let fileManager = FileManager.default
        let path = (NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as NSString).appendingPathComponent("SMImages/\(imageNameToDownload ?? "")")
        
        if !fileManager.fileExists(atPath: path) {
            btnDownload.isHidden = false
        }
        else{
            btnDownload.isHidden =  true
        }
        
    }
    
  
    
}

// MARK: - TextField Delegate Methods

class replyCell: UITableViewCell {
    
    @IBOutlet weak var profilePic: UIImageView!
    @IBOutlet weak var titleLbl: UIButton!
    @IBOutlet weak var descLbl: UILabel!
    @IBOutlet weak var dateLbl: UILabel!
    @IBOutlet weak var imgVw: UIImageView!
    @IBOutlet weak var playAudioBtn: UIButton!
    
    
    func configureCell(jsonData:getReplyPostComment, currentDateTime: String?){
        
        titleLbl.setTitle(jsonData.userName, for: .normal)
        descLbl.text = jsonData.comment
        dateLbl.text = currentDateTime?.postDateFormate()

        
    }
    
    func galleryConfigureCell(jsonData:getReplyPostComment, currentDateTime: String?){
        
        titleLbl.setTitle(jsonData.userName, for: .normal)
        descLbl.text = jsonData.comment
        dateLbl.text = currentDateTime?.postDateFormate()
        
        if let decodedData = Data(base64Encoded: jsonData.filePath, options: .ignoreUnknownCharacters) {
            
//            print("decodedData: \(decodedData)")
            imgVw.image = UIImage(data: decodedData)
            
        }
        
        
    }
    
    func playAudioConfigureCell(jsonData:getReplyPostComment, currentDateTime: String?){
        
        titleLbl.setTitle(jsonData.userName, for: .normal)
        dateLbl.text = currentDateTime?.postDateFormate()
        playAudioBtn.setTitle(jsonData.fileName, for: .normal)
        
        if let decodedData = Data(base64Encoded: jsonData.filePath, options: .ignoreUnknownCharacters) {

            print("decodedData: \(decodedData)")
//            playAudioBtn.setTitle("play", for: .normal)

        }

        
    }
    
    
}

