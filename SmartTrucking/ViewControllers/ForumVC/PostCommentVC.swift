//
//  PostCommentVC.swift
//  SmartTrucking
//
//  Created by Prachi Pandey on 03/05/19.
//  Copyright © 2019 Vivan Raghuvanshi. All rights reserved.
//

import UIKit
import MobileCoreServices
import Photos
import PhotosUI
import AVFoundation


protocol allPostDelegate {
    
    func refreshAllPostList()
    
}

class PostCommentVC: UIViewController, UINavigationControllerDelegate, UIImagePickerControllerDelegate {

    // MARK:- --------------Outlets/Variables-------------------
    @IBOutlet weak var backgroundView: UIView!
    /// AlertController Completion handler
    typealias alertCompletionBlock = ((Int, String) -> Void)?
    private var block : alertCompletionBlock?
    var imageURLArray = [[String : Any]]()
    var imgArr = [UIImage]()
    var isPhotoupload:Bool = true
    let imagePicker = UIImagePickerController()
    var delegate: allPostDelegate?
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var comentBoxTxtView: UITextView!
    @IBOutlet weak var countLbl: UILabel!
    
    
    static func initialization() -> PostCommentVC
    {
        let storyboard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let vc : PostCommentVC = storyboard.instantiateViewController(withIdentifier: "PostCommentVC") as! PostCommentVC

        
        return vc
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
          initUI()
          setupAJAlertController()
          backgroundView.layer.cornerRadius = 30.0
          imagePicker.delegate = self
    }
    
    func initUI(){
        
         comentBoxTxtView.addDoneButtonOnKeyboard()
         comentBoxTxtView.text = "Type.."
         comentBoxTxtView.textColor = UIColor.lightGray
         comentBoxTxtView.layer.cornerRadius = 20.0
        
    }
    
    @IBAction func cameraAction(_ sender: UIButton) {
        
            let alertController = UIAlertController(title: "", message: "", preferredStyle: .actionSheet)
            
            
            let gallery = UIAlertAction(title: "Choose from Library", style: .default) { action in
                self.imagePicker.allowsEditing = false
                self.imagePicker.sourceType = .photoLibrary
                self.imagePicker.mediaTypes = [kUTTypeMovie as String, kUTTypeImage as String]
                
                self.present(self.imagePicker, animated: true, completion: nil)
            }
            let camera = UIAlertAction(title: "Take a Photo", style: .default) { action in
                
                if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerController.SourceType.camera)){
                    self.imagePicker.allowsEditing = false
                    self.imagePicker.sourceType = UIImagePickerController.SourceType.camera
                    self.imagePicker.mediaTypes = [kUTTypeMovie as String, kUTTypeImage as String]
                    self.present(self.imagePicker, animated: true, completion: nil)
                }else{
                    let alert = UIAlertController(title: "Camera Not Found", message: "This device has no Camera", preferredStyle: .alert)
                    let ok = UIAlertAction(title: "OK", style:.default, handler: nil)
                    alert.addAction(ok)
                    self.present(alert, animated: true, completion: nil)
                }
            }
            let CancelAction = UIAlertAction(title: "Cancel", style: .cancel) { action in
                //            print("You've pressed Cancel Button")
                
            }
            
        
            alertController.addAction(camera)
            alertController.addAction(gallery)
            alertController.addAction(CancelAction)
            
            //alertController.view.tintColor = Common.Global.kBGCOLOR
            
            self.present(alertController, animated: true, completion: nil)
        
        
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        if let pickedImage = info[.originalImage] as? UIImage {
            let imageData = pickedImage.fixOrientation()
            imageURLArray.append(["type" : "image", "url" :imageData.jpegData(compressionQuality: 0.8)!])
            
             imgArr.append(pickedImage)
             collectionView.reloadData()
            
        }
       
        picker.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
    
    
    
    /// Inital View Setup
    
    private func setupAJAlertController(){
        
        let visualEffectView   = UIVisualEffectView(effect: UIBlurEffect(style: .dark))
        visualEffectView.alpha = 0.8
        visualEffectView.frame = self.view.bounds
        visualEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        visualEffectView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(backgroundViewTapped)))
        self.view.insertSubview(visualEffectView, at: 0)
        
        backgroundView.layer.cornerRadius  = 6.0
        backgroundView.layer.shadowOffset  = CGSize(width: 0.0, height: 0.0)
        backgroundView.layer.shadowColor   = UIColor(white: 0.0, alpha: 1.0).cgColor
        backgroundView.layer.shadowOpacity = 0.3
        backgroundView.layer.shadowRadius  = 3.0
        let border = CALayer()
        let width = CGFloat(0.5)
        border.borderColor = UIColor.darkGray.cgColor
        
        
    }
    
    /// Hide Alert Controller on background tap
    @objc func backgroundViewTapped(sender:AnyObject)
    {
        hide()
    }
    
    
    @IBAction func cancelBtnAction(_ sender: UIButton) {
        
        hide()
        
    }
    
    
    @IBAction func postBtnAction(_ sender: UIButton) {
        
        // Get Current Date
        let date = Date()
        let formattor = DateFormatter()
        formattor.dateFormat = "dd/MM/yyyy HH:mm:ss"
        let currentDate = formattor.string(from: date)
        debugPrint("DateTime: \(currentDate)")
       
        
        createPostWebService(dateTime: currentDate)
        
    }
    
    func createPostWebService(dateTime: String){
        
        let params = ["profileId":user!.userId!,
                      "comment":comentBoxTxtView.text ?? "",
                      "filePath": "",
                      "fileName": "",
                      "type": "Text",
                      "createdDateTime": dateTime]
        
        showHud("Processing..")
        
        let networkManager = NetworkManager()
        networkManager.getDataForPostRequest(createPost, andParameter: params) { (response, error) in
            
          
           
            if error == nil {
                
                print("Response: \(response ?? "")")
                
                if let result = response as? [String:Any] {
                    if let status = result["status"] as? String, (status == SUCCESS_STATUS) {
                        
                        let alert = UIAlertController(title: "", message: "\(result["message"] ?? "")", preferredStyle: .alert)
                        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
                            
                            self.hide()
                            
                        }))
                        self.present(alert, animated: true, completion: nil)
                        
                        return
                    }else
                    {
//                        showAlert("", message:INTERNAL_ERROR_MESSAGE, onView: self)
                         showAlert("", message:"\(result["message"] ?? "")", onView: self)
                    }
                }
            } else {
                showAlert("", message:error?.localizedDescription ?? SOMETHING_WRONG, onView: self)
            }
            
             hideHud()
        }
    }
    
    
    
    /// Create and Configure Alert Controller
    private func configure(message:String, btnCancelTitle:String?, btnOtherTitle:String)
    {
    
    }
    
    /// Show Alert Controller
    private func show()
    {
        if let appDelegate = UIApplication.shared.delegate, let window = appDelegate.window, let rootViewController = window?.rootViewController {
            
            var topViewController = rootViewController
            while topViewController.presentedViewController != nil {
                topViewController = topViewController.presentedViewController!
            }
            
            topViewController.addChild(self)
            topViewController.view.addSubview(view)
            viewWillAppear(true)
            didMove(toParent: topViewController)
            view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
            view.alpha = 0.0
            view.frame = topViewController.view.bounds
            
            backgroundView.alpha     = 0.0
            UIView.animate(withDuration: 0.2, delay: 0.0, options: .curveEaseOut, animations: { () -> Void in
                self.view.alpha = 1.0
            }, completion: nil)
            
            backgroundView.transform = CGAffineTransform(scaleX: 0.1, y: 0.1)
            backgroundView.center    = CGPoint(x: (self.view.bounds.size.width/2.0), y: (self.view.bounds.size.height/2.0)-10)
            UIView.animate(withDuration: 0.2 , delay: 0.1, options: .curveEaseOut, animations: { () -> Void in
                self.backgroundView.alpha = 1.0
                self.backgroundView.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
                self.backgroundView.center    = CGPoint(x: (self.view.bounds.size.width/2.0), y: (self.view.bounds.size.height/2.0))
            }, completion: nil)
        }
    }
    /// Hide Alert Controller
    private func hide()
    {
        self.view.endEditing(true)
        self.backgroundView.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
        UIView.animate(withDuration: 0.2, delay: 0.0, options: .curveEaseIn, animations: { () -> Void in
            self.backgroundView.alpha = 0.0
            self.backgroundView.transform = CGAffineTransform(scaleX: 0.6, y: 0.6)
            self.backgroundView.center    = CGPoint(x: (self.view.bounds.size.width/2.0), y: (self.view.bounds.size.height/2.0)-5)
        }, completion: nil)
        
        UIView.animate(withDuration: 0.2, delay: 0.05, options: .curveEaseIn, animations: { () -> Void in
            self.view.alpha = 0.0
            
        }) { (completed) -> Void in
            
            self.view.removeFromSuperview()
            self.removeFromParent()
        }
    }
    public func showAlertWithOkButton( aStrMessage:String,
                                       completion : alertCompletionBlock){
        
        configure(message: aStrMessage, btnCancelTitle: nil, btnOtherTitle: "")
        show()
        block = completion
    }
    
}

extension PostCommentVC : UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout
{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return imgArr.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell:postCommentCell

            cell  = collectionView.dequeueReusableCell(withReuseIdentifier: "postCommentCell", for: indexPath) as! postCommentCell
            cell.imageView.image = imgArr[indexPath.row]
            cell.imageView?.contentMode = .scaleAspectFill
            cell.setNeedsLayout()
        
            cell.layer.cornerRadius = 4
        
            return cell
        
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize{
        return CGSize.init(width: 80, height: 80)
    }
}

extension PostCommentVC: UITextViewDelegate{
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == UIColor.lightGray {
            textView.text = nil
            textView.textColor = UIColor.black
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = "Type.."
            textView.textColor = UIColor.lightGray
        }
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        let newText = (textView.text as NSString).replacingCharacters(in: range, with: text)
        let textCount = 250 - newText.count
        if newText.count <= 250 {
            countLbl.text =  "\(textCount)"
        }
        
        return newText.count <= 250
    }
    
}

extension UIImage {
    func fixOrientation() -> UIImage {
        if self.imageOrientation == UIImage.Orientation.up {
            return self
        }
        UIGraphicsBeginImageContextWithOptions(self.size, false, self.scale)
        self.draw(in: CGRect(x: 0, y: 0, width: self.size.width, height: self.size.height))
        if let normalizedImage: UIImage = UIGraphicsGetImageFromCurrentImageContext() {
            UIGraphicsEndImageContext()
            return normalizedImage
        } else {
            return self
        }
    }
}

