//
//  GalleryVC.swift
//  SmartTrucking
//
//  Created by Prachi Pandey on 02/05/19.
//  Copyright © 2019 Vivan Raghuvanshi. All rights reserved.
//

import UIKit

class GalleryVC: UIViewController {
    
    @IBOutlet weak var collectionView: UICollectionView!
    
    
    var marrImageData = [String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.getAllImages()
    }
    
    
    // get All Images From DocumentDirectory
    func getAllImages()
    {
        let nsDocumentDirectory = FileManager.SearchPathDirectory.documentDirectory
        let nsUserDomainMask    = FileManager.SearchPathDomainMask.userDomainMask
        let paths               = NSSearchPathForDirectoriesInDomains(nsDocumentDirectory, nsUserDomainMask, true)
        
        if let dirPath = paths.first
        {
            let imageURL = URL(fileURLWithPath: dirPath).appendingPathComponent("SMImages")
            
            do {
                marrImageData = try FileManager.default.contentsOfDirectory(atPath: imageURL.path)
                print("\(marrImageData)")
                collectionView.reloadData()
            } catch let error as NSError {
                print(error.localizedDescription)
                
            }
        }
    }
    
    
}


extension GalleryVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return marrImageData.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "galleryImagesCell", for: indexPath) as! galleryImagesCell
        let pathF = (NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as NSString).appendingPathComponent("SMImages/\(marrImageData[indexPath.row])")
        let imageURL = URL(fileURLWithPath: pathF)
        let image    = UIImage(contentsOfFile: imageURL.path)
        cell.imgViewGallery.image = image
        return cell
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        //        return CGSize(width:(collectionView.frame.height-90)/2, height: 100)
        
        return getCellHeaderSize(Width: self.view.frame.width / 2, aspectRatio: 177/213, padding: 6.5)
    }
    
    
}

class galleryImagesCell: UICollectionViewCell{
    @IBOutlet weak var imgViewGallery: UIImageView!
}
