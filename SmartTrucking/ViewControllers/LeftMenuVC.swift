//
//  LeftMenuVC.swift
//  SmartTrucking
//
//  Created by Vivan Raghuvanshi on 18/09/17.
//  Copyright © 2017 Vivan Raghuvanshi. All rights reserved.
//

import UIKit
import MFSideMenu

class LeftMenuVC: UIViewController {
    
    // MARK:- --------------Outlets/Variables-------------------
    // When user is a contractor
    let array_Menu = ["Your Trips","Add Expenses","Monthly Expenses","Monthly Report","Forum","Share/Invite Friends"]
    // When user is a Driver
    var array_MenuDriver = ["Your Trips","Add Expenses","Monthly Report", "Share/Invite Friends"]

    @IBOutlet var table_View: UITableView!
    @IBOutlet weak var lbl_Username:UILabel!
    @IBOutlet weak var img_View:UIImageView!

    // MARK:- -------------- View Life Cycle Methods-------------------
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Set table header/footer
        let HEADER_HEIGHT = (170 * getScaleFactor())
        table_View.tableHeaderView?.frame.size = CGSize(width: table_View.frame.width, height: CGFloat(HEADER_HEIGHT))
        table_View.tableFooterView = UIView()
        
        //Get image if it saved already
        let imgPath = getDirectoryPath()
        if let imgExist = UIImage.init(contentsOfFile: imgPath) {
            img_View.image = imgExist
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        lbl_Username.text = user!.userName!
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    // MARK:- -------------- UIButton Action Methods-------------------
    
    @IBAction func buttonClicked_LegalPolicy(_ sender:UIButton) {
        var viewController:UIViewController?
        if (APPDELEGATE.mfContainer?.centerViewController as? PrivacyViewController) == nil {
            let privacyViewController = self.storyboard?.instantiateViewController(withIdentifier: "PrivacyViewController") as! PrivacyViewController
            viewController = privacyViewController
        }
        if let centreVC = viewController {
            if self.menuContainerViewController.centerViewController as? UIViewController != centreVC {
                self.menuContainerViewController.centerViewController = centreVC
            }
        }
        self.menuContainerViewController.setMenuState(MFSideMenuStateClosed, completion: nil)

    }
    
    // Go to user profile information/Edit information
    @IBAction func buttonClicked_ProfileInfo(_ sender:UIButton) {
        sender.animateView()
        if (APPDELEGATE.mfContainer?.centerViewController as? UIViewController) != nil {
            let editProfileOptionVC = self.storyboard?.instantiateViewController(withIdentifier:"EditProfileOptionVC") as! EditProfileOptionVC
            let editNavVC = UINavigationController.init(rootViewController: editProfileOptionVC)
            editNavVC.navigationBar.isHidden = true
            if self.menuContainerViewController.centerViewController as? UIViewController != editNavVC {
                self.menuContainerViewController.centerViewController = editNavVC
            }
            self.menuContainerViewController.setMenuState(MFSideMenuStateClosed, completion: nil)
        }
    }
}

// MARK:- --------UITableViewDataSource Methods-----------

extension LeftMenuVC : UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int  {
        if user!.userTitle == Title.Driver.rawValue {
            return array_MenuDriver.count
        } else {
            return array_Menu.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let identifier = "LeftMenuCell"
        
        let leftMenuCell = tableView.dequeueReusableCell(withIdentifier:identifier)
        let cellLbl = leftMenuCell?.contentView.viewWithTag(101) as! UILabel
        if user!.userTitle == Title.Driver.rawValue {
            cellLbl.text = array_MenuDriver[indexPath.row]
        } else {
            cellLbl.text = array_Menu[indexPath.row]
        }
        leftMenuCell?.selectionStyle = .none
        return leftMenuCell!
    }
}

// MARK:- --------------UITableViewDelegate Methods-------------------

extension LeftMenuVC : UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50 * getScaleFactor()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        var viewController:UIViewController?
        // did select changed by whether the user is driver or owner
        // and for driver wether the monthly tab is there or not
        // it is checked by the count of array_MenuDriver
        if user!.userTitle == Title.Driver.rawValue {
            switch indexPath.row {
            case 0:
                // Go to trip history for both driver/contractor
                if (APPDELEGATE.mfContainer?.centerViewController as? YourTripsVC) == nil {
                    let yourTripsVC = self.storyboard?.instantiateViewController(withIdentifier: "YourTripsVC") as! YourTripsVC
                    let yourTripNavVC = UINavigationController.init(rootViewController: yourTripsVC)
                    yourTripNavVC.navigationBar.isHidden = true
                    viewController = yourTripNavVC
                }
                break
            case 1:
                // Go to add expense if any trip is on going otherwise show alert
                if let center = APPDELEGATE.mfContainer?.centerViewController as? UIViewController {
                    
                    if let currentVC = APPDELEGATE.navigationVC?.viewControllers.last {
                        if currentVC is HomeVC {
                            let homeVC = currentVC as! HomeVC
                            
                            if let existingTripId = homeVC.tripId  {
                                let addExpensesVC = self.storyboard?.instantiateViewController(withIdentifier: "AddExpensesVC") as! AddExpensesVC
                                addExpensesVC.tripId = existingTripId
                                viewController = addExpensesVC
                                
                            } else {
                                showAlert("", message: "There is no ongoing trip", onView:center)
                            }
                        }
                    }
                }
                break
            case 2:
                if (APPDELEGATE.mfContainer?.centerViewController as? ExpensesReportVC) == nil {
                    let expensesReportVC = self.storyboard?.instantiateViewController(withIdentifier: "ExpensesReportVC") as! ExpensesReportVC
                    let expensesReportNavVC = UINavigationController.init(rootViewController: expensesReportVC)
                    expensesReportNavVC.navigationBar.isHidden = true
                    
                    viewController = expensesReportNavVC
                }
                break
            case 3:
                if let center = APPDELEGATE.mfContainer?.centerViewController as? UIViewController {
                    showActivityViewToShare(center)
                }
                break
                
//            case 4:
//                if (APPDELEGATE.mfContainer?.centerViewController as? PrivacyViewController) == nil {
//                    let privacyViewController = self.storyboard?.instantiateViewController(withIdentifier: "PrivacyViewController") as! PrivacyViewController
//                    viewController = privacyViewController
//                }
//                break
                
            default:
                break
            }
        } else {
            switch indexPath.row {
            case 0:
                // Go to trip history for both driver/contractor
                if (APPDELEGATE.mfContainer?.centerViewController as? YourTripsVC) == nil {
                    let yourTripsVC = self.storyboard?.instantiateViewController(withIdentifier: "YourTripsVC") as! YourTripsVC
                    let yourTripNavVC = UINavigationController.init(rootViewController: yourTripsVC)
                    yourTripNavVC.navigationBar.isHidden = true
                    viewController = yourTripNavVC
                }
                break
            case 1:
                // Go to add expense if any trip is on going otherwise show alert
                if let center = APPDELEGATE.mfContainer?.centerViewController as? UIViewController {
                    
                    if let currentVC = APPDELEGATE.navigationVC?.viewControllers.last {
                        if currentVC is HomeVC {
                            let homeVC = currentVC as! HomeVC
                            
                            if let existingTripId = homeVC.tripId  {
                                let addExpensesVC = self.storyboard?.instantiateViewController(withIdentifier: "AddExpensesVC") as! AddExpensesVC
                                addExpensesVC.tripId = existingTripId
                                viewController = addExpensesVC
                                
                            } else {
                                showAlert("", message: "There is no ongoing trip", onView:center)
                            }
                        }
                    }
                }
                break
                
            case 2 :
                // Go to add monthly expenses
                if (APPDELEGATE.mfContainer?.centerViewController as? MonthlyExpensesVC) == nil {
                    let monthlyExpensesVC = self.storyboard?.instantiateViewController(withIdentifier: "MonthlyExpensesVC") as! MonthlyExpensesVC
                    viewController = monthlyExpensesVC
                }
                break
                
            case 3:
                // Add Expenses Report ExpensesReportVC ExpensesReportOptionVC
                if (APPDELEGATE.mfContainer?.centerViewController as? ExpensesReportVC) == nil {
                    let expensesReportVC = self.storyboard?.instantiateViewController(withIdentifier: "ExpensesReportVC") as! ExpensesReportVC
                    let expensesReportNavVC = UINavigationController.init(rootViewController: expensesReportVC)
                    expensesReportNavVC.navigationBar.isHidden = true
                    
                    viewController = expensesReportNavVC
                }
                break
            
            case 4:
                print("Forum")
                // Go to forum for both driver/contractor
                if (APPDELEGATE.mfContainer?.centerViewController as? ForumViewController) == nil {
                    let ForumVC = self.storyboard?.instantiateViewController(withIdentifier: "ForumViewController") as! ForumViewController
                    let ForumNavVC = UINavigationController.init(rootViewController: ForumVC)
                    ForumNavVC.navigationBar.isHidden = true
                    viewController = ForumNavVC
                }
                break
                
            case 5:
                if let center = APPDELEGATE.mfContainer?.centerViewController as? UIViewController {
                    showActivityViewToShare(center)
                }
                break
                
            default:
                break
            }
        }
        
        if let centreVC = viewController {
            if self.menuContainerViewController.centerViewController as? UIViewController != centreVC {
                self.menuContainerViewController.centerViewController = centreVC
            }
        }
        self.menuContainerViewController.setMenuState(MFSideMenuStateClosed, completion: nil)

    }
}
