//
//  YourTripsVC.swift
//  SmartTrucking
//
//  Created by Vivan Raghuvanshi on 23/09/17.
//  Copyright © 2017 Vivan Raghuvanshi. All rights reserved.
//

import UIKit
import MFSideMenu

class YourTripsVC: UIViewController {
    
    // MARK:- --------------Outlets/Variables-------------------

    @IBOutlet weak var tf_SelectTrip:UITextField!
    @IBOutlet weak var table_View:UITableView!
    
    @IBOutlet weak var img_Cancel:UIImageView!

    var tripPaidRows:Int?
    var tripExpensesRows:Int?
    
    weak var delegate:CommonDelegate?
    
    fileprivate var selectedTrip:Trip?{
        didSet {
            tf_SelectTrip?.text = selectedTrip?.tripName ?? ""
            table_View.reloadData()
            table_View.isHidden = false
        }
    }
    
    lazy var picker:UIPickerView = {
        let pickerView = UIPickerView()
        pickerView.dataSource = self
        pickerView.delegate = self
        return pickerView
    }()
    
    var recentTrips = [Trip]()
    
    // MARK:- --------------View Life Cycle Methods-------------------

    override func viewDidLoad() {
        super.viewDidLoad()
        
        table_View.isHidden = true
        table_View.tableFooterView = UIView()

        if recentTrips.count == 0 {
            self.getTripHistory()
        } else {
            selectedTrip = recentTrips[0]
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK:- --------------Button Action Methods-------------------
    
    @IBAction func buttonClicked_LeftMenu(_ sender:UIButton) {
        sender.animateView()
        self.menuContainerViewController.toggleLeftSideMenuCompletion {
            
        }
    }
    
    @IBAction func buttonClicked_Home(_ sender:UIButton) {
        sender.animateView()
        img_Cancel.animateView()
        APPDELEGATE.mfContainer?.centerViewController = APPDELEGATE.navigationVC
        APPDELEGATE.mfContainer?.setMenuState(MFSideMenuStateClosed, completion: nil)
    }
    
    // MARK:- --------------Private Methods-------------------
    
    fileprivate func getTripHistory() {
        showHud("Processing..")
        let getRecentTrips = GetRecentTrips()
        getRecentTrips.getAllTrips { (response, error) in
            hideHud()
            if error == nil {
                if let trips = response as? [Trip] {
                    self.recentTrips = trips
                    if self.recentTrips.count > 0 {
                        if let trip = self.selectedTrip {
                            let t = self.recentTrips.filter({ aTrip -> Bool in
                                aTrip.tripId == trip.tripId
                            })
                            
                            if t.count > 0 {
                               self.selectedTrip = t.last
                            }
                            self.picker.reloadAllComponents()
                            if let index = (self.recentTrips.index(where: { (item) -> Bool in
                                item.tripId == trip.tripId // test if this is the item you're looking for
                            })){
                               self.picker.selectRow(index, inComponent: 0, animated: true)
                            }
                        } else {
                            self.selectedTrip = self.recentTrips[0]
                             self.picker.reloadAllComponents()
                        }
                    }
                    
                    // send to home page
                    self.delegate?.tripHistoryFound(trips)
                } else {
                    showAlert("", message:"\(String(describing: response ?? ""))", onView: self)
                }
            } else {
                showAlert("", message:error?.localizedDescription ?? SOMETHING_WRONG, onView: self)
            }
        }
    }
    
    @objc fileprivate func addExpense(_ sender:UIButton) {
        
        let addExpensesVC = self.storyboard?.instantiateViewController(withIdentifier: "AddExpensesVC") as! AddExpensesVC
         addExpensesVC.tripId = selectedTrip?.tripId ?? ""
         addExpensesVC.delegate = self;
        self.navigationController?.pushViewController(addExpensesVC, animated: true)
    }
    
    //MARK:- ------------- UITextFieldDelegate Methods ----------

    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if recentTrips.count == 0 {
            showAlert("", message:"Oops! You don't have any trip history", onView: self)
            textField.resignFirstResponder()
            return;
        }
        
        if textField.text == "" {
            if recentTrips.count > 0 {
                if textField.text == "" {
                    let trip = recentTrips[0]
                    textField.text = trip.tripName
                }
            }
        }
         textField.inputView = picker
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range:NSRange, replacementString string: String) -> Bool {
        return false
    }
}

//MARK:- ------------- UITableViewDataSource Methods ----------

extension YourTripsVC : UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 4
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 1 {
            tripPaidRows = 3
            if let trip = selectedTrip {
                let loadValue = Double(trip.tripLoadValue) ?? 0.0
                if loadValue == 0.0 {
                    if let stops = trip.tripStops {
                        tripPaidRows = stops.count + 3
                    }
                }
            }
            return tripPaidRows ?? 0
            
        } else if section == 2 {
            tripExpensesRows = 2
            if let expenses = selectedTrip?.tripExpenses {
                tripExpensesRows = expenses.count + 2
            }
            return tripExpensesRows ?? 0

        } else {
            return 1
        }
    }
    // TripTitleCell TripPaidCell TotalPaidCell TotalExpenseCell TotalProfitCell
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
                let ratePerMileCell = tableView.dequeueReusableCell(withIdentifier: "RatePerMileCell")
                let mileLabel = ratePerMileCell?.contentView.viewWithTag(201) as! UILabel
                if let trip = selectedTrip {
                    if let tripMiles = Float(trip.tripMiles) {
                        if let totalPaid = Float(trip.tripTotalPaid) {
                            let rpm = totalPaid/tripMiles
                            mileLabel.text = "= $\(rpm)"
                        } else {
                            mileLabel.text = "= $0"
                        }
                    }else {
                        mileLabel.text = "= $0"
                    }
                }
                return ratePerMileCell!
        }else if indexPath.section == 1 {
             switch indexPath.row {
            case 0:
                let titleCell = tableView.dequeueReusableCell(withIdentifier:"TripTitleCell")
                let titleLabel = titleCell?.contentView.viewWithTag(101) as! UILabel
                titleLabel.text = "Paid"
                titleCell?.selectionStyle = .none
                return titleCell!
            case (tripPaidRows! - 1):
                let totalPaidCell = tableView.dequeueReusableCell(withIdentifier:"TotalPaidCell")
                let totalPaidLabel = totalPaidCell?.contentView.viewWithTag(103) as! UILabel
                 if let trip = selectedTrip {
                    totalPaidLabel.text = "= $\(trip.tripTotalPaid)"
                }
                
                totalPaidCell?.selectionStyle = .none
                return totalPaidCell!
            default:
                let tripPaidCell = tableView.dequeueReusableCell(withIdentifier:"TripPaidCell")
                let tripPaidTitle = tripPaidCell?.contentView.viewWithTag(1021) as! UILabel
                let tripPaidLabel = tripPaidCell?.contentView.viewWithTag(1022) as! UILabel

                if let trip = selectedTrip {
                    
                    if (indexPath.row == 1) {
                        var totalTripSpent = 0.00
                        
                        let loadValue = Double(trip.tripLoadValue) ?? 0.0
                        
                        if loadValue == 0.0 {
                           // if let detail = trip.tripPayDetail {
//                                tripPaidTitle.text = "\(String(describing: trip.tripPayDetail?.detail_Miles ?? "0.0")) X $\(String(describing: trip.tripPayDetail?.detail_PaidForMiles ?? "$0.0"))"
                                tripPaidTitle.text = "\(String(describing: trip.tripMiles)) X \((Double(trip.tripPerMileCharge) ?? 0.0).dollarString)"
                            
                                totalTripSpent = (Double(trip.tripMiles) ?? 0.0) * (Double(trip.tripPerMileCharge) ?? 0.0)
                                tripPaidLabel.text = "= \(totalTripSpent.dollarString)"
                         //   }
                        } else {
                            tripPaidTitle.text = "\(String(describing: trip.tripLoadValue)) X \(String(describing: trip.percentageCharge))%"
                            
                            let tripSpent = (Double(trip.tripLoadValue) ?? 0.0) * (Double(trip.percentageCharge) ?? 0.0)
                            totalTripSpent = (tripSpent / 100.0)
                            tripPaidLabel.text = "= \(totalTripSpent.dollarString)"
                        }
                    } else {
                        
                       // tripPaidTitle.text = "Stop \(indexPath.row - 1)"
                       // tripPaidLabel.text = "= $\(trip.tripPayDetail?.detail_PaidForStop ?? "$0.0")"
                        //Double(stopsPresent.count) *
                        if let stopsPresent = trip.tripStops {
                            
                            let stop = stopsPresent[indexPath.row - 2]
                            tripPaidTitle.text = "\(stop.stopAddress)"
                            
                            let stopExpense = (Double(trip.tripPerStopCharge) ?? 0.0)
                            tripPaidLabel.text = "= \(stopExpense.dollarString)"
                        } else {
                            tripPaidLabel.text = "= $0"
                        }
                    }
                }
                tripPaidCell?.selectionStyle = .none
                return tripPaidCell!
            }
        } else if indexPath.section == 2 {
            switch indexPath.row {
            case 0:
                let titleCell = tableView.dequeueReusableCell(withIdentifier:"TripTitleCell")
                let titleLabel = titleCell?.contentView.viewWithTag(101) as! UILabel
                titleLabel.text = "Spend"
                titleCell?.selectionStyle = .none
                return titleCell!
            case (tripExpensesRows! - 1):
                let totalPaidCell = tableView.dequeueReusableCell(withIdentifier:"TotalExpenseCell")
                
                //self.tripSpent
                if let trip = selectedTrip {
                    let totalPaidLabel = totalPaidCell?.contentView.viewWithTag(103) as! UILabel
                    let spent = Double(trip.tripSpent ) ?? 0.0
                    totalPaidLabel.text = "= \(spent.dollarString)"
                    
                    let addExpenseBtn = totalPaidCell?.contentView.viewWithTag(11) as! UIButton
                    addExpenseBtn.addTarget(self, action: #selector(YourTripsVC.addExpense), for: .touchUpInside)
                }
                
                totalPaidCell?.selectionStyle = .none
                return totalPaidCell!
            default:
                let tripPaidCell = tableView.dequeueReusableCell(withIdentifier:"TripPaidCell")
                let tripPaidTitle = tripPaidCell?.contentView.viewWithTag(1021) as! UILabel
                let tripPaidLabel = tripPaidCell?.contentView.viewWithTag(1022) as! UILabel
                
                if let trip = selectedTrip { //expenseType //expenseTitle
                    let expense = trip.tripExpenses?[indexPath.row - 1]
                    tripPaidTitle.text = "\(expense?.expenseType ?? "\(indexPath.row - 1)") Expense"
                    let exp = Double(expense?.expense ?? "0.0") ?? 0.0
                    tripPaidLabel.text = "= \(exp.dollarString)"

                }
                tripPaidCell?.selectionStyle = .none
                return tripPaidCell!
            }
        } else {
            let totalProfitCell = tableView.dequeueReusableCell(withIdentifier:"TotalProfitCell")
            if let trip = selectedTrip {
                let totalProfitLabel = totalProfitCell?.contentView.viewWithTag(104) as! UILabel
                let totalTripSpent = Double(trip.tripTotalPaid)! - Double(trip.tripSpent)!
                
                totalProfitLabel.text = "Total Profit = \(totalTripSpent.dollarString)"
            }
            totalProfitCell?.selectionStyle = .none
            return totalProfitCell!
        }
    }
}

//MARK:- ------------- UITableViewDelegate Methods ----------

extension YourTripsVC : UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0 {
            return 40 * getScaleFactor()
        }else if indexPath.section == 1 {
            var totalRows = 1
            totalRows = tripPaidRows == nil ? totalRows : tripPaidRows!
            
            switch indexPath.row {
            case 0:
                return 65 * getScaleFactor()
            case (totalRows - 1):
                return 60 * getScaleFactor()
            default:
                return 27 * getScaleFactor()
            }
        } else if indexPath.section == 2 {
            var totalRows = 1
            totalRows = tripExpensesRows == nil ? totalRows : tripExpensesRows!
            switch indexPath.row {
            case 0:
                return 65 * getScaleFactor()
            case (totalRows - 1):
                return 115 * getScaleFactor()
            default:
                return 27 * getScaleFactor()
            }
        } else {
            return 90 * getScaleFactor()
        }
    }
}

//MARK:- ------------- UIPickerViewDataSource Methods ----------

extension YourTripsVC : UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return recentTrips.count
    }
}

//MARK:- ------------- UIPickerViewDelegate Methods ----------

extension YourTripsVC : UIPickerViewDelegate {
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        selectedTrip = recentTrips[row]
        tf_SelectTrip?.text = selectedTrip?.tripName ?? ""
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        let trip = recentTrips[row]
        return trip.tripName
    }
}

extension YourTripsVC:AddExpensesDelegate {
    func expensesIsAdded() {
        self.getTripHistory()
    }
}
