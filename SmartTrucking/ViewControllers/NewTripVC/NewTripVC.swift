//
//  NewTripVC.swift
//  SmartTrucking
//
//  Created by Vivan Raghuvanshi on 14/09/17.
//  Copyright © 2017 Vivan Raghuvanshi. All rights reserved.
//0.915

import UIKit
import GooglePlacesSearchController
import GooglePlaces
import GoogleMaps

protocol PickupLcationDelegate {
    func googleLocationSearching(text: String, isfromPickup: Bool, isFromAddStop: Bool)
}

protocol SearchSelectedDelegate {
    
}

class NewTripVC: UIViewController, PickupLcationDelegate, AddStopDelegate {
    
    // MARK:- --------------Outlets/Variables-------------------
    
    @IBOutlet weak var topConstraint: NSLayoutConstraint!
    @IBOutlet weak var table_View:UITableView!
    @IBOutlet weak var tf_TripName:UITextField!
    @IBOutlet weak var addStopTextfiled: UITextFieldX!
    
    @IBOutlet weak var googleSearchTableView: UITableView!
    var currntlatitude:Double?
    var currentlongitude:Double?
    
    var navVC:UINavigationController?
    
    weak var delegate: CommonDelegate?
    
    var loadValue:String?
    var waypointsPlace = [PlaceDetails]()
    let placesClient = GMSPlacesClient()
    var placeId = [String]()
    var searchPlacesresult = [GMSAutocompletePrediction]()
    var sourceLocation: String?
    var destinationLocation: String?
    var isFromPickup = false
    var isFromDrop = false
    var isFromAddStop = false
    var addStops = [Stop]()
    var sourceOBject = Stop("", address: "", latitude: "", longitude: "", stopSeq: 0)
    var destinationObject = Stop("", address: "", latitude: "", longitude: "", stopSeq: 0)
    var isFromRecentHistory = false
    var recentTrips = [Trip]()
    
    // MARK:- --------------View Life Cycle Methods-------------------
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        table_View.delegate = self
        table_View.dataSource = self
        table_View.tableFooterView = UIView.init(frame:.zero)
        
        if recentTrips.count == 0 {
            self.getTripHistory()
        }
    }
    
    func addStopclicked() {
        addStopTextfiled.isHidden = false
    }
    
    func getCoordinates(placeId: String, completionHandler: @escaping (_ plac: CLLocationCoordinate2D) -> ()){
        self.placesClient.lookUpPlaceID(placeId, callback: { (place, error) -> Void in
            if let error = error {
                print("lookup place id query error: \(error.localizedDescription)")
                return
            }
            
            guard let place = place else {
                print("No place details for \(self.placeId)")
                return
            }
            completionHandler(place.coordinate)
        })
        
    }
    
    func googleLocationSearching(text: String,isfromPickup isFromPickup: Bool, isFromAddStop: Bool) {
        self.googleSearchTableView.isHidden = false
        let heightOfSuperview = self.view.bounds.height
        if isFromPickup {
            self.isFromPickup = true
            isFromDrop = false
            self.isFromAddStop = false
            topConstraint.constant = heightOfSuperview * 0.22
        } else if isFromAddStop {
            isFromDrop = false
            self.isFromPickup = false
            self.isFromAddStop = true
            topConstraint.constant = heightOfSuperview * 0.0899
        }else {
            isFromDrop = true
            self.isFromPickup = false
            self.isFromAddStop = false
            topConstraint.constant = heightOfSuperview * 0.307
        }
        if text.count ?? 0 > 3 {
            placesClient.autocompleteQuery(text, bounds: nil, filter: nil, callback: { (results, error) -> Void in
                self.searchPlacesresult.removeAll()
                if error != nil {
                    print(error?.localizedDescription)
                    return
                }
                if results == nil {
                    return
                }
                if let results = results {
                    self.searchPlacesresult = results
                }
                self.googleSearchTableView.reloadData()
            })
        }
        //googleSearchTableView.isHidden = true
        
    }
    // MARK:- --------------Button Action Methods-------------------
    
    @IBAction func buttonClicked_Back(_ sender:UIButton) {
        sender.animateView()
        self.navigationController?.popViewController(animated: true)
    }
    
    // MARK:- --------------UITextFieldDelegate Methods-------------------
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField.tag == 1001 {
            googleSearchTableView.isHidden = true
            // Payment info is restricted upto two decimal
            let text = textField.text ?? ""
            if string == "." || text.contains(find: ".") || string == "" {
                // Payment info is restricted upto two decimal
                return allowUptoTwoDecimalPlace(textField.text ?? "", forRange: range, replacementString: string)
            } else {
                return text.count < 5 ? true : false
            }
        }
        if textField == addStopTextfiled  {
            googleLocationSearching(text: textField.text!, isfromPickup: false, isFromAddStop: true)
        }
        return true
    }
    
    func convertLatLongToAddress(coordinates: CLLocationCoordinate2D) {
        let reverseGeoCoder = GMSGeocoder()
        reverseGeoCoder.reverseGeocodeCoordinate(coordinates, completionHandler: {(placeMark, error) -> Void in
            if error == nil {
                if let placeMarkObject = placeMark {
                    if placeMarkObject.results()?.count != 0 {
                        let currentLocationModel = Stop("", address: (placeMarkObject.firstResult()?.lines)!.first!, latitude: "\(coordinates.latitude)", longitude: "\(coordinates.longitude)", stopSeq: 0)
                        self.sourceLocation = (placeMarkObject.firstResult()?.lines)!.first
                        self.sourceOBject = currentLocationModel
                        self.table_View.reloadData()
                    } else {
                    }
                } else {
                    //Do Nothing
                }
            } else {
                print(error?.localizedDescription)
            }
        })
    }
    
    private func getTripHistory() {
        showHud("Processing..")
        let getRecentTrips = GetRecentTrips()
        getRecentTrips.getAllTrips { (response, error) in
            hideHud()
            print(response)
            if error == nil {
                if let trips = response as? [Trip] {
                    self.recentTrips = trips
                    
                    self.table_View.reloadData()
                    // send to home page
                    self.delegate?.tripHistoryFound(trips)
                } else {
                    showAlert("", message:"\(String(describing: response ?? ""))", onView: self)
                }
            } else {
                showAlert("", message:error?.localizedDescription ?? SOMETHING_WRONG, onView: self)
            }
        }
    }
    
    func createTrip() {
        
        var params = ["profileId":user!.userId!,
                      "sourceAddress": sourceOBject.stopAddress,
                      "sourceLatitude": sourceOBject.stopLatitude,
                      "sourceLongitude": sourceOBject.stopLongitude,
                      "destinationAddress": destinationObject.stopAddress,
                      "destinationLatitude": destinationObject.stopLatitude,
                      "destinationLongitude": destinationObject.stopLongitude,
                      "tripStatus":"started",
                      "tripName": tf_TripName.text == "" ? getDateStringFromDateInTripNameFormat(Date()):tf_TripName.text ?? "TripName",
                      "startDate":getDateStringFromDate(Date())] as [String : Any]
        
        if (user!.userOnPercaentage == true) {
            if let loadChoose = loadValue {
                params["loadValue"] = loadChoose
            }
        }
        var countSeq = 0
        if (addStops.count > 0) {
            
            let stopsArray = addStops.map({ waypoint -> [String:Any]  in
                countSeq += 1
               return ["stopAddress": waypoint.stopAddress ,
                 "stopLatitude": String(waypoint.stopLatitude),
                 "stopLongitude": String(waypoint.stopLongitude),
                 "stopSeq": countSeq]
            })
            
            params["stops"] = stopsArray
        }
        
        self.delegate?.sourcePlaceAdded(sourceOBject)
        self.delegate?.destinationPlaceAdded(destinationObject)
        self.delegate?.waypointPlaceAdded(addStops)
        
        showHud("Processing..")
        let networkManager = NetworkManager()
        networkManager.getDataForRequest(calculateTrip, andParameter: params) { (response, error) in
            
            hideHud()
            
            if error == nil {
                if let result = response as? [String:Any] {
                    if let status = result["status"] as? String, (status == SUCCESS_STATUS) {
                        if let tripInfo = result["_obj"] {
                            let tripId = "\(tripInfo)"
                            
                            self.delegate?.currentTripId(tripId, withName: "\(String(describing: params["tripName"] ?? "Trip Name"))")
                            
                            let mapVC = self.storyboard?.instantiateViewController(withIdentifier: "MapVC") as! MapVC
                            
                            if (self.delegate is HomeVC) {
                                
                                let homeVC = self.delegate as! HomeVC
                                
                                mapVC.sourcePlace = homeVC.sourcePlace
                                mapVC.destinationPlace = homeVC.destinationPlace
                                mapVC.waypointsPlace = homeVC.waypointsPlace
                                mapVC.tripName = homeVC.tripName
                                mapVC.isFromNewVC = true
                                
                                mapVC.tripId = homeVC.tripId
                                self.navigationController?.popViewController(animated: false)
                                
                                APPDELEGATE.mapNavigationVC?.viewControllers = [mapVC]
                                //UINavigationController.init(rootViewController: mapVC)
                                APPDELEGATE.mfContainer?.centerViewController = APPDELEGATE.mapNavigationVC
                            }
                        }
                    } else {
                        showAlert("", message: INTERNAL_ERROR_MESSAGE, onView: self)
                    }
                }
            } else {
                showAlert("", message:error?.localizedDescription ?? SOMETHING_WRONG, onView: self)
            }
        }
    }
    
    // Use this method to check either source or destination are filled or not
    func checkAddressField() -> Bool {
        //        if(isBlank(sourcePlace?.name ?? "")||isBlank(destinationPlace?.name ?? "")) {
        //            return false
        //        }
        return true
    }
    
    // Use this method to reload section of stops after adding every stops
    func StopCellReloadData(indexNumber:Int,sectionNumber:Int,CellType:AddAndDeleteCell) {
        if sectionNumber == 1 {
            if(CellType == .AddCell) {
                self.table_View.beginUpdates()
                self.table_View.insertRows(at: [IndexPath(row: (addStops.count)-1, section:sectionNumber)],with: .automatic)
                self.table_View.endUpdates()
            } else if(CellType == .DeleteCell) {
                
                addStops.remove(at:indexNumber)
                self.table_View.beginUpdates()
                self.table_View.deleteRows(at: [IndexPath(row:indexNumber, section:sectionNumber)],with: .automatic)
                self.table_View.endUpdates()
                
                self.perform(#selector(reloadTableData), with: nil, afterDelay: 0.3)
            }
        }
    }
    
    // perform selector method
    @objc private func reloadTableData() {
        self.table_View.reloadData()
    }
}

extension NewTripVC:UITableViewDataSource,UITableViewDelegate {
    
    // MARK:- ----------UITableViewDataSource Methods---------------
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if tableView == googleSearchTableView {
            return 1
        } else {
            return 5
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == googleSearchTableView {
            if searchPlacesresult.count > 0 {
                return searchPlacesresult.count
            }else {
                return 0
            }
        } else {
            var noOfRows = 0
            
            switch section {
            case 0:
                noOfRows = 1 // Source & Destination in one row
                break
            case 1:
                noOfRows = addStops.count // All the waypoints
                break
            case 2:
                noOfRows = 1 // Add Stop Button in one row
                break
            case 3:
                noOfRows = 1 // Create trip Button in one row
                break
            case 4:
                noOfRows = recentTrips.count > 5 ? 5:recentTrips.count // Rows for recent history
                break
                
            default:
                break
            }
            return noOfRows
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == googleSearchTableView {
            let cell = tableView.dequeueReusableCell(withIdentifier: "addCategoryCell", for: indexPath) as! SearchLocationTableViewCell
            cell.mainLocationLabel.text = searchPlacesresult[indexPath.row].attributedPrimaryText.string
            cell.fullAddressLabel.text = searchPlacesresult[indexPath.row].attributedSecondaryText?.string
            
            return cell
        } else {
            var cell:UITableViewCell?
            tableView.allowsSelection = false
            switch indexPath.section {
            case 0:
                cell = tableView.dequeueReusableCell(withIdentifier: "SourceDestinationCell", for: indexPath) as! SourceDestinationCell
                (cell as! SourceDestinationCell).delegate = self
                if let sourceExist = sourceLocation {
                    //if sourceExist.name = "" {
                    //    (cell as! SourceDestinationCell).tf_Source.text = sourceExist.name
                    //} else {
                    (cell as! SourceDestinationCell).tf_Source.text = sourceExist
                    
                    //}
                }
                
                if let destinationExist = destinationLocation {
                    // if destinationExist.name == "" {
                    //     (cell as! SourceDestinationCell).tf_Destination.text = destinationExist.name
                    //} else {
                    (cell as! SourceDestinationCell).tf_Destination.text = destinationExist
                    //}
                }
                
            case 1:
                cell = tableView.dequeueReusableCell(withIdentifier: "StopCell", for: indexPath) as! StopCell
                (cell as! StopCell).configureCell(indexPath:indexPath.row, SectionNo:indexPath.section)
                let stopPlace = addStops[indexPath.row]
                (cell as! StopCell).tf_Stop.text = stopPlace.stopAddress
                (cell as! StopCell).btn_Remove.roundCorner()
                (cell as! StopCell).delegate = self
                
            case 2:
                cell = tableView.dequeueReusableCell(withIdentifier: "AddStopCell", for: indexPath) as! StopCell
                if(addStops.count == 0) {
                    (cell as! StopCell).configureCell(indexPath:0, SectionNo:1)
                }
                (cell as! StopCell).delegate = self
                
            case 3:
                cell = tableView.dequeueReusableCell(withIdentifier: "CalCulateTrip", for: indexPath) as! StopCell
                if user!.userOnPercaentage == true {
                    (cell as! StopCell).tf_LoadValue.isHidden = false
                    (cell as! StopCell).lbl_Dollar.isHidden = false
                } else {
                    (cell as! StopCell).tf_LoadValue.isHidden = true
                    (cell as! StopCell).lbl_Dollar.isHidden = true
                }
                
            case 4:
                cell = tableView.dequeueReusableCell(withIdentifier: "RecentTripCell", for: indexPath) as! StopCell
                let trip = recentTrips[indexPath.row]
                (cell as! StopCell).tf_Stop.text = trip.tripName
                if isIpad() {
                    (cell as! StopCell).tf_Stop.paddingLeft = 64
                }
                (cell as! StopCell).btn_SelectSop.tag = indexPath.row
            // (cell as! StopCell).tf_Stop.text = "Recent Trip"
            default:
                break
            }
            return cell!
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView == googleSearchTableView {
            if isFromPickup {
                if searchPlacesresult.count > 0 {
                    sourceLocation = searchPlacesresult[indexPath.row].attributedPrimaryText.string
                    let gmsPlaces = searchPlacesresult[indexPath.row]
                    var coordinates = CLLocationCoordinate2D()
                    getCoordinates(placeId: gmsPlaces.placeID, completionHandler: { result in coordinates = result
                        let stop = Stop(gmsPlaces.placeID, address: gmsPlaces.attributedPrimaryText.string, latitude: "\(coordinates.latitude)", longitude: "\(coordinates.longitude)", stopSeq: 0)
                        self.sourceOBject = stop
                        self.table_View.reloadData()
                    })
                }
            } else if isFromDrop {
                if searchPlacesresult.count > 0 {
                    destinationLocation = searchPlacesresult[indexPath.row].attributedPrimaryText.string
                    let gmsPlaces = searchPlacesresult[indexPath.row]
                    var coordinates = CLLocationCoordinate2D()
                    getCoordinates(placeId: gmsPlaces.placeID, completionHandler: { result in coordinates = result
                        let stop = Stop(gmsPlaces.placeID, address: gmsPlaces.attributedPrimaryText.string, latitude: "\(coordinates.latitude)", longitude: "\(coordinates.longitude)", stopSeq: 0)
                        self.destinationObject = stop

                        self.table_View.reloadData()
                    })
                }
            } else if isFromAddStop {
                addStopTextfiled.isHidden = true
                if searchPlacesresult.count > 0 {
                    let gmsPlaces = searchPlacesresult[indexPath.row]
                    var coordinates = CLLocationCoordinate2D()
                    getCoordinates(placeId: gmsPlaces.placeID, completionHandler: { result in coordinates = result
                        let stop = Stop(gmsPlaces.placeID, address: gmsPlaces.attributedPrimaryText.string, latitude: "\(coordinates.latitude)", longitude: "\(coordinates.longitude)", stopSeq: 0)
                        
                        self.addStops.append(stop)

                        self.table_View.reloadData()
                    })
                    
                }
                
            }
            searchPlacesresult.removeAll()
            view.endEditing(true)
            googleSearchTableView.isHidden = true
            table_View.reloadData()
        }
    }
    
    // MARK: -------------- UITableViewDelegate Method --------------------
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if tableView == googleSearchTableView {
            return 80
        } else {
            switch indexPath.section {
            case 0:
                return 115 * getScaleFactor()
            case 1:
                if(addStops.count>0) {
                    return 43 * getScaleFactor()
                }
                return 0.01
            case 2:
                return 35 * getScaleFactor()
            case 3:
                //            if (user?.userOnPercaentage == true) {
                //               return 160 * getScaleFactor()
                //            }
                return 120 * getScaleFactor()
            default:
                return 44 * getScaleFactor()
            }
        }
    }
}
