//
//  StopCell.swift
//  SmartTrucking
//
//  Created by Vivan Raghuvanshi on 14/09/17.
//  Copyright © 2017 Vivan Raghuvanshi. All rights reserved.
//

import UIKit
import IBAnimatable

// MARK:- ----------Enum to check wether adding/deleting stop--------------


enum AddAndDeleteCell {
    case AddCell
    case DeleteCell
    
}

protocol AddStopDelegate {
    func addStopclicked()
}

class StopCell: UITableViewCell, UITextFieldDelegate {
    
    // MARK:- --------------Outlets/Variables-------------------
   
    @IBOutlet weak var tf_Stop:AnimatableTextField!
    @IBOutlet weak var btn_SelectSop:UIButton!
   
    @IBOutlet weak var btn_Remove:UIButton!
    @IBOutlet weak var addAddressButton:UIButton!
    @IBOutlet weak var tf_LoadValue:UITextField!
    @IBOutlet weak var lbl_Dollar:UILabel!
    
    var indexNo:Int?
    var sectionNo:Int?
    var delegate: AddStopDelegate?
    var hideAddStopTextField = false
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    // MARK:- --------------Button Action Methods-------------------
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {

        return true
    }
    
    // Calculate trip
    @IBAction func buttonClicked_CalculateTrip() {
        // sender.animateView()
        if let parentVC = self.parentViewController as? NewTripVC {
            // Earlier it was parentVC.sourcePlace?.name
            if(isBlank(parentVC.sourceOBject.stopAddress ?? "")||isBlank(parentVC.destinationObject.stopAddress ?? "")) {
                showAlert("", message: "Please Add Source and Destination", onView: parentVC)
                return
            } else if (parentVC.sourceOBject.stopAddress == "") || (parentVC.destinationObject.stopAddress == "") {
                showAlert("", message: "Please Add Source and Destination", onView: parentVC)
                return
            }
            if (user?.userOnPercaentage == true) {
                let loadVal = Double(tf_LoadValue.text ?? "0.0") ?? 0.0
                if loadVal <= 0.0 {
                    showAlert("", message: "Please Enter A Valid Load Value For Trip", onView: parentVC)
                    return
                }
            }
            
            parentVC.loadValue = tf_LoadValue.text
            parentVC.view.endEditing(true)
            
            parentVC.createTrip()
        }
    }
    
    // Add stop
    @IBAction func buttonClicked_AddStop() {
        delegate?.addStopclicked()
        //hideAddStopTextField = !hideAddStopTextField
       //  sender.animateView()
//        if let parentVC = self.parentViewController as? NewTripVC {
//            parentVC.controller.didSelectGooglePlaceWithLatitude(parentVC.currntlatitude ?? 17.4121521, lng: parentVC.currentlongitude ?? 78.1271483, completion: { place in
//
//                parentVC.waypointsPlace.append(place)
//                //Dismiss Search
//                parentVC.controller.isActive = false
//
//
//                parentVC.StopCellReloadData(indexNumber:self.indexNo!, sectionNumber:self.sectionNo!,CellType:AddAndDeleteCell.AddCell)
//
//            })
//            parentVC.present(parentVC.controller, animated: true, completion: nil)
//        }
    }
    
    // Remove stop
    @IBAction func buttonClicked_RemoveStop() {
        if let parentVC = self.parentViewController as? NewTripVC {
            parentVC.StopCellReloadData(indexNumber:indexNo!, sectionNumber:sectionNo!,CellType:AddAndDeleteCell.DeleteCell)
        }
    }
    
    // Add Trip from recent history
    @IBAction func buttonClicked_AddTripFromRecentHistory(_ sender:UIButton) {
         sender.animateView()
        if let parentVC = self.parentViewController as? NewTripVC {
            let trip = parentVC.recentTrips[sender.tag]
            parentVC.sourceLocation = trip.sourceAddress
            parentVC.destinationLocation = trip.destinationAddress
            parentVC.addStops = trip.tripStops!
            parentVC.isFromRecentHistory = true
            parentVC.sourceOBject = Stop(trip.tripId, address: trip.sourceAddress, latitude: trip.sourceLatitude, longitude: trip.sourceLongitude, stopSeq: 0)
            parentVC.destinationObject = Stop(trip.tripId, address: trip.destinationAddress, latitude: trip.destinationLatitude, longitude: trip.destinationLongitude, stopSeq: 0)
            
            parentVC.table_View.reloadData()
        }
    }
    
    //MARK : --------------- configue cell ------------------------
    func configureCell(indexPath:Int,SectionNo:Int) {
        indexNo = indexPath
        sectionNo = SectionNo
    }
    
}
