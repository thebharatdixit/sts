//
//  SourceDestinationCell.swift
//  SmartTrucking
//
//  Created by Vivan Raghuvanshi on 14/09/17.
//  Copyright © 2017 Vivan Raghuvanshi. All rights reserved.
//

import UIKit
import IBAnimatable
import GooglePlacesSearchController

class SourceDestinationCell: UITableViewCell {
    
    // MARK:- --------------Outlets/Variables-------------------
    
    @IBOutlet weak var tf_Source:AnimatableTextField!
    @IBOutlet weak var tf_Destination:AnimatableTextField!
    public var delegate: PickupLcationDelegate?
   
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
}

// MARK:- --------------UITextFieldDelegate Methods -------------------

extension SourceDestinationCell:UITextFieldDelegate {
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
//        if let parentVC = self.parentViewController as? NewTripVC {
//        parentVC.controller.didSelectGooglePlaceWithLatitude(parentVC.currntlatitude ?? 17.4121521, lng: parentVC.currentlongitude ?? 78.1271483, completion: { place in
//            textField.text = place.name
//
//            if textField == self.tf_Source {
//                parentVC.sourcePlace = place
//                parentVC.delegate?.sourcePlaceAdded(place)
//            } else {
//                parentVC.destinationPlace = place
//                parentVC.delegate?.destinationPlaceAdded(place)
//            }
//
//            //Dismiss Search 283338
//            parentVC.controller.isActive = false
//        })
//        parentVC.present(parentVC.controller, animated: true, completion: nil)
//        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == tf_Source {
            delegate?.googleLocationSearching(text: textField.text!, isfromPickup: true, isFromAddStop: false)
        } else if textField == tf_Destination {
            delegate?.googleLocationSearching(text: textField.text!, isfromPickup: false, isFromAddStop: false)
        }
        return true
    }
    
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        if let parentVC = self.parentViewController as? NewTripVC {
            if textField == self.tf_Source {
                parentVC.sourceOBject.stopAddress = ""
                parentVC.delegate?.sourcePlaceAdded(nil)
            } else {
                parentVC.destinationObject.stopAddress = ""
                parentVC.delegate?.destinationPlaceAdded(nil)
            }
        }
        return true
    }
}
