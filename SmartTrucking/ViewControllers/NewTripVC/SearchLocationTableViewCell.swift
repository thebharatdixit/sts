//
//  SearchLocationTableViewCell.swift
//  SmartTrucking
//
//  Created by Prabhakar G on 14/04/18.
//  Copyright © 2018 Vivan Raghuvanshi. All rights reserved.
//

import UIKit

class SearchLocationTableViewCell: UITableViewCell {

    @IBOutlet weak var fullAddressLabel: UILabel!
    @IBOutlet weak var mainLocationLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
