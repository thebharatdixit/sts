//
//  CategorySearchVC.swift
//  SmartTrucking
//
//  Created by Vivan Raghuvanshi on 02/10/17.
//  Copyright © 2017 Vivan Raghuvanshi. All rights reserved.
//

import Foundation

protocol CategorySearchDelegate {
    func choosedCategory(_ selectedCategories:[String], andSubCategories subCategories:[String])
}

class CategorySearchVC:UIViewController {
    
    // MARK:- --------------Outlets/Variables-------------------
    
    @IBOutlet weak var table_View:UITableView!
    
    fileprivate var arr_Options = ["TA/Petro", "Love's", "Ambest", "Pilot", "Flying J", "Rest Area", "Walmarts"]
    var selectedOptions = [String]()
    var selectedCategories = [String]()
    
    var delegate:CategorySearchDelegate?
    
    // MARK:- --------------View Life Cycle Methods-------------------
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        table_View.tableFooterView = UIView()
        table_View.delegate = self;
    }
    
    // MARK:- --------------Button Action Methods-------------------
    
    @IBAction func buttonClicked_Back(_ sender:UIButton) {
        sender.animateView()
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func buttonClicked_Save(_ sender:UIButton) {
        sender.animateView()
        if (selectedOptions.count > 0) {
            // --------------------------------------------------------------
            // See what categories we have selected
            for value in arr_Options {
                if selectedOptions.contains(value) {
                    selectedCategories.append("Truck Stops")
                    break
                }
            }
            if selectedOptions.contains("Weigh Station") {
                selectedCategories.append("Weigh Station")
            }
            if selectedOptions.contains("Gas Station") {
                selectedCategories.append("Gas Station")
            }

            // --------------------------------------------------------------
            self.delegate?.choosedCategory(selectedCategories, andSubCategories: selectedOptions)
            self.navigationController?.popViewController(animated: true)
        } else {
            showAlert("", message: "Please choose st least one category to save", onView: self)
        }
        
    }
    
    @IBAction func buttonClicked_Clear(_ sender:UIButton) {
        sender.animateView()
        selectedOptions.removeAll()
        table_View.reloadData()
    }
    
    // MARK:- --------------Private Methods------------------
    
    fileprivate func isAlreadySelected(_ value:String) -> Bool {

        if selectedOptions.count > 0{
            let filter = selectedOptions.filter {selectedValue -> Bool in
                value == selectedValue
            }
            let checkExist = (filter.count > 0) ? true:false
            return checkExist
        } else {
            return false
        }
    }
}

//MARK:- ------------- UITableViewDataSource Methods ----------

extension CategorySearchVC :UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return arr_Options.count + 1
        } else {
            return 2
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0 {
            let titleCell = tableView.dequeueReusableCell(withIdentifier: "CategoryTitleCell")
            titleCell?.selectionStyle = .none
            let titleLabel = titleCell?.contentView.viewWithTag(101) as! UILabel
            switch indexPath.section {
            case 0:
                titleLabel.text = "Parking Availability/Truck Stops"
                break
            case 1:
                titleLabel.text = "Weigh Station"
                break
            default:
                titleLabel.text = "Gas Station"
                break
            }
            return titleCell!
        } else {
            let chooseOptionCell = tableView.dequeueReusableCell(withIdentifier: "ChooseCategoryCell")
            let optionLabel = chooseOptionCell?.contentView.viewWithTag(102) as! UILabel
            switch indexPath.section {
            case 0:
                optionLabel.text = arr_Options[indexPath.row - 1]
                break
            case 1:
                optionLabel.text = "Weigh Station"
                break
            default:
                optionLabel.text = "Gas Station"
                break
            }
            let selectView = chooseOptionCell?.contentView.viewWithTag(103)
            if isAlreadySelected(optionLabel.text ?? "") {
                optionLabel.textColor = blue2
                selectView?.backgroundColor = blue2
            } else {
                optionLabel.textColor = UIColor.lightGray
                selectView?.backgroundColor = UIColor.lightGray
            }
            chooseOptionCell?.selectionStyle = .none
            return chooseOptionCell!
        }
    }
}

//MARK:- ------------- UITableViewDelegate Methods ----------

extension CategorySearchVC :UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if indexPath.row == 0 { return };
        var selectedCategory = ""
        
        switch indexPath.section {
        case 0:
            selectedCategory = arr_Options[indexPath.row - 1]
            break
        case 1:
            selectedCategory = "Weigh Station"
            break
        default:
            selectedCategory = "Gas Station"
            break
        }
        
        if isAlreadySelected(selectedCategory) {
            if let index = selectedOptions.index(of:selectedCategory) {
                selectedOptions.remove(at: index)
            }
        } else {
            selectedOptions.append(selectedCategory)
        }
        
        tableView.beginUpdates()
        tableView.reloadRows(at: [indexPath], with: .none)
        tableView.endUpdates()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0 {
            return 60 * getScaleFactor()
        } else {
            return 44 * getScaleFactor()
        }
    }
}


