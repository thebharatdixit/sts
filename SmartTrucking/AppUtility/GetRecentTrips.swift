//
//  GetRecentTrips.swift
//  SmartTrucking
//
//  Created by Vivan Raghuvanshi on 17/09/17.
//  Copyright © 2017 Vivan Raghuvanshi. All rights reserved.
//

import Foundation

typealias tripsFound = (_: Any?, _: Error?) -> Void

struct GetRecentTrips {
    
    func getAllTrips(_ getTrips:@escaping tripsFound) {
        
        let params = ["profileId":user?.userId ?? ""]
        
        let networkManager = NetworkManager()
        networkManager.getDataForRequest(history, andParameter: params) { (response, error) in
            
            if error == nil {
                
                if let result = response as? [String:Any] {
                    if let status = result["status"] as? String, (status == SUCCESS_STATUS) {
                        if let tripHistory = result["_obj"] as? [[String:Any]] {
                            
                            let allTripModels = tripHistory.map({ (tripInfo) -> Trip in
                                Trip(tripInfo)
                            })
                            print(allTripModels)
                            allTripModels.map({ trip in
                                trip.tripStops?.sorted(by: {(stop1, stop2) -> Bool in
                                    return stop1.stopSeq > stop2.stopSeq
                                })
                                
                            })
                            print(allTripModels)
                            let sortedTrips = allTripModels.sorted(by: { (trip1, trip2) -> Bool in
                                return trip1.tripId > trip2.tripId
                            })
                            
                            
                            getTrips(sortedTrips,nil)
                            
                        }
                    } else {
                        getTrips(INTERNAL_ERROR_MESSAGE,nil)
                    }
                }
            } else {
                getTrips(nil,error)
            }
        }
    }
}

func getAllTrailerTypes(_ getTrailers:([TrailerType])->()) {
    
    var trailerTypes = [TrailerType]()
    
    guard let jsonPath = Bundle.main.path(forResource: "TrailerTypes", ofType: "json"),
        let jsonData = try? Data(contentsOf: URL(fileURLWithPath: jsonPath)) else {
            getTrailers(trailerTypes)
            return
    }
    do {
        if let jsonObjects = try JSONSerialization.jsonObject(with: jsonData, options: .allowFragments) as? [[String:Any]] {
            
            let allTrailerTypes = jsonObjects.map({ (trailerType) -> TrailerType in
                TrailerType(object: trailerType)
            })
            trailerTypes = allTrailerTypes
            getTrailers(trailerTypes)
        }
    } catch {
        getTrailers(trailerTypes)
    }
}
