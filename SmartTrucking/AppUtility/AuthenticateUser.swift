
//
//  AuthenticateUser.swift
//  SmartTrucking
//
//  Created by Vivan Raghuvanshi on 25/08/17.
//  Copyright © 2017 Vivan Raghuvanshi. All rights reserved.
//

import Foundation

class AuthenticateUser {
    
    typealias authenticationCompletion = (_:String,_: Any?) -> Void
    
    //1- Method to Register a New User
    func registerUserWithParameters(_ params:[String:Any]?, withCompletion getResponse: @escaping authenticationCompletion) {
        let networkManager = NetworkManager()
        networkManager.getDataForRequest(signUp, andParameter: params) { (response, error) in
            
            if error == nil {
                let completion = self.extractResponseForResult(response!)
                getResponse(completion.0,completion.1)
            } else {
                getResponse("Error",error?.localizedDescription ?? "Error")
               // print(error?.code ?? "Error")
               // print(error?.domain ?? "Error")

               // print(error?.localizedDescription ?? "Error")
            }
        }
    }
    
    //2- Method to Update a Registered User User
    func updateRegisteredUserWithParameters(_ params:[String:Any]?, withCompletion getResponse: @escaping authenticationCompletion) {
        let networkManager = NetworkManager()

        networkManager.getDataForRequest(signUp, andParameter: params) { (response, error) in
            if error == nil {
                guard let result = response as? [String:Any] else { return }
                
                if let status = result["status"] as? String, (status == SUCCESS_STATUS) {
                    
                    if let userInfo = result["_obj"] as? [String:Any] {
                        UserInfo.sharedInfo.setUserProfileWithInfo(userInfo)
 
                    }                    
                    getResponse(status,"\(String(describing: result["message"] ?? ""))")
                } else {
                    let message = self.handleErrorOrFailedStatus(result)
                    getResponse("",message)

                }

                print(response!)
                
            } else {
                getResponse("Error",error?.localizedDescription ?? "Error")
                //print(error?.localizedDescription ?? "Error")
            }
        }
    }
    
    //3- Method to Login a Registered User
    func loginWithParameters(_ params:[String:String]?, withCompletion getResponse: @escaping authenticationCompletion) {
        
        let networkManager = NetworkManager()
        networkManager.getDataForRequest(signIn, andParameter: params) { (response, error) in
            
            if error == nil {
                
                let completion = self.extractResponseForResult(response!)
                
                getResponse(completion.0,completion.1)
            } else {
                getResponse("Error",error?.localizedDescription ?? "Error")
               // print(error?.localizedDescription ?? "Error")
            }
        }
    }
    
    //4- Handle the response for SignUp and Login Method
    private func extractResponseForResult(_ response:Any) -> (String,Any?) {
        if let result = response as? [String:Any] {
            if let status = result["status"] as? String, (status == SUCCESS_STATUS || status == DUPLICATE_PROCEED || status == SOCIAL_LOGIN_SUCCESS || status == INCOMPLETE_PROFILE || status == SUCCESS_SOCIAL_PROSPECT)  {
                
                if let userInfo = result["_obj"] as? [String:Any] {
                    UserInfo.sharedInfo.setUserProfileWithInfo(userInfo)
                    if status == INCOMPLETE_PROFILE {
                        return (status,"You have to complete your profile before login")
                    } else {
                        return (status,nil)
                    }
                }
            } else {
                let message = self.handleErrorOrFailedStatus(result)
                return ("\(String(describing: result["status"] ?? ""))",message)
            }
        }
        return("Error",SOMETHING_WRONG)
    }
    
    //5- Get Msg for Various Response Status
    private func handleErrorOrFailedStatus(_ result:[String:Any]) -> String {
        var message = ""
        if let status = result["status"] as? String {
            
            switch status {
            case DUPLICATE_ACCOUNT:
                message = "Email already registered with us"
                break
            case DUPLICATE_CEASE:
                message = "Email & Mobile already registered with us"
                break
            case FAILED:
                message = "Login Failed. One of username or password is incorrect"
                break
            case INTERNAL_ERROR:
                message = INTERNAL_ERROR_MESSAGE
                break
            case INCOMPLETE_PROFILE:
                message = "You have to complete your profile before login"
                break
            default:
                break
            }
            return message
        }
        return ""
    }
}
