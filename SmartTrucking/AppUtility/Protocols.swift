//
//  Protocols.swift
//  SmartTrucking
//
//  Created by Vivan Raghuvanshi on 27/09/17.
//  Copyright © 2017 Vivan Raghuvanshi. All rights reserved.
//

import Foundation
import GooglePlacesSearchController

protocol CommonDelegate:class {
    func tripHistoryFound(_ tripsFound:[Trip]?)
    func sourcePlaceAdded(_ source:Stop?)
    func destinationPlaceAdded(_ destination:Stop?)
    func waypointPlaceAdded(_ waypoint:[Stop]?)
    func currentTripId(_ tripId:String, withName name:String)
}
