//
//  Constant.swift
//  SmartTrucking
//
//  Created by Vivan Raghuvanshi on 01/09/17.
//  Copyright © 2017 Vivan Raghuvanshi. All rights reserved.
//

/*http://places.demo.api.here.com/places/v1/discover/explore?at=28.6807%2C77.4475&cat=petrol-station&Accept-Language=en%2Cen-US%3Bq%3D0.8%2Chi%3Bq%3D0.6&app_id=dNOg4zIZVImV9h4L44PG&app_code=hFF8mY4VPTdiaANNYJs5YA*/


import Foundation
import UIKit

let SAVED_CATEGORY = "SAVED CATEGORY"
let GOOGLE_API_KEY = "AIzaSyCDLlOH7BWXq1PtgCOASiO5KQ4jE3ZqRfo"

/*let APP_ID = "dNOg4zIZVImV9h4L44PG"
let APP_CODE = "hFF8mY4VPTdiaANNYJs5YA"
let HERE_MAP_KEY = "QjoK6SLkInot26vSHO81I/p+hYAoOSsaoMMkjVnQMQ4KDUtqRb+TjIoEk/Cfsq4N1afbsaABEPkL2uUTNknVoiMgLjwPWpXWcwdStymB2kf+nX3DmyM2kFsN/LU+d9tnxIBnLIJNqSS/LP12IbaP2Fnfm33mfJkUrfoPVQo7BMFAHxmIugnJ17ctOM85/z/mOT/sG5w4llG8jiXwMANcgME6C6GjkVFaAOG6AXEZedXK4mLJXbbP2KS9ed3XrmaMZYt2qt+15qRlvBjZTnODHF0d2eeL9mNXtrBPcaVolPZSksRI8p9Ww/HC9iMIWfGyHrAl24vz8jdStLsKeB+jYJeiDsJiFuKkpTNqJUJK9fakBjjfMJGDRhazunH5yiE7FO4Wc0KBr0reRXVucUfRQ2zKdRTNLjNgCAxgJEszG+9QeuDWx+DXdYUlcBKlTHnz/Jh/ppo/CucLf5hwuMkBv0fLnuIJBPs0/sOMFjRBIZ1+rl3G0S35UnkQqxsr22GgEDpd5TXTp2q2ka7kmAg8vsOSk8g1Fa84WUkVkT0tQe6uclXZR4TyB6o56A9kP/zzGtgcbGFW2P4bD5SSWUsNBbAyQLk1yac/oULUeEFlhF9NRY/6gmh3oWXannQYSqR7649dTyDFW7d5XrHRHd7PL3+y5B9p+VEwR3ODS2DeyXI=" */

let APP_ID = "FEGn6jAXUpCsaP2Dhw1S"
let APP_CODE = "XsAue7HtS8ySNi2TzvESZQ"
let HERE_MAP_KEY = "PIWQqY79heFDFK4pl7hlfNLq4hDr4m6r+xfMhOXTIb83ONr0RDq9J3bNMUmy95D120UcWzuF549Ac/TInEWg8/pIsIywLuhNxi/2QJsSQd3NChTEBQSzkrhbhKLHAmL6QFwJa9wCvzLJsGeaWJ2jB4RdTt7+P88IR3n1IHHEWzMWZpyQQsGSFUidIinyzh6nGhN0vhVDYqzchjM3CczITiVe/YAPSsxopnjWEisUKKPgWxdNns7oHmNXlt0MkpFyda3jsI1lJUUmq90bPIiSdmDAlgPtprxsgMNV/DHooDaXRxlrSolV37XlHoN6fMBbOdfpcERcsUCas6ooIT7Z3/A7xy1jXpTHUf3C0BLzZM4g8dXhu/lARx9sdjURrvzoLn/vRB04HEqStYzzBMvQACSLEtXaMg4h81Fr29NWl1W38APkhEnpEvAqeP7QA6ocOM8GTXkLxwwzYOhUdNf3tp5p8Hc8yQegpphYqhs5KzdI7tQhTHyVG8cKrfCdKYI9QuJnwaCNcqGl25r7Upfdk+GdKWSFhBfnOubhTjOVXEseFseeq3dNoN+JEWhJ7+/neJ6tRaZFVafweY6C1DFSkK+pYw2OcA5ElX+CR0flfXnX7k6PiPNS3YIUFBPSFfQvkjzS5cMB29vXGf8Av0u5dpbNctTMf9NpVyvA639dKgw="


// Response status
let SUCCESS_STATUS = "SUCCESS"
let DUPLICATE_PROCEED = "DUPLICATE_PROCEED"
let SUCCESS_SOCIAL_PROSPECT = "SUCCESS_SOCIAL_PROSPECT"
let DUPLICATE_ACCOUNT = "DUPLICATE_ACCOUNT"
let DUPLICATE_CEASE = "DUPLICATE_CEASE"
let INCOMPLETE_PROFILE = "REGISTERED_INCOMPLETE_PROFILE"
let SOCIAL_LOGIN_SUCCESS = "SUCCESS_SOCIAL_LOGGEDIN"
let SUCCESS_REGULAR_LOGGEDIN = "SUCCESS_REGULAR_LOGGEDIN"
let FAILED = "FAILED"
let INTERNAL_ERROR = "INTERNAL_ERROR"
let INTERNAL_ERROR_MESSAGE = "It seems to be a slow day. Please try after some time"

func getTime(seconds : Int) -> (Int, Int, Int) {
    return (seconds / 3600, (seconds % 3600) / 60, (seconds % 3600) % 60)
}

func getDistance(_ distanceString:String) -> String {
   // [NSString stringWithFormat:@"%.1fmi",(distance/1609.344)]);
    
    let distance = Double(distanceString) ?? 0.00
    let distanceInMiles = (distance/1609.344).rounded(toPlaces: 2)
    return "\(distanceInMiles)"
}


func convertTime(_ timeString:String) -> String {
    var finalStr = ""
    let str = timeString;
    let strArray = str.components(separatedBy: " ")
   
    for (index,element) in strArray.enumerated() {
        if element == "hours" {
            finalStr = "\(strArray[index-1])"
            
            if strArray.contains("mins")
            {
                finalStr = finalStr  + ":"
            }
            else{
                
            }
        }
        
        if element == "mins" {
            finalStr = finalStr + "\(strArray[index-1])"
            if !strArray.contains("hours") {
                finalStr = "00:" + finalStr
            }
        }
    }
    return finalStr
}

func convertDistance(_ distanceString:String) -> String {
    var finalStr = ""
    
    let strArray = distanceString.components(separatedBy: " ")
    
    if strArray.count > 0 {
        finalStr = strArray[0]
        return finalStr
    } else {
        return ""
    }
}

typealias TimeDistanceComponent = (day: String, hour: Int, min: Int, sec: String, distance: Int)




// If a pending trip exist fetch it's Information
func fetchTripInformationFor(_ tripId:Any, getResponse: @escaping (_: Any?, _: Error?) -> Void) {
    let params = ["trip_id":tripId]
    let networkManager = NetworkManager()
    networkManager.getDataForRequest(getTrip, andParameter: params) { (response, error) in
        getResponse(response, error)
    }
}



//let url = "https://maps.googleapis.com/maps/api/directions/json?origin=\(origin)&destination=\(destination)&sensor=true&mode=driving&key=\(GOOGLE_API_KEY)"

//   https://maps.googleapis.com/maps/api/distancematrix/json?units=imperial&origins=29.7291051,-95.5285517&destinations=29.7395119,-95.5212036&key=AIzaSyAivTcyTYfAwpQIwlTz5B6gv89y1iLzDts

//  https://roads.googleapis.com/v1/snapToRoads?path=-35.27801,149.12958|-35.28032,149.12907|-35.28099,149.12929|-35.28144,149.12984|-35.28194,149.13003|-35.28282,149.12956|-35.28302,149.12881|-35.28473,149.12836&interpolate=true&key=YOUR_API_KEY

//   https://maps.googleapis.com/maps/api/directions/json?origin=25.4577986,78.5745274&destination=25.320746,82.9208962&waypoints=via:28.6818571%2C77.447064%7Cvia:28.5055506%2C77.6921981&key=AIzaSyCDLlOH7BWXq1PtgCOASiO5KQ4jE3ZqRfo

