//
//  UITextFieldX.swift
//  SmartTrucking
//
//  Created by Vivan Raghuvanshi on 10/09/17.
//  Copyright © 2017 Vivan Raghuvanshi. All rights reserved.
//

import UIKit

@IBDesignable class UITextFieldX: UITextField,Designable {
    
    
    //1- Set border width
    @IBInspectable var border_Width : CGFloat = CGFloat.nan {
        didSet {
            configureBorder()
        }
    }
    
    //2- Set border color
    @IBInspectable var border_Color : UIColor = UIColor.clear {
        didSet {
            self.layer.borderColor = borderColor?.cgColor
        }
    }
    
    //3- Set corner radius
    @IBInspectable var corner_Radius : CGFloat = 0.0 {
        didSet {
            self.layer.cornerRadius = 5.0
        }
    }
    
    //4- Set left/right padding
    @IBInspectable var leftPadding: CGFloat = 0
    @IBInspectable var rightPadding: CGFloat = 0
    
    override func textRect(forBounds bounds: CGRect) -> CGRect {
        
      //  return CGRect(x: bounds.origin.x + leftPadding, y: leftPadding, bounds.origin.y, width: bounds.size.width - leftPadding - rightPadding, height: bounds.size.height)
        //return bounds.insetBy(dx: leftPadding, dy: leftPadding)
        return CGRect(x: bounds.origin.x + leftPadding, y: bounds.origin.y, width: bounds.size.width - leftPadding - rightPadding, height: bounds.size.height)

        
        
    }
    
    override func editingRect(forBounds bounds: CGRect) -> CGRect {
        return textRect(forBounds: bounds)
    }
    
    
    //5- Set left image
    @IBInspectable var leftImage: UIImage? {
        didSet {
            leftViewMode = UITextField.ViewMode.always
            let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 20, height: 20))
            imageView.image = leftImage
            // Note: In order for your image to use the tint color, you have to select the image in the Assets.xcassets and change the "Render As" property to "Template Image".
            imageView.tintColor = placeholderColor
            leftView = imageView
        }
    }
    
    //5- Set left image
    @IBInspectable var rightImage: UIImage? {
        didSet {
            rightViewMode = UITextField.ViewMode.always
            let imageView = UIImageView(frame: CGRect(x: 100, y: 0, width: 20, height: 20))
            imageView.image = rightImage
            // Note: In order for your image to use the tint color, you have to select the image in the Assets.xcassets and change the "Render As" property to "Template Image".
            imageView.tintColor = placeholderColor
            rightView = imageView
        }
    }
    
     //6- Set placeholder color
    @IBInspectable var placeholderColor: UIColor = UIColor.lightGray {
        didSet {
            attributedPlaceholder = NSAttributedString(string: placeholder != nil ?  placeholder! : "", attributes:[NSAttributedString.Key.foregroundColor: placeholderColor])
        }
    }
    
    @IBInspectable var shadow_Color : UIColor = UIColor.clear {
        didSet {
            addShadow()
        }
    }
    
    func addShadow(shadowOffset: CGSize = CGSize(width: 0.25, height: 0.5),
                   shadowOpacity: Float = 0.4,
                   shadowRadius: CGFloat = 3.0) {
        layer.shadowColor = shadow_Color.cgColor
        layer.shadowOffset = shadowOffset
        layer.shadowOpacity = shadowOpacity
        layer.shadowRadius = shadowRadius
    }
}
