//
//  ProtocolX.swift
//  SmartTrucking
//
//  Created by Vivan Raghuvanshi on 10/09/17.
//  Copyright © 2017 Vivan Raghuvanshi. All rights reserved.
//

import UIKit

protocol Designable {
    
    var border_Width : CGFloat { get set }
    var border_Color : UIColor { get set }
    
}

extension Designable where Self: UIView {
    
    func configureBorder() {
        layer.borderWidth = border_Width
        layer.borderColor = border_Color.cgColor
    }
    
    func jitter() {
        let animation = CABasicAnimation(keyPath:"position")
        animation.duration = 0.05
        animation.repeatCount = 4
        animation.autoreverses = true
        
        let fromPoint = CGPoint.init(x: self.center.x - 5.0, y: self.center.y)
        let toPoint = CGPoint.init(x: self.center.x + 5.0, y: self.center.y)

        animation.fromValue = NSValue(cgPoint:fromPoint)
        animation.toValue = NSValue(cgPoint:toPoint)
        
        layer.add(animation, forKey:"position")

        
    }
}
