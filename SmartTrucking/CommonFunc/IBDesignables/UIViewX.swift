//
//  UIViewX.swift
//  SmartTrucking
//
//  Created by Vivan Raghuvanshi on 10/09/17.
//  Copyright © 2017 Vivan Raghuvanshi. All rights reserved.
//

import UIKit

@IBDesignable class UIViewX: UIView,Designable {

    @IBInspectable var border_Width : CGFloat = 0.0 {
        didSet {
            configureBorder()
        }
    }
    
    @IBInspectable var border_Color : UIColor = UIColor.clear {
        didSet {
            configureBorder()
        }
    }
    
    @IBInspectable var corner_Radius : CGFloat = 0.0 {
        didSet {
            self.layer.cornerRadius = corner_Radius
        }
    }
    
    @IBInspectable var FirstColor : UIColor = UIColor.clear {
        didSet {
            updateView()
        }
    }
    
    @IBInspectable var SecondColor : UIColor = UIColor.clear {
        didSet {
            updateView()
        }
    }
    
    override class var layerClass: AnyClass {
        get {
            return CAGradientLayer.self
        }
    }
    
    private func updateView () {
        let layer = self.layer as! CAGradientLayer
        layer.colors = [FirstColor.cgColor, SecondColor.cgColor]
        layer.locations = [0.6]
    }
    
    @IBInspectable var shadow_Color : UIColor = UIColor.clear {
        didSet {
            addShadow()
        }
    }
    
    func addShadow(shadowOffset: CGSize = CGSize(width: 0.25, height: 0.5),
                   shadowOpacity: Float = 0.4,
                   shadowRadius: CGFloat = 3.0) {
        layer.shadowColor = shadow_Color.cgColor
        layer.shadowOffset = shadowOffset
        layer.shadowOpacity = shadowOpacity
        layer.shadowRadius = shadowRadius
    }
    
    

}
