//
//  UIButtonX.swift
//  SmartTrucking
//
//  Created by Vivan Raghuvanshi on 10/09/17.
//  Copyright © 2017 Vivan Raghuvanshi. All rights reserved.
//

import UIKit

@IBDesignable class UIButtonX: UIButton,Designable {
    
    @IBInspectable var border_Width : CGFloat = 0.0 {
        didSet {
            configureBorder()
        }
    }
    
    @IBInspectable var border_Color : UIColor = UIColor.clear {
        didSet {
            configureBorder()
        }
    }
    
    @IBInspectable var corner_Radius : CGFloat = 0.0 {
        didSet {
            self.layer.cornerRadius = 5.0
        }
    }
    
    @IBInspectable var FirstColor : UIColor = UIColor.clear {
        didSet {
            updateView()
        }
    }
    
    @IBInspectable var SecondColor : UIColor = UIColor.clear {
        didSet {
            updateView()
        }
    }
    
    override class var layerClass: AnyClass {
        get {
            return CAGradientLayer.self
        }
    }
    
    private func updateView () {
        let layer = self.layer as! CAGradientLayer
        layer.colors = [FirstColor.cgColor, SecondColor.cgColor]
        layer.locations = [0.5]
    }


}

