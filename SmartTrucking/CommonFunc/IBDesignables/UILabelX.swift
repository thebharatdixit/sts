//
//  UILabelX.swift
//  SmartTrucking
//
//  Created by Vivan Raghuvanshi on 10/09/17.
//  Copyright © 2017 Vivan Raghuvanshi. All rights reserved.
//

import UIKit

@IBDesignable class UILabelX: UILabel,Designable {
    
    @IBInspectable var border_Width : CGFloat = 0.0 {
        didSet {
            configureBorder()
        }
    }
    
    @IBInspectable var border_Color : UIColor = UIColor.clear {
        didSet {
            configureBorder()
        }
    }
    
    @IBInspectable var corner_Radius : CGFloat = 0.0 {
        didSet {
            self.layer.cornerRadius = 5.0
        }
    }

}
