//
//  DateFunc.swift
//  SmartTrucking
//
//  Created by Vivan Raghuvanshi on 19/08/17.
//  Copyright © 2017 Vivan Raghuvanshi. All rights reserved.
//

import Foundation

//*************************************

// convert local date to server date string

func getServerDateStringFromDate(_ date : Date) -> String {
    let dateFromatter = DateFormatter()
    dateFromatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
    dateFromatter.timeZone = TimeZone(abbreviation:"UTC")
    dateFromatter.locale = Locale.init(identifier:"en_US_POSIX")
    
    let serverString = dateFromatter.string(from: date)
    return serverString
}

//// convert server date string to local date string

func getLocaleDateStringToServerDateString(_ serverString : String) -> String {
    
    let dateFromatter = DateFormatter()
    dateFromatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
    dateFromatter.timeZone = TimeZone(abbreviation:"UTC")
    dateFromatter.locale = Locale.init(identifier:"en_US_POSIX")
    
    let serverDate = dateFromatter.date(from: serverString)
    
    let date_Fromatter = DateFormatter()
    date_Fromatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
    date_Fromatter.timeZone = NSTimeZone.local
    date_Fromatter.locale = Locale.init(identifier:"en_US_POSIX")
    
    let localString = dateFromatter.string(from: serverDate!)
    return localString
}

// Covert the date format
//1- Get time date together
func getTimeDateStringFromDate(_ date : Date) -> String {
    let dateFromatter = DateFormatter()
    dateFromatter.dateFormat = "yyyy-MM-dd hh:mm a"
    dateFromatter.timeZone = NSTimeZone.local //TimeZone(abbreviation:"GMT")
    dateFromatter.locale = Locale.init(identifier:"en_US_POSIX")
    
    let serverString = dateFromatter.string(from: date)
    return serverString
}

//2- Get date
func getDateStringFromDate(_ date : Date) -> String {
    let dateFromatter = DateFormatter()
    dateFromatter.dateFormat = "yyyy-MM-dd"
    dateFromatter.timeZone = NSTimeZone.local //TimeZone(abbreviation:"GMT")
    dateFromatter.locale = Locale.init(identifier:"en_US_POSIX")
    
    let serverString = dateFromatter.string(from: date)
    return serverString
}



//3- Get time
func getTimeStringFromDate(_ date : Date) -> String {
    let dateFromatter = DateFormatter()
    dateFromatter.dateFormat = "hh:mm a"
    dateFromatter.timeZone = NSTimeZone.local //TimeZone(abbreviation:"GMT")
    dateFromatter.locale = Locale.init(identifier:"en_US_POSIX")
    
    let serverString = dateFromatter.string(from: date)
    return serverString
}

//4- Get DateComponents

typealias DateComponent = (weekDay: String, date: Int, month: Int, monthStr: String, year: Int)

func getDateComponentsFromDate(_ date : Date) -> DateComponent {
    let dateFromatter = DateFormatter()
    dateFromatter.dateFormat = "yyyy-MM-dd"
    dateFromatter.timeZone = NSTimeZone.local //TimeZone(abbreviation:"GMT")
    dateFromatter.locale = Locale.init(identifier:"en_US_POSIX")
    
    let dayComponent = Calendar.current.component(.weekday, from: date)
    let weekDaystr = dateFromatter.weekdaySymbols[dayComponent - 1]
    
    let monthComponent = Calendar.current.component(.month, from: date)
    let monthstr = dateFromatter.monthSymbols[monthComponent - 1]
    
    let yearComponent = Calendar.current.component(.year, from: date)
    let dateComponent = Calendar.current.component(.day, from: date)
    
    let myDate = DateComponent(weekDay: weekDaystr, date:dateComponent, month:monthComponent, monthStr:monthstr, year:yearComponent)
    return myDate
}

//5- Get Trip name format
func getDateStringFromDateInTripNameFormat(_ date : Date) -> String {
    let dateFromatter = DateFormatter()
    dateFromatter.dateFormat = "MMMMddyyyy"
    dateFromatter.timeZone = NSTimeZone.local //TimeZone(abbreviation:"GMT")
    dateFromatter.locale = Locale.init(identifier:"en_US_POSIX")
    
    let serverString = dateFromatter.string(from: date)
    return "Trip_\(serverString)"
}

// Swift 3:
extension Date {
    
    func toMillis() -> Int64! {
        return Int64(self.timeIntervalSince1970 * 1000)
    }
    
//    var ticks: UInt64 {
//        return UInt64((self.timeIntervalSince1970 + 62_135_596_800) * 10_000_000)
//    }
//    
//    init(ticks: UInt64) {
//        self.init(timeIntervalSince1970: Double(ticks)/10_000_000 - 62_135_596_800)
//    }
}
