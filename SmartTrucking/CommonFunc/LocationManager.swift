//
//  LocationManager.swift
//  SmartTrucking
//
//  Created by Vivan Raghuvanshi on 19/08/17.
//  Copyright © 2017 Vivan Raghuvanshi. All rights reserved.
//


import UIKit
import CoreLocation

class LocationManager: NSObject {
    
    typealias completionHandler = (_ :Double?, _ :Double?, _:Error?) -> Void
    
    private let ALERT_TITLE = "Access Denied"
    private let ALERT_ACCESS = "Turn-on location services in settings"
    private let ALERT_MESSAGE = "Application is not allowed to access your current location, enable it into settings"
    
    private lazy var locationManager:CLLocationManager = {
        let location_Manager = CLLocationManager()
        location_Manager.distanceFilter = kCLDistanceFilterNone
        location_Manager.desiredAccuracy = kCLLocationAccuracyBest
        
        if CLLocationManager.locationServicesEnabled(){
            location_Manager.startUpdatingLocation()
        }
        return location_Manager
    }()
    
    fileprivate var locationFound:completionHandler?
    
    static let sharedLocation = LocationManager()
    
    func startUpdatingCurrentLocation(_ getLocation:@escaping completionHandler) {
        
        locationFound = getLocation
        
        if (CLLocationManager.locationServicesEnabled()) {
            
            self.locationManager.delegate = self
            let statusCode = CLLocationManager.authorizationStatus()
            
            switch statusCode {
            case .denied:
                self.showLocationAlert(ALERT_TITLE, message:ALERT_MESSAGE)
                break
                
            case .notDetermined:
                self.locationManager.requestWhenInUseAuthorization()
                break
                
            case .restricted:
                self.showLocationAlert(ALERT_TITLE, message:ALERT_MESSAGE)
                break
                
            default:
                break
            }
            self.locationManager.requestLocation()
        }
        else {
            showLocationAlert(ALERT_TITLE, message:ALERT_ACCESS)
        }
    }
    
    private func showLocationAlert(_ title: String, message: String) {
        
        DispatchQueue.main.async {hideHud()}
        
        if let viewController = UIApplication.shared.windows.first?.rootViewController as UIViewController? {
            let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
            
            // Create the actions
            let okAction = UIAlertAction(title: "Setting", style: UIAlertAction.Style.destructive) {
                UIAlertAction in
                UIApplication.shared.open(URL(string: UIApplication.openSettingsURLString)!)
                
            }
            let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel) {
                UIAlertAction in
            }
            
            // Add the actions
            alertController.addAction(okAction)
            alertController.addAction(cancelAction)
            
            // Present the controller
            viewController.present(alertController, animated: true, completion: nil)
        }
    }
}



extension LocationManager : CLLocationManagerDelegate {
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        if let currentLocation = locations.last {
            if let location_Found = self.locationFound {
                location_Found(currentLocation.coordinate.latitude,currentLocation.coordinate.longitude,nil)
                manager.stopUpdatingLocation()
            }
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        if let location_Found = self.locationFound {
            location_Found(nil,nil,error)
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        
        if (status == .authorizedAlways || status == .authorizedWhenInUse) {
            manager.requestLocation()
        }
            
        else if (status == .denied) {
            //denied
        }
    }
    
}



