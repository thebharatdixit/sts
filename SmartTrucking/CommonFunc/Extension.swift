//
//  Extension.swift
//  SmartTrucking
//
//  Created by Vivan Raghuvanshi on 19/08/17.
//  Copyright © 2017 Vivan Raghuvanshi. All rights reserved.
//

import Foundation
import UIKit
import IBAnimatable
import Material

let aiFrame = CGRect.init(x:(SCREEN_WIDTH - 52), y:33.5, width: 37, height: 37)

extension UIViewController : UITextFieldDelegate {
    
    func startIndicator(_ sender:UIViewController, forFrame activityFrame:CGRect?, withCenter center:CGPoint?) {
        let activity = UIActivityIndicatorView()
        activity.tag = 1000
        activity.style = .gray
        activity.hidesWhenStopped = true
        if activityFrame == nil {
            activity.frame = aiFrame
        } else {
            activity.frame = activityFrame!
        }
        sender.view.addSubview(activity)
        sender.view.bringSubviewToFront(activity)
        if let c = center {
            activity.center = c
        }
        activity.startAnimating()
    }
    
    func stopIndicator(_ sender:UIViewController) {
        DispatchQueue.main.async {
            if let activity = sender.view.viewWithTag(1000) as? UIActivityIndicatorView {
                activity.stopAnimating()
            }
        }
    }
    
    
    public func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true;
    }
}

extension UILabel {
    
    override open func awakeFromNib() {
         super.awakeFromNib()
        self.setup()
    }
    
    func setup() {
        self.font = UIFont(name: (self.font?.fontName)!, size: (self.font?.pointSize)!*getScaleFactor())
    }
}

private var kAssociationKeyMaxLength: Int = 0

@IBDesignable extension UITextField {
    
    override open func awakeFromNib() {
        super.awakeFromNib()
        self.setup()
    }
    
    func setup() {
        self.font = UIFont(name: (self.font?.fontName)!, size: (self.font?.pointSize)!*getScaleFactor())
    }
    
    @IBInspectable var maxLength: Int {
        get {
            if let length = objc_getAssociatedObject(self, &kAssociationKeyMaxLength) as? Int {
                return length
            } else {
                return Int.max
            }
        }
        set {
            objc_setAssociatedObject(self, &kAssociationKeyMaxLength, newValue, .OBJC_ASSOCIATION_RETAIN)
            addTarget(self, action: #selector(checkMaxLength), for: .editingChanged)
        }
    }
    
    @objc func checkMaxLength() {
        guard let prospectiveText = self.text,
            prospectiveText.count > maxLength
            else {
                return
        }
        
        let selection = selectedTextRange
        let maxCharIndex = prospectiveText.index(prospectiveText.startIndex, offsetBy: maxLength)
        text = prospectiveText.substring(to: maxCharIndex)
        selectedTextRange = selection
    }
    
}

extension String {
    
    func contains(find: String) -> Bool{
        return self.range(of: find) != nil
    }
    
    func containsIgnoringCase(find: String) -> Bool{
        return self.range(of: find, options: String.CompareOptions.caseInsensitive) != nil
    }
    
    var boolValue: Bool {
        return NSString(string: self).boolValue
    }
    
    /// Percent escape value to be added to a HTTP request
    ///
    /// This percent-escapes all characters besides the alphanumeric character set and "-", ".", "_", and "*".
    /// This will also replace spaces with the "+" character as outlined in the application/x-www-form-urlencoded spec:
    ///
    /// http://www.w3.org/TR/html5/forms.html#application/x-www-form-urlencoded-encoding-algorithm
    ///
    /// - returns: Return percent escaped string.
    
    func addingPercentEncodingForQuery() -> String? {
        let allowed = CharacterSet(charactersIn: "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789-._* ")
        
        return addingPercentEncoding(withAllowedCharacters: allowed)?.replacingOccurrences(of: " ", with: "+")
    }
    
    func addingPercentEncodingForURLQueryValue() -> String? {
        let generalDelimitersToEncode = ":#[]@" // does not include "?" or "/" due to RFC 3986 - Section 3.4
        let subDelimitersToEncode = "!$&'()*+,;="
        
        var allowed = CharacterSet.urlQueryAllowed
        allowed.remove(charactersIn: generalDelimitersToEncode + subDelimitersToEncode)
        
        return addingPercentEncoding(withAllowedCharacters: allowed)
    }
}


extension Dictionary {
    
    /// Build string representation of HTTP parameter dictionary of keys and objects
    ///
    /// This percent escapes in compliance with RFC 3986
    ///
    /// http://www.ietf.org/rfc/rfc3986.txt
    ///
    /// - returns: String representation in the form of key1=value1&key2=value2 where the keys and values are percent escaped
    
    public func stringFromHttpParameters() -> String {
        let parameterArray = map { key, value -> String in
            let percentEscapedKey = (key as! String).addingPercentEncodingForURLQueryValue()!
            let percentEscapedValue = (value as! String).addingPercentEncodingForURLQueryValue()!
            return "\(percentEscapedKey)=\(percentEscapedValue)"
        }
        
        return parameterArray.joined(separator: "&")
    }
    
}

extension URLRequest {
    
    /// Populate the HTTPBody of `application/x-www-form-urlencoded` request
    ///
    /// - parameter parameters:   A dictionary of keys and values to be added to the request
    
    mutating func setBodyContent(_ parameters: [String : String]) {
        let parameterArray = parameters.map { (key, value) -> String in
            return "\(key.addingPercentEncodingForQuery()!)=\(value.addingPercentEncodingForQuery()!)"
        }
        httpBody = parameterArray.joined(separator: "&").data(using: .utf8)
    }
}




extension UIView {
    
    var parentViewController: UIViewController? {
        var parentResponder: UIResponder? = self
        while parentResponder != nil {
            parentResponder = parentResponder!.next
            if let viewController = parentResponder as? UIViewController {
                return viewController
            }
        }
        return nil
    }
    
    public func animateView() {
        
        UIButton.animate(withDuration: 0.1,
                         animations: {
                            self.transform = CGAffineTransform(scaleX: 1.1, y: 1.1)
        },
                         completion: { finish in
                            UIButton.animate(withDuration: 0.1, animations: {
                                self.transform = CGAffineTransform.identity
                            })
        })
    }
    
    
    public func jitter() {
        let animation = CABasicAnimation(keyPath:"position")
        animation.duration = 0.05
        animation.repeatCount = 2
        animation.autoreverses = true
        
        let fromPoint = CGPoint.init(x: self.center.x - 5.0, y: self.center.y)
        let toPoint = CGPoint.init(x: self.center.x + 5.0, y: self.center.y)
        
        animation.fromValue = NSValue(cgPoint:fromPoint)
        animation.toValue = NSValue(cgPoint:toPoint)
        
        layer.add(animation, forKey:"position")
        
        if #available(iOS 10.0, *) {
            let generator = UIImpactFeedbackGenerator(style: .heavy)
            generator.impactOccurred()
        } else {
            // Fallback on earlier versions
        }
        
    }
    
    public func flashView() {
        UIView.animate(withDuration: 0.3, delay: 0, options: .curveEaseIn, animations: {
            self.alpha = 1
        }) { (animationCompleted) in
            if animationCompleted == true {
                UIView.animate(withDuration: 0.3, delay: 3.0, options: .curveEaseOut, animations: {
                    self.alpha = 0
                }, completion: nil)
            }
        }
    }
    
    public func roundCorner() {
        let height = self.bounds.height
        self.layer.cornerRadius = height/2
        self.clipsToBounds = true
    }
}

extension UIImage {
    
    
    
    
    func scaleImage(_ image:UIImage)->UIImage {
        
        let maxSize = 500.0 as CGFloat
        let width = image.size.width
        let height = image.size.height
        
        var newWidth = width
        var newHeight = height
        
        if (width > maxSize || height > maxSize) {
            if (width > height) {
                newWidth = maxSize;
                newHeight = (height*maxSize)/width;
            } else {
                newHeight = maxSize;
                newWidth = (width*maxSize)/height;
            }
            
            
            let newSize = CGSize.init(width: newWidth, height: newHeight)
            
            UIGraphicsBeginImageContext(newSize)
            
            image.draw(in: CGRect.init(x: 0, y: 0, width: newSize.width, height: newSize.height))
            let newImage = UIGraphicsGetImageFromCurrentImageContext()
            
            UIGraphicsEndImageContext()
            return newImage!
        }
        return image
    }
}


extension UIButton {
    
    override open func awakeFromNib() {
        super.awakeFromNib()
        self.setup()
    }
    
//    override open func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
//        self.isUserInteractionEnabled = false
//        self.perform(#selector(enableUserInteraction), with: self, afterDelay: 0.5)
//    }
//
//    @objc private func enableUserInteraction() {
//        self.isUserInteractionEnabled = true
//    }
    
    func setup() {
        self.titleLabel?.font = UIFont(name: (self.titleLabel?.font?.fontName)!, size: (self.titleLabel?.font?.pointSize)!*getScaleFactor())
    }
}

extension Double {
    /// Rounds the double to decimal places value
    func rounded(toPlaces places:Int) -> Double {
        let divisor = pow(10.0, Double(places))
        return (self * divisor).rounded() / divisor
    }
    
    var dollarString:String {
        return String(format: "$%.2f", self)
    }
}

extension TextField {
    override open func awakeFromNib() {
        super.awakeFromNib()
        self.detailColor = .red
    }

}

extension Error {
    var code: Int { return (self as NSError).code }
    var domain: String { return (self as NSError).domain }
}

extension UITextField{
    @IBInspectable var doneAccessory: Bool{
        get{
            return self.doneAccessory
        }
        set (hasDone) {
            if hasDone{
                addDoneButtonOnKeyboard()
            }
        }
    }
    
    func addDoneButtonOnKeyboard()
    {
        let doneToolbar: UIToolbar = UIToolbar(frame: CGRect.init(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 50))
        doneToolbar.barStyle = .default
        
        let flexSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let done: UIBarButtonItem = UIBarButtonItem(title: "Done", style: .done, target: self, action: #selector(self.doneButtonAction))
        
        let items = [flexSpace, done]
        doneToolbar.items = items
        doneToolbar.sizeToFit()
        
        self.inputAccessoryView = doneToolbar
    }
    
    @objc func doneButtonAction()
    {
        self.resignFirstResponder()
    }
}

extension UITextView {
    
    @IBInspectable var doneAccessory: Bool{
        get{
            return self.doneAccessory
        }
        set (hasDone) {
            if hasDone{
                addDoneButtonOnKeyboard()
            }
        }
    }
    
    func addDoneButtonOnKeyboard()
    {
        let doneToolbar: UIToolbar = UIToolbar(frame: CGRect.init(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 50))
        doneToolbar.barStyle = .default
        
        let flexSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let done: UIBarButtonItem = UIBarButtonItem(title: "Done", style: .done, target: self, action: #selector(self.doneButtonAction))
        
        let items = [flexSpace, done]
        doneToolbar.items = items
        doneToolbar.sizeToFit()
        
        self.inputAccessoryView = doneToolbar
    }
    
    @objc func doneButtonAction()
    {
        self.resignFirstResponder()
    }
    
    
}

extension String {
    
    func DateFormat() -> String{
        let localTime = UTCToLocal(date: self,datformatStr: "dd/MM/yyyy")
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.dateFormat = "dd/MM/yyyy"
        let dateFormatterPrint = DateFormatter()
        dateFormatterPrint.dateFormat = "dd/MM/yyyy "
        let date: NSDate? = dateFormatterGet.date(from: localTime)! as NSDate
        return (dateFormatterPrint.string(from: date! as Date))
    }
    
    func postDateFormate() -> String{
        
        
        let userCalendar = Calendar.current
        let formatter = DateFormatter()
        formatter.dateFormat = "dd/MM/yyyy HH:mm:ss"
        let startTime = formatter.string(from: Date())
        //convert UTC to Local
        let localTime = UTCToLocal(date: self,datformatStr:"dd/MM/yyyy HH:mm:ss" )
//        print("UTC:\(localTime)")
//        formatter.timeZone = TimeZone(abbreviation: "UTC")
        formatter.dateFormat = "dd/MM/yyyy HH:mm:ss"
        let someDateTime = formatter.date(from: localTime)
//        let dateTime = formatter.string(from: someDateTime!)
//        debugPrint("result:\((startTime,dateTime))")
        let requestedComponent: Set<Calendar.Component> = [ .month, .day, .hour, .minute, .second, .year]
        let timeDifference = userCalendar.dateComponents(requestedComponent, from: someDateTime!, to: formatter.date(from: startTime)!)
        let hourDiff:Int = timeDifference.hour!
        let minutesDiff:Int = timeDifference.minute!
        let dayDiff:Int = timeDifference.day!
        let yearDiff:Int = timeDifference.year!
        
//        debugPrint("timedifference:\(timeDifference)")
        
        if yearDiff == 0 {
            if dayDiff == 0{
                if hourDiff > 0 {
                    if  hourDiff < 24 {
                        //   print("\(String(describing: hourDiff)) Hr. Ago")
                        return "\(String(describing: hourDiff))" + " hr ago"
                    }else{
                        // print("date")
                        return self.DateFormat()
                    }
                    
                }else{
                    if minutesDiff > 0 {
                        return "\(String(describing: minutesDiff))" + " min ago"
                        
                    }else{
                        return "Just now"
                    }
                    
                    
                }
            }else if dayDiff == 1 {
//                let dateStr = self.changeDateFormat().components(separatedBy: " ")
//                if dateStr.count>1{
//                    return "Yesterday at \(dateStr[1]) \(dateStr[2])"
//                }
//
            }else if dayDiff > 1 {
                
//                return self.changeDateFormatIntoMonthsName(sameYear: true)
                
            }
        }else{
//            return self.changeDateFormatIntoMonthsName(sameYear: false)
            
        }
        return ""
    }
    
    //TimeZone UTC to Local
    func UTCToLocal(date:String,datformatStr:String) -> String {
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = datformatStr
//        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
        
        let dt = dateFormatter.date(from: date)
        dateFormatter.timeZone = TimeZone.current
        // dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
        
        print("ITC to UTC:\(dateFormatter.string(from: dt!))")
        return dateFormatter.string(from: dt!)
        
    }
    
    func fromBase64() -> String? {
        guard let data = Data(base64Encoded: self) else {
            return nil
        }
        
        return String(data: data, encoding: .utf8)
    }
    
    func base64Decoded() -> String? {
        guard let data = Data(base64Encoded: self) else { return nil }
        return String(data: data, encoding: .utf8)
    }
    
    
}

extension UITableView{
    
    func setEmptyMessage(_ message: String) {
        
        let messageLabel = UILabel(frame: CGRect(x: 0, y: self.bounds.size.height/2, width: self.bounds.size.width, height: self.bounds.size.height))
        messageLabel.text = message
        messageLabel.textColor = .gray
        messageLabel.numberOfLines = 0;
        messageLabel.textAlignment = .center;
        messageLabel.font = UIFont(name: "Roboto", size: 18)
        messageLabel.sizeToFit()
        self.backgroundView = messageLabel
        
    }
    
}

extension UIImageView {
    func circleWithBorderColor(color: UIColor, lineWidth: CGFloat) {
        let image = self
        image.layer.borderWidth = lineWidth
        image.layer.masksToBounds = false
        image.layer.borderColor = color.cgColor
        image.layer.cornerRadius = image.frame.height/2
        image.clipsToBounds = true
    }
    func dropShadow (){
        let image = self
        image.layer.cornerRadius = 4
        image.layer.masksToBounds = false
        image.layer.shadowColor = UIColor.black.cgColor
        image.layer.shadowOpacity = 0.5
        image.layer.shadowOffset = CGSize(width: 0, height: 1)
        image.layer.shadowRadius = 2
 }

}
