//
//  NetworkManager.swift
//  SmartTrucking
//
//  Created by Vivan Raghuvanshi on 23/08/17.
//  Copyright © 2017 Vivan Raghuvanshi. All rights reserved.
//

import Foundation
import UIKit
import Alamofire
import SwiftyJSON

//let baseUrl = "http://smarttrucking.crazythincos.in/"
let baseUrl = "http://console.smarttruckingapp.com/"
//1- Sign Up
let signUp = "profiles"
//2- User Sign In
let signIn = "auth"
//3- Home Screen dashboard
let dashBoard = "dashboard"
//4- Create A New Trip
let calculateTrip = "createTrip"   //createTrip  //trips
//5- Close existing Trip
let closeTrip = "closeTrip"
//Cancel a trip
let cancelTrip = "cancelTrip"
//6- Add expenses on existing Trip
let addExpenses = "expenses"
//7- Get Recent History Trip
let history = "history"
//8- Change Password
let resetPassword = "reset"
//9- Change Address
let resetAddress = "address"
//10- Change Personal Info
let personalInfo = "personalInfo"
//11- Change Personal Info
let professionalInfo = "professionalInfo"
//12- Get User Info
let getUserInfo = "info"
//13- Add Monthly Expense
let monthlyExpenses = "monthlyExpenses"
//14- Upload Image
let uploadimage = "profile/images"
//15- Fetch Image
let fetchimage = "profile/image"
//16- Send Distance to server
let distances = "distances"
//17- Get companies Listing
let companies = "companies"
//18- Monthly Expenses History
let monthlyExpensesHistory = "monthlyExpensesHistory"
//19- Get trip information
let getTrip = "trip"
//20- Get trip information
let truckOptions = "options"
//21- Get Monthwise information
let monthWiseReport = "monthWiseReport"
let poiSearch = "places"
let createPost = "createPost"
let getAllPost = "getAllPost"
let deleteAllPost = "deletePost"
let createPostComment = "createPostComment"
let getAllPostComment = "getAllPostComment"

typealias serviceCompletion = (_: Any?, _: Error?) -> Void
typealias imageCompletion = (_: UIImage?) -> Void

var getMapListArray: [GetAllPost] = []


class NetworkManager: NSObject {
    
    func getResponseDataDictionaryFromData(data: Data) -> (responseData: Dictionary<String, Any>?, error: Error?)
    {
        do
        {
            let responseData = try JSONSerialization.jsonObject(with: data, options: JSONSerialization.ReadingOptions.allowFragments) as? Dictionary<String, Any>
            print("Success with JSON: \(String(describing: responseData))")
            return (responseData, nil)
        }
        catch let error
        {
            print("json error: \(error.localizedDescription)")
            return (nil, error)
        }
    }
    
    func getDataForPostRequest(_ request: String, andParameter parameters: [String:Any]?, withCompletion getResponse: @escaping serviceCompletion) {
        
        let reuestUrl = baseUrl + request
        print(parameters ?? "")
        
        var encodingFormat: ParameterEncoding = URLEncoding()
        if request == calculateTrip {
            encodingFormat = JSONEncoding()
        }
        Alamofire.request(reuestUrl,
                          method: .post,
                          parameters: parameters,
                          encoding: JSONEncoding.default,
                          headers: nil).responseJSON(completionHandler:
                            { (DataResponse) in
//                                SVProgressHUD.dismiss()
                                switch DataResponse.result
                                {
                                case .success(let json):
                                    print("Success with JSON: \(json)")
                                    print("Success with status Code: \(String(describing: DataResponse.response?.statusCode))")
                                    
                                    if DataResponse.response?.statusCode == 200 {
                                        
                                       print("Data Response")
                                        
                                        let swiftyJsonVar = JSON(DataResponse.result.value!)
                                        print(swiftyJsonVar)
                                        let result = swiftyJsonVar.dictionaryObject
                                        getResponse(result ?? "", nil)
                                        
                                        
//                                        let response = self.getResponseDataDictionaryFromData(data: DataResponse.data!)
//                                        print("Response:\(response)")
//                                        getResponse(response , nil)
//
                                    } else {
                                        
                                 
//                                        let response = self.getResponseDataDictionaryFromData(data: DataResponse.data!)
//                                        print("Response:\(response)")
//                                        getResponse(response , nil)
                                    }
                                 
                                case .failure(let error):
                                    print("json error: \(error.localizedDescription)")
                                    getResponse(nil,error)
                                    return;
                                }
                          })
        
    }

    func getDataForRequest(_ request: String, andParameter parameters: [String:Any]?, withCompletion getResponse: @escaping serviceCompletion) {
        
        let reuestUrl = baseUrl + request
        print(parameters ?? "")
        
        var encodingFormat: ParameterEncoding = URLEncoding()
            if request == calculateTrip {
               encodingFormat = JSONEncoding()
            }
        Alamofire.request(reuestUrl, method: .post, parameters: parameters, encoding: encodingFormat, headers: nil).responseJSON{ (responseData) in
         //   print(responseData)
            print(responseData.result.value)
            if responseData.result.isSuccess {
                if((responseData.result.value) != nil) {
                    // When result is not nil
                    let swiftyJsonVar = JSON(responseData.result.value!)
                    print(swiftyJsonVar)
                    let result = swiftyJsonVar.dictionaryObject
                    getResponse(result ?? "", nil)
                }
                else {
                    //print(responseData.result)
                }
            }
            else {
                if let error = responseData.error, error.code == 4 {
                    let err = CustomError.init(description:INTERNAL_ERROR_MESSAGE)
                    getResponse(nil,err)
                    return;
                }
                getResponse(nil, responseData.error)
            }
        }
    }
    
    func getTollDataForRequest(_ request: String, andParameter parameters: [String:Any], isHasWaypoint: Bool, completionHandler: @escaping (_ totalExpenses: String) -> ()) {
        
        let url = URL.init(string: request)
        let session = URLSession.shared
        var request = URLRequest.init(url: url!)
        request.httpMethod = "POST"
        
        do {
            request.httpBody = try JSONSerialization.data(withJSONObject: parameters, options: .prettyPrinted)
        } catch let error {
           // print(error.localizedDescription)
        }
        
        print("ello - . hey")
        
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        
        let task = session.dataTask(with: request as URLRequest, completionHandler: { data, response, error in
            guard error == nil else {
                hideHud()
                return
            }
            
            guard let data = data else {
                hideHud()
                return
            }
            
            do {
                guard let json = try JSONSerialization.jsonObject(with: data, options: .allowFragments) as? [[String: Any]] else { return }
                var cash = 0
                if isHasWaypoint {
                    for item in json {
                        if let totalCost = item["total_cash_value_USA"] as? NSNumber {
                            cash = cash + totalCost.intValue
                        }
                    }
                }else {
                    for item in json {
                        if let totalCost = item["total_cash_value_USA"] as? NSNumber {
                            if cash < totalCost.intValue {
                                cash = totalCost.intValue
                            }
                        }
                    }
                }
                completionHandler("\(cash)")
                
            } catch {
                hideHud()
            }
            hideHud()
        })
        task.resume()
    }
    
    func getImageForRequest(_ request: String, andParameter parameters: [String:String]?, withCompletion getResponse: @escaping imageCompletion) {
        
        let reuestUrl = baseUrl + request
        
        let sessionConfig = URLSessionConfiguration.default
        let session = URLSession.init(configuration: sessionConfig, delegate: nil, delegateQueue:OperationQueue.main)
        
        var request = URLRequest.init(url: URL.init(string: reuestUrl)!, cachePolicy: .reloadIgnoringCacheData, timeoutInterval: 60.0)
        request.httpMethod = "POST"

        request.setBodyContent(parameters!)
        
        request.addValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")

        // Create DataTask
        let dataTask = session.dataTask(with: request) { (data, response, error) in
            print(response)
        
            if error == nil && data != nil {
                if let imgExist = UIImage.init(data: data!) {
                    saveImageDocumentDirectory(imgExist)
                    getResponse(imgExist)
                    return;
                }
                getResponse(nil)
            }
             getResponse(nil)
        }
        dataTask.resume()
    }
    
    func getApiCall(urlString: String, complete: @escaping (_ result: ResponseObj) -> ()) {
        let requestUrl = baseUrl + urlString
        
        let sessionTask = URLSession.shared
        let url = URL(string: requestUrl)!
        var request = URLRequest.init(url: url)
        request.httpMethod = "GET"
        let jsonRequest = sessionTask.dataTask(with: request as URLRequest, completionHandler: { data, response, error in
            guard error == nil else {
                hideHud()
                return
            }
            
            guard let datas = data else {
                hideHud()
                return
            }
                let jsonDecoder = JSONDecoder()
                let objArr =  try! JSONDecoder().decode(ResponseObj.self, from: datas)
                complete(objArr)
            
        })
        jsonRequest.resume()
    }
}


class CustomError:Error {
    
    var localizedDescription: String { return _description }
    
    private var _description: String
    
    init(description: String) {
        self._description = description
    }
}

struct PoiMarker: Decodable {
    let name: String
    let placeType: String
}

struct POI: Decodable {
    var placeId: Int
    var storeId: Int
    var placeName: String
    var latitude: Double
    var longitude: Double
    var address: String
    var city: String
    var state: String
    var country: String
    var zip: String
    var interstate: String
    var phoneNo: String
    var category: String
    var fax: String
    var parkingSpace: Int
    var dieselLanes: Int
    var bulkDefLanes: Int
    var showers: Int
    var catScale: Int
    var truckCareFacilities: Int
    var truckCareFacilitiesName: String
    var roadSideAssistance: Int
    var wifi: Int
    var fitnessRoom: Int
    var travelStore: Int
    var casino: Int
    var chapelMinistry: Int
    var poiMarkers: [PoiMarker]
}

struct ResponseObj: Decodable {
    var _obj: [POI]
    var status: String
    var message: String
    var errors: [String]
    var code: Int
}
