//
//  GoogleAPIManager.swift
//  SmartTrucking
//
//  Created by Vivan Raghuvanshi on 22/09/17.
//  Copyright © 2017 Vivan Raghuvanshi. All rights reserved.
//

import Foundation
import CoreLocation
import Alamofire
import SwiftyJSON

typealias placesCompletion = (_: [GooglePlace]?) -> Void
let DEFAULT = 0.0000

class GoogleAPIManager:NSObject {
    
    class func getAllNearByPlacesOfLocation(_ location:CLLocation, InRadius radius:Double, ofTypes types:[String],  withCompletion getResponse: @escaping placesCompletion) {
        
        var directionRequest = "https://maps.googleapis.com/maps/api/place/nearbysearch/json?"

        var allType = ""
        for type in types {
            allType = allType.characters.count == 0 ? "\(allType)|\(type)":"\(type)"
        }
        
        let aroundLocation = "location=\(location.coordinate.latitude),\(location.coordinate.longitude)"
        
        directionRequest = "\(directionRequest)\(aroundLocation)&radius=\(radius)&type=\(allType)&key=\(GOOGLE_API_KEY)"
        //print(directionRequest)
        GoogleAPIManager.getDataForRequest(directionRequest, andParameter: nil) { (response, error) in
            if error == nil {
                 //print(response ?? "Avaneesh response")
                if let result = response as? [String:Any] {
                    if let allResults = result["results"] as? [[String:Any]], allResults.count > 0 {
                        
                        var foundPlaces = [GooglePlace]()
                        
                        for placeInfo in allResults {
                            let googlePlace =  GooglePlace.init(placeInfo)
                            if googlePlace.placeLocation.coordinate.latitude != DEFAULT && googlePlace.placeLocation.coordinate.longitude != DEFAULT {
                                foundPlaces.append(googlePlace)
                            }
                        }
                        getResponse(foundPlaces)
                    } else {
                        getResponse(nil)
                    }
                } else {
                    getResponse(nil)
                }
            } else {
                getResponse(nil)
               // print(error ?? "Avaneesh error")
            }
        }

        
      //   let request = "https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=28.6130709,77.2074668&radius=2000&type=gas_station&key=AIzaSyCDLlOH7BWXq1PtgCOASiO5KQ4jE3ZqRfo"
    }
    
    private class func getDataForRequest(_ request: String, andParameter parameters: [String:Any]?, withCompletion getResponse: @escaping serviceCompletion) {
        
        Alamofire.request(request, method: .get, parameters: parameters, encoding: URLEncoding.default).responseJSON{ (responseData) in
            
            if responseData.result.isSuccess {
                if((responseData.result.value) != nil) {
                    // When result is not nil
                    let swiftyJsonVar = JSON(responseData.result.value!)
                    let result = swiftyJsonVar.dictionaryObject
                    getResponse(result ?? "", nil)
                }
                else {
                  //  print(responseData.result)
                }
            }
            else {
                getResponse(nil, responseData.error)
                //showAlert("Alert", message: responseData.error?.localizedDescription ?? "")
            }
        }
    }
}


struct GooglePlace {
    let placeLocation:CLLocation
    let placeName:String
    let placeType:String
    
    init(_ placeInfo:[String:Any]) {
        
        if let geometry = placeInfo["geometry"] as? [String:Any] {
            if let location = geometry["location"]  as? [String:Any] {
                if let latitude = location["lat"] {
                    if let longitude = location["lng"] {
                        
                        let lat = "\(latitude)"
                        let lng = "\(longitude)"
                        
                        let foundLocation = CLLocation.init(latitude: Double(lat) ?? DEFAULT, longitude: Double(lng) ??  DEFAULT)
                        placeLocation = foundLocation
                    } else {
                        let foundLocation = CLLocation.init(latitude:DEFAULT, longitude: DEFAULT)
                         placeLocation = foundLocation
                    }
                } else {
                     let foundLocation = CLLocation.init(latitude:DEFAULT, longitude: DEFAULT)
                     placeLocation = foundLocation
                }
            } else {
                  let foundLocation = CLLocation.init(latitude:DEFAULT, longitude: DEFAULT)
                 placeLocation = foundLocation
            }
        } else {
              let foundLocation = CLLocation.init(latitude:DEFAULT, longitude: DEFAULT)
             placeLocation = foundLocation
        }
        
        
        if let name = placeInfo["name"] as? String {
            placeName = name
        } else {
            placeName = ""
        }
        
        if let allTypes = placeInfo["types"] as? [String], allTypes.count > 0 {
            placeType = allTypes[0]
        } else {
            placeType = ""
        }
    }
}

/* {
    geometry =     {
        location =         {
            lat = "28.6072533";
            lng = "77.204407";
        };
        viewport =         {
            northeast =             {
                lat = "28.6086022802915";
                lng = "77.20575598029151";
            };
            southwest =             {
                lat = "28.6059043197085";
                lng = "77.20305801970849";
            };
        };
    };
    icon = "https://maps.gstatic.com/mapfiles/place_api/icons/shopping-71.png";
    id = e2fdbbc1f546c7dbedfad9ab3c93795ea33549f4;
    name = "Kendriya Bhandar";
    "opening_hours" =     {
        "open_now" = 1;
        "weekday_text" =         (
        );
    };
    photos =     (
        {
            height = 4608;
            "html_attributions" =             (
                "<a href=\"https://maps.google.com/maps/contrib/107651202783558334667/photos\">jiten verma</a>"
            );
            "photo_reference" = "CmRaAAAAl_kNV4K2ydHS3oWr9LT-qKZqugQpXr2KK4lTXcLb428o6qCUysOqXQ76CzTYEvpv4-7X0YGuyJ8M-Rb31BWduddHagCLFwOF1fxMEDrSq1H1vfybQIk0f24T9ytkNdLmEhCI3B2z3-x4kxiiDsXihMqyGhTrWdTiBCdhV3_4mFkIRmmJStgDnw";
            width = 3456;
        }
    );
    "place_id" = "ChIJ____5qTiDDkRn3I6wvtMyEA";
    rating = "4.2";
    reference = "CmRRAAAAYr12HN8nCiQh6cuA-ALpeIO0xQskeZop1MRmWaxuHMtpShprM1y2i4kPnX10QNSeFaMqutxkQL1WKzxebq7pv0ZM17FsObGNVMCIlSvHLEfxedNrqZASf3FBmu5gRAXJEhDQ7rGTilcN1gLmWP_LIe58GhTY8dRQW4Ko-aCHu5ZRmdjcfF3Z9g";
    scope = GOOGLE;
    types =     (
        "shopping_mall",
        "point_of_interest",
        establishment
    );
    vicinity = "Kendriya Lok Nirman Vibhag, Teen Murti Marg Area, New Delhi";
}*/
