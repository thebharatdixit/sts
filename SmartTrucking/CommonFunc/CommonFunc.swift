//
//  CommonFunc.swift
//  SmartTrucking
//
//  Created by Vivan Raghuvanshi on 19/08/17.
//  Copyright © 2017 Vivan Raghuvanshi. All rights reserved.
//

import UIKit
import SVProgressHUD

//********************************* Constants *************************************

 let SCREEN_WIDTH = UIScreen.main.bounds.width
 let SCREEN_HEIGHT = UIScreen.main.bounds.height
 let SCREEN_SIZE  = UIScreen.main.bounds
 let APPDELEGATE = UIApplication.shared.delegate as! AppDelegate
 let STORYBOARD = UIStoryboard.init(name: "Main", bundle: nil)


//********************************* Color Literals *************************************

let blue1 = #colorLiteral(red: 0.09803921569, green: 0.262745098, blue: 0.3960784314, alpha: 1)
let blue2 = #colorLiteral(red: 0.1137254902, green: 0.3019607843, blue: 0.4549019608, alpha: 1)
let blue3 = #colorLiteral(red: 0.1450980392, green: 0.3882352941, blue: 0.5843137255, alpha: 1)
let blue4 = #colorLiteral(red: 0.1712065935, green: 0.4575021267, blue: 0.6804637909, alpha: 1)
let lightG = #colorLiteral(red: 0.8509803922, green: 0.8509803922, blue: 0.8509803922, alpha: 1)


//************************ Alert titles/messages **************************

let INVALID_EMAIL = "*Please enter a valid email"
let FILL_ALL_FIELDS = "*Please fill all the required fields"

let SERVER_ERROR = "*Server error"
let SOMETHING_WRONG = "*Something went wrong, Please try again"
let EMPTY_STRING = ""

//******************************** Show/Hide Hud ********************************//

func showHud(_ title: String?) {
    DispatchQueue.main.async {
        SVProgressHUD.setDefaultAnimationType(SVProgressHUDAnimationType.flat)
        
        if title == "" || title == nil {
            SVProgressHUD.show()
        } else {
            SVProgressHUD.show(withStatus: title)
        }
    }
}

func hideHud () {
    DispatchQueue.main.async {
        SVProgressHUD.dismiss()
    }
}


//MARK:-******************************** Show Alert ********************************//

func showAlert(_ title: String?, message: String, onView sender:UIViewController) {
    
    DispatchQueue.main.async {
        showAlert(title, message: message, withAction: nil, with: true, andTitle: "Ok", onView: sender)
    }
}

func showAlert(_ title: String?, message: String, withAction addActions:[UIAlertAction]?, with isCancel:Bool, andTitle cancelTitle:String, onView sender:UIViewController) {
    
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
    let cancelAction = UIAlertAction(title:cancelTitle, style: UIAlertAction.Style.cancel) {
            UIAlertAction in
            NSLog("Cancel Pressed")
        }
        // Add the actions
        if isCancel == true {
            alertController.addAction(cancelAction)
        }
            
        if let allactions = addActions {
            for action in allactions {
                alertController.addAction(action)
            }
        }
        
        // Present the controller
        sender.present(alertController, animated: true, completion: nil)

}

func showActivityViewToShare(_ sender:UIViewController) {
    // text to share
    let text = "Look what I found,  wanted to share Smart Trucking App. It auto calculate tolls, keep track of your expense,  help you plan a trip and many more features. Try out it's simple and best of all it's free. https://itunes.apple.com/us/app/smarttrucking/id1295257446?mt=8"
    
    // set up activity view controller
    let textToShare = [ text ]
    let activityViewController = UIActivityViewController(activityItems: textToShare, applicationActivities: nil)
    activityViewController.popoverPresentationController?.sourceView = sender.view // so that iPads won't crash
    
    // exclude some activity types from the list (optional)
   // activityViewController.excludedActivityTypes = [ UIActivityType.airDrop, UIActivityType.postToFacebook ]
    
    // present the view controller
    sender.present(activityViewController, animated: true, completion: nil)
}

//MARK:-****************************** Email Validation ******************************//

func isValidEmail(_ testStr:String) -> Bool {
    let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
    let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
    return emailTest.evaluate(with: testStr)
}

//MARK:-****************************** Phone Validation ******************************//

func isValidPhoneNumberFormat(_ string:String) -> Bool {
    //
    //^(\+91[\-\s]?)?[0]?(91)?[789]\d{9}$
    //^((\\+)|(00))[0-9]{6,14}$
    let PHONE_REGEX = "([+]?1+[-]?)?+([(]?+([0-9]{3})?+[)]?)?+[-]?+[0-9]{3}+[-]?+[0-9]{4}"
        let phoneTest = NSPredicate(format: "SELF MATCHES %@", PHONE_REGEX)
        let result =  phoneTest.evaluate(with: string)
        return result
}

//"^\+?\s?\d{3}\s?\d{3}\s?\d{4}$"

//MARK:-****************************** Limit Textfields ******************************//

func checkValidPhoneNumberLength(_ text:String, forRange range:NSRange, replacementString string: String) -> Bool {
    let charsLimit = 10
    
    let startingLength = text.count
    let lengthToAdd = string.count
    let lengthToReplace =  range.length
    let newLength = startingLength + lengthToAdd - lengthToReplace
    
    return newLength <= charsLimit
}

func limitTextFieldBeforeAndAfterDecimal(_ text:String,forRange range:NSRange, replacementString string: String) -> Bool {
    
    let checkString = text as NSString
    
    let editedString = checkString.replacingCharacters(in: range, with: string)
    if let regex = try? NSRegularExpression(pattern: "^[0-9]{2,0}*((\\.|,)[0-9]{0,2})?$", options: .caseInsensitive) {
         return regex.numberOfMatches(in: editedString, options: .reportProgress, range: NSRange(location: 0, length: (editedString as NSString).length)) > 0
    }
    return false
}

func allowUptoTwoDecimalPlace(_ text:String, forRange range:NSRange, replacementString string: String)-> Bool {
    
    let checkString = text as NSString
    
    let newText = checkString.replacingCharacters(in: range, with: string)
    if let regex = try? NSRegularExpression(pattern: "^[0-9]*((\\.|,)[0-9]{0,2})?$", options: .caseInsensitive) {
        return regex.numberOfMatches(in: newText, options: .reportProgress, range: NSRange(location: 0, length: (newText as NSString).length)) > 0
    }
    return false
} //"\\d{0,10}(\\.\\d{0,3})?"

//MARK:-****************************** Blank Checking ******************************//

func isBlank(_ text:String) -> Bool {
    if text.trimmingCharacters(in: .whitespaces).isEmpty {
        return true
    }
    return false
}

func isBlankField(_ textField:UITextField) -> Bool {
    if textField.text!.trimmingCharacters(in: .whitespaces).isEmpty {
        textField.text = ""
        return true
    }
    return false
}


//MARK:-****************************** Factor of Screen ******************************//

func getScaleFactor() -> CGFloat {
    let screenRect:CGRect = UIScreen.main.bounds
    let screenWidth:CGFloat = screenRect.size.height
    let scalefactor:CGFloat!
    scalefactor = screenWidth / 568.0
    if UIDevice.current.userInterfaceIdiom == .pad {
        return scalefactor
    } else {
        return scalefactor < 1.3 ? scalefactor:1.3
    }
}

//MARK:-****************************** Save and fetch from document directory ******************************//

func saveImageDocumentDirectory(_ image:UIImage?){
    let fileManager = FileManager.default
    let paths = (NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as NSString).appendingPathComponent("apple.jpg")
    if image == nil {
        do {
            try fileManager.removeItem(atPath: paths)
        } catch {
            //print(error)
        }
    } else {
        
        let imageData = image!.jpegData(compressionQuality: 0.8)
        fileManager.createFile(atPath: paths as String, contents: imageData, attributes: nil)
    }
}

func getDirectoryPath() -> String {
    let paths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)
    let documentsDirectory = paths[0]
    return "\(documentsDirectory)/apple.jpg"
}

func isIpad() -> Bool {
    let device = UIDevice.current.userInterfaceIdiom
    if device == .pad {
        return true
    }
    return false
}

