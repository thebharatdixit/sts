//
//  PhoneCodePicker.swift
//  SmartTrucking
//
//  Created by Vivan Raghuvanshi on 19/08/17.
//  Copyright © 2017 Vivan Raghuvanshi. All rights reserved.
//

import Foundation

import UIKit
import CoreTelephony

@objc public protocol PhoneCodePickerDelegate {
    func countryPhoneCodePicker(_ picker: PhoneCodePicker, didSelectCountryWithName name: String, countryCode: String, phoneCode: String, flag: UIImage)
}

struct CountryPhone {
    var code: String?
    var name: String?
    var phoneCode: String?
    var flag: UIImage?
    
    init(code: String?, name: String?, phoneCode: String?, flag: UIImage?) {
        self.code = code
        self.name = name
        self.phoneCode = phoneCode
        self.flag = flag
    }
}

open class PhoneCodePicker: UIPickerView, UIPickerViewDelegate, UIPickerViewDataSource {
    
    var countries: [CountryPhone]!
    open weak var phoneCodePickerDelegate: PhoneCodePickerDelegate?
    open var showPhoneNumbers: Bool = true
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    func setup() {
        countries = countryNamesByCode()
        
        super.dataSource = self
        super.delegate = self
    }
    
    // MARK: - Country Methods
    
    open func setCountry(_ code: String) {
        var row = 0
        for index in 0..<countries.count {
            if countries[index].code == code {
                row = index
                break
            }
        }
        
        self.selectRow(row, inComponent: 0, animated: true)
        let country = countries[row]
        if let PhoneCodePickerDelegate = phoneCodePickerDelegate {
            PhoneCodePickerDelegate.countryPhoneCodePicker(self, didSelectCountryWithName: country.name!, countryCode: country.code!, phoneCode: country.phoneCode!, flag: country.flag ?? UIImage(named:"truneed")!)
        }
    }
    
    open func setCountryByPhoneCode(_ phoneCode: String) {
        var row = 0
        for index in 0..<countries.count {
            if countries[index].phoneCode == phoneCode {
                row = index
                break
            }
        }
        
        self.selectRow(row, inComponent: 0, animated: true)
        let country = countries[row]
        if let PhoneCodePickerDelegate = phoneCodePickerDelegate {
            PhoneCodePickerDelegate.countryPhoneCodePicker(self, didSelectCountryWithName: country.name!, countryCode: country.code!, phoneCode: country.phoneCode!, flag: country.flag ?? UIImage(named:"truneed")!)
        }
    }
    
    // Populates the metadata from the included json file resource
    
    func countryNamesByCode() -> [CountryPhone] {
        var countries = [CountryPhone]()
       // let frameworkBundle = Bundle(for: type(of: self))
        
        
        guard let jsonPath = Bundle.main.path(forResource: "Code", ofType: "json"),
            let jsonData = try? Data(contentsOf: URL(fileURLWithPath: jsonPath)) else {
            return countries
        }
        
        do {
            if let jsonObjects = try JSONSerialization.jsonObject(with: jsonData, options: JSONSerialization.ReadingOptions.allowFragments) as? NSArray {
                
               // print(jsonObjects)
                
                for jsonObject in jsonObjects {
                    
                    guard let countryObj = jsonObject as? NSDictionary else {
                        return countries
                    }
                    
                    guard let code = countryObj["code"] as? String, let phoneCode = countryObj["dial_code"] as? String, let name = countryObj["name"] as? String else {
                        return countries
                    }
                    
                   // let flag = UIImage(named: "PhoneCodePicker.bundle/Images/\(code.uppercased())", in: Bundle(for: type(of: self)), compatibleWith: nil)
                    
                      let flag = UIImage(named:"")
                    
                    let country = CountryPhone(code: code, name: name, phoneCode: phoneCode, flag: flag)
                    countries.append(country)
                }
                
            }
        } catch {
            return countries
        }
        return countries
    }
    
    // MARK: - Picker Methods
    
    open func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    open func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return countries.count
    }
    //Avaneesh 21Jul
    var selectedRow = 0
    
    open func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
        var resultView: PhoneCodeView
        
        if view == nil {
            resultView = PhoneCodeView()
        } else {
            resultView = view as! PhoneCodeView
        }
        //Avaneesh 21Jul
        //print(selectedRow)
        if row == selectedRow {
            resultView.backgroundColor = UIColor.white
        } else {
            resultView.backgroundColor = UIColor.clear
        }
        
        
        resultView.setup(countries[row])
        if !showPhoneNumbers {
            resultView.countryCodeLabel.isHidden = true
        }
        return resultView
    }
    
    open func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        //Avaneesh 21Jul
        selectedRow = row
        pickerView.reloadAllComponents()
        
        let country = countries[row]
        if let PhoneCodePickerDelegate = phoneCodePickerDelegate {
            PhoneCodePickerDelegate.countryPhoneCodePicker(self, didSelectCountryWithName: country.name!, countryCode: country.code!, phoneCode: country.phoneCode!, flag: country.flag ?? UIImage(named:"truneed")!)
        }
    }
    
    //#warning added by ravi
    open func pickerView(_ pickerView: UIPickerView, rowHeightForComponent component: Int) -> CGFloat {
        return 44
    }
    
}
