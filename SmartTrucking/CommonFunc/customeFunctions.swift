//
//  customeFunctions.swift
//  Fitbook
//
//  Created by Anand on 5/29/18.
//  Copyright © 2018 Anand Praksh. All rights reserved.
//

import UIKit


func getCellHeaderSize(Width:CGFloat, aspectRatio:CGFloat, padding:CGFloat) -> CGSize {
    
    let cellWidth = (Width ) - padding
    let cellHeight = cellWidth / aspectRatio
    
    return CGSize(width: cellWidth, height: cellHeight)
    
}
