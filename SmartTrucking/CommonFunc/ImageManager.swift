//
//  ImageManager.swift
//  SmartTrucking
//
//  Created by Vivan Raghuvanshi on 23/10/17.
//  Copyright © 2017 Vivan Raghuvanshi. All rights reserved.
//

import Foundation
import UIKit

class ImageManager:NSObject {
    
    private let picker = UIImagePickerController()
    
    var imgSelected:((_:UIImage?)->Void)?
    var viewSender:UIViewController?
    
    override init() {
        super.init()
        picker.allowsEditing = false;
        picker.delegate = self
    }
    
    func showImagePicker(_ sender:UIViewController,getImage:@escaping (_:UIImage?)->Void) {
        
        viewSender = sender
        imgSelected = getImage
        
        let alert = UIAlertController(title: nil, message: "Please choose to share", preferredStyle: .actionSheet)
        
        let cancel = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        
        let camera = UIAlertAction.init(title: "Camera", style: .default) { (alert) in
            self.takeImageFrom(.camera, sender: sender)
        }
        
        let gallery = UIAlertAction.init(title: "Gallery", style: .default) { (alert) in
            if(UIImagePickerController.isSourceTypeAvailable(.savedPhotosAlbum)){
                self.takeImageFrom(.savedPhotosAlbum, sender: sender)
            }
        }
        alert.addAction(camera)
        alert.addAction(gallery)
        alert.addAction(cancel)
        
        viewSender?.present(alert, animated: true, completion: nil)
    }
    
    private func takeImageFrom(_ source:UIImagePickerController.SourceType, sender:UIViewController) {
        if (UIImagePickerController.isSourceTypeAvailable(source)) {
            self.picker.sourceType = source
            sender.present(self.picker, animated: true, completion: nil)
        }
    }
}

extension ImageManager:UIImagePickerControllerDelegate {
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        imgSelected!(nil)
        picker.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        if let pic = info[.originalImage] as? UIImage {
            imgSelected!(pic)
        }
        picker.dismiss(animated: true, completion: nil)
    }
}

extension ImageManager:UINavigationControllerDelegate {
    
}
